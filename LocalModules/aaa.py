from LocalModules.PlotlyDash import DashTable
import pandas as pd

df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder2007.csv')
dashtable = DashTable(df,page_size = 10,id = 'putte')

#%%
aaa = 5
app = dash.Dash(__name__)
app.layout = dashtable.layout
dashtable.callback()

if __name__ == '__main__':
    app.run_server(debug=True)