
# IMPORTS: GeoID 

## OS,I/O
import os
from urllib.request import urlopen
import json

## Process
import numpy as np
import pandas as pd
from collections import Counter



# IMPORTS: RASTER LINES AND POLYGON SURFACES 

## Process
import numpy as np
import pandas as pd

## Plot
import seaborn as sns
from matplotlib import pyplot as plt


##########################################

####################
## CLASS: GeoID


class GeoID:
    '''
    GeoID: a class for managing geo_id codes, which includes fips.  
    
    GeoID.coords_to_geoid(self, coords)    :   Returns Geo_ID code for map coordinates (latitude,longitude)
    GeoID.shed_brackets(bracketed_list)    :   A general utility function that removes brackets from a nested list 
                                               until the list contains more than one element
    '''
    
    def __init__(self, path = './data', entities = ['counties'] ):
        '''
        path     : path to subdirectory for data used by GeoID
        entities : the type of entities that Geo_IDs represent (such as 'counties')
        '''
        self.path     = path + '/'
        self.entities = entities
        
        # Ensure path
        if not os.path.exists(path):
            os.mkdir(path)
        
        # Setup (initially exists only for counties - can add more)
        for entity in entities:
            string = 'self._setup_'+entity+'()'
            eval(string)
            
        # Required dictionaries: 
        #  self._df_coords_to_geoid
        if os.path.exists(self.path+'_df_coords_to_geoid.pkl'):
            self._df_coords_to_geoid = pd.read_pickle(self.path+'_df_coords_to_geoid.pkl')
        else:
            self._df_coords_to_geoid = pd.DataFrame(columns = ['latitude','longitude','geo_id'])  
          

        # Function short names
        path          = self.path
        edges         = GeoID.edges
        shed_brackets = GeoID.shed_brackets
        to_json  = GeoID.to_json
        read_json     = GeoID.read_json
        

    def _setup_counties(self, data_url = 'https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json'):
        '''        
        Adds county-data to object:    
        * self.counties: (dict)
            - imported geojson info for counties, including names, GEO_IDs, shapes / polygon-coordinates
            - added min/max lat/long - values for counties
            - polygon-edges
        * self.df_geo: (pandas dataframe) self.counties as a dataframe 
        
        Ensures datafiles:
        * path/counties_fips.json  for self.counties
        * path/df_geo_counties.pkl for self.df_geo         
        '''
        # function short names
        path          = self.path
        edges         = GeoID.edges
        shed_brackets = GeoID.shed_brackets
        to_json       = GeoID.to_json
        read_json     = GeoID.read_json
        
        ##########
        # FETCH COUNTY DATA
        # Fetch Geo-data for counties; names, fips and shapes, vertex-coordinates for polygons 
        # If not on disk, fetch online and save to disk

        if not os.path.exists(path+'counties_fips.json'):
            with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
                counties = json.load(response)
            # ADD TO COUNTY-DATA: 1) POLYGON-EDGES . 2) MAX/MIN VALUES FOR LAT/LONG COORDS, RESPECTIVELY
            for county in counties['features']:
                vertex_list = county['geometry']['coordinates']
                county['geometry']['edges']    = {}
                county['geometry']['max_lat']  = {} 
                county['geometry']['min_lat']  = {}
                county['geometry']['max_long'] = {} 
                county['geometry']['min_long'] = {}
                # len(vertex_list) is the number of independent polygons for the county
                # in most cases len(vertex_list) = 1, but can be larger than one when several polygons are required 
                # for mapping the county - for example for a coastal county with islands. 
                for i in range(len(vertex_list)):
                    verts = vertex_list[i]
                    county['geometry']['edges'][i]    = edges(shed_brackets(verts))
                    lats,longs = np.transpose(np.array(verts))
                    county['geometry']['max_lat'][i]  = float(max(lats))
                    county['geometry']['min_lat'][i]  = float(min(lats))
                    county['geometry']['max_long'][i] = float(max(longs))
                    county['geometry']['min_long'][i] = float(min(longs) )
            self.to_json(counties,path+'counties_fips.json')
        self.counties = self.read_json(path+'counties_fips.json')

        if not os.path.exists(path+'df_geo_counties.pkl'):
            df_all = pd.DataFrame(counties['features'])
            df_geo = pd.DataFrame(df_all['geometry'].to_dict()).T
            df_geo.index = df_all['properties'].apply(lambda x: x['GEO_ID'])
            df_geo['max_lat']  = df_geo['max_lat'].apply(lambda x: max(x.values()))
            df_geo['min_lat']  = df_geo['min_lat'].apply(lambda x: min(x.values()))
            df_geo['max_long'] = df_geo['max_long'].apply(lambda x: max(x.values()))
            df_geo['min_long'] = df_geo['min_long'].apply(lambda x: min(x.values()))
            df_geo.to_pickle(path+'/df_geo_counties.pkl')
        df_geo = pd.read_pickle(path+'df_geo_counties.pkl')
        self.df_geo = df_geo
        return self


    ####
    # COORDS TO GEO_ID
    #
    # Selects the counties' min/max-lat/long squares that include the point coords
    # ToDo: narrow down to the county polygon(s) that include(s) coords         

    def coords_to_geoid(self, coords, regis = False):
        '''
        input: lat.long   ,  output: county id (GEO_ID)
        
        Find GEO_ID/fips of map coordinates. 
        If conversion not available in lookup-table ( self._coords_to_geoid_dic ), 
        calculate the GEO_ID for 'coords' and add to lookup-table. 

        Algorithm: Draw a line from a point anywhere inside a polygon. It will cross the borders of the 
        polygon an odd number of times. Any other polygon's borders will be crossed an even number of times. 

        1. Pre-select all counties with max/min-lat/long that span the coordinate
        2. Create a dataframe with their polygon-edges
        3. Select all polygon edges spanning the input longitude 
        2. From these, select 
                a. all edges with latitudes *larger* than the input latitude, and
                b. all edges *spanning* the input latitude (these will be few). 
                    i.  Calculate the edge latitude at the point of the input longitude
                    ii. If it exceeds the input latitude, save it (otherwise discard)
        3. Count GEO_IDs for all selected edges (a+b above), return the GEO_ID with an odd number of counts
        '''
        # Ensure that coordinates are numbers, not string
        
        if type(coords) is not tuple:
            return coords 
        
        coords = tuple(self._eval_if(coo) for coo in self._eval_if(coords))
        lat,long = coords

        
        df = self._df_coords_to_geoid
        result = df[(df['latitude']  == lat) & 
                    (df['longitude'] == long)]
        if len(result) is 0:
            result = self._coords_to_geoid(coords)
        else:
            result = result['geo_id']
        return result 

    def _coords_to_geoid(self, coords):
        '''
        input: lat.long   ,  output: county id (GEO_ID)

        Algorithm: Draw a line from a point anywhere inside a polygon. It will cross the borders of the 
        polygon an odd number of times. Any other polygon's borders will be crossed an even number of times. 

        1. Pre-select all counties with max/min-lat/long that span the coordinate
        2. Create a dataframe with their polygon-edges
        3. Select all polygon edges spanning the input longitude 
        2. From these, select 
                a. all edges with latitudes *larger* than the input latitude, and
                b. all edges *spanning* the input latitude (these will be few). 
                    i.  Calculate the edge latitude at the point of the input longitude
                    ii. If it exceeds the input latitude, save it (otherwise discard)
        3. Count GEO_IDs for all selected edges (a+b above), return the GEO_ID with an odd number of counts
        '''
        # Function short names
        path          = self.path
        edges         = GeoID.edges
        shed_brackets = GeoID.shed_brackets
        to_json       = GeoID.to_json
        read_json     = GeoID.read_json
    
        pre_select_fips = self._pre_select_fips
        
        # Ensure that coordinates are numbers, not string
        coords = tuple(self._eval_if(coo) for coo in self._eval_if(coords))
        lat,long = coords
        
        
        # Preselect counties with max/min-lat/long that include the coords
        df_counties = pre_select_fips(lat,long)
        df_counties['geo_id'] = df_counties.index
        counties = df_counties.T.to_dict()
        # Create a dataframe with all polygon-segments, tagged by GEO_ID (fips)   
        edges = {}
        def edg(poly_coords):
            '''
            edg: returns edges; the digram of the polygon-coordinates
            edg([a,b,c,d]) = [[a,b],[b,c],[c,d]]  where a/b/c/d = [lat,long]
            '''
            edges = [np.concatenate([poly_coords[i],poly_coords[i+1],[geo_id]]) for i in range(len(poly_coords)-1)]
            return edges 

        for county in counties.values():
            geo_id = county['geo_id']
            edges[geo_id] = np.concatenate([edg(shed_brackets(poly_coords)) for poly_coords in county['coordinates']])
        # create and save df_edges listing all polygon edges where each edge is labeled by the county GEO_ID
        df = pd.DataFrame(np.concatenate(list(edges.values())),columns = ['lat_1','long_1','lat_2','long_2','GEO_ID'])
        # All values in df are str, convert the polygon coordinates to numbers
        df[['lat_1', 'long_1', 'lat_2', 'long_2']] = df[['lat_1', 'long_1', 'lat_2', 'long_2']].applymap(eval)
        # Select all polygon edges spanning the input longitude 
        df = df[df['long_2'].apply(lambda x: x >= long ) ^ df['long_1'].apply(lambda x: x >= long )] 
        # Select all edges *spanning* the input latitude (need more processing) 
        dfx = df[df['lat_2'].apply(lambda x: x >= lat )  ^ df['lat_1'].apply(lambda x: x >= lat )].copy() 
        # Select all edges *exceeding* the input latitude
        df = df[df['lat_2'].apply(lambda x: x >= lat )  & df['lat_1'].apply(lambda x: x >= lat )]
        # If there are edges spanning the input latitude, select the ones 
        # exceeding the input latitude for the input longitude
        if len(dfx) > 0:
            def lat_larger(edge):
                kd = (edge['long_2'] - edge['long_1'])

                if kd > 0.0001:
                    kn = (edge['lat_2']  - edge['lat_1'])
                    k =  kn/kd
                    l = edge['lat_1']
                    x = long - edge['long_1']
                    y = k*x + l
                    if y >= lat:
                        return True
                return False
            dfx = dfx[dfx.T.apply(lat_larger)]
            df.append(dfx).reset_index(drop=True)
        # Count GEO_IDs, return the one with odd number of counts     
        crossings = pd.Series(Counter(df['GEO_ID']))
        geo_id = list(crossings[crossings%2 == 1].index)

        row = pd.Series(list(coords) + [geo_id], index = ['latitude','longitude','geo_id'])
        self._df_coords_to_geoid = self._df_coords_to_geoid.append(row,ignore_index = True)
        return  geo_id
    
    def _pre_select_fips(self,lat,long):
        '''
        1. Pre-select all GEO_IDs with max/min-lat/long that span the coordinates lat,long
        2. Return a dataframe with their polygon-edges
        '''
        df = self.df_geo.copy()
        df = df[df['max_lat']  >= lat]
        df = df[df['max_long'] >= long]
        df = df[df['min_lat']  <= lat]
        df = df[df['min_long'] <= long] 
        return df   
    
    ## GENERAL PURPOSE FUNCTIONS
    def shed_brackets(bracketed_list):
        ''' 
        remove 'wrapping' brackets from list:  
            shed_brackets([[[[a]]]])              -> [a]
            shed_brackets([[[ [a,b,c]] ]])        -> [a,b,c]
            shed_brackets([[ [a,[b1,b2],c] ]])    -> [a,[b1,b2],c]
        '''
        # Function short names
        shed_brackets = GeoID.shed_brackets

        if len(bracketed_list) is 1: 
            # strip a bracket
            bracketed_list = shed_brackets(bracketed_list[0])
        if len(bracketed_list) is not 1:
            shedded_list = bracketed_list
        return shedded_list


    def edges(vertex_list):
        '''
        'edges()' returns the edges for a list of polygon-vertex coordinates. It is identical to digrams. 
        input [a,b,c,d]
        output [[a,b],[b,c],[c,d]]
        a/b/c is usually a list - the coordinates of vertex a/b/c respectively
        '''
        edge_list = [vertex_list[i:i+2] for i in range(len(vertex_list)-1)]
        return edge_list

    def to_json(self,variable,filename):
        '''
        Saves 'variable' as json-file 'filename'
        '''
        js = json.dumps(variable)
        f = open(filename,"w")
        f.write(js)
        f.close()
        return

    def read_json(self,filename):
        '''
        Reads 'variable' from json-file 'filename'
        '''
        with open(filename) as json_file:
            data = json.load(json_file)
            json_file.close()
        return data

    def save_lookup(self):
        self._df_coords_to_geoid.to_pickle(self.path+'_df_coords_to_geoid.pkl')
        return self

    def read_lookup(self):
        self._df_coords_to_geoid = pd.read_pickle(self.path+'_df_coords_to_geoid.pkl')
        return self
    
    def _eval_if(self,a):
        if type(a) is str:
            return eval(a)
        else:
            return a





if __name__ is '__main__': 
    print('***DEMO: GeoID class***')
    geo = GeoID()
    lat,long = -86.4111,40.7073
    geo_id = geo.coords_to_geoid((lat,long))
    print('GEO_ID for lat,long = '+str([lat,long])+': '+ str(geo_id)+'\n \n')



####################
## CLASS: Raster
#%% 



class Raster:
    '''
    Rasters geometrical objects
    '''
    
    def __init__(self,vertex_list):
        '''
        vertex_list  : coordinates defining the shape of the geometrical object (Implemented: polygon) 
        '''
        self.vertex_list = vertex_list
    
    def line(edge, step = 0.001):
        '''
        input:  edge - [start,end], where start and end are lists
        output: pixelated line
        '''
        start,end = edge
        scale = 1/step
        st     = np.array([int(i) for i in (scale * np.array(start))])
        en     = np.array([int(i) for i in (scale * np.array(end))])
        diff   = en - st
        x      = np.absolute(diff).argmax()
        n      = abs(diff[x])
        delta  = diff/n
        raster = ([np.round(i * delta) for i in range(n)] + st)/scale
        return raster

    def polygon(self, raster_surface = False, step=0.001, concatenate = True, return_result = False ):
        if type(self) is list:
            return_result = True
            self = Raster(self)        
        vertex_list = self.vertex_list
        self.polygon_edges = self._polygon_edges(step=step, concatenate_edges = concatenate)
        if raster_surface:
            self.polygon_surface = self._polygon_surface(step=step, concatenate_rows = concatenate)
        if return_result:
            if raster_surface:
                return self.polygon_surface
            else:
                return self.polygon_edges
        else:
            return self
            


    def _polygon_edges(self, step=0.001, concatenate_edges = True):
        '''
        input: list of polygon vertices
        '''
        df_edges        = pd.DataFrame(pd.Series(vertex_list[:-1]),columns = ['start'])
        df_edges['end'] = pd.Series(vertex_list[1:])
        raster     = np.array([Raster.line(edge,step=step) for edge in df_edges.values])
        if concatenate_edges:
            raster = np.concatenate(raster)
        return raster

    def _polygon_surface(self, step=0.001, concatenate_rows = True):
        vertex_list = self.vertex_list
        shape = self.polygon_edges
        dfm = pd.DataFrame(shape).groupby(0)
        df = dfm.min()
        df.columns = ['min']
        df['max'] = dfm.max()
        df['fill'] = df.apply(lambda x: np.arange(x['min'],x['max'],0.01),axis=1)
        df['n'] = df['fill'].apply(len)
        dic = df[df['n'] >0]['fill'].to_dict()
        fill = {}
        for item in dic.items():
            ix,x = item
            y = np.full(len(x),ix)
            row = np.transpose(np.array([y,x])) 
            fill[ix] = row
        fill = np.array(list(fill.values()))
        if concatenate_rows:
            fill = np.concatenate(fill)
        return fill    


if __name__ is '__main__': 
    print('***DEMO: Raster class***')
    print('County border raster:')
    vertex_list = [[-86.165024, 40.562649],
                    [-86.184044, 40.562459],
                    [-86.374417, 40.561361],
                    [-86.373521, 40.69246],
                    [-86.525164, 40.692419],
                    [-86.581739, 40.735765],
                    [-86.581532, 40.910925],
                    [-86.468509, 40.909889],
                    [-86.169012, 40.909836],
                    [-86.165024, 40.562649]]
    rr = Raster.polygon(vertex_list,raster_surface = False)
    x,y = np.transpose(rr)
    sns.scatterplot(x,y)
    plt.show()
    print('County surface raster:')
    rr = Raster.polygon(vertex_list,raster_surface = True)
    x,y = np.transpose(rr)
    sns.scatterplot(x,y)
    plt.show()



# # IMPORTS: GeoID 

# ## OS,I/O
# import os
# from urllib.request import urlopen
# import json

# ## Process
# import numpy as np
# import pandas as pd
# from collections import Counter



# # IMPORTS: RASTER LINES AND POLYGON SURFACES 

# ## Process
# import numpy as np
# import pandas as pd

# ## Plot
# import seaborn as sns
# from matplotlib import pyplot as plt


# ##########################################

# ####################
# ## CLASS: GeoID


# class GeoID:
#     '''
#     GeoID: a class for managing geo_id codes, which includes fips.  
    
#     GeoID.coords_to_geoid(self, coords)    :   Returns Geo_ID code for map coordinates (latitude,longitude)
#     GeoID.shed_brackets(bracketed_list)    :   A general utility function that removes brackets from a nested list 
#                                                until the list contains more than one element
#     '''
    
#     def __init__(self, path = './data', entities = ['counties'] ):
#         '''
#         path     : path to subdirectory for data used by GeoID
#         entities : the type of entities that Geo_IDs represent (such as 'counties')
#         '''
#         self.path     = path + '/'
#         self.entities = entities
        
#         # Ensure path
#         if not os.path.exists(path):
#             os.mkdir(path)
        
#         # Setup (initially exists only for counties - can add more)
#         for entity in entities:
#             string = 'self._setup_'+entity+'()'
#             eval(string)
            
#         # Required dictionaries: 
#         #  self._df_coords_to_geoid
#         if os.path.exists(self.path+'_df_coords_to_geoid.pkl'):
#             self._df_coords_to_geoid = pd.read_pickle(self.path+'_df_coords_to_geoid.pkl')
#         else:
#             self._df_coords_to_geoid = pd.DataFrame(columns = ['latitude','longitude','geo_id'])  
          

#         # Function short names
#         path          = self.path
#         edges         = GeoID.edges
#         shed_brackets = GeoID.shed_brackets
#         to_json  = GeoID.to_json
#         read_json     = GeoID.read_json
        

#     def _setup_counties(self, data_url = 'https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json'):
#         '''        
#         Adds county-data to object:    
#         * self.counties: (dict)
#             - imported geojson info for counties, including names, GEO_IDs, shapes / polygon-coordinates
#             - added min/max lat/long - values for counties
#             - polygon-edges
#         * self.df_geo: (pandas dataframe) self.counties as a dataframe 
        
#         Ensures datafiles:
#         * path/counties_fips.json  for self.counties
#         * path/df_geo_counties.pkl for self.df_geo         
#         '''
#         # function short names
#         path          = self.path
#         edges         = GeoID.edges
#         shed_brackets = GeoID.shed_brackets
#         to_json       = GeoID.to_json
#         read_json     = GeoID.read_json
        
#         ##########
#         # FETCH COUNTY DATA
#         # Fetch Geo-data for counties; names, fips and shapes, vertex-coordinates for polygons 
#         # If not on disk, fetch online and save to disk

#         if not os.path.exists(path+'counties_fips.json'):
#             with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
#                 counties = json.load(response)
#             # ADD TO COUNTY-DATA: 1) POLYGON-EDGES . 2) MAX/MIN VALUES FOR LAT/LONG COORDS, RESPECTIVELY
#             for county in counties['features']:
#                 vertex_list = county['geometry']['coordinates']
#                 county['geometry']['edges']    = {}
#                 county['geometry']['max_lat']  = {} 
#                 county['geometry']['min_lat']  = {}
#                 county['geometry']['max_long'] = {} 
#                 county['geometry']['min_long'] = {}
#                 # len(vertex_list) is the number of independent polygons for the county
#                 # in most cases len(vertex_list) = 1, but can be larger than one when several polygons are required 
#                 # for mapping the county - for example for a coastal county with islands. 
#                 for i in range(len(vertex_list)):
#                     verts = vertex_list[i]
#                     county['geometry']['edges'][i]    = edges(shed_brackets(verts))
#                     lats,longs = np.transpose(np.array(verts))
#                     county['geometry']['max_lat'][i]  = float(max(lats))
#                     county['geometry']['min_lat'][i]  = float(min(lats))
#                     county['geometry']['max_long'][i] = float(max(longs))
#                     county['geometry']['min_long'][i] = float(min(longs) )
#             self.to_json(counties,path+'counties_fips.json')
#         self.counties = self.read_json(path+'counties_fips.json')

#         if not os.path.exists(path+'df_geo_counties.pkl'):
#             df_all = pd.DataFrame(counties['features'])
#             df_geo = pd.DataFrame(df_all['geometry'].to_dict()).T
#             df_geo.index = df_all['properties'].apply(lambda x: x['GEO_ID'])
#             df_geo['max_lat']  = df_geo['max_lat'].apply(lambda x: max(x.values()))
#             df_geo['min_lat']  = df_geo['min_lat'].apply(lambda x: min(x.values()))
#             df_geo['max_long'] = df_geo['max_long'].apply(lambda x: max(x.values()))
#             df_geo['min_long'] = df_geo['min_long'].apply(lambda x: min(x.values()))
#             df_geo.to_pickle(path+'/df_geo_counties.pkl')
#         df_geo = pd.read_pickle(path+'df_geo_counties.pkl')
#         self.df_geo = df_geo
#         return self


#     ####
#     # COORDS TO GEO_ID
#     #
#     # Selects the counties' min/max-lat/long squares that include the point coords
#     # ToDo: narrow down to the county polygon(s) that include(s) coords         

#     def coords_to_geoid(self, coords, regis = False):
#         '''
#         input: lat.long   ,  output: county id (GEO_ID)
        
#         Find GEO_ID/fips of map coordinates. 
#         If conversion not available in lookup-table ( self._coords_to_geoid_dic ), 
#         calculate the GEO_ID for 'coords' and add to lookup-table. 

#         Algorithm: Draw a line from a point anywhere inside a polygon. It will cross the borders of the 
#         polygon an odd number of times. Any other polygon's borders will be crossed an even number of times. 

#         1. Pre-select all counties with max/min-lat/long that span the coordinate
#         2. Create a dataframe with their polygon-edges
#         3. Select all polygon edges spanning the input longitude 
#         2. From these, select 
#                 a. all edges with latitudes *larger* than the input latitude, and
#                 b. all edges *spanning* the input latitude (these will be few). 
#                     i.  Calculate the edge latitude at the point of the input longitude
#                     ii. If it exceeds the input latitude, save it (otherwise discard)
#         3. Count GEO_IDs for all selected edges (a+b above), return the GEO_ID with an odd number of counts
#         '''
#         # Ensure that coordinates are numbers, not string
        
#         if type(coords) is not tuple:
#             return coords 
        
#         coords = tuple(self._eval_if(coo) for coo in self._eval_if(coords))
#         lat,long = coords

        
#         df = self._df_coords_to_geoid
#         result = df[(df['latitude']  == lat) & 
#                     (df['longitude'] == long)]
#         if len(result) is 0:
#             result = self._coords_to_geoid(coords)

#         return result 

#     def _coords_to_geoid(self, coords):
#         '''
#         input: lat.long   ,  output: county id (GEO_ID)

#         Algorithm: Draw a line from a point anywhere inside a polygon. It will cross the borders of the 
#         polygon an odd number of times. Any other polygon's borders will be crossed an even number of times. 

#         1. Pre-select all counties with max/min-lat/long that span the coordinate
#         2. Create a dataframe with their polygon-edges
#         3. Select all polygon edges spanning the input longitude 
#         2. From these, select 
#                 a. all edges with latitudes *larger* than the input latitude, and
#                 b. all edges *spanning* the input latitude (these will be few). 
#                     i.  Calculate the edge latitude at the point of the input longitude
#                     ii. If it exceeds the input latitude, save it (otherwise discard)
#         3. Count GEO_IDs for all selected edges (a+b above), return the GEO_ID with an odd number of counts
#         '''
#         # Function short names
#         path          = self.path
#         edges         = GeoID.edges
#         shed_brackets = GeoID.shed_brackets
#         to_json       = GeoID.to_json
#         read_json     = GeoID.read_json
    
#         pre_select_fips = self._pre_select_fips
        
#         # Ensure that coordinates are numbers, not string
#         coords = tuple(self._eval_if(coo) for coo in self._eval_if(coords))
#         lat,long = coords
        
        
#         # Preselect counties with max/min-lat/long that include the coords
#         df_counties = pre_select_fips(lat,long)
#         df_counties['geo_id'] = df_counties.index
#         counties = df_counties.T.to_dict()
#         # Create a dataframe with all polygon-segments, tagged by GEO_ID (fips)   
#         edges = {}
#         def edg(poly_coords):
#             '''
#             edg: returns edges; the digram of the polygon-coordinates
#             edg([a,b,c,d]) = [[a,b],[b,c],[c,d]]  where a/b/c/d = [lat,long]
#             '''
#             edges = [np.concatenate([poly_coords[i],poly_coords[i+1],[geo_id]]) for i in range(len(poly_coords)-1)]
#             return edges 

#         for county in counties.values():
#             geo_id = county['geo_id']
#             edges[geo_id] = np.concatenate([edg(shed_brackets(poly_coords)) for poly_coords in county['coordinates']])
#         # create and save df_edges listing all polygon edges where each edge is labeled by the county GEO_ID
#         df = pd.DataFrame(np.concatenate(list(edges.values())),columns = ['lat_1','long_1','lat_2','long_2','GEO_ID'])
#         # All values in df are str, convert the polygon coordinates to numbers
#         df[['lat_1', 'long_1', 'lat_2', 'long_2']] = df[['lat_1', 'long_1', 'lat_2', 'long_2']].applymap(eval)
#         # Select all polygon edges spanning the input longitude 
#         df = df[df['long_2'].apply(lambda x: x >= long ) ^ df['long_1'].apply(lambda x: x >= long )] 
#         # Select all edges *spanning* the input latitude (need more processing) 
#         dfx = df[df['lat_2'].apply(lambda x: x >= lat )  ^ df['lat_1'].apply(lambda x: x >= lat )].copy() 
#         # Select all edges *exceeding* the input latitude
#         df = df[df['lat_2'].apply(lambda x: x >= lat )  & df['lat_1'].apply(lambda x: x >= lat )]
#         # If there are edges spanning the input latitude, select the ones 
#         # exceeding the input latitude for the input longitude
#         if len(dfx) > 0:
#             def lat_larger(edge):
#                 kd = (edge['long_2'] - edge['long_1'])

#                 if kd > 0.0001:
#                     kn = (edge['lat_2']  - edge['lat_1'])
#                     k =  kn/kd
#                     l = edge['lat_1']
#                     x = long - edge['long_1']
#                     y = k*x + l
#                     if y >= lat:
#                         return True
#                 return False
#             dfx = dfx[dfx.T.apply(lat_larger)]
#             df.append(dfx).reset_index(drop=True)
#         # Count GEO_IDs, return the one with odd number of counts     
#         crossings = pd.Series(Counter(df['GEO_ID']))
#         geo_id = list(crossings[crossings%2 == 1].index)

#         row = pd.Series(list(coords) + [geo_id], index = ['latitude','longitude','geo_id'])
#         self._df_coords_to_geoid = self._df_coords_to_geoid.append(row,ignore_index = True)
#         return  geo_id
    
#     def _pre_select_fips(self,lat,long):
#         '''
#         1. Pre-select all GEO_IDs with max/min-lat/long that span the coordinates lat,long
#         2. Return a dataframe with their polygon-edges
#         '''
#         df = self.df_geo.copy()
#         df = df[df['max_lat']  >= lat]
#         df = df[df['max_long'] >= long]
#         df = df[df['min_lat']  <= lat]
#         df = df[df['min_long'] <= long] 
#         return df   
    
#     ## GENERAL PURPOSE FUNCTIONS
#     def shed_brackets(bracketed_list):
#         ''' 
#         remove 'wrapping' brackets from list:  
#             shed_brackets([[[[a]]]])              -> [a]
#             shed_brackets([[[ [a,b,c]] ]])        -> [a,b,c]
#             shed_brackets([[ [a,[b1,b2],c] ]])    -> [a,[b1,b2],c]
#         '''
#         # Function short names
#         shed_brackets = GeoID.shed_brackets

#         if len(bracketed_list) is 1: 
#             # strip a bracket
#             bracketed_list = shed_brackets(bracketed_list[0])
#         if len(bracketed_list) is not 1:
#             shedded_list = bracketed_list
#         return shedded_list


#     def edges(vertex_list):
#         '''
#         'edges()' returns the edges for a list of polygon-vertex coordinates. It is identical to digrams. 
#         input [a,b,c,d]
#         output [[a,b],[b,c],[c,d]]
#         a/b/c is usually a list - the coordinates of vertex a/b/c respectively
#         '''
#         edge_list = [vertex_list[i:i+2] for i in range(len(vertex_list)-1)]
#         return edge_list

#     def to_json(self,variable,filename):
#         '''
#         Saves 'variable' as json-file 'filename'
#         '''
#         js = json.dumps(variable)
#         f = open(filename,"w")
#         f.write(js)
#         f.close()
#         return

#     def read_json(self,filename):
#         '''
#         Reads 'variable' from json-file 'filename'
#         '''
#         with open(filename) as json_file:
#             data = json.load(json_file)
#             json_file.close()
#         return data

#     def save_lookup(self):
#         self._df_coords_to_geoid.to_pickle(self.path+'_df_coords_to_geoid.pkl')
#         return self

#     def read_lookup(self):
#         self._df_coords_to_geoid = pd.read_pickle(self.path+'_df_coords_to_geoid.pkl')
#         return self
    
#     def _eval_if(self,a):
#         if type(a) is str:
#             return eval(a)
#         else:
#             return a





# if __name__ is '__main__': 
#     print('***DEMO: GeoID class***')
#     geo = GeoID()
#     lat,long = -86.4111,40.7073
#     geo_id = geo.coords_to_geoid((lat,long))
#     print('GEO_ID for lat,long = '+str([lat,long])+': '+ str(geo_id)+'\n \n')



# ####################
# ## CLASS: Raster
# #%% 



# class Raster:
#     '''
#     Rasters geometrical objects
#     '''
    
#     def __init__(self,vertex_list):
#         '''
#         vertex_list  : coordinates defining the shape of the geometrical object (Implemented: polygon) 
#         '''
#         self.vertex_list = vertex_list
    
#     def line(edge, step = 0.001):
#         '''
#         input:  edge - [start,end], where start and end are lists
#         output: pixelated line
#         '''
#         start,end = edge
#         scale = 1/step
#         st     = np.array([int(i) for i in (scale * np.array(start))])
#         en     = np.array([int(i) for i in (scale * np.array(end))])
#         diff   = en - st
#         x      = np.absolute(diff).argmax()
#         n      = abs(diff[x])
#         delta  = diff/n
#         raster = ([np.round(i * delta) for i in range(n)] + st)/scale
#         return raster

#     def polygon(self, raster_surface = False, step=0.001, concatenate = True, return_result = False ):
#         if type(self) is list:
#             return_result = True
#             self = Raster(self)        
#         vertex_list = self.vertex_list
#         self.polygon_edges = self._polygon_edges(step=step, concatenate_edges = concatenate)
#         if raster_surface:
#             self.polygon_surface = self._polygon_surface(step=step, concatenate_rows = concatenate)
#         if return_result:
#             if raster_surface:
#                 return self.polygon_surface
#             else:
#                 return self.polygon_edges
#         else:
#             return self
            


#     def _polygon_edges(self, step=0.001, concatenate_edges = True):
#         '''
#         input: list of polygon vertices
#         '''
#         df_edges        = pd.DataFrame(pd.Series(vertex_list[:-1]),columns = ['start'])
#         df_edges['end'] = pd.Series(vertex_list[1:])
#         raster     = np.array([Raster.line(edge,step=step) for edge in df_edges.values])
#         if concatenate_edges:
#             raster = np.concatenate(raster)
#         return raster

#     def _polygon_surface(self, step=0.001, concatenate_rows = True):
#         vertex_list = self.vertex_list
#         shape = self.polygon_edges
#         dfm = pd.DataFrame(shape).groupby(0)
#         df = dfm.min()
#         df.columns = ['min']
#         df['max'] = dfm.max()
#         df['fill'] = df.apply(lambda x: np.arange(x['min'],x['max'],0.01),axis=1)
#         df['n'] = df['fill'].apply(len)
#         dic = df[df['n'] >0]['fill'].to_dict()
#         fill = {}
#         for item in dic.items():
#             ix,x = item
#             y = np.full(len(x),ix)
#             row = np.transpose(np.array([y,x])) 
#             fill[ix] = row
#         fill = np.array(list(fill.values()))
#         if concatenate_rows:
#             fill = np.concatenate(fill)
#         return fill    


# if __name__ is '__main__': 
#     print('***DEMO: Raster class***')
#     print('County border raster:')
#     vertex_list = [[-86.165024, 40.562649],
#                     [-86.184044, 40.562459],
#                     [-86.374417, 40.561361],
#                     [-86.373521, 40.69246],
#                     [-86.525164, 40.692419],
#                     [-86.581739, 40.735765],
#                     [-86.581532, 40.910925],
#                     [-86.468509, 40.909889],
#                     [-86.169012, 40.909836],
#                     [-86.165024, 40.562649]]
#     rr = Raster.polygon(vertex_list,raster_surface = False)
#     x,y = np.transpose(rr)
#     sns.scatterplot(x,y)
#     plt.show()
#     print('County surface raster:')
#     rr = Raster.polygon(vertex_list,raster_surface = True)
#     x,y = np.transpose(rr)
#     sns.scatterplot(x,y)
#     plt.show()

