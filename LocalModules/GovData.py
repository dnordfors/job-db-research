
#%%
# IMPORT LIBRARIES

## OS
import os

# MANAGE
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'

import numpy as np
import scipy
import collections

## FIT
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate
from sklearn.model_selection import cross_val_score
from sklearn import linear_model
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
import xgboost as xgb

from sklearn.metrics import r2_score

## DECOMPOSITION
from sklearn.decomposition import NMF
from scipy.linalg import svd

## GRAPHS
import networkx as nx
from networkx.algorithms import community

## PRESENTATION

    ### Graphics
from matplotlib import pyplot as plt
import seaborn as sns
sns.set(rc={'figure.figsize':(11.7,8.27)})

import plotly
import plotly.graph_objs as go
import plotly.graph_objects as go
import plotly.figure_factory as ff
import plotly.express as px

    ### Rich text 
from IPython.display import display, Markdown, Latex

    ### Apps & Widgets
import ipywidgets as widgets

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output


## I/O
import zipfile
import requests
import pickle

## DOWNLOAD
import urllib.request
from tqdm import tqdm

# ## NLP
# import nltk
# lemmatizer = nltk.stem.WordNetLemmatizer()
# from nltk.corpus import stopwords
# set(stopwords.words('english'))
# from nltk.tokenize import RegexpTokenizer
# tokenizer = RegexpTokenizer(r'\w+')

class DownloadProgressBar(tqdm):
    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)

def download_url(url, output_path):
    with DownloadProgressBar(unit='B', unit_scale=True,
                             miniters=1, desc=url.split('/')[-1]) as t:
        urllib.request.urlretrieve(url, filename=output_path, reporthook=t.update_to)


## CREATE PATHS / DIRECTORIES 

# path to home directory (the location of this file)
#path0 = !pwd
#path = path0.n

path = os.path.curdir

# os.chdir(path)

#Check if directories exist - creat directories if needed

paths = {}
paths['data'] = './data'
paths['census'] = paths['data'] + '/census'
paths['onet'] = paths['data'] + '/onet'

[os.makedirs(pth, exist_ok=True) for pth in paths.values()]


## GENERAL FUNCTIONS 
### NORMALIZATION
# Statistic normalization - subtract mean, scale by standard deviation

def norm_stat(vec, weights = False):
    '''
    Normalizes a vector v-v.mean())/v.std() 
    '''
    if weights:
        return  np.mean(abs(vec - vec.mean()))  
    return (vec-vec.mean())/vec.std()

# Algebraic normalization - dot product
def norm_dot(vec, weights = False):
    '''
    Normalizes the columns of a DataFrame (dot product)
    '''
    if weights:
        return  np.sqrt(vec @ vec)
    
    return vec / np.sqrt(vec @ vec)

# Algebraic normalization - dot product
def norm_sum(vec, weights = False):
    '''
    Normalizes the columns of a DataFrame (dot product)
    '''
    if weights:
        return  vec.sum()
    
    return vec / vec.sum()

# 
# Scaled Normalization -
def scale(vec, weights = False):
    stop_divide_by_zero = 0.00000001
    if weights:
        return (vec.max()-vec.min() + stop_divide_by_zero)
    return (vec-vec.min())/(vec.max()-vec.min() + stop_divide_by_zero)



### SELECTION 
def is_string(a):
    '''
    typically used for Boolean masking in Pandas, e.g.  
               df[df['column'].apply(is_string)] 
    returns all rows in df where df['column'] has a string value   
    '''
    return isinstance(a,str)


## CLASSES

### DATA DICTIONARY 

class    Datadic: 
    def __init__(self):
        # Set up FIPS CODES for states and regions
        fips_codes_exists = os.path.isfile('data/state-geocodes-v2016.xls')
        if not fips_codes_exists:
            print('*** FIPS State Geocodes is missing. Downloading from Census...')
            # !curl -o ./data/state-geocodes-v2016.xls -O https://www2.census.gov/programs-surveys/popest/geographies/2016/state-geocodes-v2016.xls
            download_url('https://www2.census.gov/programs-surveys/popest/geographies/2016/state-geocodes-v2016.xls','./data/state-geocodes-v2016.xls')
            print('*** Complete.')

        self.dfips = pd.read_excel('data/state-geocodes-v2016.xls')[5:] #five first rows are comment
        self.name_to_fips = self.dfips.set_index('Unnamed: 3')['Unnamed: 2'].to_dict()
        self.fips_to_name = self.dfips.set_index('Unnamed: 2')['Unnamed: 3'].to_dict()
        self.translate_fips = {**self.name_to_fips,**self.fips_to_name}
        
        self.state_to_abbrev = {'Alabama': 'AL','Alaska': 'AK','Arizona': 'AZ','Arkansas': 'AR','California': 'CA','Colorado': 'CO',
                'Connecticut': 'CT','Delaware': 'DE','District of Columbia': 'DC','Florida': 'FL','Georgia': 'GA','Hawaii': 'HI',
                'Idaho': 'ID','Illinois': 'IL','Indiana': 'IN','Iowa': 'IA','Kansas': 'KS','Kentucky': 'KY','Louisiana': 'LA',
                'Maine': 'ME','Maryland': 'MD','Massachusetts': 'MA','Michigan': 'MI','Minnesota': 'MN','Mississippi': 'MS','Missouri': 'MO',
                'Montana': 'MT','Nebraska': 'NE','Nevada': 'NV','New Hampshire': 'NH','New Jersey': 'NJ','New Mexico': 'NM',
                'New York': 'NY','North Carolina': 'NC','North Dakota': 'ND','Ohio': 'OH','Oklahoma': 'OK','Oregon': 'OR',
                'Pennsylvania': 'PA','Rhode Island': 'RI','South Carolina': 'SC','South Dakota': 'SD','Tennessee': 'TN','Texas': 'TX',
                'Utah': 'UT','Vermont': 'VT','Virginia': 'VA','Washington': 'WA','West Virginia': 'WV','Wisconsin': 'WI','Wyoming': 'WY'}
        self.abbrev_to_state = {v: k for k, v in self.state_to_abbrev.items()}
        self.translate_state_abbrev = {**self.state_to_abbrev,**self.abbrev_to_state}
        
        #Set up CENSUS/ACS PUMS DATA DICTIONARY
        pums_datadic_exists = os.path.isfile('./data/census/PUMS_Data_Dictionary.csv')
        if not pums_datadic_exists:
            print('*** Census ACS/PUMS Data Dictionary is missing. Downloading from Census...')
            # !curl -o ./data/census/PUMS_Data_Dictionary.csv -O https://www2.census.gov/programs-surveys/acs/tech_docs/pums/data_dict/PUMS_Data_Dictionary_2017.csv
            download_url('https://www2.census.gov/programs-surveys/acs/tech_docs/pums/data_dict/PUMS_Data_Dictionary_2017.csv','./data/census/PUMS_Data_Dictionary.csv')
            print('*** Complete.')
        self.census = pd.read_csv("data/census/PUMS_Data_Dictionary.csv").drop_duplicates()
        self.census_variable_definitions = self.census.groupby('RT').first()['Record Type'].to_dict()      
            
    def fips(self,name_or_fipsnr):
        nn = datadic.abbrev_to_state.get(name_or_fipsnr,name_or_fipsnr)
        return self.translate_fips.get(nn)
    
    def state_abbrev(self,name_or_abbrev):
        return self.translate_state_abbrev.get(name_or_abbrev)
    
    def census_def(self,variable_code):
        return self.census_variable_definitions.get(variable_code)

    # All definitions containing a select string
    def clk(self,census_col,search_string):
        return self.census[census_col].fillna('nan').apply(lambda x: search_string.lower() in x.lower())
    
    def census_about(self,search_string):
        return self.census[self.clk('Record Type',search_string) | self.clk('Unnamed: 6',search_string) ]
    
datadic = Datadic()


class Onet:
    '''
    Onet() is an object based on the Onet labor market database. 

    my_onet.source  - string: URL for importing the onet database from source
    my_onet.path    - string: local path to the directory where the onet database is stored. (set in 'paths' dictionary)
    my_onet.name    - string: the prefix of stored files, e.g. zipped DB:      path + '/'+ name +'.zip'
    my_onet.toc()   - function: returns table of contents for onet database

   # Data in Onet database
    my_onet.data()
                    - function: returns dataset named by label.
    my_onet.matrix() 
                    - function: returns onet dataset in matrix form
    my_onet.n_matrix() 
                    - function: normalized onet matrix
 
   
    '''
    def __init__(self,path = paths['onet'], name = 'onet', source = 'https://www.onetcenter.org/dl_files/database/db_24_1_excel.zip'):
        
        self.path = path
        self.name = name
        self.source = source
        self.dataset = {}
        self.matrix_dic = {}
        zip_file = path + '/'+ name +'.zip'
        onet_exists = os.path.isfile(zip_file)
        if not onet_exists:
            print('*** Onet database does not exist. Downloading from Onet...')
            #shcmd = 'curl -o '+zip_file+' -O '+source
            #!$shcmd'
            download_url(source,zip_file)
            print('*** Complete.')
        self.zip = zipfile.ZipFile(zip_file)
        self.tocdf = self.make_toc()
        self.socp_titles = self.data('Alternate Titles',socp_shave = 8)[['SOCP_shave','Title']].drop_duplicates()
        self.titles = pd.DataFrame() #initialize empty dataframe - required by dash dashboard
        self.G = nx.Graph()
        self.soc_aggregations = pd.DataFrame({
            'Category': {0: 'intermediate aggregation',1: 'intermediate aggregation',2: 'intermediate aggregation',3: 'intermediate aggregation',4: 'high-level aggregation',5: 'high-level aggregation',6: 'high-level aggregation',
                    7: 'high-level aggregation',8: 'high-level aggregation',9: 'major group',10: 'major group',11: 'major group',12: 'major group',13: 'major group',14: 'major group',15: 'major group',16: 'major group',
                    17: 'major group',18: 'major group',19: 'major group',20: 'major group',21: 'major group',22: 'major group',23: 'major group',24: 'major group',25: 'major group',26: 'major group',27: 'major group',
                    28: 'major group',29: 'major group',30: 'major group',31: 'major group'},
            'Label': {0: 1.0,1: 2.0,2: 3.0,3: 5.0,4: 1.0,5: 2.0,6: 3.0,7: 4.0,8: 5.0},
            'Major groups': {0: '[ 11 , 12 , 13 ]',1: '[ 15 , 16 , 17 , 18 , 19 ]',2: '[ 21 , 22 , 23 , 24 , 25 , 26 , 27 ]',3: '[ 31 , 32 , 33 , 34 , 35 , 36 , 37 , 38 , 39 ]',
                    4: '[ 11 , 12 , 13 , 14 , 15 , 16 , 17 , 18 , 19 , 20 , 21 , 22 , 23 , 24 , 25 , 26 , 27 , 28 , 29 ] ',5: '[ 31 , 32 , 33 , 34 , 35 , 36 , 37 , 38 , 39 ]',6: '[ 41 , 42 , 43  ]',
                    7: '[ 45 , 46 , 47 , 48 , 49  ]',8: '[ 51 , 52 , 53  ]',
                    9: '[ 11 ]',10: '[ 13 ]',11: '[ 15 ]',12: '[ 17 ]',13: '[ 19 ]',14: '[ 21 ]',15: '[ 23 ]',16: '[ 25 ]',17: '[ 27 ]',18: '[ 29 ]',19: '[ 31 ]',20: '[ 33 ]',21: '[ 35 ]',22: '[ 37 ]',23: '[ 39 ]',
                    24: '[ 41 ]',25: '[ 43 ]',26: '[ 45 ]', 27: '[ 47 ]', 28: '[ 49 ]', 29: '[ 51 ]', 30: '[ 53 ]', 31: '[ 55 ]'},
            'Aggregation title': {0: 'Management, Business, and Financial Occupations',
                    1: 'Computer, Engineering, and Science Occupations',2: 'Education, Legal, Community Service, Arts, and Media Occupations',3: 'Service Occupations',4: 'Management, Business, Science, and Arts Occupations',
                    5: 'Service Occupations',6: 'Sales and Office Occupations',7: 'Natural Resources, Construction, and Maintenance Occupations',8: 'Production, Transportation, and Material Moving Occupations',
                    9: 'Management Occupations',10: 'Business and Financial Operations Occupations', 11: 'Computer and Mathematical Occupations',12: 'Architecture and Engineering Occupations',13: 'Life, Physical, and Social Science Occupations',
                    14: 'Community and Social Service Occupations',15: 'Legal Occupations',16: 'Educational Instruction and Library Occupations',17: 'Arts, Design, Entertainment, Sports, and Media Occupations',18: 'Healthcare Practitioners and Technical Occupations',
                    19: 'Healthcare Support Occupations',20: 'Protective Service Occupations',21: 'Food Preparation and Serving Related Occupations',22: 'Building and Grounds Cleaning and Maintenance Occupations',23: 'Personal Care and Service Occupations',
                    24: 'Sales and Related Occupations',25: 'Office and Administrative Support Occupations',26: 'Farming, Fishing, and Forestry Occupations',27: 'Construction and Extraction Occupations',28: 'Installation, Maintenance, and Repair Occupations',
                    29: 'Production Occupations',30: 'Transportation and Material Moving Occupations',31: 'Military Specific Occupations '}
        })

    
    def make_toc(self,sep ='.'):
        '''
        Creates table of contents for Onet Database, returns as my_onet.tocdf (dataframe)
        '''
        nlst = np.array(self.zip.namelist())
        dr = nlst[0]
        nl = pd.DataFrame(nlst)
        self.tocdf = pd.DataFrame(nl[0].apply(lambda x: np.char.split(x.replace(dr,''),sep = '.'))[1:].to_dict(),
                                    index = ['name','extension']).T
        return self.tocdf
    
    def toc(self, name_contains= False, extension = False):
        '''
        Returns table of contents for Onet Database (dataframe) masked by string and/or extension
        '''
        selection = self.tocdf
        if extension:
            selection = selection[selection['extension'] == extension  ]['name']
        if name_contains:
            search_string = name_contains
            selection = selection[selection['name'].apply(lambda x: search_string.lower() in x.lower())]['name']
        return selection
         
    
    def data(self,label, socp_shave = 6):
        '''
        Returns onet dataset named 'label'
        '''
        # If dataframe in dictionary:  
        if label in self.dataset.keys():
            df = self.dataset[label]
            if 'O*NET-SOC Code' in str(df.columns):
                df['SOCP_shave'] = df['O*NET-SOC Code'].apply(lambda x: x.replace('.','').replace('-','')).apply(lambda x: x[:socp_shave])
            return self.dataset[label]

        # If dataframe NOT in dictionary: 
        # If pickled dataframe does not exist, create from zipped excel
        # Read pickled dataframe into dictionary
        pkl_name = self.path +'/'+ self.name +'_'+ label +'.pkl'
        pkl_exists = os.path.isfile(pkl_name)

        if not pkl_exists:
            print('*** '+label+'.pkl does not exist. Creating...')
            xlsx_name = self.zip.namelist()[0] + label +'.xlsx'
            pd.read_excel(self.zip.extract(xlsx_name)).to_pickle(pkl_name)
            print('*** Complete.')
        df = pd.read_pickle(pkl_name)
        if 'O*NET-SOC Code' in str(df.columns):
            df['SOCP_shave'] = df['O*NET-SOC Code'].apply(lambda x: x.replace('.','').replace('-','')).apply(lambda x: x[:socp_shave])
        self.dataset[label] = df                              
        return self.dataset[label]
    
    def grpby(self, label, columns = ['Scale Name','Element Name'], data_value = 'Data Value', scale_name = 'Level'):
        grp = pd.DataFrame(self.data(label).copy().groupby(columns).apply(lambda x: x[data_value].values))
        return grp

    def matrix(self,label, xx = 'Element Name',  yy = 'SOCP_shave',socp_shave = 6 , data_value = 'Data Value', scale_name = 'Level',
                show = 'mean', norm = False):
        '''
        Converts onet dataset into a matrix 
        xx          - matrix columns
        yy          - matrix index
        scale_name  - value category 
        data_value  - data values
        socp_shave  - number of digits in 'shaved' SOCP number
        show        - output matrix shows 'mean'(default), 'std' or 'count' (relevant for groupby socp_shave)
        norm        - columns are normalized: 'norm = norm_dot'  [ col/sqrt(col@col) ] 
                                              'norm = norm_stat' [ (col - col.mean) / col.std ] 
                                         
        '''
        if not (label,xx,yy,socp_shave,data_value,scale_name,norm) in self.matrix_dic.keys():
            print('*** Onet matrix not in dictionary. Constructing....')
            columns = ['Scale Name',yy,xx] # Default columns
            grpb = self.data(label,socp_shave = socp_shave).groupby(columns)
            mat_mean   = grpb.mean().loc[scale_name][data_value].unstack()
            mat_std    = grpb.std().loc[scale_name][data_value].unstack()
            mat_count = grpb.count().loc[scale_name][data_value].unstack().T.iloc[[0]].T
            if norm:
                w = mat_mean.apply(lambda col: norm(col,weights = True) )
                mat_mean = mat_mean.apply(norm)
                mat_std = mat_std/w
            self.matrix_dic[label,xx,yy,socp_shave,data_value,scale_name,norm] = {
                'mean':mat_mean,'std':mat_std,'count':mat_count}
            print('*** Complete')
        return self.matrix_dic[label,xx,yy,socp_shave,data_value,scale_name,norm][show]
    
    def _func(self):
        for i in range(len(self.titles)):
            row = self.titles.iloc[i]
            df = self.soc_categorize(row['O*NET-SOC Code'])
            row[df.index] = df['Aggregation title']
        return


    def soc_categorize(self,soc):
        mgrp = ' '+soc[:2]+' ' 
        df =self.soc_aggregations.copy()
        result = df[df['Major groups'].apply(lambda st: mgrp in st )]
        return result[['Category','Aggregation title']].set_index('Category')


    def alternate_titles_graph(self, title):
        G = self.G
        aaa =self.data('Alternate Titles')
        bbb = self.data('Sample of Reported Titles')
        a1 = aaa[aaa['Title'].apply(lambda st: title.lower().replace('-',' ') in (st+' ').lower().replace('-',' '))]
        a2 = aaa[aaa['Alternate Title'].apply(lambda st: title.lower().replace('-',' ') in (st+' ').lower().replace('-',' '))]
        b1 = bbb[bbb['Title'].apply(lambda st: title.lower().replace('-',' ') in (st+' ').lower().replace('-',' '))]
        b2 = bbb[bbb['Reported Job Title'].apply(lambda st: title.lower().replace('-',' ') in (st+' ').lower().replace('-',' '))]
        b2['Alternate Title'] = b2['Reported Job Title'] 
        b1['Alternate Title'] = b1['Reported Job Title']  
        b2 = b2.drop(columns=['Reported Job Title'])
        b1 = b1.drop(columns=['Reported Job Title'])
        self.titles = a1.append(b1,sort=True).append(a2).append(b2,sort=True)
        self.titles = self.titles[['O*NET-SOC Code','Title','Alternate Title']].drop_duplicates().sort_values(by='O*NET-SOC Code')
        self.titles['intermediate aggregation'] = 'N/A'
        self.titles['high-level aggregation']   = 'N/A'
        self.titles['major group']              = self.titles['O*NET-SOC Code'].apply(lambda x: x[:2])
        self._func()
        self.titles['detailed occupation']      = self.titles['O*NET-SOC Code'].apply(lambda x: self.occupation_data.get(x[:7]+'.00')['Title'])
        self.titles = self.titles[['Alternate Title', 'Title', 'high-level aggregation',
        'intermediate aggregation',  'major group',
        'detailed occupation','O*NET-SOC Code']].sort_values(by = 'Alternate Title').drop_duplicates()
        self.titles.apply(lambda x: G.add_edge(x['Title'],x['Alternate Title']),axis = 1)
        return  

    def _try(self,func,arg):
        try:
            return func(arg)
        except:
            return str(arg)+' Exception'

    def disambiguation_evaluator(self,title):
        self.disambiguation_title = title
        self.alternate_titles_graph(title)
        self._func()
        set_len = lambda x:len(set(x))
        result = self.titles.apply(lambda x: self._try(set_len,x))
        result['search'] = title
        result = result[['search','high-level aggregation',
            'intermediate aggregation', 'major group', 'detailed occupation','Title']]
        result.index = ['Search', '#high-level aggregations',
            '#intermediate aggregations', '#major groups', '#detailed occupations','#job titles']
        self.disambiguation_levels = result
        return result.to_dict().values() 

    def tasks_for(self,job_descriptor,descriptors = 'O*NET-SOC Code'):
        df = self.data('Task Statements')
        result = df[df[descriptors].apply(lambda x: job_descriptor.lower() in x.lower())]
        return result

     
# Instantiate Onet() as 'onet'
onet = Onet()            


class   Census:
    '''
    Census() is an object containing census ACS/PUMS data 
    my_census.source    - string: URL for importing the census data from US Census online
    my_census.path      - string: local path to the directory where the census data is stored. (set in 'paths' dictionary)
    my_census.name      - string: the prefix of stored files, e.g. pickle:      path + '/'+ name +'.pkl'

    my_census.data(self,state, socp_shave = n)
                        - function: returns the acs/pums for 'state', with an SOCP number of 'n' digits (default n=6)
    my_census.dataset   - dataframe: acs/pums data for a state (state abbreviation used for naming)
    my_census.import_from_source(self,state)
                        - function: imports data for 'state' from my_census.source and converts to pickled dataframe 


    # my_census.state    – String: name of state, e.g. 'California', or abbreviation, e.g. 'CA'
    # my_census.data     – DataFrame: imported census ACS/PUMS data (from pickle)
    # my_census.workers  – DataFrame: people fulfilling 'workers' criteria (see below)
    # my_census.occupations         – DataFrame: Occupations of the workers (groupby SOCP-number)
    
    # my_census.workers_occupations(age_low = 40, age_high = 65, std_max = 0.5, socp_granularity = 5):
    #      - Function: populates my_census.workers / .occupations according to criteria
    #          – Workers: age_low (default 40)  and age_high (default 65)
    #          – Occupations: socp_granularity (default 5) ; the length of the SOCP number, default 5 digits.
    
    '''

    def __init__(self,path = paths['census'], 
                 name = 'census', 
                 source = 'https://www2.census.gov/programs-surveys/acs/data/pums/2017/5-Year/'):
        self.path = path
        self.name = name
        self.source = source
        self.dataset = {}
        
    
    def data(self,state, socp_shave = 6):
        '''
        READ CENSUS/ACS PUMS DATABASE. Search order: Dictionary, Pickle; Create dictionary/pickle if non-existent.
        socp_shave  : number of digits in 'shaved' SOCP number
        '''
        state_abbr = datadic.state_to_abbrev.get(state,state)
        pkl_name = self.path +'/'+ self.name +'_'+ state_abbr +'.pkl'
        pkl_exists = os.path.isfile(pkl_name)
        if not pkl_exists:
            self.import_from_source(state)
        df = pd.read_pickle(pkl_name)
        df['SOCP_shave'] = df['SOCP'].apply(lambda x: x[:socp_shave].replace('X','0') if type(x)==str else x)
        self.dataset[state_abbr] = df
        return self.dataset[state_abbr]


    # Create and execute shell command fetching state census zip-file 
    def import_from_source(self,state):
        '''
        Imports ACS/PUMS dataset from US Census online, URL: my_census.source
        '''
        print('*** Downloading '+state+' ACS/PUMS dataset from US Census...')
        state_abbr = datadic.state_to_abbrev.get(state,state)
 #       shcmd = "curl -o "+self.path+'/'+self.name+"_tmp.zip -O "+ self.source +self.state_zipfile_name(state_abbr)
 #       ! $shcmd
        download_url(self.source +self.state_zipfile_name(state_abbr),self.path+'/'+self.name+"_tmp.zip")
        print('*** Reformatting...')
        with zipfile.ZipFile(self.path+'/'+self.name+"_tmp.zip", 'r') as zipObj:
            zipObj.extractall(self.path)
        csv_filename = self.path+'/psam_p'+datadic.fips(state)+'.csv'
        pkl_filename = self.path+'/'+self.name+'_'+state_abbr+'.pkl'
        pd.read_csv(csv_filename).to_pickle(pkl_filename)
        # ! rm $csv_filename 
        os.remove(csv_filename)
        print('*** Complete.')
        return 

    def state_zipfile_name(self,state_abbr):
        '''
        Input: State name abbreviation. Returns census-convention name of zipped csv-file
        '''
        return 'csv_p'+state_abbr.lower()+'.zip'
    

# Instantiate Census() as 'census'
census = Census()





