# Name #

## Job_Services ##

# Side Projects #
# Job Quality Prediction #

This application is used as a method of predicting industries for a given jobs. This service provide industry prediction Using Tensorflow DNN Classifier. 

This methods filters out irrelevent descriptions such as Equal Employment Act, Contact information, and Auto Gerenated summary(Associate Topics:). Predict the industry based on Job Description and Job title. We also include Company name in it which was used for dropping irrelvant sentences.

# Prerequisites #

- Full list of packages in the requirements.txt
- Python 3.6 or python 3.4
- Pip (latest verion)               


# Structure #

- Job_Parsing.py  # Sentence Split and drop
- FeatureCreation.py # Create features based on SentenceList and Drop list
- Preprocess.py  # Preprocess the data(1. Drop sentences; 2. Drop stop words; 3. Lemmatisation )
- Prediction.py # Predict Scores based on trained model
- setup.py # Predict Scores based on trained model

## Data ##

The Training data consist of 477201 training job records from January 2019. and 110127 job records from year 2017. 
The purpose of data from 2017 is to oversample to less popular job industry and balanced the distribution between industries. 

## Install Packages ##

Download the IndustryPredictor file after pulling the changes from BitBucket to load the virtual-env with latest code
Download the model folder "SavedModel0" and put it in the same folder where the PredictionApp.py stored.  It contain Deep Neural Network weights with size about 2Gbs.

Run, 
    pip install . 

## Examples ##

```python
from IndustryPredictor.PredictionApp import IndustryPrediction
obj = IndustryPrediction()
# Load model
model_path = '/Users/kaijin/Desktop/Bold_Projects/RankJobs/Dash_Plotly/IndustryPrediction/IndustryPredictor/IndustryPredictor'
#obj.load_model(model_path)
```

```python
import IndustryPredictor
# 46 Industry list 
IndustryPredictor.PredictionApp.IndustryList
# 46 Industries map to Bold 51 Industry
IndustryPredictor.PredictionApp.SubDict
# Burning Glass SubIndustry to Industry 
IndustryPredictor.PredictionApp.SubIndustry_Industry
```

```python
help(IndustryPredictor)
```
    Help on package IndustryPredictor:

    NAME
        IndustryPredictor

    PACKAGE CONTENTS
        FeatureCreation
        Job_Parsing
        PredictionApp
        Preprocess

    FILE
        /anaconda3/lib/python3.6/site-packages/IndustryPredictor/__init__.py
    

## for batch process
```python
# For batch process
# it has 3 options 'data_frame', 'dictionary', 'json'
# pf_df can be a pandas dataframe with column name "Description", "Title", "Company"
# or pd_df can also have option as a dictionary or json object with above keys
# It takes a list of dictionary with 3 keys, such as( "Description", "Title", "Company") and corresponding values 
# The number of processors are number of processor for multiple process
# For batch process, we should process cleaning data before load model, beacuse the size of the model is very large

# clean data
obj.data_process_batch_mult(pd_df, "Description", "Title", "Company" ,"data_frame", number_of_processor = 4)
# load data
obj.load_model(model_path)
# make predictions, we 
result = predict_batch()
```


## for single process
```python
# Load model
model_path = '/Users/kaijin/Desktop/Bold_Projects/RankJobs/Dash_Plotly/IndustryPrediction/IndustryPredictor/IndustryPredictor'
obj.load_model(model_path)
```

```python
# Start prediction 
description="<p>The primary role of the Dental Assistant is to perform a variety of patient care, office, and laboratory duties. Dental assistants prepare patients for oral examination and assist other dental professionals in providing treatment to the teeth, mouth, and gums. Exemplifying the practices 3 core values is key to the Dental Assistant becoming a great team player</p>"
company = "Peak Dental Services"
title = "Dental Assistant"
n=5 # number of industries that you need
obj.predict_industry(description, company, title, n)

```
[('Dental', 0.9),
 ('Healthcare', 0.13099999725818634),
 ('Administrative Support', 0.09399999678134918),
 ('Education and Training', 0.04100000113248825),
 ('Nursing', 0.01600000075995922)]

```python
%%timeit
obj.predict_industry(description, company, title, n)
```
    25.5 ms ± 449 µs per loop (mean ± std. dev. of 7 runs, 10 loops each)


# Who do I talk to? #
* Repo owner or admin