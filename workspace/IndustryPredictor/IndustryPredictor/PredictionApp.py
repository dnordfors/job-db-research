## This file is used to predict the industry from a list of 51 industry 
## Take Description, Job title, length of document as input and  output an industry

## Option 1: Plain Predictions from DeepNN 
# Option 2: append a second layer of further prediction based on option1 
# Option 3: reduce the number of industry to increase the accuarcy 

#pd.set_option('display.max_columns', 100)

import traceback
import tensorflow as tf
import tensorflow_hub as hub

from sklearn.preprocessing import MultiLabelBinarizer, LabelEncoder
import tensorflow as tf
from sklearn.preprocessing import MultiLabelBinarizer, LabelEncoder

import pickle
import numpy as np
import os
import sys
import pandas as pd
from multiprocessing import Pool
import re

from . import Preprocess
from multiprocessing import Pool

IndustryDic={'Skilled Trades': 0, 'Healthcare': 1, 'Sales': 2, 'Accounting and Finance': 3, 'Marketing, Advertising and PR': 4, 'Transportation and Distribution': 5, 'Engineering': 6, 'Customer Service': 7, 'Education and Training': 8, 'Manufacturing and Production': 9, 'Personal Services': 10, 'Human Resources': 11, 'Installation and Maintenance': 12, 'Retail': 13, 'Science': 14, 'Management': 15, 'Banking and Financial Services': 16, 'Administrative Support': 17, 'Law Enforcement and Security': 18, 'Insurance': 19, 'Nursing': 20, 'Telecommunications and Wireless': 21, 'Art, Fashion and Design': 22, 'Food and Beverage': 23, 'Green Jobs': 24, 'Architecture': 25, 'Natural Resources and Agriculture': 26, 'Business': 27, 'Construction': 28, 'Legal': 29, 'Pharmacy': 30, 'Psychology': 31, 'Performing Arts': 32, 'Entertainment and Media': 33, 'Fitness and Recreation': 34, 'Community and Public Service': 35, 'Dental': 36, 'Library': 37, 'Government': 38, 'Textile and Apparel': 39, 'Travel and Hospitality': 40, 'Real Estate': 41, 'Beauty and Spa': 42, 'Military': 43, 'Childcare': 44, 'Social Sciences': 45, 'Veterinary': 46, 'Sports': 47, 'Humanities and Liberal Arts': 48, 'Funeral Services': 49, 'Computers and Technology': 50}
IndustryDicRev={}
for key in IndustryDic:
    IndustryDicRev[IndustryDic[key]]=key

KeyWordsCheck={'Library': {'library', 'archivist', 'library media', 'librarian'}, 'Telecommunications and Wireless': {'telecommunications', 'broadcast'}, 'Funeral Services': {'funeral service', 'embalm', 'funeral', 'cremation cemetery', 'home cemeteries', 'funeral cremation', 'funeral home', 'embalmer'}, 'Humanities and Liberal Arts': {'black history', 'restoration technician'}, 'Social Sciences': {'political science', 'historians', 'sociologist', 'anthropologists', 'archaeological', 'archeologists', 'sociology', 'policy analyst'}, 'Performing Arts': {'painter'}, 'Entertainment and Media': {'photography', 'newscasts'}, 'Architecture': {'cad designer', 'design surpass', 'deconstruction salt', 'building project', 'cad technician', 'cad operator', 'piping designer', 'leadership in energy and environmental design', 'cad drafter', 'gpcp', 'document conformity', 'abb value chain', 'mechanical designer', 'civil infrastructure', 'civil design', 'landscape architect', 'guiding principles compliance professional', 'Drafter'}, 'Beauty and Spa': {'body care', 'deep tissue', 'massage envy', 'esthetician', 'skin care', 'cosmetology', 'license massage', 'massage therapy', 'nail technician', 'estheticians', 'massage therapist', 'benefit massage', 'body treatments'}, 'Community and Public Service': {'social service', 'social worker'}, 'Childcare': {'child life', 'childcare', 'child care', 'child development'}, 'Dental': {'accredit dental', 'quality dental', 'practice dentistry', 'dentistry', 'dental hygienists', 'orthodontic', 'dental hygienist', 'dental assistant', 'dentist'}, 'Green Jobs': {'gardening', 'waste and recycling', 'conservation', 'florists', 'recycling', 'environmental scientists', 'gardeners'}, 'Veterinary': {'veterinary', 'veterinary hospitals', 'veterinary technician', 'veterinary care', 'animal care', 'veterinary clinical', 'veterinary medicine', 'veterinary assistant', 'veterinarian'}}
SubDict={'Manufacturing and Production': ['Manufacturing and Production', 'Textile and Apparel', 'Military', 'Natural Resources and Agriculture'], 'Sales': ['Sales', 'Retail'], 'Education and Training': ['Education and Training', 'Sports'], 'Healthcare': ['Healthcare'], 'Psychology': ['Psychology'], 'Veterinary': ['Veterinary'], 'Humanities and Liberal Arts': ['Humanities and Liberal Arts'], 'Dental': ['Dental'], 'Pharmacy': ['Pharmacy'], 'Funeral Services': ['Funeral Services'], 'Skilled Trades': ['Skilled Trades'], 'Telecommunications and Wireless': ['Telecommunications and Wireless'], 'Performing Arts': ['Performing Arts'], 'Construction': ['Construction'], 'Personal Services': ['Personal Services'], 'Community and Public Service': ['Community and Public Service'], 'Science': ['Science'], 'Real Estate': ['Real Estate'], 'Insurance': ['Insurance'], 'Accounting and Finance': ['Accounting and Finance'], 'Marketing, Advertising and PR': ['Marketing, Advertising and PR'], 'Transportation and Distribution': ['Transportation and Distribution'], 'Engineering': ['Engineering'], 'Customer Service': ['Customer Service'], 'Childcare': ['Childcare'], 'Fitness and Recreation': ['Fitness and Recreation'], 'Human Resources': ['Human Resources'], 'Installation and Maintenance': ['Installation and Maintenance'], 'Management': ['Management'], 'Banking and Financial Services': ['Banking and Financial Services'], 'Administrative Support': ['Administrative Support'], 'Entertainment and Media': ['Entertainment and Media'], 'Library': ['Library'], 'Law Enforcement and Security': ['Law Enforcement and Security'], 'Nursing': ['Nursing'], 'Art, Fashion and Design': ['Art, Fashion and Design'], 'Green Jobs': ['Green Jobs'], 'Architecture': ['Architecture'], 'Business': ['Business'], 'Legal': ['Legal'], 'Government': ['Government'], 'Travel and Hospitality': ['Travel and Hospitality'], 'Food and Beverage': ['Food and Beverage'], 'Beauty and Spa': ['Beauty and Spa'], 'Computers and Technology': ['Computers and Technology'], 'Social Sciences': ['Social Sciences']}
SubIndustry_Industry={'Humanities and Liberal Arts': {'Museums and Galleries'}, 'Library': {'Archivists', 'Circulation Services', 'Librarians'}, 'Natural Resources and Agriculture': {'Farming', 'Logging', 'Forestry', 'Wood Workers', 'Ranch', 'Watershed Management', 'Agriculture and Food Scientists', 'Fishing and Fisheries', 'Mining and Extraction'}, 'Installation and Maintenance': {'Railroad', 'Repairers', 'HVAC and Refrigeration', 'Highway Maintenance', 'Installation and Repair', 'Septic Tanks and Sewers', 'Installation Managers', 'Building Inspectors', 'Facilities, Maintenance and Cleaning'}, 'Transportation and Distribution': {'Taxi Drivers and Chauffeurs', 'Mail Service', 'Truck Drivers', 'Shipping and Freight', 'Bus Drivers', 'Logistics', 'Post Office', 'Air Traffic Controllers', 'Aircraft', 'Airline', 'Packing and Packaging', 'Delivery', 'Transportation and Distribution'}, 'Business': {'Surveyors', 'Purchasing and Procurement', 'Organizational Development', 'Data Entry and Word Processing'}, 'Management': {'Operations Management', 'Management', 'Senior Executive', 'Office Management', 'Store Management'}, 'Fitness and Recreation': {'Fitness and Personal Training', 'Coaches, Trainers and Scouts', 'Fitness Instructors'}, 'Banking and Financial Services': {'Investment Banking', 'Bookkeepers', 'Controllers and Treasurers', 'Collections', 'Development and Fundraising', 'Securities and Financial Services', 'Bank Managers', 'Loan Officers and Counselors', 'Financial Management'}, 'Sales': {'Outside Sales', 'Telephone Sales', 'Sales Support', 'Sales Management', 'Sales'}, 'Science': {'Economists', 'Mathematics and Statistics', 'Biotechnology', 'Chemists', 'Life and Physical Sciences', 'Forensic Scientists', 'Physicists', 'Operations Research', 'Research', 'Biological Scientists', 'Science Technicians'}, 'Performing Arts': {'Singers and Musicians', 'Illustration', 'Dancers and Choreographers', 'Performing Artists', 'Painters', 'Curators', 'Sound'}, 'Pharmacy': {'Pharmacy Technicians', 'Pharmacy Aides', 'Pharmacology and Pharmaceuticals', 'Alternative and Holistic Medicine'}, 'Computers and Technology': {'Quality Assurance and Testing', 'Database Development and Administration', 'Interactive Design and Development', 'Computer Hardware Engineers', 'Network Analysts and Administration', 'Programmers', 'General IT', 'IT Consulting', 'Software Development', 'IT Management', 'Computer Security', 'Web Development', 'Technical Support', 'Software Project Managers'}, 'Education and Training': {'Preschool', 'Professors and Lecturers', 'Training and Development', 'Education and Vocational Counseling', 'Self Enrichment', 'Elementary School', 'Vocational Training', 'Middle School', 'School Administration', 'Teaching Assistants', 'Continuing Education', 'Kindergarten', 'Teaching and Classroom Support', 'Language Instruction', 'College and University', 'Special Education', 'Adult Literacy', 'Secondary School', 'Principals and Directors', 'Instructors and Trainers'}, 'Administrative Support': {'Payroll Administrators', 'Office Assistants', 'Executive Assistants', 'Administrative Assistants', 'Receptionists', 'Editing', 'Reporters, Announcers and Analysts', 'Underwriters', 'Copywriting and Editing', 'Business Administration', 'Writers, Editors and Proofreaders'}, 'Food and Beverage': {'Fast Food', 'Cafeteria and Food Service', 'Restaurant and Food Service', 'Restaurant Management', 'Bakers', 'Bartenders', 'Food Processing', 'Butchers and Meat Processing', 'Baristas and Cafe Workers', 'Catering', 'Concessions and Refreshments', 'Cooks', 'Diet and Nutrition', 'Bussers', 'Chefs and Sommeliers'}, 'Legal': {'Judges', 'Legal Secretaries', 'Legal Assistants', 'Attorneys and Lawyers', 'Court Reporting'}, 'Veterinary': {'Animal Trainers', 'Veterinary Medicine', 'Animal Care and Service'}, 'Retail': {'Demonstrator', 'Merchandising'}, 'Beauty and Spa': {'Spa Management', 'Barbers', 'Hairstylists and Hairdressers', 'Estheticians', 'Manicurists', 'Massage Therapists', 'Beauty'}, 'Skilled Trades': {'Carpenters', 'Electricians', 'Welders', 'Carpet , Tile and Floor', 'Automotive and Body', 'Plasterers', 'Glaziers', 'Plumbers and Pipefitters'}, 'Travel and Hospitality': {'Tourism', 'Housekeeping', 'Hosts/Hostesses', 'Hotels', 'Waiters and Servers'}, 'Green Jobs': {'Waste and Recycling', 'Horticulture and Gardening', 'Environment and Conservation', 'Gardeners and Groundskeepers', 'Florists', 'Environmental Science', 'Conservation and Environmental Scientists', 'Conservation'}, 'Dental': {'Dental Assistance', 'Dental Hygienists', 'Dentistry', 'Dentists and Oral Surgeons'}, 'Real Estate': {'Property Management', 'Brokers and Agents'}, 'Community and Public Service': {'Public Service', 'Parking', 'Community Service Specialists', 'Social Workers'}, 'Accounting and Finance': {'Tax Preparation', 'Accountants', 'Accounts Payable/Receivable', 'Auditors', 'Financial Analysts', 'Tax Professionals'}, 'Nursing': {'Nursing Aides and Attendants', 'Nurses', 'Licensed Practical and Vocational Nurses', 'Registered Nurses', 'Nurse Practitioner', 'Psychiatric Nurses'}, 'Manufacturing and Production': {'Metal Workers', 'Energy and Utilities', 'Tool and Die Makers', 'Automotive and Motor Vehicles', 'Material Handlers', 'Industrial Machinery', 'Manufacturing and Production', 'Plant and System Operators', 'Traffic and Production Management', 'Testers and Inspectors', 'Plastic Workers', 'Warehouse', 'Operations', 'Technicians', 'Assemblers and Fabricators', 'Machinists', 'Inspectors', 'Production Supervisors and Managers'}, 'Art, Fashion and Design': {'Graphic Art and Design', 'Stage Design', 'Designers', 'Art Directors', 'Crafts People and Artisans', 'Animation and Multimedia Design', 'Digital Arts and Design', 'Fashion', 'Interior Design'}, 'Military': {'Enlisted', 'Dispatch'}, 'Insurance': {'Compensation and Benefits', 'Insurance Agents and Brokers', 'Actuaries'}, 'Childcare': {'Nannies', 'Daycare'}, 'Law Enforcement and Security': {'Security Management', 'Adjusters, Investigators and Collectors', 'Police Officers', 'Detectives and Criminal Investigators', 'Correctional Officers', 'Protective Services', 'Security Guards and Surveillance Officers'}, 'Sports': {'Athletes and Sports Competitors', 'Umpires and Referees'}, 'Architecture': {'Architects', 'Drafters', 'Landscape Architects'}, 'Engineering': {'Industrial Engineers', 'Chemical Engineers', 'Aerospace Engineers', 'Pilots and Flight Engineers', 'Engineering', 'Product Design and Engineering', 'Marine Engineers', 'Engineering Management', 'Civil Engineers', 'Electrical and Electronics', 'Mechanical Engineers', 'Electrical and Electronic Engineers', 'Petroleum Engineers', 'Engineering Technicians', 'Mining Engineers'}, 'Marketing, Advertising and PR': {'Online Marketing and Social Media', 'Market Research', 'Public Relations'}, 'Construction': {'Brick and Cement Masons', 'Drywall', 'Insulation', 'Occupational Health and Safety', 'Construction Equipment Operators', 'Construction Laborers', 'Construction Management', 'Roofers'}, 'Social Sciences': {'Political Science', 'Anthropologists and Archeologists', 'Sociology', 'Historians'}, 'Psychology': {'Mental Health', 'Camp Counselors', 'Counselors', 'Psychology', 'Counseling', 'Psychologists', 'Psychiatry'}, 'Government': {'Officers', 'Government Administration', 'Firefighters', 'Government Examiners and Inspectors', 'Animal Control', 'Urban and Regional Planning', 'Elected Officials and Policy Makers'}, 'Telecommunications and Wireless': {'Television and Radio', 'Telecommunications'}, 'Entertainment and Media': {'Actors', 'Photographers', 'Directors and Producers', 'Film', 'Videographers', 'Gaming', 'Special Effects'}, 'Human Resources': {'Human Resources', 'Talent Agents', 'Recruiting and Employment', 'HR Coordinators', 'HR Generalists', 'Casting and Talent Agents'}, 'Customer Service': {'Customer Service', 'Tellers and Customer Service', 'Customer Service Representative', 'Client Services'}, 'Healthcare': {'Medical and Precision Equipment', 'Medical Scientists and Epidemiologists', 'Healthcare Management', 'Rehabilitation and Substance Abuse', 'Emergency Medical Technicians', 'Medical and Lab Technicians', 'Therapists and Practitioners', 'Physicians and Surgeons', 'Home Health Aides', 'Acupuncture', 'Occupational and Physical Therapy', 'Optometry', 'Medical Records, Billing and Transcription', 'Chiropractors', 'Speech Pathology and Audiology', 'Medical Assistance and Support', 'Medical Technology and Equipment', 'Physician and Medical Assistants', 'Public Health'}, 'Textile and Apparel': {'Tailors and Sewers', 'Pattern Makers and Dress Makers', 'Costume Design', 'Textile Equipment Operators', 'Textile Workers'}, 'Personal Services': {'Laundry and Dry Cleaning', 'Religion and Theology', 'Amusement Parks', 'Cleaning Services', 'Service Station', 'Pest Control', 'Printing', 'Event Planning and Coordination'}, 'Funeral Services': {'Morticians and Embalmers', 'Funeral Directors'}}

SubDictInv={}
for key in SubDict.keys():
    for i in SubDict[key]:
        SubDictInv[i]=key
IndustryList=[key for key in SubDict.keys()]
   

def KeyWordsCount(Des ,KeyWordsCheck=KeyWordsCheck):
    KWcount={}
    for key in KeyWordsCheck.keys():

        Number=0
        for wd in KeyWordsCheck[key]:
            Number=Number+Des.count(wd)
        KWcount[key]=Number
    return(KWcount)

def KeyWordsAdjust(scoreDNN, Max,Min,ratio,proportion ):
    """
    ScoreDNN: score from DNN model
    Max: maxium score that it can reach
    Min: minium score that it can reach
    ratio: ratio for adjustment 
    proportion: geting from log(wordcount)/5
    
    """
    
    score=scoreDNN+proportion*ratio
    score=min(Max, score)
    score=max(Min, score)
    return(score)


def Word_count(s):
    """
    Word code from stings
    """
    if type(s)!=float:
        return(len(re.findall("[a-zA-Z_]+", s)))
    else:
        return(0)


def dedup_score(industry_tuples):
    industry_dict = {}
    dedup_list = ['Dental', 'Childcare', 'Green Jobs', 'Telecommunications and Wireless']
    if float(industry_tuples[0][1])==float(industry_tuples[1][1]):
        if industry_tuples[0][0] in dedup_list:
            industry_tuples[0] = (industry_tuples[0][0], industry_tuples[0][1]-0.05)
        elif industry_tuples[1][0] in dedup_list:
            industry_tuples[1] = (industry_tuples[1][0], industry_tuples[1][1]-0.05)
        return(industry_tuples)
    else:
        return(industry_tuples)  

def combine_industry(industry_dict):
    """
    combine industry in the subindustry
    """
    res3 = []
    for key in SubDict.keys():
        Checklist=SubDict[key]
        Value=0
        for i in Checklist:
            IndCount=industry_dict.get(i) 
            if IndCount==None:
                IndCount=0
            Value=Value+float(IndCount)
        res3.append( (key, Value )  )
    res3.sort(key=lambda tup: tup[1],reverse=True)    
    return(res3)

def multiprocessing(func, args, workers):
    if workers == 1:
        result = [func(item) for item in args]
    else:
        pool = Pool(processes=workers)
        result = pool.map(func,args)
        pool.close()
        pool.join()
    return result

def _create_float(v):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[v]))

def _create_str(v):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[bytes(v, 'utf-8')]))

def clean_job_description(tuple_i):
    """
    Clean text
    """
    description, company = tuple_i
    text = Preprocess.CleanSentence(description,company).CleanDescription()
    return(text)
from time import time

def CreateFeatures_batch_mult(inputData,description, title,company, data_type, number_of_processor):
    types = ['data_frame', 'dictionary', 'json']
    # TF input transfer
    batch_features, texts = [], []
    try:
    #Cleaning the JD with multiprocess
        # TF type transfer
        st = time() 
        if data_type == 'data_frame':
            text_tuple = [(t,c) for t,c in zip(inputData[description], inputData[company])]
            titles = inputData[title].apply(_create_str)
            #dlens = inputData[length].apply(_create_float)
        else:
            text_tuple = [(t[description],c[company]) for t,c in zip(inputData, inputData)]
            titles = [_create_str(ti[title]) for ti in  inputData] 
            #dlens  = map(_create_float, inputData[length])
        print("time check", time() - st)

        texts = multiprocessing(clean_job_description, text_tuple ,number_of_processor)
        lengths = multiprocessing(Word_count, texts ,number_of_processor)
        dlens = [_create_float(leni) for leni in lengths]
        texts = [_create_str(item) for item in texts]

        for desc, length, title in zip(texts, dlens, titles):
            feat = {'clean_text':desc,'len':length, 'Title':title}
            example = tf.train.Example(features=tf.train.Features(feature=feat))
            inputs = example.SerializeToString()
            batch_features.append(inputs)

    except BaseException as e:
        traceback.print_exc()
        print(e)
        print("Batch data must be in the following formats :{}".format(types)) 

    return batch_features, texts


def Find3Fit_batch(batch_features, texts, predict_fn, n, ratio=3):
    preds = predict_fn({"inputs":batch_features})
    
    softmax_score = np.around(preds['scores'],3).tolist()
    classes_list = preds['classes'].tolist()
    
    out_classes = []
    for score , class_names, description in zip(softmax_score, classes_list,texts):

        final = dict(zip(score,class_names))
        Max = 0.9
        Min = 0.1
        kwc = KeyWordsCount(str(description))

        res = sorted(final.items(), key = lambda x:x[0],reverse=True)
        res = [(float(a[0]),IndustryDicRev[int(a[1])]) for a in res ]

        res2 = []
        # check each industry
        for i in IndustryDic.keys():
            # check if score exist for all industry?
            if i in [a[1] for a in res]:   
                j = res[ [a[1] for a in res].index(i) ]
            else:
                j = (0, i)

            if kwc.get(i) != None:

                if kwc.get(i) > 0:
                    # adjust score if there are keywords invovled 
                    proportion = np.log(kwc.get(i))/5
                    score = KeyWordsAdjust(j[0], Max, Min, ratio, proportion)
                    res2.append((j[1], score))

                else:
                    # add score for other cases
                    res2.append((j[1], j[0]))
            else:
                # add score for other cases
                res2.append((j[1], j[0]))      

        res2 = sorted(res2, key = lambda x:x[1], reverse=True)
        #Adjust duplicated scores
        res2 = dedup_score(res2)

        # adjust the score to sum up to 1
        OverallWeights = sum([r[1] for r in res2])
        res2 = [(r[0],r[1]/OverallWeights) for r in res2  ]
        res2 = dict(res2)
        # combine industries that are rarely apeared
        res3 = combine_industry(res2)
        ## only keep the first 3 elements 
        out_classes.append(res3[:n])

    return out_classes

def Find3Fit(text, title, dlen, predict_fn, ratio=3):
    #n = 1
    #ind = resume_trainable.loc[n].industry
    Description = str(text)
    text = _create_str(text)
    
    dlen = _create_float(dlen)
    title = _create_str(title)
    feat = {'clean_text':text,"Title":title ,'len':dlen}

    example = tf.train.Example(features=tf.train.Features(feature=feat))
    inputs = example.SerializeToString()

    preds = predict_fn({"inputs":[inputs]})
    g = np.around(preds['scores'],3).tolist()
    f = preds['classes'].tolist()

    final = dict(zip(g[0],f[0]))

    res = sorted(final.items(), key = lambda x:x[0],reverse=True)
    
    # refine the score by keywords
    Max = 0.9
    Min = 0.1
    kwc = KeyWordsCount(Description)

    res = [(float(a[0]),IndustryDicRev[int(a[1])]) for a in res ]
    
    res2 = []
    # check each industry
    for i in IndustryDic.keys():
        # check if score exist for all industry?
        if i in [a[1] for a in res]:   
            j = res[ [a[1] for a in res].index(i) ]
        else:
            j = (0, i)

        if kwc.get(i) != None:

            if kwc.get(i) > 0:
                # adjust score if there are keywords invovled 
                proportion = np.log(kwc.get(i))/5
                score = KeyWordsAdjust(j[0], Max, Min, ratio, proportion )
                res2.append((j[1], score))

            else:
                # add score for other cases
                res2.append((j[1], j[0]))
        else:
            # add score for other cases
            res2.append((j[1], j[0]))      

    res2 = sorted(res2, key=lambda x:x[1], reverse=True)
    
    #Adjust duplicated scores
    res2 = dedup_score(res2)

    # adjust the score to sum up to 1
    OverallWeights = sum([r[1] for r in res2])
    res2 = [(r[0],r[1]/OverallWeights) for r in res2  ]
    res2 = dict(res2)

    # combine industries that are rarely apeared
    res3 = combine_industry(res2)
    return(res3)



class IndustryPrediction:

    def __init__(self):

        self.predict_fn = None
        #self.length = length

    def data_process_batch_mult(self, inputData, description, title, company, data_type, number_of_processor = 4):
        """
        # For batch process
        # it has 3 options 'data_frame', 'dictionary', 'json'
        # pf_df can be a pandas dataframe with column name  "Description", "Title", "Company"
        # or pd_df can also have option as a dictionary or json object with above keys
        # there value should be a list of records for each key
        # The number of processors are number of processor for multiple process
        # For batch process, we should process cleaning data before load model, beacuse the size of the model is very large
        """
        batch_features, feature_text = CreateFeatures_batch_mult(inputData, description, title,
                                                                 company, data_type, number_of_processor)
        self.batch_features = batch_features
        self.feature_text = feature_text

    def load_model(self, base_path):
        model_path = os.path.join(base_path, 'SavedModel0/1548959632')
        if os.path.exists(model_path):
            self.predict_fn = tf.contrib.predictor.from_saved_model(model_path)
        else:
            print("Path - {} does not exists.".format(model_path))
            
    #def predict_batch(self, inputData, description,title,length,company, data_type):
        #batch_features, feature_text = CreateFeatures_batch(inputData,description,title,length,company,data_type)
    def predict_batch(self, n=3):
        return Find3Fit_batch(self.batch_features, self.feature_text, self.predict_fn, n)
            

    def predict_industry(self, Description, Company, title, n=3):
        """
        Clean List
        Drop Stop sentence 
        Drop stopwords lem and stem with Sentence_to_Wordlist
        """
        #cleaning text
        text = Preprocess.CleanSentence(Description,Company).CleanDescription()
        length = Word_count(text)
        #prediction
        Matches = Find3Fit(text,title,length,self.predict_fn)
        n=min(n,len(Matches))

        return(Matches[:n])


## test


if __name__ == '__main__':
    Description="<p>The primary role of the Dental Assistant is to perform a variety of patient care, office, and laboratory duties. Dental assistants prepare patients for oral examination and assist other dental professionals in providing treatment to the teeth, mouth, and gums. Exemplifying the practices 3 core values is key to the Dental Assistant becoming a great team player</p><p>1. Putting Patient & Doctors experience first</p><p>2. Demonstrating ownership for results</p><p>3. Fostering compassion, optimism, & celebration</p><p>4. Practicing fanatical attention to consistency and detail</p><p>5. Being committed to continuous improvement</p><p>6. Contributing to it being a great place to work</p><p>ESSENTIAL FUNCTIONS: Include the following. Other duties may be assigned.</p><p>Customer Experience Leadership</p><p>1. Actively participate in our Peak Patient Experience by striving to keep our patients focused on optimal treatment meanwhile making patients know we care about them as individuals.</p><p>2. Take an active role in getting to know both new and regular patients we strive for our patients to know we just dont care about their oral hygiene, we care about them as individuals</p><p>3. Promotes dentistry to the patient by being in communication with the patient and promoting the treatment plan recommended by the doctor</p><p>4. Explain as necessary dental treatment or procedures to the patient when questions are asked</p><p>5. Seat patients at the beginning of an appointment and route patents considering extensive treatment to the Team Leader for appropriate financial arrangements prior to beginning treatment</p><p>6. Responsible for maximizing the production scheduled for the day by keeping a watch on the treatment plan to see that all possible work is completed that day</p><p>7. Responsible to keep the operatories on times for that the patients are seen on time. If running behind, the DA/EDDA will notify the Team Leader who will in turn notify the next patient</p><p>8. Instruct and educate patients about clinical procedures and give post-operative instructions</p><p>9. Clean each operatory in accordance with the American Dental Association guidelines, as the patient has exited the room. Keep in mind the 'view from the chair'</p><p><strong>Requirements</strong></p><p>QUALIFICATIONS:</p><p>To perform this job successfully, an individual must be able to perform each essential duty satisfactorily with or without accommodation. The requirements listed below are representative of the knowledge, skill, and/or ability required. Reasonable accommodations may be made to enable individuals with disabilities to perform the essential job functions.</p><p>KNOWLEDGE/SKILLS/ABILITIES:</p><p>Ability to read, analyze, and interpret documents such as business periodicals, professionals journals, technical procedure manuals, safety rules, operating and maintenance instructions, and governmental regulations.</p><p>Ability to communicate effectively and present information, both verbally and in writing, to patients and co-workers.</p><p>Ability to interpret a variety of instructions furnished in written, verbal, or diagram form.</p><p>Ability to add, subtract, multiply, and divide in all units of measure, using whole numbers, decimals and percentages.</p><p>Ability to compute rate, ratio, and percentages.</p><p>SUPERVISORY RESPONSIBILITY None.</p><p>WORK ENVIRONMENT While performing the duties of this job, the employee is regularly exposed to wet or humid conditions (non-weather); work near moving mechanical parts and fumes or airborne particles. The employee is frequently exposed to toxic or caustic chemicals and risk of radiation. The employee is occasionally exposed to risk of electrical shock and vibration.</p><p>The noise level in the work environment is usually moderate.</p><p>PHYSICAL DEMANDS The physical demands described here are representative of those that must be met by an employee to successfully perform the essential functions of this job. Reasonable accommodations may be made to enable individuals with disabilities to perform the essential functions.</p><p>While performing the duties of this job, the employee is regularly required to use hands to finger, handle, or feel; reach with hands and arms; talk, hear, and smell. The employee is frequently</p><p>Job Description</p><p>Rev. 1/26/2016</p><p>required to stand, walk, and sit. The employee is occasionally required to stoop, kneel, or crouch. The employee must frequently lift and/or move up to 10 pounds. Specific vision abilities required by this job include close vision, peripheral vision, and depth perception.</p><p>EDUCATION AND EXPERIENCE Equivalent to high school diploma or general education degree (GED), and specified training courses as mandated by state for certification, licensure, or registration</p>"
    Company='Peak Dental Services'
    Title='Dental Assistant'

    obj = IndustryPrediction()
    obj.load_model("model_path")
    obj.predict_industry(Description, Company, Title)
