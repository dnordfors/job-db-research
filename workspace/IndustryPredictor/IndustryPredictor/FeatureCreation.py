#################################################################
## This document is design to create features based on SentenceList and Drop list
#################################################################

###################
# Read in data
###################
import pandas as pd
import numpy as np

# Import statements required for Plotly  
#pd.set_option('display.max_columns', 40)

from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import LancasterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
stops = set(stopwords.words("english"))

import html2text
import nltk
from commonregex import CommonRegex
import pyap
import re

#import usaddress

ps = PorterStemmer()

from bs4 import BeautifulSoup

def lemmatize_Stem_words(words):
        """
        Lemmatize verbs in list of tokenized words
        Stem it after that 
        """
        lemmatizer = WordNetLemmatizer()
        lemmas = []
        for word in words:
            lemma = lemmatizer.lemmatize(word, pos='v')
            #Use
            #lemma= ps.stem(lemma)
            lemmas.append(lemma)
        return lemmas

def Sentence_to_Wordlist(review, remove_stopwords=True ):
        """
            Function to convert a sentence to a sequence of words,
            optionally removing stop words.  Returns a new doc that got cleaned.
            
        """

        # 1. Remove HTML, which is already done in sentence split 
        #print(len(review))
        #review_text = BeautifulSoup(review, "html5lib").get_text()
        #print(len(review_text))
        # 2. Remove non-letters
        review_text = re.sub("[^a-zA-Z]"," ", review)
        # 3. Convert words to lower case and split them
        # 4. Optionally remove stop words (false by default)
        if remove_stopwords:
            words = review_text.lower().split()
            words = [w for w in words if not w in stops]
            words=lemmatize_Stem_words(words)
        else:
            words = review_text.split()# not convert to lower
        
        #words=lemmatize_Stem_words(words)
        # 5. Return a list of words
        return(" ".join(words))

def Word_count(s):
    """
    Word code from stings
    """
    if type(s)!=float:
        return(len(re.findall("[a-zA-Z_]+", s)))
    else:
        return(0)

def Word_count3(s):
    """
    Word count from a list of strings
    """
    s=" ".join(s)
    if type(s)!=float:
        return(len(re.findall("[a-zA-Z_]+", s)))
    else:
        return(0)

def numbers_count(s):
    """
    Number count from a list of strings
    """
    Number=0
    for si in s:
        #Number count
        #print(si)
        #if type(si)!=float:
        Number= Number + len(re.findall("[0-9_]+", si))
    return(Number)
    
def Average_Length_Sentence(s):
    return(np.mean([len(si.split(" ")) for si in s]))




class CleanSentence:

    def __init__(self, SentenceList,DropList):
        self.SentenceList = SentenceList
        self.DropList = DropList


    def CleanSentenceList(self):
        """
        Clean List
        Drop Stop sentence 
        Drop stopwords lem and stem with Sentence_to_Wordlist
        """
        SentenceList=self.SentenceList
        DropList=self.DropList
        Listi=SentenceList[:]
        if DropList is None:
            Des=" ".join(Listi) 
        else:
            # Drop stop sentence
            try:
                [Listi.remove(xi) for xi in DropList]
            except:
                [print(x) for x in DropList]
        Des=" ".join(Listi)
        #Clean the remaining Description
        CleanList2 = Sentence_to_Wordlist(Des)
        return(CleanList2)

    def FilterWrongSentence2(self):
        """
        Remove drop sentence
        """
        SentenceList=self.SentenceList
        DropList=self.DropList

        if DropList is None:
            return(SentenceList)
        else:
            return([s for s in SentenceList if s not in DropList])





