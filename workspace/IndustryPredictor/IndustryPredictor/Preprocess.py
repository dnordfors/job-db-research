## Preprocess data 
## 1. extract sentence 
## 2. remove stop sentence
## 3. remove stop words, lemmatize, and lower case

from . import Job_Parsing
from . import FeatureCreation

import html2text
import nltk
from nltk.stem import LancasterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
stops = set(stopwords.words("english"))
import re 
from nltk.stem import LancasterStemmer, WordNetLemmatizer
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
stops = set(stopwords.words("english"))
from multiprocessing import Pool

def cleanText(HtmlMessage):
    
    if bool(BeautifulSoup(HtmlMessage, "html.parser").find()):

        h = html2text.HTML2Text()
        h.ignore_images = True
        h.ignore_links = True
        HtmlMessage2 = h.handle(HtmlMessage)
        if len(HtmlMessage2)<2:
            HtmlMessage2=BeautifulSoup(HtmlMessage, "html5lib").get_text()

        return(Sentence_to_Wordlist(HtmlMessage2))
    else:
        return(Sentence_to_Wordlist(HtmlMessage))

    
def Sentence_to_Wordlist(review, remove_stopwords=True ):
        """
            Function to convert a sentence to a sequence of words,
            optionally removing stop words.  Returns a new doc that got cleaned.
            
        """

        # 1. Remove HTML, which is already done in sentence split 
        #print(len(review))
        #review_text = BeautifulSoup(review, "html5lib").get_text()
        #print(len(review_text))
        # 2. Remove non-letters
        review_text = re.sub("[^a-zA-Z]"," ", review)
        # 3. Convert words to lower case and split them
        words = review_text.lower().split()
        # 4. Optionally remove stop words (false by default)
        if remove_stopwords:
            words = [w for w in words if not w in stops]
        words=lemmatize_Stem_words(words)
        # 5. Return a list of words
        return(" ".join(words))

def Word_count(s):
    """
    Word code from stings
    """
    if type(s)!=float:
        return(len(re.findall("[a-zA-Z_]+", s)))
    else:
        return(0)
    
def lemmatize_Stem_words(words):
        """
        Lemmatize verbs in list of tokenized words
        Stem it after that 
        """
        lemmatizer = WordNetLemmatizer()
        lemmas = []
        for word in words:
            lemma = lemmatizer.lemmatize(word, pos='v')
            #Use
            #lemma= ps.stem(lemma)
            lemmas.append(lemma)
        return lemmas

def multiprocessing(func, args, workers):
    pool = Pool(processes=workers)
    result = pool.map(func,args)
    pool.close()
    pool.join()
    return result

def ExtractSentence(Tuple):
    Description=Tuple[0]
    Company=Tuple[1]
    SentenceList=Job_Parsing.Job_data_clean(Description, Company).SentenceSplit_AdjustHTML()
    #DropSL=Job_Parsing.DropSentence(SentenceList).Checking_words(AddressExtract=True)
    # Sentence after drop stop sentence
   # CleanDescription2=FeatureCreation.CleanSentence(SentenceList,DropSL).CleanSentenceList()
    return(SentenceList)

class CleanSentence:

    def __init__(self, Description,Company):
        self.Description = Description
        self.Company = Company


    def CleanDescription(self):
        """
        Clean List
        Drop Stop sentence 
        Drop stopwords lem and stem with Sentence_to_Wordlist
        """
        
        Description=self.Description
        Company=self.Company

        SentenceList=Job_Parsing.Job_data_clean(Description, Company).SentenceSplit_AdjustHTML()
        DropSL=Job_Parsing.DropSentence(SentenceList).Checking_words(AddressExtract=True)
        # Sentence after drop stop sentence
        CleanDescription2=FeatureCreation.CleanSentence(SentenceList,DropSL).CleanSentenceList()

        return(CleanDescription2)







