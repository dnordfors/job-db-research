#%%
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
application = app.server
app.title = 'JobDeX'

app.layout = html.Div(
    # Section 1
    html.Div([ 
        # H1 header with description
        html.Div([
            html.H1(children='JobDeXX - Job Description Xplorer',
                    className = "nine columns",
                    style={
                    'margin-top': 20,
                    'margin-right': 20
                    },
            ),            
            dcc.Markdown(children='''
                        lorem ipsum
                        1. bullet
                        2. another bullet
                        ''',
                    className = 'nine columns')
        ], className = "row"),

        # H2 header 
        html.Div([
            html.H2(children='ARCHETYPES:VARIABLES',
                    className = "nine columns",
                    style={
                    'margin-top': 20,
                    'margin-right': 20
                    },
            )
        ], className = "row"),

        # Progress
        html.Div(
            [
                dbc.Progress(value=80, id="animated-progress", striped=True),
                dbc.Button(
                    "Toggle animation", id="animation-toggle", className="mt-3"
                ),
            ]
        ),

        # Dropdown for
        html.Div(
                    [
                        html.Label('Variables', style={'font-weight' : 'bold'}),
                        dcc.Dropdown(
                            id = 'Var',
                            options=[
                                {'label': 'Keywords'  ,'value': 'keywords'},
                                {'label': 'Entities'  ,'value': 'entities'},
                                {'label': 'Concepts'  ,'value': 'concepts'},
                                {'label': 'Categories','value': 'categories'},
                            ],
                            value = 'keywords',
                        ) 
                    ],
                    className = 'two columns',
                    style={'margin-top': '30'}
                ),

        # Dropdown for
        html.Div(
            [
                html.Div(
                    [
                        html.Label('#Archetypes', style={'font-weight' : 'bold'}),
                        dcc.Dropdown(
                            id = 'NoA',
                            options=[{'label':k,'value':k} for k in range(2,100)],
                            value = 6,
                            multi = False
                        ) 
                    ],
                    className = 'one columns offset-by-one',
                    style={'margin-top': '30'}
                ),

                html.Div(
                    [
                        html.Label('Cut at', style={'font-weight' : 'bold'}),
                        dcc.Dropdown(
                            id = 'Threshold',
                            options=[{'label':str(k)+'%','value':k/100} for k in range(1,99)],
                            value = 0.1,
                            multi = False
                        ) 
                    ],
                    className = 'one columns offset-by-one',
                    style={'margin-top': '30'}
                ),
            ], className="row"
        ),

        # Graph
        html.Div([
            html.Div([
                dcc.Graph(
                    id='variables-heatmap'
                )
            ])
        ]),
        # html.Div([
        #     html.H3('DICTATIONS MAPPED ONTO ARCHETYPES'),
        #     dcc.Graph(id='dictations')
        # ])
     ])
)

@app.callback(
    Output("animated-progress", "animated"),
    [Input("animation-toggle", "n_clicks")],
    [State("animated-progress", "animated")],
)
def toggle_animation(n, animated):
    if n:
        return not animated
    return animated

# @app.callback(
#     dash.dependencies.Output('variables-heatmap', 'figure'),
#     [dash.dependencies.Input('Var', 'value'),
#      dash.dependencies.Input('NoA', 'value'),
#      dash.dependencies.Input('Threshold', 'value')]
# )

# def arch_heatmap_variables(typ, n_archs, threshold):
#     variables = (typ,n_archs,threshold)

#     def f(i):
#         return display_archetype(arch_nr=i,typ=typ,n_archs=n_archs,threshold=threshold).sort_values(by=i) #Sort by archetype i

#     maxrows = int(1+ n_archs//3)
#     cols = 3
#     fig = make_subplots(rows=maxrows, cols=cols, horizontal_spacing=0.2)  
#     for i in range(n_archs):
#         fig.add_trace( go.Heatmap(  z = f(i),
#                                     y = f(i).index,
#                                     x = f(i).columns,
#                                     xgap = 1,
#                                     ygap = 1,
#                         ), col = i%cols +1,row = int(i//cols)+1
#             )
#     fig.update_layout(height=400*maxrows, width=1200, title_text="Subplots")
#     return fig



# @app.callback(
#     dash.dependencies.Output('dictations', 'figure'),
#     [dash.dependencies.Input('Var', 'value'),
#      dash.dependencies.Input('NoA', 'value'),
#      dash.dependencies.Input('Threshold', 'value')]
# )

# def arch_heatmap_dictations(typ, n_archs, threshold):
#     variables = (typ,n_archs,threshold)
#     f = archetypes(typ,n_archs).o
#     fig = go.Heatmap(   z = f
#             )
#     return fig
