import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    requirements = fh.read()

setuptools.setup(
    name = "Job_Description_Segment",
    version = "0.0.2",
    author = "Kai Jin",
    author_email = "kai.jin@bold.com",
    description = "A package that is desgeign to split job description into sections",
    long_description = long_description,
    #long_description_content_type = "text/markdown", # remove for pip3.4
    install_requires = requirements.split('\n'),
    include_package_data = True,
    classifiers = [
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages = setuptools.find_packages(),

)