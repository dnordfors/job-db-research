# Name #

## Job_Services ##

# Side Projects #
# Job Sentence Segmentation #

This application is used as segment job descriptions into sentences and classify those sentences.

It could be used to extract relevant information from job description such as Certifications, Educations. It can also be used to improve semantic match by only use relevant information. 

There are 5 classes:
- CompanyBenefitsList : It contains company, department, benefits, contact and other information that are less important in terms of the job context. 
- QualificationList : It contains job qualifications, requirements, skills, and experiences. 
- PositionList : It contains job duties, responibilities, and position descriptions.
- EducationList : It contains education requirments. 
- CertificationList : It contains certification requirments. 

# Prerequisites #

- Python 3.6
- Pip (latest verion)   
- requirements.txt            

# Installation #

- pip3.6 install . 
- or pip3.7 install .

# data #
- SavedModel1 # Not in the bitbucket(due to it's size) ask Kai for the model file. 
              # Or download through this following google drive link  
              # https://drive.google.com/file/d/1WRzGSRWJiDITaA3h2dd_wQ1tnlBja25R/view?usp=sharing
- data/headers_dict.json #key is headers in Job Description in lower case, value is the class that they belongs to
- data/key_words_dict.json #few list of key words that was used to modify predicted classes after Tensorflow Dnnclassifer 


## Examples ##
### Load the data ###
```python
import sentence_classifier

```

```python
Description="<p>The primary role of the Dental Assistant is to perform a variety of patient care, office, and laboratory duties. Dental assistants prepare patients for oral examination and assist other dental professionals in providing treatment to the teeth, mouth, and gums. Exemplifying the practices 3 core values is key to the Dental Assistant becoming a great team player</p><p>1. Putting Patient & Doctors experience first</p><p>2. Demonstrating ownership for results</p><p>3. Fostering compassion, optimism, & celebration</p><p>4. Practicing fanatical attention to consistency and detail</p><p>5. Being committed to continuous improvement</p><p>6. Contributing to it being a great place to work</p><p>ESSENTIAL FUNCTIONS: Include the following. Other duties may be assigned.</p><p>Customer Experience Leadership</p><p>1. Actively participate in our Peak Patient Experience by striving to keep our patients focused on optimal treatment meanwhile making patients know we care about them as individuals.</p><p>2. Take an active role in getting to know both new and regular patients we strive for our patients to know we just dont care about their oral hygiene, we care about them as individuals</p><p>3. Promotes dentistry to the patient by being in communication with the patient and promoting the treatment plan recommended by the doctor</p><p>4. Explain as necessary dental treatment or procedures to the patient when questions are asked</p><p>5. Seat patients at the beginning of an appointment and route patents considering extensive treatment to the Team Leader for appropriate financial arrangements prior to beginning treatment</p><p>6. Responsible for maximizing the production scheduled for the day by keeping a watch on the treatment plan to see that all possible work is completed that day</p><p>7. Responsible to keep the operatories on times for that the patients are seen on time. If running behind, the DA/EDDA will notify the Team Leader who will in turn notify the next patient</p><p>8. Instruct and educate patients about clinical procedures and give post-operative instructions</p><p>9. Clean each operatory in accordance with the American Dental Association guidelines, as the patient has exited the room. Keep in mind the 'view from the chair'</p><p><strong>Requirements</strong></p><p>QUALIFICATIONS:</p><p>To perform this job successfully, an individual must be able to perform each essential duty satisfactorily with or without accommodation. The requirements listed below are representative of the knowledge, skill, and/or ability required. Reasonable accommodations may be made to enable individuals with disabilities to perform the essential job functions.</p><p>KNOWLEDGE/SKILLS/ABILITIES:</p><p>Ability to read, analyze, and interpret documents such as business periodicals, professionals journals, technical procedure manuals, safety rules, operating and maintenance instructions, and governmental regulations.</p><p>Ability to communicate effectively and present information, both verbally and in writing, to patients and co-workers.</p><p>Ability to interpret a variety of instructions furnished in written, verbal, or diagram form.</p><p>Ability to add, subtract, multiply, and divide in all units of measure, using whole numbers, decimals and percentages.</p><p>Ability to compute rate, ratio, and percentages.</p><p>SUPERVISORY RESPONSIBILITY None.</p><p>WORK ENVIRONMENT While performing the duties of this job, the employee is regularly exposed to wet or humid conditions (non-weather); work near moving mechanical parts and fumes or airborne particles. The employee is frequently exposed to toxic or caustic chemicals and risk of radiation. The employee is occasionally exposed to risk of electrical shock and vibration.</p><p>The noise level in the work environment is usually moderate.</p><p>PHYSICAL DEMANDS The physical demands described here are representative of those that must be met by an employee to successfully perform the essential functions of this job. Reasonable accommodations may be made to enable individuals with disabilities to perform the essential functions.</p><p>While performing the duties of this job, the employee is regularly required to use hands to finger, handle, or feel; reach with hands and arms; talk, hear, and smell. The employee is frequently</p><p>Job Description</p><p>Rev. 1/26/2016</p><p>required to stand, walk, and sit. The employee is occasionally required to stoop, kneel, or crouch. The employee must frequently lift and/or move up to 10 pounds. Specific vision abilities required by this job include close vision, peripheral vision, and depth perception.</p><p>EDUCATION AND EXPERIENCE Equivalent to high school diploma or general education degree (GED), and specified training courses as mandated by state for certification, licensure, or registration</p>"
Company='Peak Dental Services'
```

```python
json_path = "/Users/kaijin/job_services/Job_Description_Segment/data/" # where 2 json files are stored 
base_path = "/Users/kaijin/job_services/Job_Description_Segment/data/" # where the SavedModel1/1550860709 stored

SentenceClassifier = sentence_classifier.main_classifer.SentenceClassifier()
SentenceClassifier.load_model(base_path, json_path)
```

```python
result = SentenceClassifier.predict_Sentence(description, company)
# It is a dictionary contrain two keys: SentenceList and SentenceTag
## For certification, If the class is in CompanyBenefitsList and probablity of CertificationList is smaller than 0.01, then we are confident that this sentence has a very small chance that include a certification. 
print(result["SentenceList"][0])
result["SentenceTag"][0]
```
    The primary role of the Dental Assistant is to perform a variety of patient care, office, and laboratory duties.

    [(0.8479508327845399, 'PositionList'),
    (0.14119066406671532, 'CompanyBenefitsList'),
    (0.010856621721370979, 'QualificationList'),
    (1.881427373898507e-06, 'CertificationList')]

### better visualize the results ###

```python
ColorPrint = sentence_classifier.common.color_print.Color_Print_Sentence()
ColorPrint.print_sentence(result, detail_mode=False)
```
### test ###

```python
# inside test there is a .py file called _human_test_.py
# it is not include in the package, you need to find the file and load the .py file
import _human_test_
fname = "unlabeled_job_{}.json".format(id_)

# it will create a json file that you can label them 
_human_test_.Test_Sentence_Tag().write_into_unlabeled(result, fname)

#'QualificationList': {'* Qualifications','**Requirements**','ACCOUNTABILITIES',"EXPERIENCE AND REQUIRED SKILLS"}
#'PositionList': {'**Job Description:**','**RESPONSIBILITIES**', 'ABOUT THE JOB','ABOUT THIS POSITION',"Duties",'More #specifically, you will:',}
#'CompanyBenefitsList': {'* Overview','**Benefits**','ABOUT US:','About Our Company','About This Location'}
# Note all irrelevant content for example email, webpage, Job ids ,location, departments, contact info, 
# and all random irrevelant contents should all be classified in CompanyBenefitsList 
# A block of irrelevant signs and numbers should be in 'NoInformation'. 
#'CertificationList': {"LICENSE",'CERTIFICATE','REGISTRATIONS',}
#'EducationList': {'Degrees Equivalent Experience/Education','Desired Education:', 'EDUCATION','EDUCATION & EXPERIENCE',}
 
```
```python
valid_input = {
                1 : 'PositionList',
                2 : 'CompanyBenefitsList',
                3 : 'QualificationList',
                4 : 'CertificationList',
                5 : 'EducationList',
                0 : 'NoInformation'
            }
```

```python
match_check = _human_test_.Test_Sentence_Tag().read_labeled_data(result, fname, stat_mode = True)
```
***********************************
Number of class as first human label
Counter({'PositionList': 24, 'QualificationList': 17, 'NoInformation': 16, 'CompanyBenefitsList': 11, 'EducationList': 1})
***********************************
Total number of valid tags  53
Total number of invalid tags  16
Matches in the first class:  35
Matches in the second class:  5
Percentage of Matches in the first class:  0.660377358490566
Percentage of Matches in the second class:  0.09433962264150944
MSE:  0.3745577737851861

* For each sentence

{'The primary role of the Dental Assistant is to perform a variety of patient care, office, and laboratory duties.': {'Labled tag:': ['PositionList'],
    'Predicted tag:': [(0.6760570426169848, 'PositionList'),
    (0.15837921911956357, 'CompanyBenefitsList'),
    (0.007702968626963955, 'QualificationList'),
    (2.693127139539894e-06, 'CertificationList')]}


# Who do I talk to? #

* Repo owner or admin