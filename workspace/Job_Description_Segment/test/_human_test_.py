import numpy as np 
import ast
import json
from collections import Counter

class Test_Sentence_Tag:

    def __init__(self):
        self.valid_input = {
                1 : 'PositionList',
                2 : 'CompanyBenefitsList',
                3 : 'QualificationList',
                4 : 'CertificationList',
                5 : 'EducationList',
                0 : 'NoInformation'
            }
        pass

    def write_into_unlabeled(self, result, fname):
        sl = result["SentenceList"]
        dictionary = {}
        for si in sl:
            dictionary[si] = []

        with open(fname, 'w') as fn:
            json.dump(dictionary, fn, indent=4)

    def read_labeled_data(self, result, fname, stat_mode = True):
        #sl = result["SentenceList"]
        with open(fname, 'r') as f:
            dictionary = json.load(f)
        
        sl = result["SentenceList"]
        tags = result["SentenceTag"]
        
        Sentence_tag_check = {}
        
        total_valid_tags = 0
        total_invalid_tags = 0
        total_corrected_predicted_first_tag = 0
        total_corrected_predicted_second_tag = 0
        
        score_diff = []
        
        unique_first_label = []
        for si, ti in zip(sl,tags):
            classi = dictionary.get(si)
            if classi == None:
                print("The following sentence is missing.")
                print(si)
            
            classi = [self.valid_input.get(ci) for ci in classi ]
            if any([ci == None for ci in classi]):
                print("The following input is not found")
                print(dictionary.get(si))
            
            if len(classi) == 0:
                continue
            
            unique_first_label.append(classi[0])
            
            Sentence_tag_check[si] = {"Labled tag:": classi,
                                    "Predicted tag:": ti}  
            
            predicted_first_tag = ti[0][1]
            predicted_second_tag = ti[1][1] 
            
            if classi != ['NoInformation']:
                total_valid_tags = total_valid_tags + 1
            else:
                total_invalid_tags = total_invalid_tags + 1
                continue
            
            if predicted_first_tag in classi:
                total_corrected_predicted_first_tag = total_corrected_predicted_first_tag + 1
            elif predicted_second_tag in classi:
                    total_corrected_predicted_second_tag = total_corrected_predicted_second_tag + 1

            predicted_socre_dict = {}
            for psi in ti:
                predicted_socre_dict[psi[1]] = psi[0]
            
            sdiff_i = 1 - max([predicted_socre_dict.get(ci) for ci in classi if ci !="NoInformation"])
            score_diff.append(sdiff_i)
        
        if stat_mode:
            print("*******"*5)
            print("Number of class as first human label")
            print(Counter(unique_first_label))
            print("*******"*5)
            print("Total number of valid tags ", total_valid_tags)
            print("Total number of invalid tags ", total_invalid_tags)
            print("Matches in the first class: ", total_corrected_predicted_first_tag)
            print("Matches in the second class: ", total_corrected_predicted_second_tag)
            print("Percentage of Matches in the first class: ", total_corrected_predicted_first_tag/total_valid_tags)
            print("Percentage of Matches in the second class: ", total_corrected_predicted_second_tag/total_valid_tags)
            print("MSE: ", sum([(sdi)**2 for sdi in score_diff])/len(score_diff) )

        return(Sentence_tag_check)
        