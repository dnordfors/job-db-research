import re
from sentence_classifier.common.nlp_basic import Text_Clean

class HeaderCheck:

    def __init__(self):
        pass

    def identify_header(self, header, header_dict):
        if len(header.split())>5:
            return(False)
        else:
            header = Text_Clean().lower_case_letters(header)
            if header_dict.get(header) == None:
                return(False)
            else:
                return(True)
