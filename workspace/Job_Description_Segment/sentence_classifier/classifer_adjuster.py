from sentence_classifier.jobs_modifier.sentence_clean import SentenceClean
from sentence_classifier.classifer_keywords_adjuster import Key_Word_Adjuster


import re
import numpy as np


def predicted_class_list_adjust_vector(intial_prediction, adjust_vector,
                                      cumulated_count, classdic, ratio=0.55):
    
    """
    Based on adjust vector from transition matrix
    Adjust the predicted probablity by combining both inital_prediction and adjust vector
    Note that the cumulated count are there to stop assigning high weights after 
              consecutive appearance of one same class.
    """
    # skip certification 
    if intial_prediction[0][1] == "CertificationList":
        return(intial_prediction)

    adjusted_prediction = []
    for probi, classi in intial_prediction:
        weight_i = adjust_vector[classdic[classi]]

        if classi in ["EducationList","CertificationList","CompanyBenefitsList"]:
            if (cumulated_count <= 2):
                probi_adj = ratio*probi + (1-ratio)*weight_i
            else:
                weight_i = min(0.4, weight_i)
                probi_adj = ratio*probi+(1-ratio)*weight_i
        else:
            if  (cumulated_count <= 5):
                probi_adj = ratio*probi+(1-ratio)*weight_i
            else:
                weight_i = min(0.4,weight_i)
                probi_adj = ratio*probi+(1-ratio)*weight_i 
        
        adjusted_prediction.append((probi_adj, classi))

    adjusted_prediction.sort(key=lambda tup: tup[0], reverse=True )  

    return(adjusted_prediction) 



def predicted_class_list_adjuster_transition_matrix(predicted_class_list,sentencelist,initial_vector,transition_matrix, 
                                                    classdic, classdic_rev):
    
    result_predicted_class_list=[]   

    #intial value 
    cumulated_count=0  # cumulated appearance/ basic ideas, less likely there exist more than 6 sentence states education
    adjust_vector = initial_vector
    predicted_class_adj = predicted_class_list[0]
    previous_class_0 = predicted_class_adj[0][1]

    # for sentence, change based on it's previous sentence
    for index in range(len(predicted_class_list)):
        intial_prediction = predicted_class_list[index]

        if index != 0:
            adjust_vector = transition_matrix[classdic[previous_class_0]]
            
        predicted_class_adj = predicted_class_list_adjust_vector(intial_prediction, adjust_vector,
                                                                  cumulated_count, classdic)

        class_0 = predicted_class_adj[0][1]

        if class_0 == previous_class_0:
            cumulated_count = cumulated_count +1
        else:
            cumulated_count = 0

        previous_class_0 = class_0

        #SavedRatio=float(PSL2[0][0])
        # normalize and sort the scores
        SumScore=sum([ ji[0] for ji in predicted_class_adj])
        predicted_class_adj=[(ji[0]/SumScore, ji[1]) for ji in predicted_class_adj]
        result_predicted_class_list.append(predicted_class_adj)
        
    return(result_predicted_class_list)


class Classifer_Adjuster:
    
    def __init__(self, key_words_dict, headers_dict):
        self.initial_vector=[0.011312152688339358,0.31832120526416585,0.3195649317441888,0.0016626474457185263,5.9847269767553205e-06]
        self.transition_matrix = np.array([[5.91389900e-01, 2.11827072e-03, 1.79836861e-02, 3.24225111e-03,3.45840118e-04],
                                            [2.00111046e-02, 4.96901671e-01, 6.81229092e-03, 2.18206193e-03,1.06442046e-04],
                                            [1.09687196e-02, 1.53772002e-02, 7.64723589e-01, 7.34746766e-04,5.24819119e-05],
                                            [8.73477892e-03, 1.17494371e-03, 7.66805371e-03, 8.65608083e-01,2.61270378e-03],
                                            [9.54111498e-03, 3.37409988e-03, 7.99128920e-03, 1.53368177e-03,8.61000000e-01]])

        classlist=['QualificationList', 'PositionList', 'CompanyBenefitsList','EducationList', 'CertificationList']
        classdic={}
        for i,x in enumerate(classlist):
            classdic[x]=i
        classdic_rev={}
        for key in classdic:
            classdic_rev[classdic[key]]=key
        self.classdic = classdic
        self.classdic_rev = classdic_rev
        self.lowercase_letter = SentenceClean().lowercase_letter
        self.classlist = classlist

        self.predicted_class_list_adjuster_key_words = Key_Word_Adjuster(key_words_dict, headers_dict).predicted_class_list_adjuster_key_words


    def score_adjuster(self, sentencelist, predicted_class_list, company):

        # adjust by key words
        predicted_class_list = self.predicted_class_list_adjuster_key_words(sentencelist, predicted_class_list, company)
        #adjust by transition matrix 
        predicted_class_list = predicted_class_list_adjuster_transition_matrix(predicted_class_list, sentencelist, self.initial_vector, self.transition_matrix, 
                                                                               self.classdic, self.classdic_rev)
        return(predicted_class_list)


