import json 
import pickle

class Reader:

    def __init__(self):
        pass
    
    def read_json(self, filepath):
        with open(filepath) as f:
            file = json.load(f)
        return(file)

    def read_pickle(self, filepath):
        with open(filepath, 'rb') as fp:
            file = pickle.load(fp)
        return(file)
