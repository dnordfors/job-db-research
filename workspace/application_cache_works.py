# #%% 

# WEB 
publish_on_web = True

# OS
import os
import sys
import urllib
import requests
import copy

import operator as op

# LOGGING / MONITORING
import inspect
import tqdm
from tqdm.notebook import tqdm_notebook
import time
from datetime import datetime

# redis - dictionary
import redis


# PROCESSING
import random
import numpy as np
import pandas as pd
import scipy
import collections
from collections import Counter


# MODELS / FITTING
from scipy.cluster import hierarchy

# GEO / TIME
import addfips

# NETWORK GRAPHS
import networkx as nx
import pydot
from networkx.drawing.nx_pydot import write_dot

# DATABASE + I/O
import io
import base64
import pickle

## mongo
import pymongo
from pymongo import MongoClient
import bson.json_util as bj
## json
import json
from json import dumps
from bson import ObjectId



# NLP
from bs4 import BeautifulSoup

from nltk.corpus import stopwords
stopw = set(stopwords.words("english"))

# function for cleaning each sentence
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')

# GRAPHICS
import seaborn as sns
from matplotlib import pyplot as plt

## plotly dash

import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_bio as dashbio
import dash_table
import dash_table.FormatTemplate as FormatTemplate
from dash_table.Format import Sign
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import plotly.graph_objs as go
import plotly.graph_objects as go
import plotly.figure_factory as ff
from plotly.subplots import make_subplots
import plotly.express as px

with open('mapboxtoken.txt','r') as file:
    mapboxtoken = file.read()
file.close()

## download csv 
from six.moves.urllib.parse import quote

# LocalModules (sub-directory)
from LocalModules.GeoMap import GeoID
from LocalModules.MatrixFactorization import *
import LocalModules.GovData as gd
gd.onet.occupation_data =  gd.onet.data('Occupation Data').set_index('O*NET-SOC Code')[['Title','Description']].T


# CREATE PATHS / DIRECTORIES 

# path to home directory (the location of this file)

path = os.path.curdir
paths = {}
paths['.'] = os.path.curdir
paths['data'] = './data/'
paths['cache']= paths['data']+'cache/'
paths['session_containers'] = './session_containers'

#Check if directories exist - creat directories if needed
[os.makedirs(pth, exist_ok=True) for pth in paths.values()]

# GENERAL DICTIONARIES
session = {}

class JobDescriptions:
    '''
    JobDescriptions(db,client,collection) creates an instance of a MongoDB Collection
    containing job ads/descriptions.
    
    ## Connecting the database
    
    input    : client        = name of Mongo Client   : 'mongodb://localhost:27017/' 
    attribute: jd.client     = MongoClient(client)  
    
    input    : db            = name of Mongo database : 'job_db'
    attribute: jd.db         = jd.client[db]
    
    input    : collection    = name of db-collection  : 'ServicebusCreateMessage'
    attribute: jd.collection = jd.db[collection]
    
    ## Key attributes and functions:    
    
    jd.get()                    : get 'max_items' job descriptions with 'string' in 'field'
                                  places the dataset at the top of the stack. 
    jd.mark_duplicates()        : adds column to jd.df marking duplicate job descriptions
    jd.drop_duplicates()        : removes duplicate job descriptions from jd.df 
    jd.common_titles_count()    : counts occurences of job-title n-grams in jd.get()
    
      jd.params                 : parameters for current jd.get()
      jd.df                     : current jd.get(jd.params) job descriptions as dataframe 
      jd.common_titles          : result from latest common_titles_count()
      
  
    
    
    
    
    '''
    def __init__(self,                                         \
                 db              = 'job_db',                        \
                 client          = 'mongodb://localhost:27017/',    \
                 collection      = 'ServicebusCreateMessage',       \
                 data_path       = paths['data'],                        \
                 cache_path      = paths['cache'],                  \
                 geoid_entities  = ['counties'],                    \
                 uid             = False  ):
        
        self.client     = MongoClient(client)
        self.db         = self.client[db]
        self.collection = self.db[collection]
        self.collection.doc_count = self.collection.estimated_document_count()      
        self.geo        = GeoID(path = data_path, entities = geoid_entities)
        self.uid        = uid
        # self.geoid      = self.GeoID(path = data_path, entities = geoid_entities)
        self._data_path = data_path
        self._cache_path= cache_path
        
        # Dictionaries: Function Caches
        # key named 'params' in functions

        # self.get_dic                    = {}  # removed in favor of saving get() on disk cache
        self.duplicates_dic             = {}
        self.common_titles_dic          = {}
        self.word_set_matrix_dic        = {}
        self.cluster_dic                = {}
        self.classify_job_description_dic = {}
        self.ngram_analysis_dic         = {}
        
        self.current_parameters         = {}

        self.title_dic                  = {}
        self.common_dic                 = {}




    def get(self, string = 'manager', field = 'Title', max_items = 1000, uid='', *args, **kwargs):
        '''
        1. Gathers job descriptions in MongoDB "collection" for jobs with the sub-string 'string' in the job title. 
                Speeds up by indexing 'collection' on job titles if index does not already exist 
                - THIS TAKES TIME, but does only need to be done once for a collection.
        2. Parses gathered job descriptions into dictionaries

        Returns Pandas Dataframe with columns = [ObjectId, 
                                                time stamp, 
                                                title, 
                                                title set,
                                                company,
                                                description,
                                                length, (#characters in clean description )
                                                parsed description,
                                                all bullet headers
                                                word_set]
        '''
        uid = self.uid
        regis = session[uid].regis
        
        # Check if result already in dictionary
        par = {'string':string,
               'field':field,
               'max_items':max_items, 
               'date': datetime.now().strftime('%Y-%m-%d')}
        self.params = str(par)
        params = self.params
        # If not in cache, connect to database and proceed...
        path = self._get_cache_path(params)
        if not os.path.exists(path):
#         # If not in dictionary, connect to database and proceed...
#         if params not in self.get_dic:
            collection = self.collection
            
            # Gather records with "string" in job title
            
            ## JUPYTER NOTEBOOK Indexing : weird - indexing of 'Description' works in notebook, not in Dash app
            # if field is 'Description':
            #     collection.create_index([('Message.'+field,pymongo.TEXT)]) 
            # else:
            #     collection.create_index('Message.'+field)

            cursor = collection.find({'Message.'+field:{'$regex':"(?i).*"+string+".*"}})
            cursor.limit(max_items)
            progress_bar = tqdm_notebook(total=max_items,desc="get("+string+')')
            regis.set('current_process','Fetching job descriptions') 
            regis.set('progress_name','get '+self.params[1:-1])
            cnt = 0
            gather_list = []
            while cursor.alive:
                progress_bar.update(1)
                cnt +=1 
                regis.set('progress',100* cnt / max_items)
                db_item = cursor.next()
                titl    = db_item.get('Message',{'Title':'N/A'}).get('Title')
                comp    = db_item.get('Message',{'Company':'N/A'}).get('Company')
                state   = db_item.get('Message',{'State':'N/A'}).get('State')
                city    = db_item.get('Message',{'City':'N/A'}).get('City')
                country = db_item.get('Message',{'Country':'N/A'}).get('Country')
                # location= ', '.join(map(str,[city,state,country]))
                # location= location.replace('None','')
                coords  = db_item.get('Message',{'Coordinates':'N/A'}).get('Coordinates')                                 
                id_nr   = db_item.get('_id')        # ObjectId().generation_time contains time stamp
                time    = id_nr.generation_time
                msg     = db_item.get('Message',{'Description':'N/A'}).get('Description')
                msg2    = db_item.get('Model',{'Description':'N/A'}).get('Description')
                length  = len(msg2)
                gather_list.append([id_nr,time,length,titl,comp,city,state,country,coords,msg,msg2])

            # Convert 'gather_list' (above) to DataFrame     
            #   Column labels matching variables in gather_list 
            regis.set('current_process','Building dataframe')
            df_columns  = ['ObjectId','time stamp','length', 'title',\
                           'company','city','state','country','coordinates','description','clean description']            
            df          = pd.DataFrame(gather_list, columns = df_columns )
            # Convert coordinates from string to numbers
            df['coordinates'] = df['coordinates'].apply(eval).apply(lambda x: (x[1],x[0]))
            # Parse job descriptions
            regis.set('current_process','Parse job descriptions')
            df['parsed description']    = df['description'].apply(self.parse_job_description)
            df['all bullet headers']           = df['parsed description'].apply(lambda x: list('\n### HEAD: '+key+' ###' for key in x.keys()))
            df['word_set']              = df['clean description'].apply(lambda x: set(x.split()))
            df['title set']             = df['title'].apply(lambda x: set(x.split())) 
            # Save result to disk cache (for saving to dictionary, replace with code right below)
            regis.set('current_process','Saving to disk cache')
            self.df = df
            self._get_to_cache()
        else:
            self._get_from_cache()
#             # Save result in dictionary
#             regis.set('current_process','Saving to dictionary')
#             self.get_dic[params] = df 
#         self.df = self.get_dic[params]
        self.current_parameters.update({'fetched items':len(self.df)})
        regis.set('current_params',str(self.current_parameters))
        regis.set('progress_name','')
        regis.set('progress',0)
        regis.set('current_process','')
        return self

    
# GET_CACHE Store get()-results in disk-cache

    def _get_cache_path(self,params=False, ext = '.df.pkl'):
        '''
        Returns the path to the disk-cache for self.get(**params) 
        '''
        if not params:
            params = self.params
        cache_path = self._cache_path
        paramsX = self.params.replace(' ','xSPACEx')
        key = ''.join(e for e in paramsX if e.isalnum()).replace('xSPACEx','_')
        path = self._cache_path+key+ext
        return path
    
    def _get_to_cache(self): 
        '''
        Adds the current self.get() result to disk cache
        '''
        path = self._get_cache_path()
        self.df.to_pickle(path)
        return self

    def _get_from_cache(self,
                 params = False,
                 ext = '.df.pkl',*args,**kwargs): 
        '''
        retrieves self.get(**params) from disk cache
        '''
        if not params:
                params = self.params
        path = self._get_cache_path(params=params)
        if os.path.exists(path):
            self.df = pd.read_pickle(path)
            self.params = params
        else:
            pass
    #         jd.get(**params)    # Use if using jd.get_to_cache() as wrapper for jd.get()
        return self

    def add_geoid(self):
        df = self.df
        # Add Geo_ID (fips) codes. This is done with a lookup-table for coordinates. 
        # If no lookup-value, calculate Geo_ID and add to lookup. When ready, 
        # save updated lookup table to file. 
        ## this can take time - add **progress bar**
        max_items = len(df)
        ### progress bar for notebook
        progress_bar = tqdm_notebook(total=max_items,desc="adding Geo_id")
        ### progress bar for plotly/dash app
        regis.set('current_process','Building dataframe: Adding Geo_IDs')
        counter = Counter('t')
        ### add progress bar to function
        def geoid_progress(coords):
            progress_bar.update(1)
            counter.update('t')
            regis.set('progress',100* (counter['t']-1) / max_items)
            return self.geo.coords_to_geoid(coords)   
        ### apply function
        df['geo_id']      = df['coordinates'].apply(geoid_progress)
        regis.set('progress_name','')
        regis.set('progress',0)
        regis.set('current_process','Saving Geo_ID lookup table')
        self.geo.save_lookup()
        return self

    def plot_map(self,labels='title',counts=1, mode = 'markers', title = 'default' ):
        df = pd.DataFrame(self.df['coordinates'].to_dict()).T
        df.columns = ['long','lat']
        df['text'] = 'Title: ' + self.df['title'] + '<br>' + 'Employer: ' + self.df['company'] +  '<br>' + \
                        'Location: ' + self.df['city'] + ', ' + self.df['state'] 
        if type(counts) is str:
            df['cnt'] = self.df['counts']
        else:
            df['cnt'] = counts
        fig = go.Figure(data=go.Scattergeo(
                lon = df['long'],
                lat = df['lat'],
                text = df['text'],
                mode = mode,
                marker = dict(
                            size = 2,
                            opacity = 0.8,
                            reversescale = True,
                            autocolorscale = False,
                            symbol = 'circle',
                            line = dict(
                                width=1,
                                color='rgba(102, 102, 102)'
                            ),
                            colorscale = 'Blues',
                            cmin = 0,
                            color = df['cnt'],
                            cmax = df['cnt'].max()
                        )
                ) 
        ) 
        if title == 'default':
            field   = str(jd.current_parameters['field'])
            string  = str(jd.current_parameters['string'])
            fetched = str(jd.current_parameters['fetched items'])
            if field == ' ':
                titl = ''
            else:
                titl = field + ' with \'' + string +'\' - ' + fetched +' samples'
                # fig.update_layout(geo_scope='usa')
        fig.update_layout(
                title = titl,
                geo_scope='usa',
                height = 500,
                plot_bgcolor = 'white',
                paper_bgcolor = 'white',
            )
        return fig

    def get_disk(self,params):
        
        
        return

    def remove_html(self,column=False, separator = ' /n '):
        if not column:
            column = self.current_parameters['field']
        bbb = self.df[self.df[column].dropna().apply(lambda x: '<p>' in x)][column]
        aaa = bbb.apply(lambda x: BeautifulSoup(x).get_text(separator = separator))
        result = aaa.combine_first(self.df[column])
        self.df[column] = result
        return self
 
    # def classify_sentences(self,round_values = True, n_samples=False, random_state = 42):      
    #     if n_samples:
    #         df = self.df.sample(n_samples,random_state = random_state)
    #     else:
    #         df = self.df
    #     df_array = df.apply(lambda x: classify_one_job_description(x,round_values = round_values),axis = 1)
    #     df_classified = pd.concat(list(df_array),sort=False).reset_index(drop=True)
    #     self.classified_sentences = df_classified
    #     return self

     
    # def classify_job_descriptions(self,round_values = True, n_samples=False, random_state = 42):   
    #     if n_samples:
    #         df = self.df.sample(n_samples,random_state = random_state)
    #     else:
    #         df = self.df
    #     tqdm.notebook.tqdm.pandas(desc="classify_job_descriptions()")
    #     df_array = df.progress_apply(lambda x: classify_one_job_description(x,round_values = round_values),axis = 1)
    #     df_classified = pd.concat(list(df_array),sort=False).reset_index(drop=True)
    #     self.classified_sentences = df_classified
    #     return self
    
    def build_classified_word_set(self, category = 'QualificationList'):
        df = self.classified_sentences[
            self.classified_sentences[category] >0.5
        ][['ObjectId','SentenceList']].groupby('ObjectId').aggregate(list)
        df['cleaned'] = df['SentenceList'].apply(sentence_cleaner,tokenized = True)
        df[category+' word_set'] = df['cleaned'].apply(lambda x: set(sum(x,[])))
        self.df = self.df.merge(df,left_on = 'ObjectId',right_index = True)
        return self

    def parse_job_description(self,string):
        '''
        Converts description to dictionary
        1. Splits the job description into a list:  split at linebreak and html tags for bullet/paragraph
        2. A line ending with ":" is assigned as a key
        3. Immediately following lines beginning with "*" are assigned as values for the key   
        4. All other lines are assigned as values to key "000 Leftover bucket"

        (Developer's comment: Quick & dirty version. Can be done more elegantly with RegEx)
        '''
        # Split job description into items
        split_string = string.replace('<li>','\n *'). \
                                replace('</li>','\n').\
                                replace('<p>','\n').  \
                                replace('</p>','\n'). \
                                split('\n')
        # Initiate dictionary
        key = "000 Leftover bucket"
        dic = {}
        dic["000 Leftover bucket"]= []

        # Sort items into dictionary
        for item in split_string:
            # item  = item.lower()
            item0 = item.replace(' ','')
            if item0 == '':
                pass
            elif item0[0] == "*":
                dic[key].append(item[1:])
            elif item0[-1] == ':':
                key = item.replace(':','') 
                dic[key] =[]
            else:
                dic["000 Leftover bucket"].append(item)
        return dic
    
    # DUPLICATES 
    def mark_duplicates(self,inplace = True,thresh=0.8):
        '''
        Scalable procedure for identifying duplicates 
        1. Select companies with at least two job descriptions
        2. For each company, sort job descriptions by length (from longer to shorter)
        3. Calculate word_set-similarity between neighboring job descriptions
        4. If the word_set (jaccard) similarity with previous neighbor is larger than 0.8, mark as duplicate
        '''
        uid = self.uid
        regis = session[uid].regis
        self.current_parameters['duplicates']={}
        self.current_parameters['duplicates']['threshold']=thresh
        regis.set('current_process','Mark duplicates')
        
        params = self.params
        df = self.df
        if 'duplicate' not in df.columns:
            grp_co = df[['length','ObjectId','company','word_set']].groupby('company').agg(lambda x: list(x))
            grp_co['nr of posts'] = grp_co['length'].apply(len) 

            # Select companies with THREE or more job postings
            # ********* TODO ******** : REMOVE DUPLICATES FOR COMPANIES sith TWO POSTINGS.
            companies = grp_co[grp_co['nr of posts'] > 2]
            # Identify duplicate job descriptions, selected by word_set-similarity between descriptions of similar lengths 
            # Save their ObjectId's in a dict
            dupes_dic = {}
            for company in companies.index:
                company_df         = pd.DataFrame(companies.loc[company].to_dict()).sort_values(by='length',ascending = False)
                dupes_dic[company] = self.dupes(company_df,thresh = thresh)['ObjectId']

            # Save the dict with company names and ObjectIds for dupes 
            self.duplicates_dic[params] = dupes_dic
            dupes_df = pd.DataFrame(1,index = np.concatenate(list(self.duplicates_dic[self.params].values())),columns = ['duplicate'])
            self.df = pd.merge(self.df,dupes_df,left_on='ObjectId',right_index = True, how='outer')
        if inplace is True:
            # self.get_dic[params] = self.df
            self._get_to_cache()
         
        #regis
        self.current_parameters['duplicates']['count']=int(self.df['duplicate'].count())
        self.current_parameters['duplicates']['status'] = 'marked'
        regis.set('current_params',str(self.current_parameters))
        
        return self

    def drop_duplicates(self,inplace = False):    
        '''
        Duplicate job descriptions are dropped from the dataframe self.df
        If inplace = True they are permanently erased from the dataframe 
        They will remain available in 
         - self.duplicates_dic[self.params] and in 
         - self.duplicates_dic["{'string': <value>, 'max_items': <value>}"]
        '''
        uid = self.uid
        regis = session[uid].regis
        regis.set('current_process','Dropping duplicates')
        regis.set('progress_name','Active')
        
        if 'duplicate' not in self.df.columns:
            self.mark_duplicates()
        self.df = self.df[self.df['duplicate']!=1].drop(columns = ['duplicate'])
        if inplace is True:
            self.get_dic[self.params] = self.df
        
        #regis
        regis.set('current_process',' ')
        regis.set('progress_name','')
        self.current_parameters['duplicates']['status'] = 'dropped'
        regis.set('current_params',str(self.current_parameters))
        
        return self  
 
    
    def dupes(self,df,thresh = 0.8):
        '''
        Measures word set similarity 
        '''
        crps = pd.DataFrame(self.crpsim_previous(df['word_set']),index = df.index)
        return df[crps[0] > thresh]
    
    def crpsim_previous(self,corpora):
        return [0] +[self.word_set_similarity(pair) for pair in self.consecutive_pairs(corpora)] 
    
    def word_set_similarity(self,corps):
        '''
        Measures similarity between two word_sets (type:set)
        '''
        corp1,corp2 = corps
        diff = len(corp1.symmetric_difference(corp2))
        total= len(corp1)+len(corp2)
        return 1- diff/total 
    
    def consecutive_pairs(self,array):
        return np.array([array[1:],array[:-1]]).transpose()


    def bullets_column(self,label,keystrings,bullet_formatted=True, *args,**kwargs):
        self.df[label] = self.df["parsed description"].apply(lambda dic: self.identify_bullets(dic,keystrings))
        if bullet_formatted:
            self.df[label] = self.df[label].apply(lambda lst: str(lst).replace(
                                    '\"','\''       ).replace(
                                    '[[\'','*'      ).replace(
                                    '\', \'','\n*'  ).replace(
                                    '\'], [\'','\n*').replace(
                                    '\']]',''       ).replace(
                                    '[]',''         )
                                    )
        self.current_parameters['bullets for'] = keystrings
        return self
    
    def identify_bullets(self,dic,keystrings):
        '''
        return values for keys containing any of the listed strings
        '''
        def return_true(item,truth = False): ## TODO: Replace with filter()
            if truth:
                return item

        return list(filter(None,[return_true(['### HEAD: '+ item[0] +' ###']+item[1],any(key.lower() in item[0].lower() for key in keystrings)) for item in dic.items()]))
        
    def nn_classify(self,*args,**kwargs):
        '''
        '''
        self.get(self.title,**self.kwargs)['nn classified']=self.get(self.title,**self.kwargs)[['description','company']].T.apply(lambda x: SentenceClassifier.predict_Sentence(**x))


    def ngrams(self,array, N=2, string_form = False):
        '''
        Returns all N-element long sequences of neighboring elements. Works both for strings and lists.
        string_form = False       : operates on lists, no splits
                     'sentences'  : splits text into list of sentences, then splits sentences into words
                     'words'      : splits string into words
                     'characters' : operates on strings, no splits

        Examples:
        [in ]  ngrams([1,2,3,4])
        [out]  array([[1, 2],[2, 3],[3, 4]])

        [in ]  ngrams('one two three four', string_form = 'words')
        [out]  array(['one two', 'two three', 'three four'], dtype='<U10')

        [in ]  ngrams('one two three four', string_form = 'characters')
        [out]  array(['on', 'ne', 'e ', ' t', 'tw', 'wo', 'o ', ' t', 'th', 'hr', 're',
                      'ee', 'e ', ' f', 'fo', 'ou', 'ur'], dtype='<U2')
        '''
        # Split string
        if string_form:
            if type(array) is str:
                if string_form == 'words':
                    array = array.split()
                else:
                    string_form = 'characters'
                    array = [i for i in array]

        # NoneType
        if array == '':
            return ''
        if array == []:
            return []

        # Build N-grams
        if type(array[0]) in (list, np.ndarray):
            result = [self.ngrams(item,N=N) for item in array]
        else:
            array0 = np.append(np.array(array),0)
            result = np.transpose([array0[i:i-N] for i in range(N)])

        # Return result
        if string_form:
            spacer = {'words':' ', 'characters':''}
            result = np.array([spacer[string_form].join(item) for item in result])
        return result

    def ngrms(self,column, N_max = 8,uid=''):
        '''
        '''
        regis.set('current_process','Creating n-gram tables') 
        regis.set('progress_name','ngrms('+column+')')
        regis.set('progress',0)
        regis.set('cnt',0)
        regis.set('max', len(self.df[column]))


        sntc = lambda x: sentence_cleaner(x,tokenized = True)
        n_grms = lambda x: [
            [' '.join(ngr) for ngr in self.ngrams(sntc(x), N=nlen)] 
            for nlen in range(min((len(sntc(x))+1),N_max))
        ]
#        cnt = lambda x: Counter(np.concatenate(n_grms(x)))
        def cnt(x):
            regis_max = eval(regis.get('max'))
            regis_cnt = eval(regis.get('cnt'))+1
            regis.set('cnt',regis_cnt)
            regis.set('progress',100* regis_cnt / regis_max)
            return Counter(np.concatenate(n_grms(x)))

        tqdm_notebook.pandas(desc='ngrms('+column+')')
        self.df[column + ' ngrams'] = self.df[column].progress_apply(cnt) 

        regis.set('current_process','') 
        regis.set('progress_name',' ')
        regis.set('progress',0)
        return self

    def common_words_count(self,column):
        cnt = Counter()
        self.ngrms(column).df[column+' ngrams'].apply(lambda x: cnt.update(x))
        self.common_words = cnt
        return self

#### map_common / ngrams_count / n_grammer can be made to replace map_common_titles 

    def map_common(self,column,threshold = 5):
        '''
        Counts n-grams for 'column' and maps the n-grams with more than 'threshold' counts on the column 
        '''
        self.ngrams_count(column)
        ngr = pd.Series(self.ngr)
        
        map_from = set(ngr[ngr >= threshold].keys())
        map_on   = self.df[column+' ngrams'] 
        map_func = lambda x: set(x).intersection(set(map_from))
        self.df[column+' common'] = map_on.apply(map_func)
        return self


    def ngrams_count(self,column):
        cnt = Counter()
        self.ngrammer(column).df[column+' ngrams'].apply(lambda x: cnt.update(x))
        self.ngr = cnt
        return self

    def ngrammer(self,column):
        if not (column + ' ngrams') in self.df:
            sntc = lambda x: sentence_cleaner(x,tokenized = True)
            ngrms = lambda x: [
                [' '.join(ngr) for ngr in self.ngrams(sntc(x), N=nlen)] 
                for nlen in range(len(sntc(x))+1)
            ]
            cnt = lambda x: Counter(np.concatenate(ngrms(x)))
            tqdm_notebook.pandas(desc="ngrammer("+column+')')
            self.df[column + ' ngrams'] = self.df[column].progress_apply(cnt) 
        return self


## N-GRAM ANALYSIS

    def ngram_analysis(self, field = 'title', ngram_min_count=10, *args, **kwargs):
        '''
        Perform n-gram analysis of 'field' in self.df

        1) 
        '''
        regis.set('current_process','ngram_analysis('+field+')')
        # set parameters        
        self.current_parameters['field']           = field
        self.current_parameters['ngram_min_count'] = ngram_min_count
        
        if self.params not in self.ngram_analysis_dic:
            self.ngram_analysis_dic[self.params] = {}
        if field not in self.ngram_analysis_dic[self.params]:
            self.ngram_analysis_dic[self.params][field] = {}

        # Build n_grams and count them
        self.common_words_count(field)
        self.map_common(field)
        #convert jd.common_words to pandas series / include only ngrams with at least n_min counts  
        cw = pd.Series(self.common_words) 
        cw = cw[cw >= ngram_min_count]
        cw = cw.sort_values(ascending = False)
        # use remaining common_words as vocabulary
        # add column 'title vocab' to jd.df = 'title common' with ngrams outside of the vocabulary removed
        vocab = set(cw.index)
        tqdm_notebook.pandas(desc="reduced n-gram vocabulary")
        self.df[field+' vocab'] = self.df[field+' common'].progress_apply(lambda st: vocab.intersection(st))
        self.build_tree_graph()
        regis.set('current_params',str(self.current_parameters))
        return self
    
    
    def build_tree_graph(self):
        '''
        BUILDING A TREE-DIAGRAM OF COMMON WORDS (NGRAMS)
        '''
        field           = self.current_parameters['field'] 
        root            = eval(self.params)['string'] 
        ngram_min_count = self.current_parameters['ngram_min_count']
        # Create a dataframe from the pandas series of common words (raw count / not normalized)
        cw = pd.Series(self.common_words) 
        cw = cw[cw >= ngram_min_count] 
        cw = cw.sort_values(ascending = False)
        c1 = cw.copy().reset_index()
        c1['list']=c1['index'].apply(lambda x: x.split())
        c1['len'] = c1['list'].apply(len)
        c1['set'] = c1['list'].apply(set)
        c1['set_len'] = c1['set'].apply(len)
        c1['set_str'] = c1['set'].apply(str)
        c1.set_index('index',inplace = True, drop=True)
        
        # Keep only words including the root-word
        c1 = c1[c1['list'].apply(lambda x: root in x)]
        

        # Build the tree-structure as a dictionary, the levels are the number of words in the n-grams

        tree_dic = {}
        n_levels = c1['len'].max()
        leaf_sum_dic = {}
        for n in range(1,n_levels-1):
            branches = c1[c1['len']==n]
            leaves = c1[c1['len']==n+1]
            tree_dic[n]={}
            for branch in branches.index:
                branch_leaves = leaves[[(branch in leaf) for leaf in leaves.index]].index
                #Remove the branch if it has only one leaf, the leaf takes over the role of the branch
                if len(branch_leaves) == 1:
                    old_branch = branch
                    new_branch = branch_leaves[0]
                    for item in tree_dic[n-1].items():
                        key  = item[0]
                        vals = item[1]
                        for m in range(2,n-1):
                            tree_dic[m][key] = [new_branch if x==old_branch else x for x in vals]
                if len(branch_leaves) >1 :
                    tree_dic[n][branch] = list(branch_leaves)

        # convert dictionary to graph
        ldata = list(tree_dic.values())
        data = {}
        [data.update(dic) for dic in ldata]
        self.tree_dic = tree_dic
        # convert the dictionary into a graph with networkx
        self.tree_graph = nx.Graph(data) 
        # using a vocabulary consisting of only the nodes, construct 'field node vocab' in self.df
        self.node_vocab = set(self.tree_graph.nodes)
        self.df[field+' node vocab']= self.df[field+' common'].apply(lambda x: self.node_vocab.intersection(x))
        return self

    def plot_tree(self, prog = 'twopi'):
        G = self.tree_graph
        # plot the tree
        pos = nx.nx_pydot.pydot_layout(G, prog=prog)
        plt.figure(3,figsize=(14,14)) 
        nx.draw_networkx(G,pos,node_size=20,width = 0.4,font_size=10,alpha = 1)
        # nx.draw_networkx(G,pos,node_size=0,font_color=(1,0,0),font_size=10,alpha = 1)
        plt.show()
        return self
    
    def build_archetypes(self, n_archetypes = 10, node_vocab = False):
        self.current_parameters['n_archetypes'] = n_archetypes
        self.current_parameters['node_vocab']   = node_vocab
        field     = self.current_parameters['field'] 
        
        if node_vocab:
            vocab = field+' node vocab'
        else:
            vocab = field+' vocab'
        # *** ARCHETYPING
        if 'archetypes' not in self.ngram_analysis_dic[self.params][field]:
            self.ngram_analysis_dic[self.params][field]['archetypes'] = {}
        if vocab not in self.ngram_analysis_dic[self.params][field]['archetypes']:
            self.ngram_analysis_dic[self.params][field]['archetypes'][vocab]={}
        if n_archetypes not in self.ngram_analysis_dic[self.params][field]['archetypes'][vocab]:
            # Construct the word-matrix, each job-title is a word-vector
            # Normalize for each word, so that the matrix shows the distribution of a word across the job-titles
            word_mat = pd.DataFrame(self.df[vocab].apply(Counter).to_dict()).fillna(0)
            word_matn = word_mat.T.apply(norm_dot).T
            # Archetype the vocabulary, using the matrix of normalized word-vectors
            arc    = Archetypes(word_matn.T,n_archetypes)
            self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes] = arc
        self.archetypes = self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes]
        return self

    # Heatmap plot of Archetypes, choose an archetype (col-number), sort in descending order, cut at threshold & plot
    # - they seem quite well separated

    def plot_archetypes(self,col=4,thresh=0.05, node_vocab_filter = True, root_vocab_filter= False, figsize=(16, 10)):
        n_archetypes=self.current_parameters['n_archetypes']
        field       =self.current_parameters['field']
        node_vocab  =self.current_parameters['node_vocab']
        root = eval(self.params)['string']
        if node_vocab:
            vocab = field+' node vocab'
        else:
            vocab = field+' vocab'
       
        arc = self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes]
        arc_f = arc.f.T.copy()
        if node_vocab_filter:
            arc_f = arc_f.loc[self.node_vocab.intersection(arc_f.index)]
        if root_vocab_filter:
            arc_f = arc_f[arc_f.index.map(lambda x: root in x)]
        arc_fn = arc_f.apply(norm_dot) 
   
        plt.figure(figsize=figsize)
        arc_plot = arc_fn[arc_fn[col]>thresh].sort_values(by=col,ascending = False)
        sns.heatmap(arc_plot)
        self.current_arc_plot = arc_plot
        return
    
    def correlated_ngrams(self,keyword,node_vocab_filter = True, root_vocab_filter= False):
        
        n_archetypes=self.current_parameters['n_archetypes']
        field       =self.current_parameters['field']
        node_vocab  =self.current_parameters['node_vocab']
        root = eval(self.params)['string']
        if node_vocab:
            vocab = field+' node vocab'
        else:
            vocab = field+' vocab'
       
        arc = self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes]
        arc_f = arc.f.T.copy()
        if node_vocab_filter:
            arc_f = arc_f.loc[self.node_vocab.intersection(arc_f.index)]
        if root_vocab_filter:
            arc_f = arc_f[arc_f.index.map(lambda x: root in x)]
        arc_fn = arc_f.apply(norm_dot) 

        # Calculate word-embedding: shows which words are similar / co-occur
        word_corr = arc_fn@arc_fn.T

        # Example: 
        # Here only correlated words including 'nurse' - i.e. potential job titles - are included
        # root       = keyword in the latest get(keyword)
        correlated = word_corr[keyword].sort_values(ascending = False)
        return correlated
    

    


# ## CLUSTERING 

#     def word_vectors(self,column='QualificationList word_set',word2vec=glove_dic):
#         '''
#         ** What: Converts word sets (in 'column') to word-vectors, using GloVe, returning a DataFrame
#         ** How: The word vector for a set is the  sum of the word-vectors for the words in the set. 
#         todo: this is very course - should be improved 
#         ** Output: DataFrame - each row is the word-vec for a job description word-set  
#         '''
#         cluster_dic = self.cluster_dic
#         if self.params not in cluster_dic:
#             cluster_dic[self.params] = {}
#         df = self.df.copy()
#         glove_set = set.union(*df[column]).intersection(glove_dic)
#         cluster_dic[self.params]['glove_set'] = glove_set
#         df['glove_set'] = df[column].apply(lambda word_set: word_set.intersection(glove_set)-stopw)
#         coords = df['glove_set'].apply(lambda x: glove_df[x].T.sum()).T
#         coords.columns = df['ObjectId']
#         return coords
    
#     def cluster(self,column='QualificationList word_set',n_clusters=10,n_samples=500,random_state=42):
#         '''
#         Cluster job descriptions by description content.
#         Steps:
#         1. transform to word_vectors  
#         2. Create clusters with K-Means (todo: test other cluster methods)
#         3. Tag each job description with the cluster number
#         '''
#         params = self.params
#         X             = self.word_vectors(column).T
#         X_train       = X.sample(n_samples,random_state = random_state)
#         print('*** cluster: fitting model....')
#         model         = KMeans(n_clusters=n_clusters, random_state=random_state).fit(X_train)
#         self.model    = model
#         self.cluster_dic[params]['model'] = model
#         print('*** cluster: predicting....')
#         self.cluster_predict = model.predict
#         col_title     = str(n_clusters)+' clusters'
#         print('*** cluster: writing self.clusters ....')
#         X[col_title]  = model.predict(X)
#         self.cluster_dic[params]['X'] = X
#         self.clusters = X
#         print('*** cluster: merging cluster assignments into self.df ....')
#         self.df       = self.df.merge(X[col_title],left_on="ObjectId",right_index = True)
#         print('*** cluster: complete')
#         return self


#     def get_word_set_matrix(self):
#         if self.params not in self.word_set_matrix_dic:
#             tcrp = Counter()
#             self.df['word_set'].apply(lambda x: tcrp.update(x))
#             df = pd.DataFrame.from_dict(tcrp, orient = 'index', columns = ['counts']).sort_values(by='counts',ascending = False)
#             for i in jd.df['word_set'].index:
#                 df[i] = pd.Series(1,index = jd.df['word_set'].loc[i])
#             self.word_set_matrix_dic[self.params] = df.T
#         self.word_set_matrix = self.word_set_matrix_dic[self.params]
#         return self.word_set_matrix
    
    

    
    
    def subtract_fundamental(self,normalize = True):
        '''
        *** Experimental / under development *** 
        
        operates on the corpus-matrix
        
        Returns the corpus_matrix, subtracting the 0th eigenvector component from each corpus.
        The intention is to reduce features that don't differentiate the corpora, such as stopwords. 
        
        The 0th eigenvector is approximated by the normalized vector of word occurences across all corpora. 
        
        Challenge: returned vectors can have negative coefficients, getting in the way of NMF
        
        Suggested continuation - split the positive & negative parts of the returned vectors, i.e. the 'domi-matrix'
        
        '''
        def sub_ev0(vec,normalize = normalize):
            v0   = norm_dot(df0.T['counts'])
            v    = norm_dot(vec)
            frac = np.sqrt(v@v0)
            if normalize:
                vec1 = (v - frac * v0)
                return norm_dot(vec1)
            else:
                nrm  = np.linalg.norm(vec)
                vec1 = (v - frac * v0)*nrm
                return vec1
        df0 = self.get_corpus_matrix().fillna(0)
        df  = pd.DataFrame([sub_ev0(df0.loc[idx]) for idx in df0.index], index = df0.index)
        return df
'''
# JOB DESCRIPTION DATABASE

* 1.5M active job ads
* 3-month time window
* Untapped source of info - good for knowledge graph

## DB ORIGINAL CONTENT

* Timestamp & Status
* Original ad text (formatted)
* Pre-processed text:
    - unformatted ad text
    - extracted keywords / entities / skills 

The preprocessed keywords / entities / skills are compact and general and may have 
involved more data in the processing, such as Onet data for occupations.  
'''

## MATH
# # %%
# # Test: Get the first job description in the collection
# jd.collection.find()[0]
classify_job_description_dic = {}
# def classify_one_job_description(pd_series,round_values=True):
#     objectid = pd_series['ObjectId']
#     if objectid not in classify_job_description_dic:
#         Description= pd_series['description']
#         Company=pd_series['company']
#         result = SentenceClassifier.predict_Sentence(Description, Company)
#         df_classification = pd.DataFrame([dict((subitem[1],subitem[0]) for subitem in item) for item in result['SentenceTag']])
#         if round_values:
#             df_classification = df_classification.fillna(0).applymap(round)
#         df_sentence = pd.DataFrame(pd.Series(objectid, index = range(len(result['SentenceList']))),columns = ['ObjectId'])
#         df_sentence['SentenceList'] = result['SentenceList']
#         df = df_sentence.merge(df_classification, left_index=True, right_index = True, how='left')
#         classify_job_description_dic[objectid] = df
#     return classify_job_description_dic[objectid]


jd = JobDescriptions()
# initialize jd.df - needed for initial empty map in app
jd.df = pd.DataFrame({'title':['a','a'],'coordinates':[(0,0),(0,0)],'counts':[1,1],'company':['a','a'],
                    'city':['a','a'],'state':['a','a'],'country':['a','a'], 'description':['a','a']})
jd.current_parameters = {'field':' ','string':' ','fetched items':' '}




#%%
#####################################
#####################################   WEB APP  ######################################
#####################################


## DASH/PLOTLY  WEB APP


###############
# APP COMPONENTS
##############


# TABS

tabs_styles = {
    'height': '44px'
}
tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': 'gainsboro',
    'color':'dimgray',
    'padding': '6px',
    'fontWeight': 'bold'
}

tab_selected_style = {
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    # 'backgroundColor': '#119DFF',
    'backgroundColor': 'mediumseagreen',
    'color': 'white',
    'padding': '6px'
}


# REGIS 

class Regis:
        def __init__(self,no_log = ['progress'],registers=[''],uid=''):
            self.uid = uid
            self._setup_redis()
            self.no_log = no_log
            self.regis.set('progress',0)
            self.regis.set('progress_name','')
        
        def time(self):
            t1 = datetime.now()
            time = "%s-%s-%s %s:%s:%s" % (t1.year,t1.month,t1.day,t1.hour, t1.minute,t1.second)
            return time
        
        def _setup_redis(self,db=0):
            self.regis = redis.Redis(host='localhost',port=6379,db=db)
            return self
        
        def set(self,key,string):
            if self.uid == '':
                return self
            uid = self.uid
            string = str(string)
            id_key = key+'['+uid+']'
            aaa = self.regis.set(id_key,string)
            return self
        
        def get(self,key):
            if self.uid == '':
                return self
            uid = self.uid
            id_key = key+'['+uid+']'
            value = self.regis.get(id_key)
            return value
        
        def append(self,key,string):
            if self.uid == '':
                return self
            uid = self.uid
            string = str(string)
            id_key = key+'['+uid+']'
            aaa = self.regis.append(id_key,string)
            return self 


regis = Regis()
uid = ''
regis.set('progress',0)
regis.set('progress_name','')
regis.set('current_params','none')
regis.set('current_process',' ')


# SESSION CONTAINER

class SessionContainer:
    
    
    def __init__(self,uid=0,resume_session=True):
        self.uid           = str(uid)
        self.path          = os.path.curdir + '/session_containers/' + self.uid + '/'
        self.regis         = Regis(uid = self.uid)
        self.created_when  = self.regis.time()
        
        # Create uid session directory if it doesn't exist 
        os.makedirs(self.path, exist_ok=True)

# session = {} #moved up to dependencies/init


####################################
# DP - DASH-PARTS

class DP:

    def __init__(self):
        self._aaa = 'aaa'

        # # operators for dashtable filtering
        # self.operators = [['ge ', '>='],
        #     ['le ', '<='],
        #     ['lt ', '<'],
        #     ['gt ', '>'],
        #     ['ne ', '!='],
        #     ['eq ', '='],
        #     ['contains '],
        #     ['datestartswith ']]


    # DASHTABLE
    def dashtable(self,df,id):
        return dash_table.DataTable(
                            id=id,
                            columns=[
                                {"name": i, "id": i, "deletable": True, "selectable": True} for i in df.columns
                            ],
                            data=df.to_dict('records'),
                            editable=True,
                            filter_action="native",
                            sort_action="native",
                            sort_mode="multi",
                            column_selectable="single",
                            row_selectable="multi",
                            row_deletable=True,
                            selected_columns=[],
                            selected_rows=[],
                            selected_cells=[],
                            page_action="native",
                            # page_current= 0,
                            page_size= 15,
                            style_data={'whiteSpace': 'pre-line'},
                            style_data_conditional=[
                                {
                                    'if': {'row_index': 'odd'},
                                    'backgroundColor': 'floralwhite'
                                }
                            ],
                            style_table={
                                'maxWidth': '100%',                       
                                'overflowX': 'scroll',
                                'maxHeight': '1000px',
                                'overflowY': 'scroll',
                                'padding' : '10px'},
                            style_cell={
                                'height': '30px',
                                'textAlign': ['center'],
                                # all three widths are needed
                                'minWidth': '20px', 'width': '150px', 'maxWidth': '400px',
                                'whiteSpace': 'normal',
                                'font-family':'sans-serif',
                                'font-size':12,
                                'backgroundColor': 'papayawhip'
                            },
                            style_header={'backgroundColor': 'wheat'},
                            style_cell_conditional=[
                                {'if': {'column_id': 'O*NET-SOC Code'},
                                'width': '40px'},
                                {'if': {'column_id': 'title'},
                                'width': '100px'},
                                {'if': {'column_id': 'title'},
                                'textAlign': 'left'},
                                {'if': {'column_id': 'company'},
                                'width': '20px'},
                                {'if': {'column_id': 'city'},
                                'width': '10px'},
                                {'if': {'column_id': 'state'},
                                'width': '5px'},
                                {'if': {'column_id': 'country'},
                                'width': '40px'},
                                {'if': {'column_id': 'all bullet headers'},
                                'width': '120px'},
                                {'if': {'column_id': 'all bullet headers'},
                                'textAlign': 'left'},
                                {'if': {'column_id': 'bullets'},
                                'width': '460px'},
                                {'if': {'column_id': 'bullets'},
                                'textAlign': 'left'},
                            ],
                        )
    
    def split_filter_part(self,filter_part):
        
        operators = [['ge ', '>='],
                ['le ', '<='],
                ['lt ', '<'],
                ['gt ', '>'],
                ['ne ', '!='],
                ['eq ', '='],
                ['contains '],
                ['datestartswith ']]

        for operator_type in self.operators:
            for operator in operator_type:
                if operator in filter_part:
                    name_part, value_part = filter_part.split(operator, 1)
                    name = name_part[name_part.find('{') + 1: name_part.rfind('}')]

                    value_part = value_part.strip()
                    v0 = value_part[0]
                    if (v0 == value_part[-1] and v0 in ("'", '"', '`')):
                        value = value_part[1: -1].replace('\\' + v0, v0)
                    else:
                        try:
                            value = float(value_part)
                        except ValueError:
                            value = value_part

                    # word operators need spaces after them in the filter string,
                    # but we don't want these later
                    return name, operator_type[0].strip(), value

        return [None] * 3

    def update_table(self,filter):
        filtering_expressions = filter.split(' && ')
        dff = df
        for filter_part in filtering_expressions:
            col_name, operator, filter_value = split_filter_part(filter_part)

            if operator in ('eq', 'ne', 'lt', 'le', 'gt', 'ge'):
                # these operators match pandas series operator method names
                dff = dff.loc[getattr(dff[col_name], operator)(filter_value)]
            elif operator == 'contains':
                dff = dff.loc[dff[col_name].str.contains(filter_value)]
            elif operator == 'datestartswith':
                # this is a simplification of the front-end filtering logic,
                # only works with complete fields in standard format
                dff = dff.loc[dff[col_name].str.startswith(filter_value)]

        return dff.to_dict('records')


    # UPLOAD CONTENT TO DASHTABLE
    def parse_contents(self,contents, filename, date):
        '''
        Upload CSV and Excel from Dash app
        '''
        content_type, content_string = contents.split(',')

        decoded = base64.b64decode(content_string)
        try:
            if 'csv' in filename:
                # Assume that the user uploaded a CSV file
                df = pd.read_csv(
                    io.StringIO(decoded.decode('utf-8')))
            elif 'xls' in filename:
                # Assume that the user uploaded an excel file
                df = pd.read_excel(io.BytesIO(decoded))
        except Exception as e:
            print(e)
            return html.Div([
                'There was an error processing this file.'
            ])

        return html.Div([
            html.H5(filename),
            html.H6(datetime.fromtimestamp(date)),
            dp.dashtable(df,filename),
            html.Hr(),  # horizontal line

            # For debugging, display the raw contents provided by the web browser
            html.Div('Raw Content'),
            html.Pre(contents[0:200] + '...', style={
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-all'
            })
        ])

dp = DP()

parse_contents = dp.parse_contents


# TOP NAV/INFO BAR + INITIATE SESSION
dp.info_bar = html.Div([
        dbc.Row([
            dbc.Col([
                html.Div(children='JobDX',
                        # style = tab_selected_style,
                        style={
                        'font-family':'sans-serif',
                        'fontSize':21,
                        'textAlign':'center',
                        'padding':3,
                        'margin-top': 0,
                        'margin-right': 0,
                        'color':'green',
                        'backgroundColor':'honeydew',
                        },
                ),
            ],width = {'size': 1}
            ),
            # Initiate session id
            dbc.Col(
                dbc.Row([dbc.Button('START',id='init_session_id', color="success",outline=True),
                    dcc.Input(id='uid', type='text',size='20', placeholder = 'enter id',style={'backgroundColor':'white','color':'green','width':'100px','margin-left':'0px'}),       
                ]),width = {'size':2}
            ),
            dbc.Col(id = 'current_process',width = {'size':1}
            ),
            dbc.Col(id = 'progress_bar',width = {'size':3}
            ), 
            dbc.Col([
                # # Current set in use
            ],width = {'size':4}
            ),
            dbc.Col(id = 'progress_spinner',width = {'size':1}
            ),        
        ])
    ],
)

dp.progress_bar = [
    # Progress bar
    dbc.Row(html.Div(id='progress_name',style={'font-size':'70%','margin-top':'10px'},children='')),
    html.Div(
        [
            dcc.Interval(id="interval", n_intervals=0, interval=250),
            dbc.Progress(id="progress"),
        ]
    ),
]

# Top Margin (covered by top bar)
dp.top_margin = html.Div('#### space',style = {'margin-bottom' : '70px'})

# Pop-up / dialogue

dp.modal = html.Div(
    [

        dbc.Modal(
            [
                dbc.ModalHeader(id='modal_header',style={'backgroundColor':'wheat'}),
                dbc.ModalBody(id='modal_body'),
                dbc.ModalFooter([
                    dbc.Button("SHOW FULL", id="show_full", color='success',outline=True ),
                    dbc.Button("CLOSE", id="close_modal", color='danger',outline=True  ),       
                ],style={'backgroundColor':'snow'}),
            ],
            id="modal",
            size='lg',
            scrollable=True,
            keyboard =True,
        ),
    ]
)




# ONET Analysis - lookup title
#   onet_treemap    &     onet_table

df0 = pd.DataFrame(columns = [' '])

dp.onet_table = html.Div([
    dbc.Row([dcc.Markdown(''' 
# JOB TITLES  

**Features:**   &#9658; Search among a thousand ONET-occupations with 50.000 alternate names for titles with keywords  &#9658; Study how title-keywords are shared between titles, occupations, groups and sectors  
&#9658; Interactive table and plots

**Planned:** &#9655 Include task descriptions  &#9655 Expand to BOLD job titles and task descriptions  &#9655 Integrate NLP
    ''')],style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10, 
               }),
        html.Div([
            dbc.Row([
                dbc.Button('GET',id='onet_submit', color="success", outline=True),
                dcc.Markdown('#### occupations and titles containing the character string ',style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10}),
                dcc.Input(id='onet_search_string', placeholder='search string', value = 'nurse', type='text', size='50',style = {'width':'200px','backgroundColor':'snow'}),                   
                            ])
        ],style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10, 'vertical-align':'center'
               }),
        html.Div(id='onet_tabs')
])

dp.onet_tabs =  dcc.Tabs([
            dcc.Tab(label='TABLE',
                children=[dp.dashtable(df0,'datatable-interactivity')
                ]
            ),
            dcc.Tab(label='BAR PLOT', 
                # style=tab_style, 
                # selected_style=tab_selected_style,
                children=[
                    html.Div(id='datatable-interactivity-container_bars'),
                ]
            ),
            dcc.Tab(label='SUNBURST', 
                # style=tab_style, 
                # selected_style=tab_selected_style,
                children=[
                    html.Div(id='datatable-interactivity-container_sunburst'),
                ]
            ),
        ])

# Get job descriptions

dp.jd_table = html.Div([
    dbc.Row([dcc.Markdown(''' 
# JOB DESCRIPTIONS 

**Features:**   &#9658; Search 1.5M job ads for a job title, company name, keywords  &#9658; Select a max number of hits to extract from the database  &#9658 Extract bullets for keywords, such as 'require'   
&#9658 Apply filters, select/delete/edit rows/columns to interactive table with results. &#9658 Download table as spreadsheet  &#9658 Explore interactive geographical map with job locations                                                   

**Planned:**  &#9655 Regularly Updated database adding millions of more job ads &#9655 Time slider &#9655 Generalize "typical" jobs for jobs in the table &#9655 GIS applications for map 


    ''')],style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10, 
               }),
    html.Div([dp.modal]),
    dcc.Markdown('## BUILD TABLE: ',style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10}),
    dbc.Row([
        dbc.Button('GET',id='jd_submit', color="success",outline=True),
                dcc.Input(id='jd_max_items', 
            placeholder = '#items', 
            type='number', 
            value = 1000,
            style={ 'backgroundColor':'snow','width':'90px'}
        ),
        dcc.Markdown('#### job-descriptions with ',style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10}),
        dcc.Input(id='jd_search_string', placeholder='search string', value = 'nurse',type='text', size='50',style = {'width':'150px','backgroundColor':'snow'}),
        dcc.Markdown('#### in the ',style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10}),
        html.Div(
            dcc.Dropdown(
                id='jd_field',
                options=[
                    {'label': 'Title', 'value': 'Title'},
                    {'label': 'Company', 'value': 'Company'},
                    {'label': 'Description', 'value': 'Description'}
                ],
                placeholder = 'search field',
                value = 'Title',
                style = {'width':'100px','backgroundColor':'snow','height':'30px'}
            ),
        ),
        dcc.Markdown('#### field, and display bullets with ',style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10}),
        dcc.Input(id='jd_bullets_search_string', placeholder='all bullet headers containing', value = 'duties,responsib,functions',type='text', size='50',style={'width':'160px','backgroundColor':'snow'}), 
        dcc.Markdown('#### in headers. ',style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10}),
        dcc.Dropdown(
            id = 'jd_duplicates',
            options=[
                {'label': 'Show', 'value': 'keep'},
                {'label': 'Hide', 'value': 'drop'}
            ],
            value='drop',
            style = {'width':'100px','backgroundColor':'snow','height':'30px'}
        ),
        dcc.Markdown('#### duplicates (>80% similar).',style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10}),  
    ],style={'marginLeft': 10, 'marginRight': 10, 'marginTop': 10, 'marginBottom': 10, 'vertical-align':'center'
               }),
    html.Div(id='jd_progress'),
    html.Div(id='jd_tabs'),
]) 

# Tabs for get job description table + figs; to be published by callback
dp.jd_tabs = dcc.Tabs([
        dcc.Tab(label='TABLE',
            children=[
                dbc.Col([
                    html.Div(id='jd_download_div'),
                    dp.dashtable(df0,'jd_datatable-interactivity'),
                ])
            ]
        ),
        dcc.Tab(label='BULLETS', 
            children=[
                html.Div(id='jd_download_bullets_div'),
                html.Div(id='jd_datatable-bullets_only-container'),
            ],
        ),
        dcc.Tab(label='MAP', 
            children=[
                html.Div(id='jd_datatable-interactivity-container'),
            ],
        ),
    ])

dp.uploads = html.Div([dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # Allow multiple files to be uploaded
        multiple=True
    ),
    html.Div(id='output-data-upload'),
])


# Archetypes / Topics / Personas
dp.personas = html.Div([
            dbc.Row(html.Div('__________________________________')),
            dbc.Row(dcc.Markdown('# personas')),

            # Build Archetypes / Topics / Personas
            dbc.Row(
                [  
                    # button: submit
                    # dbc.Col(html.Div([
                    #     html.Div('Submit'),
                    # html.Button(id='arcs_submit', type='submit', children='BUILD')
                    #     ]),width = 2
                    # ),
                    # dropdown: field to do n-gram analysis on
                    dbc.Col(html.Div([
                        html.Div('Based on'),
                        dcc.Dropdown(
                                id='ngrams',
                                options=[
                                    {'label': 'titles', 'value': 'title'},
                                    {'label': 'descriptions', 'value': 'description'}
                                ],
                                #value='select',
                            
                            )
                        ]), width = 2        
                    ),
                    # dropdown: number of archetypes / topics
                    dbc.Col(html.Div([
                        html.Div(' #Personas'),
                        dcc.Dropdown(
                            id='n_arcs',
                            options=[{'label': k, 'value': k}
                                    for k in range(1, 16)],
                            value=4,
                            multi=False,
                            style={'width':'100px'}
                            ),
                        ]), width = 2 
                    ),
                    # input field: display/threshold
                    dbc.Col(html.Div([
                        html.Div('Cut at %'),
                        dcc.Dropdown(
                            id='arcs_thresh',
                            options=[{'label': str(k) , 'value': k/100}
                                    for k in range(1,100)],
                            value=30/100,
                            multi=False
                            ),
                        # html.Div('threshold'),
                        # dcc.Input(id='arcs_thresh', value=0.05, type='number')
                        ]), width = 2 
                    ),
                    # input field: display/normalize
                    dbc.Col(html.Div([
                        html.Div('Normalize:'),
                        dcc.Dropdown(
                                id='norm_arcs',
                                options=[
                                    {'label': 'None', 'value': 'none'},
                                    {'label': 'Sum of Keyword values in Persona =1', 'value': 'arcs_sum'},
                                    {'label': 'Sum of Persona values in Keyword =1 ', 'value': 'ngrams_sum'},
                                    {'label': 'Max Keyword value in Persona =1', 'value': 'arcs_scale'},
                                    {'label': 'Max Persona value in Keyword =1 ', 'value': 'ngrams_scale'},
                                    {'label': 'Persona scalar product w self =1', 'value': 'arcs_dot'},
                                    {'label': 'Keyword scalar product w selt =1 ', 'value': 'ngrams_dot'},
                                ],
                                value='ngrams_sum'
                            ),
                        ])
                    ),
                ]
            ),

            # PERSONA FIGURES
            dbc.Row([
                html.Div([
                    dcc.Graph(
                        id='arcs_clustermap'
                    )
                ])
            ]),

            dbc.Row([
                html.Div([
                    dcc.Graph(
                        id='arcs_sorted'
                    )
                ])
            ]),


        ] 
    )

dp.app_layout = dcc.Tabs([
        dcc.Tab(label='JOB DESCRIPTIONS', 
            style=tab_style, 
            selected_style=tab_selected_style,
            children=[
                dp.jd_table,
            ]
        ),
        dcc.Tab(label='ONET', 
            style=tab_style, 
            selected_style=tab_selected_style,
            children=[
                dp.onet_table,
            ]
        ),
        dcc.Tab(label='DISAMBIGUATION', 
            style=tab_style, 
            selected_style=tab_selected_style,
            children=[
                dp.personas,
            ]
        ),
        dcc.Tab(label='UPLOAD EXCEL',
            style=tab_style, 
            selected_style=tab_selected_style,
            children=[
                dp.uploads
            ]
        ),
    ]),

###############
# APP LAYOUT
##############

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css',dbc.themes.BOOTSTRAP]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.config.suppress_callback_exceptions = True
server = app.server

app.layout = html.Div([
    dp.info_bar,
    # the callback 'initialize_session' places dp.app_layout inside the html.Div
    html.Div(id="app layout")
],style={'fontSize':14, 'font-family':'sans-serif','margin-left':'50px'})




###############
# APP CALLBACKS
##############




#########################################################################
# INITIALIZE SESSION

# Initialize user session

@app.callback([
            Output('app layout','children'),
            Output('uid','style'),
            Output('progress_bar','children')],
            [Input('init_session_id','n_clicks')],
            [State('uid','value'),
             State('uid','style')]
)
def initialize_session(n_clicks,uid,style):
    if n_clicks is None:
        raise PreventUpdate
    elif uid is None:
        raise PreventUpdate
    else:
        # change color of uid box
        uid_newstyle = {'color':'green','backgroundColor':'honeydew','textAlign':'center','width':'100px','font-size':20}
        # initialize session container and objects
        print('Initialize uid=',uid)
        session[uid] = SessionContainer(uid=uid)
        sess = session[uid]

        sess.regis   = Regis(uid=uid)
        sess.regis.set('progress','0')
        sess.regis.set('current_process','')
        sess.regis.set('progress_name','')

        sess.jd      = JobDescriptions(uid=uid)
        sess.uploads = {}
        # initialize app layout and change uid background
        return dp.app_layout, uid_newstyle,dp.progress_bar


#########################################################################
# ONET ANALYSIS

# Search onet database and make table
@app.callback(
    [
    Output('onet_tabs','children'),
    Output('datatable-interactivity','data'), 
    Output('datatable-interactivity','columns')],
    [Input('onet_submit','n_clicks')],
    [State('onet_search_string','value'),
     State('uid','value')])
def update_table(n_clicks,string,uid):
    if n_clicks is None:
        raise PreventUpdate
    else:
        sess = session[uid]
        regis = sess.regis
        regis.set('current_process','Disambiguating title')
        gd.onet.disambiguation_evaluator(string)
        regis.set('current_process','Building dashtable')
        df = gd.onet.titles[['O*NET-SOC Code','high-level aggregation','intermediate aggregation','major group','detailed occupation','Title','Alternate Title']].drop_duplicates().sort_values(by='O*NET-SOC Code')
        # sess.dashtable_onet = DashTable(df, id = 'onet_table-sorting-filtering', page_size = 10000)    # FIRST TIME create dashtable object 
        sess.dashtable_onet = dp.dashtable(df, id = 'onet_table-sorting-filtering')    # FIRST TIME create dashtable object
        dashtable_onet = sess.dashtable_onet
        regis.set('current_process','') 
        columns=[
            {"name": i, "id": i, "deletable": True, "selectable": True} for i in df.columns
        ]
        return dp.onet_tabs,df.to_dict('records'),columns

# Plot results: Bar chart and Sunburst
@app.callback(
    [Output('datatable-interactivity-container_sunburst', "children"),
    Output('datatable-interactivity-container_bars', "children")],
    [Input('datatable-interactivity', "derived_virtual_data"),
     Input('datatable-interactivity', "derived_virtual_selected_rows")],
     [State('onet_search_string','value'),
    State('uid','value')])
def update_graphs(rows, derived_virtual_selected_rows,search_string,uid):
    if pd.DataFrame(rows).empty:
        raise PreventUpdate 
    else:
        if derived_virtual_selected_rows is None:
            derived_virtual_selected_rows = []
        df = pd.DataFrame(rows)
        colors = ['#7FDBFF' if i in derived_virtual_selected_rows else '#0074D9'
                for i in range(len(df))]


        # **BAR CHART** fig1

        ## ORDER: Sort aggregations and occupations by SOC Code  
        df  = df.sort_values(by='O*NET-SOC Code')[['O*NET-SOC Code','high-level aggregation','major group', 'detailed occupation','Title','Alternate Title']]

        ## SELECT columns to include in bar chart
        cols = ['high-level aggregation','major group','Title','Alternate Title']

        ## STACK dataframe to match plot format 
        cnt = df[cols].apply(Counter)
        df_cnt = pd.DataFrame(pd.DataFrame(cnt)[0].to_dict()).fillna(0)
        # df_cnt = df_cnt.sort_values(by='detailed occupation',ascending=False).sort_values(
        #     by='major group',ascending=False).sort_values(by='high-level aggregation',ascending=False)
        df_stack = pd.DataFrame(df_cnt.T.stack()).reset_index()
        df_stack.columns = ['Level','Name','Count']

        # 2. Create color-scale from SOC-codes and add a column 'color'
        # df = df0.copy()
        df['color'] = df['O*NET-SOC Code'].apply(lambda x: eval(x.replace('-','')))
        soc2name_dic = df.groupby('color').agg(list).apply(lambda x: np.concatenate(x.values),axis = 1).apply(set).apply(list).to_dict()
        from collections import defaultdict

        name2soc_dic = defaultdict(list)
        for k,value in soc2name_dic.items():
            for v in value:
                name2soc_dic[v].append(k)
        colors = pd.Series(name2soc_dic).apply(np.mean).apply(int).to_dict()
        df_stack['colors'] = df_stack['Name'].apply(lambda x: colors[x])

        # 3. Sort df_stack by color (orders columns by SOC-code)
        df_stack = df_stack.sort_values(by = 'colors')
        # 4. Bar Plot
        fig_bar = px.bar(df_stack, x='Level', y='Count', height=800,
                    hover_name = 'Name',
                    color = 'colors',
                    title='Stacked Bar Chart - Hover on individual items')
        fig_bar.update_layout(xaxis={'categoryorder':'array', 'categoryarray':cols})


        # 4. Sunburst treeplot
        df['root'] = search_string
        sunb_path = ['root','major group', 'Title','Alternate Title']

        fig_tree = px.sunburst(df, 
                        path=sunb_path, 
                        color='color', 
                        height = 900,
                        maxdepth = 2,
        #               values='pop',
        #               hover_data=['Alternate Title'],
        #               color_continuous_scale='RdBu',
        #               color_continuous_midpoint=np.average(df['lifeExp'], weights=df['pop'])
                        )

        return dcc.Graph(figure = fig_tree, id = 'onet_tree'),dcc.Graph(figure = fig_bar, id = 'onet_bar') 


###############################################
# UPLOADS

# Upload csv/excel files
@app.callback(Output('output-data-upload', 'children'),
              [Input('upload-data', 'contents')],
              [State('upload-data', 'filename'),
               State('upload-data', 'last_modified'),
               State('uid','value')])

def update_output(list_of_contents, list_of_names, list_of_dates,uid):

    if list_of_contents is not None:
        sess = session[uid]
        children = [
            parse_contents(c, n, d) for c, n, d in
            zip(list_of_contents, list_of_names, list_of_dates)]
        
        return children


########################################################################%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# GET JOB DESCRIPTIONS 

# Create active job description table
@app.callback(
    [
    Output('jd_tabs','children'),
    Output('jd_datatable-interactivity','data'),
    Output('jd_datatable-interactivity','columns'),
    Output('jd_datatable-interactivity','hidden_columns'),
    Output('jd_datatable-interactivity','css'),
    Output('jd_download_div','children')],
    [Input('jd_submit','n_clicks'),
     Input('jd_duplicates','value')],
    [State('jd_search_string','value'),
     State('jd_field', 'value'),
     State('jd_max_items','value'),
     State('jd_bullets_search_string','value'),
     State('uid','value')]) 

def update_table(n_clicks,duplicates,string,field,max_items,bullets_for,uid):
    if n_clicks is None:
        raise PreventUpdate
    else:
        sess = session[uid]
        jd = sess.jd
        jd.get(string,field=field,max_items = max_items)
        if duplicates == 'drop':
            jd.drop_duplicates()
        keystrings = list(bullets_for.split(','))
        n=50
        jd.bullets_column('bullets - listformat',keystrings)
        jd.df['bullets'] = jd.df['bullets - listformat']
        df = jd.df[['title','bullets','company','city','state','coordinates','description','parsed description','all bullet headers']]
        df = df[df['bullets']!='']
        df['parsed description'] = df['parsed description'].apply(str)
        deletable_dic = {'title':False,'bullets':False}
        def deletable(col):
            return deletable_dic.get(col,True)
        columns=[
            {"name": i, "id": i, "deletable": deletable(i), "selectable": True} for i in df.drop(columns=['coordinates','parsed description']).columns
        ]
        hidden_columns = ['description']

        css=[{"selector": ".show-hide", "rule": "display: none"}]

        # Download as csv button
        if len(df) >0 :
            download_button = dbc.Row([
                # Number of job descriptions in table
                dcc.Markdown('### '+str(len(df))+' job descriptions',id = 'jd_tab-info',style = {'margin-left':'50px','padding':'10px','color':'grey'}),
                #  Downlowad Button
                html.A(
                    dbc.Button('DOWNLOAD TABLE', color="success",outline=True,style={'margin-left':'20px'}),
                    id='download-link',
                    download="job_descriptions.csv",
                    href="",
                    target="_blank"
                ),
                ## Total Number of job descriptions in DB
                # dcc.Markdown('### Total database:'+str(jd.collection.doc_count)+' job descriptions',id = 'jd_tab-info-2',style = {'margin-left':'50px','padding':'10px','color':'grey'})
            ])
        else:
           download_button = dbc.Row([html.A()]) # return empty

        return dp.jd_tabs,df.to_dict('records'),columns,hidden_columns,css,download_button

# Geo-map for active job description table
@app.callback(Output('jd_datatable-interactivity-container', "children"),
            [Input('jd_datatable-interactivity', "derived_virtual_data"),
            Input('jd_datatable-interactivity', "derived_virtual_selected_rows")]
     )
            
def update_graphs(rows, derived_virtual_selected_rows):
    if pd.DataFrame(rows).empty:
        raise PreventUpdate 
    else:
        if derived_virtual_selected_rows is None:
            derived_virtual_selected_rows = []
        df0 = pd.DataFrame(rows)
        df = pd.DataFrame(df0['coordinates'].to_dict()).T
        df.columns = ['long','lat']
        df['cnt']  = 1
        n = 50 # characters linewidth of hover text
        df0['title - split'] = df0['title'].apply(lambda string: '<br>'.join( [(string[i:i+n]) for i in range(0, len(string), n)] ) )
        df['text'] = 'Title: ' + df0['title - split']  # + '<br>' + 'Employer: ' + df0['company'] +  '<br>' + 'Location: ' + df0['city'] + ', ' + df0['state'] 
        fig = go.Figure(data=go.Scattergeo(
                lon = df['long'],
                lat = df['lat'],
                text = df['text'],
                hoverlabel = dict(
                    bgcolor = 'yellow',
                ),
                hoverinfo = 'text',
                mode = 'markers',
                marker = dict(
                            size = 2,
                            opacity = 0.8,
                            reversescale = True,
                            autocolorscale = False,
                            symbol = 'circle',
                            line = dict(
                                width=1,
                                color='rgb(102, 0, 0)'
                            ),
                            colorscale = 'Blues',
                            cmin = 10,
                            color = df['cnt'],
                            cmax = df['cnt'].max()
                        )
                ), 
        )
        # fig.update_layout(geo_scope='usa')
        fig.update_layout(
                title = 'In table: '+str(len(df))+' job descriptions',
                geo_scope='usa',
                height = 1000,
                dragmode = 'select',
                plot_bgcolor = 'white',
                paper_bgcolor = 'white',
            )
        return dcc.Graph(figure = fig, id = 'jd_map') 

# Download active job description table as csv

@app.callback(
    [dash.dependencies.Output('download-link', 'href'),
    dash.dependencies.Output('jd_tab-info', 'children')],
    [dash.dependencies.Input('jd_datatable-interactivity', "derived_virtual_data")])
def update_download_link(rows):
    if rows is None:
        raise PreventUpdate
    df = pd.DataFrame(rows)
    if 'title' not in df.columns:
        raise PreventUpdate
    # dff = df[['title','bullets','company','city','state','all bullet headers']]
    dff = df
    if 'all bullet headers' in dff.columns:
        dff['all bullet headers'] = dff['all bullet headers'].apply(lambda x: ' '.join(x))
    exclude_columns = ['parsed description','coordinates']
    for col in exclude_columns:
        if col in dff.columns:
            dff = dff.drop(columns=[col])
    csv_string = dff.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8,%EF%BB%BF" + quote(csv_string)
    tab_len = '### '+str(len(dff))+' job descriptions'
    return csv_string,tab_len

# Download all bullets in active job description table as csv - one bullet per line
@app.callback(
    dash.dependencies.Output('download-bullets-link', 'href'),
    [dash.dependencies.Input('jd_datatable-interactivity', "derived_virtual_data")])
def update_download_link(rows):
    if rows is None:
        raise PreventUpdate
    df = pd.DataFrame(rows)
    if 'title' not in df.columns:
        raise PreventUpdate
    # dff = df[['title','bullets','company','city','state','all bullet headers']]
    dff = pd.Series(df['bullets'].apply(lambda x: x.split('\n')).apply(list).sum())
    csv_string = dff.to_csv(index=False, encoding='utf-8')
    csv_string = "data:text/csv;charset=utf-8,%EF%BB%BF" + quote(csv_string)
    return csv_string

# Dashtable with list of all bullets in active job description table + download button
@app.callback(
    [Output('jd_datatable-bullets_only-container', 'children'),
    Output('jd_download_bullets_div','children')],
    [Input('jd_datatable-interactivity', "derived_virtual_data")])
def update_download_link(rows):
    if rows is None:
        raise PreventUpdate
    df = pd.DataFrame(rows)
    if 'title' not in df.columns:
        raise PreventUpdate
    # dff = df[['title','bullets','company','city','state','all bullet headers']]
    df = pd.DataFrame(pd.Series(df['bullets'].apply(lambda x: x.split('\n')).apply(list).sum()),columns = ['bullets'])
    
    table = dp.dashtable(df,'jd_bullets_only_table')

    # Download bullets as csv button
    if len(df) >0 :
        download_button = dbc.Row([
            # Number of job descriptions in table
            dcc.Markdown('### '+str(len(df))+' bullets',id = 'jd_bullets_tab-info',style = {'margin-left':'50px','padding':'10px','color':'grey'}),
            #  Downlowad Button
            html.A(
                dbc.Button('DOWNLOAD BULLETS', color="success",outline=True,style={'margin-left':'20px'}),
                id='download-bullets-link',
                download="bullets.csv",
                href="",
                target="_blank"
            ),
            ## Total Number of job descriptions in DB
            # dcc.Markdown('### Total database:'+str(jd.collection.doc_count)+' job descriptions',id = 'jd_tab-info-2',style = {'margin-left':'50px','padding':'10px','color':'grey'})
        ])
    else:
        download_button = dbc.Row([html.A()]) # return empty

    return table,download_button



# POP-UP FOR TABLE CONTENT

# OPEN/CLOSE Pop-up / Dialogue
@app.callback(
    [Output("modal", "is_open"),
     Output("show_full",'n_clicks')],
    [Input("jd_datatable-interactivity", "active_cell"), 
     Input("close_modal", "n_clicks")],
    [State("modal", "is_open")],
)
def toggle_modal(n1, n2, is_open):
    if n1 or n2:
        return not is_open,0
    return is_open,0


# de-activate table cell when closing pop-up
@app.callback(Output("jd_datatable-interactivity", "active_cell"),
    [Input("close_modal", "n_clicks")],
    [State("modal", "is_open")],
)
def deactivate_cell(n1, is_open):
    if n1 and not is_open:
        return False,''


# pop-up content
@app.callback(
    [Output("modal_header", "children"),
     Output("modal_body", "children"),
    Output("show_full", "children")],
    [Input("jd_datatable-interactivity", "active_cell"), 
     Input("show_full",'n_clicks')],
    [State('jd_datatable-interactivity', "derived_viewport_data")]
)
def toggle_modal_content(active_cell,show_full,rows):
    if active_cell == None:
        raise PreventUpdate
    else:
        df = pd.DataFrame(rows)
        job = df.iloc[active_cell['row']]
        modal_header = dcc.Markdown('## '+job['title']+'\n **'+job['company']+'** , '+job['city']+', '+job['state'])

        if show_full:
            show_full = show_full%2
        # Show FULL JOB DESCRIPTION in modal popup 
        if show_full:
            modal_body   = job['description']
            button_label = 'GO BACK'
        
        # Show CELL CONTENT in modal popup 
        else:
            modal_body   = job[active_cell['column_id']]
            button_label = 'SHOW FULL'

        if type(modal_body) is list:
            modal_body = '\n'.join(modal_body)

        # show html or markdown
        if '<p>' in modal_body:
            modal_body = html.Iframe(srcDoc = modal_body, width = '100%', height= '500px')
        else:
            modal_body   = dcc.Markdown(modal_body.replace('*#','#').replace('### HEAD:','#### ').replace('**','* ').replace('> ','>').replace(' <','<'). \
                replace('<strong>','**').replace('</strong>','**'))
        
        return modal_header,modal_body,button_label

#########################################################################
# BUILD ARCHETYPES / TOPICS
@app.callback([Output('arcs_sorted','figure'),
            Output('arcs_clustermap','figure'),], 
            [Input('n_arcs','value'),
            Input('arcs_thresh','value'),
            Input('norm_arcs','value'),
            Input('ngrams','value')
            ],[State('uid','value')])

def update_output_get(n_arcs,thresh,norm_type,ngrams,uid):
    if type(ngrams) is not str:
        raise PreventUpdate
    else:
        sess = session[uid]
        regis= sess.regis
        if jd.current_parameters['field'] is not ngrams:
            regis.set('current_process','Analyzing word combinations')
            jd.ngram_analysis(field = ngrams)
        regis.set('current_process','building '+str(n_arcs)+' topics')
        df = jd.build_archetypes(n_arcs).archetypes.f
        vocab = jd.node_vocab.intersection(df.columns)
        df = df[vocab]
        norm_fun = {
            'none'   : (lambda x: x.T),
            'ngrams_sum' : (lambda x: x.apply(norm_sum).T),
            'arcs_sum'   : (lambda x: x.T.apply(norm_sum)),
            'ngrams_scale' : (lambda x: x.apply(scale).T),
            'arcs_scale'   : (lambda x: x.T.apply(scale)),
            'ngrams_dot' : (lambda x: x.apply(norm_dot).T),
            'arcs_dot'   : (lambda x: x.T.apply(norm_dot)),
        }
        dfn = norm_fun[norm_type](df)

        #dfn = df.T.apply(norm_dot)

        def f(i):
            sorted = dfn.sort_values(by=i,ascending=False) #Sort by archetype/topic i
            cut = sorted[sorted[i]>=thresh]
            result = cut.sort_values(by=i)
            return result

        maxrows = int(1+ n_arcs//3)
        cols = 3
        fig = make_subplots(rows=maxrows, cols=cols, horizontal_spacing=0.2)  
        for i in range(n_arcs):
            fig.add_trace( go.Heatmap(  z = f(i),
                                        y = f(i).index,
                                        x = f(i).columns,
                                        xgap = 1,
                                        ygap = 1,
                            ), col = i%cols +1,row = int(i//cols)+1
                )
        fig.update_layout(height=400*maxrows, width=1200, title_text="TOPICS")
        
        dfc =dfn.copy().T
        dfc['str_index'] = 'persona '
        dfc['str_index'] = dfc['str_index'] + dfc.index.map(str)
        dfc = dfc.set_index('str_index',drop=True)
        dfc = clustermap(dfc)


        figc = go.Figure(
                    data=go.Heatmap(
                        z=dfc.values,
                        x=dfc.columns,
                        y=dfc.index,
                        xgap = 0.3,
                        ygap = 0.3
                    ),
                    layout= go.Layout(
                                        width=(n_arcs+1)*60, height=dfc.shape[0]*24, 
                                        yaxis={"title": "WORD COMBINATIONS","tickfont": {"size": 8}, "tickangle": -20},
                                        xaxis={"title": "PERSONAS",
                                            "tickmode": "array","tickvals":dfc.columns,
                                            "tickfont": {"size": 8}, "tickangle": -20}
                    )
        ) 
        


        
        
        regis.set('current_process','')
        return fig, figc




# CURRENT PROCESS
@app.callback(
    Output(component_id='current_process', component_property='children'),
    [Input("interval", "n_intervals")],
    [State('uid','value')]
)
def update_output_div(input_value,uid):
    regis = session[uid].regis
    return regis.get('current_process').decode()

# CURRENT PARAMETERS
@app.callback(
    Output(component_id='current_params', component_property='children'),
    [Input("progress_name", "children")],
    [State('uid','value')]
)
def update_output_div(input_value,uid):
    regis = session[uid].regis
    return regis.get('current_params').decode()

# PROGRESS BAR
@app.callback(
    Output(component_id='progress_name', component_property='children'),
    [Input("interval", "n_intervals")],
    [State('uid','value')]
)
def update_output_div(input_value,uid):
    regis = session[uid].regis
    progname = regis.get('progress_name').decode()
    return progname

@app.callback(  [Output("progress", "value"),
                 Output("progress", "children")], 
                [Input("interval", "n_intervals")],
                [State('uid','value')]
    )
def advance_progress(n,uid):
    regis = session[uid].regis
    progress = int(eval(regis.get('progress')))
    return progress, f"{progress} %" if progress >= 5 else ""





#%%


if __name__ == '__main__':
    if publish_on_web:  
        app.run_server(host = '0.0.0.0', debug=True)
    else:
        app.run_server(port=8080, debug=True)


# # When test-running on Jupyter Notebook:
# uid = 'aaa'
# initialize_session(1,uid,'')
# sess = session[uid]
# jd = sess.jd
# regis = sess.regis
# jd.get('nurse',max_items = 1000)
