# Name #

## Job_Services ##

# Side Projects #
# Job Quality Prediction #

This application is used as a method of identifying the low quality jobs and assigning a score for a given jobs. This service goes beyond words counts method to provide more relevant measurements of the quality of jobs.

This methods filters out irrelevent descriptions such as Equal Employment Act, Contact information, and Auto Gerenated summary(Associate Topics:). Taking the following features into considerations, pass them through PCA to reduce dimension, and predict the quality of jobs using Random Forest. 

    - 'Word_count2': Word count after drop stop-sentence and Stop_words 
    - 'Numbers_count': Number count after drop stop-sentence 
    - 'Certifications_count': Number of certifications/ pulled from a Premium_Match (a string match based on different industry)
    - 'Commonskills_count': Number of  Common skills/ pulled from a Premium_Match (a string match based on different industry)
    - 'Education_count': Number of  Education requirement/ pulled from a Premium_Match (a string match based on different industry)
    - 'hardskills_count': Number of  hard skills/ pulled from a Premium_Match (a string match based on different industry)
    - 'Used_SentenceCount': Number of sentence after drop stop sentence
    - 'Average_Length_Sentence: Average Length of sentences

The current training data sets are 300 self rated jobs with binary response,like or not. The benefits of this methods is that it can accuratly identify the bad quality jobs. The limitation of this method is that the majority of the good jobs have a very close rating. This can be improved by using a different set of training data with larger ranges of response rather than Binary response. 

More features of this application are under construction.

# Prerequisites #

- Full list of packages in the requirements.txt
- Python 3.6 or 3.4.3
- Pip (latest verion)               


# Structure #

- Job_Parsing.py  # Sentence Split and drop
- FeatureCreation.py # Create features based on SentenceList and Drop list
- premium_match_1_2_kai/PremiumMatch.py # Finding skills certification educations based on Job description and industry
                                        # Current Version is still using spacy, need to adjust next step 
- Preprocess2   # Preprocess the data using the above 3 
- Training.py # Training model based on features 
- SavedModel_PCA_RF #File that saved previously trained model 
- Prediction.py # Predict Scores based on trained model

## Data ##

The current training data sets are 300 self rated jobs with binary response,like or not. PCA is trained from 100K jobs.

## Install Packages ##

Download the Job_quality_pkg file

- pip install dist/Job_quality_pkg-0.0.6-py3-none-any.whl
- pip install .

- You need to manually install the following packages 
  - sudo pip install python-igraph
  

Download premium_match_1_3 and SavedModel_PCA_RF folders, and TitleToIndustry.pkl files. 
Premium match package and saved Random Forest, PCA models are inside the folders.

- You may need to adjust to directory when using these functions and folders. 

- For example:
ExistFilePath = '/Users/kaijin/Desktop/Bold_Projects/Rank Jobs/ProductionUse/premium_match_clean/premium_match_1_3/PremiumMatch.py'

currentpath = os.path.dirname(ExistFilePath)

sys.path.insert(0, curentpath)




## Examples ##

### Load the data ###
```python
import Job_quality_pkg
```

```python
MasterTable=pd.read_csv("/Users/kaijin/Desktop/Bold_Projects/Rank Jobs/SupportedDoc/MasterTable_v3_1MJobs.csv",index_col=0)
TestData=MasterTable.iloc[:200000,:]
```

```python
# This package does not take na as imput
# Need to fillna first 
# Company name has a slightly impact on how the sentences are seperated
# Title name impacts how to assign industry
MasterTable["Company"]=MasterTable.Company.fillna('CompanyMissing')
MasterTable["Title"]=MasterTable.Title.fillna('TitleMissing')

```

```python
RatedJobs_Clean=pd.read_csv("SavedModel_PCA_RF/RatedJobs.csv",index_col=0)
```


### Create A score using saved model ###

Input: Description, Company, Title

For a single job, Take str as input and predict the score.
Note: set SingleJobMode=True 
Speed: On average, 66 ms per job 

```python
i=1
JobQuality=Job_quality_pkg.main.Job_quality(TestData.Description[i],TestData.Company[i],TestData.Title[i])
Score_i=JobQuality.ScorePrediction(number_of_processor=1,SingleJobMode=True)
```

For batch of jobs, take iterable such as list or panda table's column as input
Note: SingleJobMode=True
Speed: On average, for 1 processor takes 32 ms per job
                   for 8 processor takes 8-12 ms per job
                   Depends on the size of imput list: the more jobs, the faster the speeds. Normally with 10k jobs, it will be close to 8.5 ms per job. 

```python
JobQuality=Job_quality_pkg.main.Job_quality(TestData.Description,TestData.Company,TestData.Title)
Score_i=JobQuality.ScorePrediction(number_of_processor=1,SingleJobMode=False )
```



### Preprocess ###

```python
%%time
# To create matrix directly, not suggested
# Try to use CreateMatrix_DedupVersion
PreProcessor=Job_quality_pkg.Preprocess2.Preprocess_matrix(TestData.Description,TestData.Company,TestData.Title)
Matrix_X=PreProcessor.CreateMatrix(number_of_processor=8)
```

```python
%%time
# To create matrix after applying deduplication
# Faster than CreateMatrix when there are more input data 
PreProcessor=Job_quality_pkg.Preprocess2.Preprocess_matrix(TestData.Description,TestData.Company,TestData.Title)
Matrix_X=PreProcessor.CreateMatrix_DedupVersion(number_of_processor=8)
```

    Start Finding Industry
    Takes  157.6193699836731  second to find Industry
    Start extracting sentences
    Takes  359.9080231189728  seconds to extract sentences
    Takes  196.60550904273987  seconds to deduplicate
    93403  unique jobs out of  200000 Input jobs
    Start Creating X matrix, it may takes a little longer, estimated time 15ms per job
    commonskills  certifications  hardskills  education  wordscount2  numbercount  sentencecount   Ave_sent_length
    [3, 0, 1, 9, 397, 8, 46, 13.08695652173913]
    Takes  1265.018786907196  seconds to create matrix
    Start Map scores to the duplicated jobs
    CPU times: user 7min 44s, sys: 1min 25s, total: 9min 9s
    Wall time: 27min 10s

```python
%%time
# To create x matrix for 300 Jated Jobs
PreProcessor=Job_quality_pkg.Preprocess2.Preprocess_matrix(RatedJobs_Clean.Description,
                                                           RatedJobs_Clean.Company,RatedJobs_Clean.Title)
Matrix_X_RJ=PreProcessor.CreateMatrix(number_of_processor=8)
```

### Train PCA ###


```python
Job_quality_pkg.Training.PCA_ForJobs(Matrix_X).TrainingPCA(n_comp=2)
```

    Ratio explained:  [0.99151631 0.00514832]
    Singular Value [69028.25588033  4974.04655569]
    PCA score(everage Log Like) -26.575865347355414
    Model is saved at  SavedModel_PCA_RF/PCA_Model.sav

    array([[137.99439941,  -5.58760942],
           [ 22.35631951,  -6.71108871],
           [104.69454739,   1.53380941],
           ...,
           [-62.57751114,  -3.33440967],
           [ 36.84826085,  -3.61344558],
           [-69.34700698,  -9.09620476]])



###Predict PCA ###


```python
%%time
filename="SavedModel_PCA_RF/PCA_Model_init.sav"
loaded_model = pickle.load(open(filename, 'rb')) 
PCs_rj=Job_quality_pkg.Prediction.PCA_Prediction(Matrix_X_RJ,loaded_model).PredictPCs()
```

    CPU times: user 5.76 ms, sys: 6.89 ms, total: 12.7 ms
    Wall time: 11.9 ms



```python
PCs_TestData=Job_quality_pkg.Prediction.PCA_Prediction(Matrix_X,loaded_model).PredictPCs()
```

### Train Random Forest ###


```python
Job_quality_pkg.Training.RandomForest_ForJobs(PCs_rj,RatedJobs_Clean.Rating_Perference).TrainingRandomForest()
```

    Features Importance:  [0.96001358 0.03998642]
    Mean Square Error:  0.08075318446795136
    Model is saved at  SavedModel_PCA_RF/RandomForest_Model.sav


### Predict RF ###


```python
from Job_quality_pkg import Job_Parsing
```


```python
filename="SavedModel_PCA_RF/RandomForest_init.sav"
loaded_model = pickle.load(open(filename, 'rb')) 
CheckPredictions=Job_quality_pkg.Prediction.RandomForest_Prediction(PCs_TestData,loaded_model).Predict_Score()

```


```python
# CheckPredictions is Predicted scores for job quality from 200000 test data 
len(CheckPredictions)
```
    200000
    On average, takes less than 10 ms to predict a score with 8 processors on a local machine.


### Extral examples: Sentence Extractions and Drops ###

```python
from Job_quality_pkg import Job_Parsing
```

```python
%%time
## Sentence Split
# Input a str of Description and a str of company  
# output a list of sentences
SentenceList=Job_Parsing.Job_data_clean(Description, Company).SentenceSplit_AdjustHTML()
```

    CPU times: user 3.99 ms, sys: 32 µs, total: 4.02 ms
    Wall time: 4.03 ms

```python
%%time
## Sentence dropped
# Input a list of sentences
# output a list of dropped sentences
DropList=Job_Parsing.DropSentence(SentenceList).Checking_words()
print(DropList)
```

    ['For more information, visit fallonhealth.org.']
    CPU times: user 13.7 ms, sys: 688 µs, total: 14.4 ms
    Wall time: 13.7 ms


# Who do I talk to? #

* Repo owner or admin