#%%
import dash
from dash.dependencies import Input, Output
import dash_html_components as html
from numpy import *
import dash_table
import pandas as pd


class DashTable:
    def __init__(self, df , id = 'dashtable', page_size = 15, deletable = True, selectable = True, action = 'custom', app = dash.Dash(__name__),**kwargs):
        self.kwargs     = kwargs
        self.df         = df
        self.action     = action
        self.deletable  = deletable
        self.selectable = selectable
        self.id         = id
        self.app        = app
        self.page_size  = page_size

    def layout(self): 
        table = html.Div([dash_table.DataTable(
            id=self.id,
            editable=True,
            columns=[
                 {"name": i, "id": i, "deletable": self.deletable, "selectable": self.selectable} for i in sorted(self.df.columns)
            ],
            data=self.df.to_dict('records'),
            page_current= 0,
            page_size= self.page_size,
            page_action=self.action,
            filter_action=self.action,
            filter_query='',
            sort_action=self.action,
            sort_mode='multi',
            sort_by=[],
            row_deletable=self.deletable,
            column_selectable={True:"multi"}.get(self.selectable,False),
            row_selectable   ={True:"multi"}.get(self.selectable,False),
            selected_columns=[],
            selected_rows=[],
            selected_cells=[],
            style_data={'whiteSpace': 'pre-line'},
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'floralwhite'
                }
            ],
            style_table={
                'maxWidth': '100%',                       
                'overflowX': 'scroll',
                'maxHeight': '1000px',
                'overflowY': 'scroll',
                'padding' : '10px'},
            style_cell={
                'height': '30px',
                'textAlign': ['center'],
                # all three widths are needed
                'minWidth': '20px', 'width': '150px', 'maxWidth': '400px',
                'whiteSpace': 'normal',
                'font-family':'sans-serif',
                'font-size':12,
                'backgroundColor': 'papayawhip'
            },
            style_header={'backgroundColor': 'wheat','color':'black','fontWeight':'bold','borderTop': '2px solid sienna','borderBottom': '1px solid sienna','textAlign': ['center']},
            style_filter={'backgroundColor':'linen','borderBottom': '2px solid silver'},
        **self.kwargs
        )])
        return table

    def split_filter_part(self,filter_part):
        operators = [['ge ', '>='],
                ['le ', '<='],
                ['lt ', '<'],
                ['gt ', '>'],
                ['ne ', '!='],
                ['eq ', '='],
                ['contains '],
                ['datestartswith ']]
        # if filter is formula (lambda x - function):
        if 'y=' in filter_part :
            pos = filter_part.find('y=')
            name_op = filter_part[:pos]
            formula = filter_part[pos+2:-1]
            name = name_op[name_op.find('{') + 1: name_op.rfind('}')].strip()
            op = 'formula'
            return name, op, formula

        # if filter is operator
        for operator_type in operators:
            for operator in operator_type:
                if operator in filter_part:
                    name_part, value_part = filter_part.split(operator, 1)
                    name = name_part[name_part.find('{') + 1: name_part.rfind('}')]

                    value_part = value_part.strip()
                    v0 = value_part[0]
                    if (v0 == value_part[-1] and v0 in ("'", '"', '`')):
                        value = value_part[1: -1].replace('\\' + v0, v0)
                    else:
                        try:
                            value = float(value_part)
                        except ValueError:
                            value = value_part

                    # word operators need spaces after them in the filter string,
                    # but we don't want these later
                    return name, operator_type[0].strip(), value

        return [None] * 3
    
    def filter_callback(self):
        @self.app.callback(
            Output(self.id, 'data'),
            [Input(self.id, "page_current"),
            Input(self.id, "page_size"),
            Input(self.id, 'sort_by'),
            Input(self.id, 'filter_query')])
        def update_table(page_current, page_size, sort_by, filter):
            filtering_expressions = filter.split(' && ')
            dff = self.df.copy()
            for filter_part in filtering_expressions:
                col_name, operator, filter_value = self.split_filter_part(filter_part)
               
                if operator == 'formula':
                    fun = eval('lambda x:'+filter_value)
                    try:
                        # todo: apply equation as boolean mask
                        dff = dff[dff[col_name].apply(fun)]
                    except:
                        # apply function to column values
                        dff[col_name] = dff[col_name].apply(fun)

                elif operator in ('eq', 'ne', 'lt', 'le', 'gt', 'ge'):
                    # these operators match pandas series operator method names
                    dff = dff.loc[getattr(dff[col_name], operator)(filter_value)]
                elif operator == 'contains':
                    dff = dff.loc[dff[col_name].str.contains(filter_value, case=False)]
                elif operator == 'datestartswith':
                    # this is a simplification of the front-end filtering logic,
                    # only works with complete fields in standard format
                    dff = dff.loc[dff[col_name].str.startswith(filter_value)]

            if len(sort_by):
                dff = dff.sort_values(
                    [col['column_id'] for col in sort_by],
                    ascending=[
                        col['direction'] == 'asc'
                        for col in sort_by
                    ],
                    inplace=False
                )

            page = page_current
            size = page_size
            return dff.iloc[page * size: (page + 1) * size].to_dict('records')
    


# df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder2007.csv')
# dashtable = DashTable(df,page_size = 10,id = 'putte')

# #%%

# app = dash.Dash(__name__)
# app.layout = dashtable.layout
# dashtable.callback()

# if __name__ == '__main__':
#     app.run_server(debug=True)