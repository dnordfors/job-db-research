import numpy as np
import html2text
import re
from bs4 import BeautifulSoup

from sentence_classifier.jobs_modifier.long_sentence_split import Long_Sentence_Split
from sentence_classifier.common.nlp_basic import Text_Clean



def filter_wrong_sentence(Sentencelist):
    """
    Filter out sentence list are empty and have only one letter
    """
    Sentencelist=list(filter(None, Sentencelist))
    Sentencelist=[s for s in Sentencelist if len(s)>1]
    return(Sentencelist)

class Job_Sentence_Split:

    def __init__(self, headers_dict):
        self.caps = "([A-Z])"
        self.prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
        self.suffixes = "(Inc|Ltd|Jr|Sr|Co)"
        self.starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
        self.acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
        self.acronyms2 = "(i)[.](e)[.]"
        self.acronyms3 = "(e)[.](g)[.]"
        self.websites = "[.](com|net|org|io|gov|edu)"
        self.websites2 = "(www|WWW)[.]"
        self.websites3 = "(https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+)[.]"
        self.websites4 = "[.](com|net|org|io|gov|edu)"
        self.h = html2text.HTML2Text()
        self.headers_dict = headers_dict


    def split_into_sentences(self, text, CompanyName, HTML=False, LargeSentenceAdjustMode=True):
        
        if HTML==False:
            text = text.replace("\n","<stop>")

        text = " " + text + "   "
        text = text.replace("\n","  ")
        text = re.sub(self.prefixes,"\\1<prd>",text)
        text = re.sub(self.websites,"<prd>\\1",text)
        text = re.sub(self.websites2,"\\1<prd>",text)
        text = re.sub(self.websites3,"\\1<prd>",text)
        if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
        text = re.sub("\s" + self.caps + "[.] "," \\1<prd> ",text)
        text = re.sub(self.acronyms+" "+self.starters,"\\1<stop> \\2",text)
        text = re.sub(self.acronyms2, "\\1<prd>\\2<prd>",text)
        text = re.sub(self.acronyms3, "\\1<prd>\\2<prd>",text)
        text = re.sub(self.caps + "[.]" + self.caps + "[.]" + self.caps + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
        text = re.sub(self.caps + "[.]" + self.caps + "[.]","\\1<prd>\\2<prd>",text)
        # SYS.HR.NUR.001
        text = re.sub("([A-Z]{2,3})" + "[.]" + "([A-Z]{2,3})" + "[.]" + "([A-Z]{2,3})" +"[.]","\\1<prd>\\2<prd>\\3<prd>",text)
        # LL.C.
        text = re.sub("([A-Z]{2})" + "[.]" + self.caps + "[.]","\\1<prd>\\2<prd>",text)
        # LL.C
        text = re.sub("([A-Z]{2})" + "[.]" + self.caps ,"\\1<prd>\\2",text)
        text = re.sub(" "+self.suffixes+"[.] "+self.starters," \\1<stop> \\2",text)
        text = re.sub(" "+self.suffixes+"[.]"," \\1<prd>",text)
        text = re.sub(" " + self.caps + "[.]"," \\1<prd>",text) # one captalized letter
        if '."' in text: 
            text = text.replace('."' ,'".')
        if '.\"' in text: 
            text = text.replace('.\"', '\".')
        if "!\"" in text: 
            text = text.replace('!\"','\"!')
        if '?\"' in text: 
            text = text.replace('?\"' ,'\"?')
        text = text.replace(".",".<stop>")
        text = text.replace("?","?<stop>")
        text = text.replace("!","!<stop>")
        text = text.replace("   ","<stop>")
        text = text.replace(";","<stop>")
        text = text.replace("<prd>",".")    
        text = text.replace(" * ","<stop>* ")
        text = text.replace("###","<stop>")
        text = text.replace("To apply","<stop>To apply")
        text = text.replace("\\.",".<stop>")
        text = text.replace("\\","")   
        sentences = text.split("<stop>")
        sentences = sentences[:-1]
        
        ### Adjust Large Chunck Of Words
        ### Split only when Sentences length is too large 
        ## 16776/ 232/ 60041/4207/52387
        if LargeSentenceAdjustMode:
            sent2=[]
            for sentence in sentences:
                if len(sentence.split(" "))<=40:
                    sent2.append(sentence)
                else:
                    sent2 = sent2 + Long_Sentence_Split().sentencesplit_longsentencemode(sentence, CompanyName, self.headers_dict)

            sentences=sent2

        sentences = [s.strip() for s in sentences]
        sentences = [" ".join(s.split()) for s in sentences] # remove spaces

        return sentences
   
    def sentence_split_adjustHTML(self, html_message, companyname, stemlem=False):
        """
        #Give a file and split into sentences 
        ## poential problem if a sentences is ended by spaces, Model can not tell
        #if "driver" not in  [x.lower() for x in MasterTable.Title[i].split()] and "cdl-a" 
        # not in  [x.lower() for x in MasterTable.Title[i].split()]:
        
        Speed: 2min 45s for 10 k jobs 
        """    
        if bool(BeautifulSoup(html_message, "html.parser").find()):
            h = self.h 
            h.ignore_images = True
            h.ignore_links = True
            html_message2 = h.handle(html_message)
            html_message2
            if len(html_message2)<2:
                html_message2=BeautifulSoup(html_message, "html5lib").get_text()
            
            sentencesplit = self.split_into_sentences(html_message2, companyname, True)
        else:
            sentencesplit = self.split_into_sentences(html_message, companyname)
        
        if stemlem:
            # Stem and lemtize the sentence and clean the sentence
            sentencesplit=[Text_Clean().sentence_to_wordlist(s) for s in sentencesplit]
        
        return(filter_wrong_sentence(sentencesplit))

