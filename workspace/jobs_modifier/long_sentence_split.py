from sentence_classifier.jobs_modifier.header_check import HeaderCheck
import re
import nltk

def longSent_postag_check(first_word, second_word, first_word_tag, space_indicator, constant_dict):
    """
    Determine wether 2 words belong in a same sentence.
    
    type of the first_wordcheck "DT" or "IN" 
    IN: Preposition or subordinating conjunction
    DT: Determiner
    VBD: Verb, past tense
    VBG: Verb, gerund or present participle
    VBN: Verb, past participle
    VBP: Verb, non-3rd person singular present
    VBZ: Verb, 3rd person singular present

    first word   "'s"
    first word   "A-Z" + "." in case break the certification abbreviation
    first_word end with "(" 
    second_word is all caplialized with more than one letters
    second_word end with ,
    first word in ["and","or","nor"]
    '-' need to check again for dash_valid_check exist in dash_valid_check list.
    """

    if first_word_tag in ["IN","DT"]+constant_dict["tag_addtion_type"]: 
        return(True)
    elif first_word[-2:] in constant_dict["first_word_end_2"]:
        return(True)
    elif first_word[-1] in constant_dict["first_word_end_1"]:
        return(True)
    elif first_word[-1] in '-':
        if len(first_word)>1 and space_indicator == False:
            return(True)
        # - only valid in a fixed list
        #if first_word[:-1].lower() in dash_valid_check:
        #    return(True)
        #if second_word.lower() in dash_valid_check:
        #    return(True)
    elif first_word in constant_dict["firt_word_include"]:
        return(True)
    elif first_word[-1] in constant_dict["spcial_characers_join"]:
        return(True)
    elif bool(re.match(r"[A-Za-z][/.?]",first_word)) and bool(re.match(r"[A-Za-z][/.?]",second_word)):
        return(True)
    elif len(second_word)>1 and second_word.isupper():
        return(True)
    elif second_word[-1] in constant_dict["second_word_end"]:
        return(True)
    return(False)
           
def longSent_company_name(first_word, second_word, companyname):
    """
    Determine wether 2 words belong in a same sentence.
    
    Check if both first word and second word in the company name.
    Do not break the company name 
    """
           
    if type(companyname) != float:
        if companyname!= None:
            companynameList= companyname.split(" ")
    else: 
        companynameList=[]
    companynameList = [i.lower() for i in companynameList]
    if first_word.lower() in companynameList and second_word.lower() in companynameList:
        return(True)
           
    return(False)

def longSent_captalize(first_word, second_word):
    """
    Determine wether 2 words belong in a same sentence.
     
    Check if both first word and second word in the company name.
    Do not break the company name 
    """
    if first_word[0].isupper() and second_word[0].isupper():
        return(True)
    return(False)
        
def long_sent_join_word(first_word, second_word, CompanyName, first_word_tag, space_indicator, constant_dict):
    """
    Determine wether 2 words belong in a same sentence.
    """
    if first_word[-1] not in constant_dict["spcial_characers_break"]:
        if longSent_postag_check(first_word, second_word, first_word_tag, space_indicator, constant_dict):
            return(True)
        if longSent_company_name(first_word, second_word, CompanyName):
            return(True)
        if longSent_captalize(first_word, second_word):
            return(True)
    return(False)

def long_sent_combine_short(first_word, second_word, previous_sentence, current_sentence, second_word_tag, constant_dict):
    if first_word[-1] not in constant_dict["spcial_characers_break"]:
        if second_word_tag in constant_dict["tag_addtion_type"]:
            if len(current_sentence) <= 3:
                return(True)
        else:
            if len(current_sentence) <= 5:
                return(True)
    return(False)

def long_sent_bracket(current_sentence, joint_sentence, long_sentence, constant_dict):
    """
    adjust ("(",")") , ("[","]") , ("{","}") , ("<",">")
    """
    for bri in constant_dict["bracket_list"]:
        if bri[0] in joint_sentence and bri[1] not in joint_sentence:
            if bri[1] in " ".join(current_sentence):
                return(True)
            if bri[1] in long_sentence and len(current_sentence)<=3:
                return(True)

    return(False)

def long_sent_headers(joint_sentence, current_sentence, long_sentence, headers_dict):
    """
    decide if joint sentence is a header
    """
    possibel_next_sentence = joint_sentence + " " + " ".join(current_sentence)
    
    if HeaderCheck().identify_header(joint_sentence, headers_dict) == True and HeaderCheck().identify_header(possibel_next_sentence, headers_dict) == False:
        
        if  ":" in long_sentence and  ":" not in joint_sentence:
            return(False)
        else:
            return(True)
    
    return(False) 
                
def long_sent_join_all(first_word, second_word, previous_sentence, current_sentence, CompanyName,
                       joint_sentence, long_sentence, first_word_tag, second_word_tag, space_indicator, headers_dict, constant_dict):
    """
    summary of determine wether 2 sentences belong in a same sentence.
    """
    if long_sent_headers(joint_sentence, current_sentence, long_sentence, headers_dict):
        # if joint sentence is a header, we are not join two sentences 
        return(False)
    
    if long_sent_join_word(first_word, second_word, CompanyName, first_word_tag, space_indicator, constant_dict):
        return(True)
    elif long_sent_combine_short(first_word, second_word, previous_sentence, current_sentence, second_word_tag, constant_dict):
        return(True)
    elif long_sent_bracket(current_sentence, joint_sentence, long_sentence, constant_dict):
        return(True)
    else:
        return(False)

def add_space_check(first_word, second_word, constant_dict):
    """
    Check cases for ["{","[","(",'"',"'"]
                     ["/","&"]
    When there exist a space or not
    """
    if (first_word[-1] in constant_dict["spcial_characers_join"]):
        return(False)
    if (first_word[-1] in constant_dict["spcial_characers_join_end"] and len(first_word)!=1):
        return(False)
    if first_word[-1]=="-":
        if len(first_word)!=1:
            return(False)
    # bool(re.match(r"[A-Za-z][/.?]",first_word))
    if first_word[-1]==".":
        if len(second_word) <= 3:
            if bool(re.match(r"[A-Z]",second_word[0])):
                return(False)

    return(True)


class Long_Sentence_Split:

    def __init__(self):
        spcial_characers_join = ["{","[","(",'"',"'"]
        spcial_characers_join_end =  ["/","&"]
        spcial_characers_break = [":","*"]
        second_word_end = ["," ,'"',")","."]
        firt_word_include = ["and","or","nor"]
        bracket_list = [("(",")") , ("[","]") , ("{","}") , ("<",">")]
        first_word_end_1 = ["&", ","] #lenth 1 first word end 
        first_word_end_2 = ["'s"]
        tag_addtion_type = ["VBD","VBG","VBN","VBP","VBZ","JJ"]
        self.constant_dict = {
            "spcial_characers_join": spcial_characers_join,
            "spcial_characers_join_end": spcial_characers_join_end,
            "spcial_characers_break": spcial_characers_break,
            "second_word_end": second_word_end,
            "firt_word_include": firt_word_include,
            "bracket_list": bracket_list,
            "first_word_end_1": first_word_end_1,
            "first_word_end_2": first_word_end_2,
            "tag_addtion_type": tag_addtion_type,
        }

    def sentencesplit_longsentencemode(self, long_sentence,CompanyName, headers_dict):
        """
        Logic:
        1. find out words that start with capital letters
        2. treat words like NASDAC as one single word
        3. Interjections Determiner will collapse with next sentence
        4. "'s" will collapse with next sentence
        5. Sentence shorter than 6 words will collapse with next short sentence if it`s exist.
        6. Check if company name, collapse
        """
        all_caps_splits=re.findall('[A-Z]*[^A-Z]*', long_sentence)
        inital_sentence_split=[[ x for x in s2i.split(" ")] for s2i in all_caps_splits]
        inital_sentence_split = [wl for wl in inital_sentence_split if len([i for i in wl if i!= ""])>0]
        final_sentence_split=[]
        joint_sentence=""
        
        
        for i in range(len(inital_sentence_split)):
            if i==0:
                joint_sentence = " ".join(inital_sentence_split[0])
            else:
                previous_sentence = inital_sentence_split[i-1]
                current_sentence = inital_sentence_split[i]
                #Record space for larter adjust
                if previous_sentence[-1] == "":
                    space_indicator = True
                else:
                    space_indicator = False
                # remove all empty in sentence and extract word and it's tags
                previous_sentence = list([i for i in previous_sentence if i!= ""])
                current_sentence = list([i for i in current_sentence if i!= ""]) 
                first_word = previous_sentence[-1]
                second_word = current_sentence[0]
                first_word_tag = nltk.pos_tag([first_word])[0][1]
                second_word_tag = nltk.pos_tag([second_word])[0][1]
                # decide wehter to join two sentence
                if long_sent_join_all(first_word, second_word, previous_sentence, current_sentence, CompanyName, joint_sentence, long_sentence, 
                                        first_word_tag, second_word_tag, space_indicator, headers_dict, self.constant_dict):
                    if add_space_check(first_word, second_word, self.constant_dict) == False and space_indicator == False:
                        joint_sentence = joint_sentence + " ".join(current_sentence)
                    else:
                        joint_sentence = joint_sentence + " " + " ".join(current_sentence)
                else:
                    joint_sentence = " ".join(joint_sentence.split())
                    final_sentence_split.append(joint_sentence)
                    joint_sentence = " ".join(current_sentence)
                
                # last sentence case
                if i == len(inital_sentence_split)-1:
                    joint_sentence = " ".join(joint_sentence.split())
                    final_sentence_split.append(joint_sentence)
        # no split case
        if final_sentence_split == []:
            joint_sentence = " ".join(joint_sentence.split())
            final_sentence_split = [joint_sentence]

        return(final_sentence_split)
