import numpy
import itertools
from collections import Counter
from configuration.config import Config
import pickle
from ast import literal_eval
import networkx as nx


config_obj = Config()
mongo_conn = config_obj.mongo_connection


def prepare_adjacency(section_data):

    prune_list = []

    varnames = tuple(sorted(set(itertools.chain(*section_data))))
    # Get a list of all of the combinations you have
    expanded = [tuple(itertools.combinations(d, 2)) for d in section_data]

    expanded = itertools.chain(*expanded)

    expanded = [tuple(sorted(d)) for d in expanded]

    for lists in section_data:

        prune_list.extend(lists)

    p_counter = Counter(prune_list)

    counter_pruned = {k: v for k, v in p_counter.items() if v >= 50}

    varnames = [x.strip() for x in varnames if x in counter_pruned]

    c = Counter(expanded)

    c2 = []

    for x in c:

        if c[x] > 50:

            continue

        c2.append(x)

    for i in c2:

        c.pop(i)

    table = numpy.zeros((len(varnames), len(varnames)), dtype=int)

    for i, v1 in enumerate(varnames):

        for j, v2 in enumerate(varnames[i:]):

            j += i

            table[i, j] = c[v1, v2]

            table[j, i] = c[v1, v2]

    return table, varnames


def create_graph(adjacency_mat, varnames):

    G = nx.from_numpy_matrix(adjacency_mat)
    numbers = list(range(0, len(varnames)))

    mapping = dict(zip(numbers,varnames))

    H = nx.relabel_nodes(G, mapping)

    '''prune base graph'''
    f = [(u, v, d) for (u, v, d) in H.edges(data=True) if d['weight'] >= 10]

    prunded_graph = nx.Graph()

    prunded_graph.add_edges_from(f)

    return prunded_graph



with open("query_file.json", 'r') as fh:
    q = fh.read()

    query = literal_eval(q)

'''section keys : ['EducationProfile', 'CommonSkills', 'JobTitles', 'JobSkills', 'Certifications']'''

keys = ["EducationProfile", "CommonSkills", "JobTitles", "JobSkills", "Certifications"]

with open("/Users/rmohan/knowledge-bases/premium_match_1_1/graph_data/aggregated_Office_resume_data.pkl", "rb") as fh:

    aggregated_data = pickle.load(fh)

for items in keys:

    document = aggregated_data[items]

    adjaceny, varss = prepare_adjacency(document)

    graph = create_graph(adjaceny,varss)

    with open('aggregated_%s.pkl'%(items),'wb') as fh:
        pickle.dump(graph,fh)
