from spacy.matcher import PhraseMatcher
from collections import defaultdict
import networkx as nx


def create_matcher(nlp_object, attribute_lists_zip_object):

    matcher_rule_id = PhraseMatcher(nlp_object.vocab)

    for attribute_name, lists in attribute_lists_zip_object:
        for items in lists:
            try:
                matcher_rule_id.add(attribute_name, None, nlp_object(items))
            except:
                pass

    return matcher_rule_id


def get_tokens_id(matcher,doc, nlp):

    collector = defaultdict(set)

    matches = matcher(doc)

#    print(matches)
    for match_id,start,end in matches:


        rule_id = nlp.vocab.strings[match_id]  # get the unicode ID
        span = doc[start : end]  # get the matched slice of the doc
        #print(rule_id, span.text)
        collector[rule_id].add(span.text)

    return collector

def min_max(graph, keyword):

    maxw, minw = float('-inf'), float('inf')
    for node in graph[keyword]:
        weight = graph[keyword][node]['weight']

        if weight > maxw:
            maxw = weight
        if weight < minw:
            minw = weight

    return maxw, minw

def create_subgraph(parent_graph, keyword):

    maxw , minw = min_max(parent_graph,keyword)

    g1 = nx.Graph()
    for node in parent_graph[keyword]:

        weight = parent_graph[keyword][node]['weight']
        g1.add_edge(keyword,node,weight=weight)

    return g1, maxw,minw



def get_subgraph_data(item, attribute_graph):


    subgraph,upper,lower = create_subgraph(attribute_graph, item)



    intervals = create_intervals_graph(item, subgraph,lower, upper-lower)


    return intervals

def normalize(weight, minw, min_max_diff):

    return (weight - minw)/max(min_max_diff,1)

def create_intervals_graph(item, subgraph, minw, min_max_diff):


    sub_1=[[u,v,normalize(d['weight'], minw, min_max_diff)] for u,v,d in subgraph.edges(data=True) if 0.2 > normalize(d['weight'], minw, min_max_diff) >= 0.1]
    sub_2=[[u,v,normalize(d['weight'], minw, min_max_diff)] for (u,v,d) in subgraph.edges(data=True) if 0.4 > normalize(d['weight'], minw, min_max_diff) >= 0.2]
    sub_3=[[u,v,normalize(d['weight'], minw, min_max_diff)] for (u,v,d) in subgraph.edges(data=True) if 0.6 > normalize(d['weight'], minw, min_max_diff) >= 0.4]
    sub_4=[[u,v,normalize(d['weight'], minw, min_max_diff)] for (u,v,d) in subgraph.edges(data=True) if 0.8 > normalize(d['weight'], minw, min_max_diff) >= 0.6]
    sub_5=[[u,v,normalize(d['weight'], minw, min_max_diff)] for (u,v,d) in subgraph.edges(data=True) if normalize(d['weight'], minw, min_max_diff) >= 0.8]

    return sub_1, sub_2,sub_3,sub_4, sub_5


def locate_intervals(subgraph_intervals,query_skill):

    for interval in subgraph_intervals:
        for items,weight in interval:
            if query_skill == items:
                return(items,weight)
            else:
                pass


def get_sections_data(docid,mongo_client_connection):
    db = mongo_client_connection.resume_db
    collection = db.ResumeParsedData

    data2 = collection.aggregate([
    { "$match": {"DocId": docid }},

    {
        "$project": {
            "JobSkills": {
                "$reduce": {
                    "input": {
                            "$concatArrays": ["$ResumeDetails.WorkProfile.JobSkills.OtherSkills",
                                              ]
                        },
                    "initialValue": [],
                    "in": { "$concatArrays": [ "$$this", "$$value" ] }
                }
            },

            "JobTitles": {
                "$reduce": {
                    "input": {
                            "$concatArrays": [["$ResumeDetails.WorkProfile.JobTitle"]]
                        },
                    "initialValue": [],
                    "in": { "$concatArrays": [ "$$this", "$$value" ] }
                }
            },

            "EducationProfile": {
                "$reduce": {
                    "input": {
                            "$concatArrays": [["$ResumeDetails.EducationProfile.Degree"]]
                        },
                    "initialValue": [],
                    "in": { "$concatArrays": [ "$$this", "$$value" ] }
                }
            },


            "Certifications": {
                "$reduce": {
                    "input": {
                            "$concatArrays": [["$ResumeDetails.Certifications"]]
                        },
                    "initialValue": [],
                    "in": { "$concatArrays": [ "$$this", "$$value" ] }
                }
            },

            "AdditionalSkills": {"$setUnion": [
                "$ResumeDetails.AdditionalSkills.CommonSkills", "$ResumeDetails.WorkProfile.JobSkills.CommonSkills"]}
        ,
            "AdditionalOtherSkills": {"$setUnion": [
                "$ResumeDetails.AdditionalSkills.OtherSkills"]}
        ,

        }
    },
    {"$lookup": {"from": "Skill", "localField": "JobSkills", "foreignField": "_id", "as": "JobSkills"}},
	{"$lookup": {"from": "Skill", "localField": "AdditionalSkills", "foreignField": "_id", "as": "AdditionalSkills"}},
    {"$lookup": {"from": "Skill", "localField": "AdditionalOtherSkills", "foreignField": "_id", "as": "AdditionalOtherSkills"}},
	{"$project": {"_id":0,'JobSkills.Name': 1,'AdditionalOtherSkills.Name':1, 'AdditionalSkills.Name': 1,"EducationProfile":1,"Certifications":1,
                  "JobTitles":1}}

])
    sections={}
    sections["JobTitles"] = []
    sections["EducationProfile"] = []
    sections["JobSkills"] = []
    sections["CommonSkills"] = []
    sections["Certifications"] = []

    prune_list = []


    for c, records in enumerate(data2):


        sections["JobTitles"].extend([x.lower() for x in records["JobTitles"]])

        sections["JobSkills"].extend([x["Name"].lower() for x in records["JobSkills"]+ records["AdditionalOtherSkills"]])
        sections["EducationProfile"].extend([x.lower() for x in records["EducationProfile"]])
        sections["CommonSkills"].extend([x["Name"].lower() for x in records["AdditionalSkills"]])
        sections["Certifications"].extend([x.lower() for x in records["Certifications"]])

    return sections
