from .graph_query import create_matcher
from .certifications_jobs_education import get_ed_job_cert
import sys
import pickle


#curentpath=os.getcwd()
# path to folder contain this packages
# ExistFilePath = '/Users/kaijin/Desktop/Bold_Projects/Rank Jobs/ProductionUse/Job_quality_pkg/Job_quality_pkg/premium_match_1_3/PremiumMatch.py'
# currentpath = os.path.dirname(ExistFilePath)

currentpath = sys.path[0]
DirectionAdjust="/premium_match_1_3/graph_data/"
path=currentpath+DirectionAdjust


def select_industry_objects(ind_list, nlp_object):
    sections_by_industry, _ = get_ed_job_cert()

    industry_specific_object = {}

    dir_path = path + "pre_prep_data/"

    for industry in ind_list:
        ### adjust healcare seems not passing this 
        if industry !="healthcare":
            #print(industry)
            with open(dir_path+industry+"/miscl_skills") as fh:
                skills = [line.rstrip('\n') for line in fh]
        else:
            skills=[]
        sections_by_industry, graph_map = get_ed_job_cert()


        with open(path+industry+'/aggregated_JobTitles.pkl', 'rb') as fh:
            G_job_titles = pickle.load(fh)
        with open(path+industry+"/aggregated_JobSkills.pkl", 'rb') as fh:
            G_hard_skills = pickle.load(fh)

        with open(path+industry+'/aggregated_CommonSkills.pkl', 'rb') as fh:
            G_common_skills = pickle.load(fh)

        with open(path+industry+'/aggregated_EducationProfile.pkl','rb') as fh:
            G_education_profile = pickle.load(fh)


        with open(path+industry+'/aggregated_Certifications.pkl','rb') as fh:
            G_certification = pickle.load(fh)

    

        hard_skills = list(G_hard_skills.edge.keys())+skills
        common_skills = list(G_common_skills.edge.keys())

        certification = list(G_certification.edge.keys()) + sections_by_industry[industry]['certifications']
        education = list(G_education_profile.edge.keys()) + sections_by_industry[industry]['education']

        #print(type(G_job_titles))
        attribute_name = ["hardskills", "commonskills", "education", "certifications"]
        attribute_lits = [hard_skills, common_skills, education, certification]

        final = zip(attribute_name, attribute_lits)

        #create matcher
        combined_matcher = create_matcher(nlp_object,final)

        graph_map = {'JobSkills': G_hard_skills, 'CommonSkills': G_common_skills, 'JobTitles': G_job_titles,
                     'EducationProfile': G_education_profile, 'Certifications': G_certification}


        industry_specific_object[industry] = {"matcher":combined_matcher, "section_graphs":graph_map}

    return industry_specific_object

