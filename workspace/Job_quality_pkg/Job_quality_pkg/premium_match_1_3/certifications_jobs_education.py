import os
import sys
import json

# ExistFilePath = '/Users/kaijin/Desktop/Bold_Projects/Rank Jobs/ProductionUse/Job_quality_pkg/Job_quality_pkg/premium_match_1_3/PremiumMatch.py'
# currentpath = os.path.dirname(ExistFilePath)
currentpath = sys.path[0]
DirectionAdjust="/premium_match_1_3/graph_data/pre_prep_data/"
#print(curentpath)
#print("**"*20)
dir_path=currentpath+DirectionAdjust

#dir_path = '/Users/kaijin/Desktop/LearningMateria/premium_match_1_1/graph_data/pre_prep_data/'

def get_ed_job_cert():

    f = os.path.dirname(dir_path)

    dirs = os.listdir(f)
    section_attribute_dict = {}
    industry_job_map = {}

    for di in dirs:
        if di.startswith("."):
            continue
        files = os.listdir(dir_path+di)

        name = str(di.lower())
        section_attribute_dict[name] = {}
        for j in files:
            key_name = j.split(".")[0].lower()

            section_attribute_dict[name][key_name] = []

            with open(dir_path+di+"/"+j) as fh:

                for lines in fh:
                    lines = lines.strip().lower().replace("*", '')
                    if lines == '\ufeff' or lines == '':
                        continue
                    else:

                        section_attribute_dict[name][key_name].append(lines)

    for job in section_attribute_dict["nursing"]["jobs"]:
        if job in section_attribute_dict["healthcare"]["jobs"]:
            section_attribute_dict["healthcare"]["jobs"].remove(job)

    for industry in section_attribute_dict:

        jobs = section_attribute_dict[industry]["jobs"]

        for job in jobs:

            job = job.strip().lower()

            industry_job_map[job] = industry

        #
    return section_attribute_dict, industry_job_map


def get_acronym_mapping(industry_name):
    cert_file = dir_path+industry_name+"/cert_acronym_mapping"
    job_file = dir_path+industry_name+"/job_acronym_mapping"

    certs_map = {}
    jobs_map = {}
    try:
        with open(cert_file) as fh, open(job_file) as fh2:
            certs = json.load(fh)
            jobs = json.load(fh2)

        for j, k in certs.items():

            for j1, k1 in k.items():

                if ' ' not in j1:

                    certs_map[j1] = k1

        for j, k in jobs.items():

            for j1, k1 in k.items():

                if ' ' not in j1:

                    jobs_map[j1] = k1

        return certs_map, jobs_map

    except FileNotFoundError as e:
        return certs_map, jobs_map


def cannonicalize_output(attribute_list, attribute_map=None):

    normalized_attributes = set()
    if attribute_map:
        for items in attribute_map:
            if items in attribute_list:
                normalized_attributes.add(attribute_map[items])
    else:
        normalized_attributes.add(attribute_list)

    return normalized_attributes
