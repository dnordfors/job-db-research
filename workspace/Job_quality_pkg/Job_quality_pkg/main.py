########
# Use saved model to predict scores
# given inputs of Descriptions Company and Title
########
import os
import sys
import pickle
from . import Preprocess2, Prediction

# Load saved pca model

dir_path = sys.path[0]
filename="SavedModel_PCA_RF/PCA_Model_init.sav"
loaded_PCA = pickle.load(open(os.path.join(dir_path, filename), 'rb'))


# Load saved random forest model
filename="SavedModel_PCA_RF/RandomForest_init.sav"
loaded_RF = pickle.load(open(os.path.join(dir_path, filename), 'rb'))

def EmptyDescriptionAdjust(Str1):

    try:
        if len(Str1)==0 :
            return(1)
    except:
        return(1)


    return(0)


class Job_quality:

    def __init__(self, Descriptions,Company,Title ):
        self.Descriptions = Descriptions
        self.Company = Company
        self.Title = Title
    def ScorePrediction(self,number_of_processor=8,SingleJobMode=False ):

        """
        Giving a list of Descriptions Company and title,
        return a score that represent the quality of the jobs 
        """

        Descriptions = self.Descriptions
        Company = self.Company
        Title = self.Title

        if SingleJobMode:
            
            if EmptyDescriptionAdjust(Descriptions):
                return(0)

            try:
                if EmptyDescriptionAdjust(Title):
                    Title=""

                PreProcessor=Preprocess2.Preprocess_matrix(Descriptions,Company,Title)
                Matrix_X=PreProcessor.CreateMatrix(number_of_processor=1)
                PCs=Prediction.PCA_Prediction([Matrix_X],loaded_PCA).PredictPCs()
                CheckPredictions=Prediction.RandomForest_Prediction(PCs[0],loaded_RF).Predict_Score()
                return(CheckPredictions[0])
            except:
                #print(Descriptions)
                raise Exception('Single mode only take one Job descriptions at a time. Input type should be string. ')

        # Adjust na in job title 
        job_title2=[]
        for ji in Title:
            if EmptyDescriptionAdjust(ji):
                job_title2.append("")
            else:
                job_title2.append(ji)
                
        Title=job_title2


        # Adjust empty Job Descriptions
        RecordNone=[]
        RecordFull=[]
        for i,xi in enumerate(Descriptions):
            if EmptyDescriptionAdjust(xi):
                RecordNone.append(i)
            else:
                RecordFull.append(i)

        # predict full job descriptions
        job_desc_FullScore=[ Descriptions[i] for i in  RecordFull]
        company_FullScore=[Company[i] for i in  RecordFull]
        job_title_FullScore=[ Title[i] for i in  RecordFull]

        PreProcessor=Preprocess2.Preprocess_matrix(job_desc_FullScore,company_FullScore,job_title_FullScore)
        Matrix_X=PreProcessor.CreateMatrix_DedupVersion(number_of_processor=number_of_processor)
        PCs=Prediction.PCA_Prediction(Matrix_X,loaded_PCA).PredictPCs()
        CheckPredictions=Prediction.RandomForest_Prediction(PCs,loaded_RF).Predict_Score()
        
        #Assign other scores
        job_score_full=[]
        j=0 
        for i,xi in enumerate(Descriptions):
            if i in RecordNone:
                job_score_full.append(0)
            if i in RecordFull:
                job_score_full.append(CheckPredictions[j])
                j=j+1


        return(job_score_full)
