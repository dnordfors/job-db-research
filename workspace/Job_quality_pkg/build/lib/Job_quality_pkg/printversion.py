import pandas as pd
import pickle


import nltk 
import numpy as np
import html2text
import commonregex
import IPython
import spacy
import networkx
import fuzzywuzzy
import html5lib
import scipy
import sklearn
import igraph 

print("List of requirments:  ")
print("nltk",nltk.__version__)
print("pandas",pd.__version__)
print("numpy",np.__version__)
print("html2text",html2text.__version__)
print("commonregex","https://github.com/madisonmay/CommonRegex","1.5.5")
print("pyap","https://pypi.org/project/pyap/#description","0.1.0")
print("pyap","https://pypi.org/project/pyap/#description","0.1.0")
print("IPython",IPython.__version__)
print("spacy",spacy.__version__) # python -m spacy download en 
print("networkx",networkx.__version__)
print("fuzzywuzzy",fuzzywuzzy.__version__)
print("html5lib",html5lib.__version__)
print("scipy",scipy.__version__)
print("sklearn",sklearn.__version__)
print("igraph",sklearn.__version__," http://igraph.org/python/","   sudo pip install python-igraph")


import sys

sys.path.insert(0, '/premium_match_1_2_kai')

from time import time 

import Job_quality_pkg


MasterTable=pd.read_csv("/Users/kaijin/Desktop/Bold_Projects/Rank Jobs/MasterTable100K_PCA_v1_1.csv",index_col=0)
MasterTable=MasterTable.reindex()
MasterTable["Company"]=MasterTable.Company.fillna('CompanyMissing')
MasterTable["Title"]=MasterTable.Title.fillna('TitleMissing')

STs=time()
print("file loaded")
n=1000
seed=1

TestData=MasterTable.sample(n,random_state=seed)
TestData=TestData.reset_index()

PreProcessor=Job_quality_pkg.Preprocess2.Preprocess_matrix(TestData.Description,TestData.Company,TestData.Title)
Matrix_X=PreProcessor.CreateMatrix(number_of_processor=8)

print("Matrix created")

filename="SavedModel_PCA_RF/PCA_Model_init.sav"
loaded_model = pickle.load(open(filename, 'rb')) 
PCs_TestData=Job_quality_pkg.Prediction.PCA_Prediction(Matrix_X,loaded_model).PredictPCs()


filename="SavedModel_PCA_RF/RandomForest_init.sav"
loaded_model = pickle.load(open(filename, 'rb')) 
CheckPredictions=Job_quality_pkg.Prediction.RandomForest_Prediction(PCs_TestData,loaded_model).Predict_Score()


print("Time needed to pass through 1000 jobs ",time()-STs, "seconds.")
print("Number of jobs",len(CheckPredictions))

#print(CheckPredictions[:10])
#print(Matrix_X[:10])