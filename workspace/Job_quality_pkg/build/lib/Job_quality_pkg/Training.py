#############
#The goal is to take a set of X
#Train and save the model
############

import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestRegressor
from sklearn.decomposition import PCA
import pickle


class PCA_ForJobs:

    def __init__(self, X_matrix ):
        self.X_matrix = X_matrix


    def TrainingPCA(self,n_comp=2):
        """
        Giving a x matrix and returning two Principle Components  
        print out some summary analysis

        Ratio explained: refer to with two principle compoents, 
        how many percentage of the data is represented by the top principle components.

        """

        X_matrix = self.X_matrix
        
        pca = PCA(n_components=n_comp)
        pca.fit(X_matrix)
        PCs = pca.transform(X_matrix)


        print("Ratio explained: ",pca.explained_variance_ratio_) 
        print("Singular Value", pca.singular_values_)  
        print("PCA score(everage Log Like)", pca.score(X_matrix))

        filename = 'SavedModel_PCA_RF/PCA_Model.sav'
        pickle.dump(pca, open(filename, 'wb'))
        print("Model is saved at ",filename )

        return(PCs)



class RandomForest_ForJobs:
    
    def __init__(self, X_matrix_PCs,y_rate):
        self.X_matrix_PCs = X_matrix_PCs
        self.y_rate = y_rate
        
    
    def TrainingRandomForest(self):
        """
        Train Random Forest and save the model and save the model

        """
        X_matrix_PCs = self.X_matrix_PCs
        y_rate = self.y_rate


        RF_Reg=RandomForestRegressor(max_depth=2,n_estimators=1000, random_state=0)
        RF_Reg.fit(X_matrix_PCs, y_rate)
        Z = RF_Reg.predict(X_matrix_PCs)

        print("Features Importance: ",RF_Reg.feature_importances_) 
        print("Mean Square Error: ", np.mean((Z-y_rate)**2))


        filename = 'SavedModel_PCA_RF/RandomForest_Model.sav'
        pickle.dump(RF_Reg, open(filename, 'wb'))
        print("Model is saved at ",filename )