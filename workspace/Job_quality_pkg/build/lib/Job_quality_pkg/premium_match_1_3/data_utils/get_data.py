from configuration.config import Config
from pymongo import MongoClient
from ast import literal_eval
config_obj = Config()
mongo_conn = config_obj.mongo_connection

with open("query_file.json",'r') as fh:
    q = fh.read()

    query = literal_eval(q)


def full_resume_mongo_data(collection, query):
    client  = MongoClient(mongo_conn)
    db = client.resume_db
    collection = db.ResumeParsedData
    return collection.aggregate(query)


def collect_sections_resume(mongo_conn, query):

    mongo_data = full_resume_mongo_data(mongo_conn, query)
    sections={}
    sections['JobTitles'] = []
    sections['EducationProfile'] = []
    sections['JobSkills'] = []
    sections['CommonSkills'] = []
    sections['Certifications'] = []

    prune_list = []

    for c, records in enumerate(mongo_data):
        if c== 100000:
            break

        sections['JobTitles'].append([x.lower() for x in records['JobTitles']])

        sections['JobSkills'].append([x['Name'].lower() for x in records['JobSkills']+ records['AdditionalOtherSkills']])
        sections['EducationProfile'].append([x.lower() for x in records['EducationProfile']])
        sections['CommonSkills'].append([x['Name'].lower() for x in records['AdditionalSkills']])
        sections['Certifications'].append([x.lower() for x in records['Certifications']])

    return sections

