import re
from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords


class Text_Clean:

    def __init__(self):
        self.ps = PorterStemmer()
        self.stops = set(stopwords.words("english"))
        pass

    def lower_case_letters(self, inputstring):
        inputstring = inputstring.lower()
        inputstring = re.sub("[^a-z]"," ", inputstring)
        inputstring = " ".join(inputstring.split())
        return(inputstring)

    def lemmatize_Stem_words(self, words):
        """
        Lemmatize verbs in list of tokenized words
        Stem it after that 
        """
        lemmatizer = WordNetLemmatizer()
        lemmas = []
        for word in words:
            lemma = lemmatizer.lemmatize(word, pos='v')
            lemma= self.ps.stem(lemma)
            lemmas.append(lemma)
        return lemmas

    def sentence_to_wordlist(self, review, remove_stopwords=True ):
        """
            Function to convert a sentence to a sequence of words,
            optionally removing stop words.  Returns a new doc that got cleaned.
            
        """
        # 1. Remove HTML, which is already done in sentence split 
        #print(len(review))
        #review_text = BeautifulSoup(review, "html5lib").get_text()
        #print(len(review_text))
        # 2. Remove non-letters
        review_text = re.sub("[^a-zA-Z]"," ", review)
        # 3. Convert words to lower case and split them
        words = review_text.lower().split()
        # 4. Optionally remove stop words (false by default)
        if remove_stopwords:
            words = [w for w in words if not w in self.stops]
        words=self.lemmatize_Stem_words(words)
        # 5. Return a list of words
        return(" ".join(words))

class WordCount:

    def __init__(self):
        pass

    def wordcount(self,doc):
        """
        word count for text 
        """
        words = re.findall("[a-zA-Z_]+", doc.lower())
        return(len(words))

    def wordcount_sl(self,sl):
        """
        word count for sentence list 
        """
        doc = " ".join(sl)
        words = re.findall("[a-zA-Z_]+", doc.lower())
        return(len(words))


