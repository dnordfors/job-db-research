import colored



class Color_Print_Sentence:

    def __init__(self):
        ColorPrint={
                    "QualificationList": "green",
                    "PositionList":"blue",
                    "CompanyBenefitsList":"red",
                    "EducationList":"magenta",
                    "CertificationList":"yellow",
                }
        print(colored.fg("red") + "CompanyBenefitsList")
        print(colored.fg("green") + "QualificationList")
        print(colored.fg("blue") + "PositionList") #Responsibilities
        print(colored.fg("magenta") + "EducationList")
        print(colored.fg("yellow") + "CertificationList")
        self.ColorPrint = ColorPrint

    def print_sentence(self, predicted_sentence_dict, detail_mode = False):

        for sentence, classes in zip(predicted_sentence_dict["SentenceList"], predicted_sentence_dict["SentenceTag"]):
            color = self.ColorPrint[classes[0][1]]
            print(colored.fg(color) + sentence)
            if detail_mode:
                print(classes[:2])
        print(colored.fg("black") + "**"*20)
