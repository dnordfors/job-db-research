import re

def sentence_start_qualification(sentence, qualification_check_inital_words, qualification_check_inital_words_negative):
    """
    For qualifications, check inital words and see if it's meet all the requirements
    """
    sentence=sentence.lower()
    Wordlist=re.findall("[a-zA-Z_]+",sentence)
    if len(Wordlist)==0:
        return(False)
    #text = Wordlist[0]
    if Wordlist[0] in qualification_check_inital_words:
        return(True)
    if any([i in sentence for i in  qualification_check_inital_words]):
        if any( [i  in sentence for i in qualification_check_inital_words_negative])==False:
            if any([i in Wordlist for i in ["we","us"]])==False:
                return(True)
    return(False)

def result_list_adjuster(inital_prediction, adjust_class, adjust_weight, classlist, ignore_mode = False):
    """
    Given a inital_prediction,
    adjust weighe based on the class that need to be adjusted
    ignore_mode is to decide wether ignore the oringal weights in the intial prediction
    """
    adjusted_prediction = []
    if ignore_mode and adjust_weight == 1:
        for ci in classlist:
            if adjust_class == ci:
                adjusted_prediction.append((1,ci))
            else:
                adjusted_prediction.append((0,ci))
        return(adjusted_prediction)
    else:
        if adjust_class in [pi[1] for pi in inital_prediction]: # there are some cases when some class are missing
            for i in range(len(inital_prediction)):
                if adjust_class == inital_prediction[i][1]:
                    adjust_score = inital_prediction[i][0] + adjust_weight
                    adjusted_prediction.append((adjust_score, adjust_class))
                else:
                    adjusted_prediction.append((inital_prediction[i][0], inital_prediction[i][1]))
            return(adjusted_prediction)
        else:
            inital_prediction.append((adjust_weight, adjust_class)) 
            return(inital_prediction)


def matching_string_in_sentence(string_list, sentence):
    """
    Find string in the list is in the sentence
    Find sum of words counts
    """
    matching_list = [si for si in string_list if si in sentence]
    matching_number = 0
    for mi in matching_list:
        matching_number = matching_number + len(mi.split())
    return(matching_number)

def single_score_adjuster(sentence_word_count, matching_count, max_adjust, ratio):
    """
    Return adjust scores based on word count and ratio
    """
    ratio = float(ratio)
    sentence_word_count = min(sentence_word_count,20) # limit the word count
    adjustscore = ratio/sentence_word_count*matching_count
    adjustscore = min(max_adjust,adjustscore)
    return(adjustscore)

def regular_expression_score_adjuster(sentence, predicted_class, sentence_word_count, classlist):
    """
    adjust predicted class by regular expressions
    """ 
    # adjust some regular expression
    if bool(re.search("(year).{,30}(experience)", sentence)):
        score_added = single_score_adjuster(sentence_word_count, matching_count = 1, max_adjust = 4, ratio = 40.)  
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'QualificationList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False) 

    if bool(re.search("(provide).{,20}(analysis)", sentence)):
        score_added = single_score_adjuster(sentence_word_count, matching_count = 1, max_adjust = 4, ratio = 40.)  
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'QualificationList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)  

    if bool(re.search("^((we are)|(we're)).{,20}(company|companies|enterprise|international|communit|global|diverse|lab|group|organization|institution|firm|business|employer|corporation|brand|hospital|team)" , 
                sentence)) :
        if all([word in sentence for word in ["looking", "candidate" , "who"]]) ==  False:
            score_added = single_score_adjuster(sentence_word_count, matching_count = 1, max_adjust = 4, ratio = 40.)  
            predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'CompanyBenefitsList',
                                                   adjust_weight = score_added, classlist = classlist, ignore_mode = False)  

    if  bool(re.search("((complete)|(fill)).{,20}(application)",sentence)) or bool(re.search("((pass)|(undergo)|(required)|(take)|(obtain)).{,10}(drug).{,10}(test)",sentence)) or (
             bool(re.search("(profit).{,20}(organization)",sentence)) ):
        if all([word in sentence for word in ["looking", "candidate" , "who"]]) ==  False:
            score_added = single_score_adjuster(sentence_word_count, matching_count = 1, max_adjust = 4, ratio = 40.)  
            predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'CompanyBenefitsList',
                                                   adjust_weight = score_added, classlist = classlist, ignore_mode = False)   
    return(predicted_class)
                

def single_sentence_adjuster_key_words(sentence, predicted_class, classlist,
                                        positionListCheck, qualificationListCheck, qualificationListCheck_weak,
                                        certificationListCheck, educationListCheck, companyListCheck, companyListcheck_weak,
                                        qualification_check_inital_words, qualification_check_inital_words_negative,
                                        headers_dict):
    """
    adjust the predicted class by some fixed list
    """ 
    predicted_class=list(predicted_class) #j
    sentence=str(sentence.lower()) #i
    sentence_word_count = max(1,len([word for word in  sentence.split() if len(word)>=3])) # get the number of words in sentence ignore short words

    # adjust header
    if headers_dict.get(sentence)!=None:
        header_class = headers_dict.get(sentence)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = header_class,
                                                 adjust_weight = 1, classlist = classlist, ignore_mode = True)
        return(predicted_class)

    # adjust PositionListcheck
    matching_count = matching_string_in_sentence(positionListCheck, sentence)
    if matching_count != 0:
        score_added = single_score_adjuster(sentence_word_count, matching_count, max_adjust = 3, ratio = 15.)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'PositionList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)
    
    # adjust QualificationListcheck
    matching_count = matching_string_in_sentence(qualificationListCheck, sentence)
    if matching_count != 0:
        score_added = single_score_adjuster(sentence_word_count, matching_count, max_adjust = 3, ratio = 15.)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'QualificationList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)

    # adjust CertificationListcheck
    matching_count = matching_string_in_sentence(certificationListCheck, sentence)
    if matching_count != 0:
        score_added = single_score_adjuster(sentence_word_count, matching_count, max_adjust = 1, ratio = 5.)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'CertificationList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)

    # adjust EducationListcheck
    matching_count = matching_string_in_sentence(educationListCheck, sentence)
    if matching_count != 0:
        #print("Find match ", matching_count)
        score_added = single_score_adjuster(sentence_word_count, matching_count, max_adjust = 4, ratio = 6.)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'EducationList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)

    # adjust CompanyListcheck

    matching_count = matching_string_in_sentence(companyListCheck, sentence)
    if matching_count != 0:
        score_added = single_score_adjuster(sentence_word_count, matching_count, max_adjust = 3, ratio = 20.)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'CompanyBenefitsList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)
    # adjust CompanyListcheck2
    matching_count = matching_string_in_sentence(companyListcheck_weak, sentence)
    if matching_count != 0:
        score_added = single_score_adjuster(sentence_word_count, matching_count, max_adjust = 1, ratio = 5.)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'CompanyBenefitsList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)

    # adjust QualificationListcheck2
    matching_count = matching_string_in_sentence(qualificationListCheck_weak, sentence)
    if matching_count != 0:
        score_added = single_score_adjuster(sentence_word_count, matching_count = 3, max_adjust = 3, ratio = 10.)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'QualificationList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)


    # inital words for position list
    if sentence_start_qualification(sentence, qualification_check_inital_words, qualification_check_inital_words_negative):
        score_added = min(3,30./sentence_word_count)
        predicted_class = result_list_adjuster(inital_prediction = predicted_class, adjust_class = 'PositionList',
                                                 adjust_weight = score_added, classlist = classlist, ignore_mode = False)

    # adjust predicted class by some regular expression 
    predicted_class = regular_expression_score_adjuster(sentence, predicted_class, sentence_word_count, classlist)

    return(predicted_class)
 

class Key_Word_Adjuster:

    def __init__(self, key_words_dict, headers_dict):
        
        self.positionListCheck = set(key_words_dict["PositionListcheck"])
        self.qualificationListCheck = set(key_words_dict["QualificationListcheck"])
        self.qualificationListCheck_weak = set(key_words_dict["QualificationListcheck2"])
        self.certificationListCheck = set(key_words_dict["CertificationListcheck"])
        self.educationListCheck = set(key_words_dict["EducationListcheck"])
        self.companyListCheck = set(key_words_dict["CompanyListcheck"])
        self.companyListcheck_weak = set(key_words_dict["CompanyListcheck2"])
        self.qualification_check_inital_words = set(key_words_dict["qualification_check_inital_words"])
        self.qualification_check_inital_words_negative = set(key_words_dict["qualification_check_inital_words_negative"])

        self.headers_dict = headers_dict
        self.classlist=['QualificationList', 'PositionList', 'CompanyBenefitsList','EducationList', 'CertificationList']


    def predicted_class_list_adjuster_key_words(self, sentencelist, predicted_class_list, company):
        
        companyListCheck = self.companyListCheck
        if company != "":
            if len(company)>=5:
                company=company.lower()
                companyListCheck.add(company)
            
        adjusted_predicted_class_list = []
        previous_predicted_class = None

        for sentence, predicted_class in zip(sentencelist, predicted_class_list):
            #print(sentence)
            predicted_class = single_sentence_adjuster_key_words(sentence, predicted_class, self.classlist,
                                                                positionListCheck = self.positionListCheck,
                                                                qualificationListCheck = self.qualificationListCheck,
                                                                qualificationListCheck_weak = self.qualificationListCheck_weak,
                                                                certificationListCheck = self.certificationListCheck,
                                                                educationListCheck = self.educationListCheck,
                                                                companyListCheck = companyListCheck,
                                                                companyListcheck_weak = self.companyListcheck_weak,
                                                                qualification_check_inital_words = self.qualification_check_inital_words,
                                                                qualification_check_inital_words_negative = self.qualification_check_inital_words_negative,
                                                                headers_dict = self.headers_dict)
            #print(predicted_class)
            letter_length = len("".join(re.findall("[a-zA-Z]", sentence)))
            if previous_predicted_class != None and letter_length <= 5:
                adjusted_predicted_class_list.append(previous_predicted_class)
            else:
                predicted_class.sort(key=lambda tup: tup[0], reverse=True )  
                # record it to adjust next short sentence
                previous_predicted_class=list(predicted_class) 
                #add to the resulting list
                adjusted_predicted_class_list.append(predicted_class)

        return(adjusted_predicted_class_list)