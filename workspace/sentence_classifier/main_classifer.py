import os
import tensorflow as tf
from sentence_classifier.jobs_modifier.job_sentence_split import Job_Sentence_Split
from sentence_classifier.dnn_sentence_classifer import DNN_Classifer
from sentence_classifier.classifer_adjuster import Classifer_Adjuster
from sentence_classifier.common.reader import Reader
from sentence_classifier.common.color_print import Color_Print_Sentence


class SentenceClassifier:

    def __init__(self):

        self.predict_fn = None
        #self.length = length

    def load_model(self, base_path, json_path):
        # load dictionary for sentence adjuster
        header_path = os.path.join(json_path, "headers_dict.json" )
        headers_dict = Reader().read_json(header_path) 
        key_words_path = os.path.join(json_path, "key_words_dict.json" )
        key_words_dict = Reader().read_json(key_words_path)
        for key,value in key_words_dict.items():
            value=[i.lower() for i in value]
            key_words_dict[key]=set(value) 
            
        # load sentence split function
        # load sentence_list_predicter function
        self.sentence_split_adjustHTML = Job_Sentence_Split(headers_dict).sentence_split_adjustHTML
        self.sentence_list_predicter   = DNN_Classifer().sentence_list_predicter
        self.score_adjuster = Classifer_Adjuster(key_words_dict, headers_dict).score_adjuster

        # load predict_fn for sentence_list_predicter
        model_path = os.path.join(base_path, "SavedModel1/1550860709" )
        if os.path.exists(model_path):
            self.predict_fn = tf.contrib.predictor.from_saved_model(model_path)
        else:
            print("Path - {} does not exists.".format(model_path))
        
        #self.base_path=base_path

    def predict_Sentence(self, description, company):
        """
        Clean List
        Drop Stop sentence 
        Drop stopwords lem and stem with Sentence_to_Wordlist
        """

        if company == None:
            company = ""
        if type(company) == float:
            company = ""
        
        sentencelist = self.sentence_split_adjustHTML(description, company)

        predicted_sentence_list = self.sentence_list_predicter((sentencelist, company, self.predict_fn ))

        predicted_sentence_list = self.score_adjuster(sentencelist, predicted_sentence_list, company)
        
        return( {"SentenceList":sentencelist,"SentenceTag":predicted_sentence_list})
