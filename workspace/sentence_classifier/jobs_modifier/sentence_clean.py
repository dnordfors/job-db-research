import re



class SentenceClean:

    def __init__(self):
        pass

    def lowercase_letter(self, sentence):
        """
        Clean sentence keep only words in lower case    
        """
        #extract only words
        sentence = re.sub("[^a-zA-Z]"," ", sentence)
        # reformat it and drop duplicate space
        sentence = ' '.join(sentence.split())
        sentence = sentence.lower()

        # 3. Convert words to lower case and split them
        #Sentencelength = len(sentence.lower().split())

        # words = [w for w in words if not w in stops]
        return(sentence)

    




