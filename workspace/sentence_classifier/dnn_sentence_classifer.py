import tensorflow as tf
#import tensorflow_hub as hub
import numpy as np
from sentence_classifier.jobs_modifier.sentence_clean import SentenceClean


def _create_float(v):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[v]))

def _create_int(v):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[v]))

def _create_str(v):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[bytes(v, 'utf-8')]))


class DNN_Classifer:

    def __init__(self):
        classlist=['QualificationList', 'PositionList', 'CompanyBenefitsList','EducationList', 'CertificationList']
        classdic={}
        for i,x in enumerate(classlist):
            classdic[x]=i
        classdic_rev={}
        for key in classdic:
            classdic_rev[classdic[key]]=key
        self.classdic_rev = classdic_rev
        self.lowercase_letter  = SentenceClean().lowercase_letter
    
    def single_sentence_predicter(self, sentence,sentence_length,company_name_check,normalized_location,
                                        number_of_sentence,predict_fn):

        """
        given 5 input, and predict_function
        predict single sentence's classes 
        """
        # use DNNclassifer to predict classe
        # intialize input

        sentence = _create_str(sentence)
        sentence_length = _create_float(sentence_length)
        company_name_check = _create_int(company_name_check)
        normalized_location = _create_float(normalized_location)
        number_of_sentence = _create_float(number_of_sentence)
        
        feat = {"CleanSentence":sentence,
                "SentenceLength":sentence_length,
                'CompanyNameCheck': company_name_check,
                'NormalizedLocation': normalized_location ,
                'NumberOfSentence': number_of_sentence,}

        example = tf.train.Example(features=tf.train.Features(feature=feat))
        inputs = example.SerializeToString()

        # predict class
        preds = predict_fn({"inputs":[inputs]})
        g = np.around(preds['scores'],3).tolist()
        f = preds['classes'].tolist()        
        final = dict(zip(g[0],f[0]))

        #sort values and find correct lable
        res = sorted(final.items(), key=lambda x:x[0], reverse=True)
        res = [(float(a[0]), self.classdic_rev[int(a[1])]) for a in res ]
        return(res)


    def sentence_list_predicter(self, tuple):
        """
        give a list of sentence and company name 
        produce a list of predicted classes dictionary
        
        """
        sentence_list = tuple[0]
        company = tuple[1]
        predict_fn = tuple[2]
        #clean sentence
        sentence_list = [self.lowercase_letter(x) for x in sentence_list]
        
        #run through sentence class predicter for each sentence
        result_list = []
        for si in range(len(sentence_list)):
            
            #prepare input for sentence predictor 
            company = str(company)

            if company.lower() in sentence_list[si].lower() and len(company)>=5:
                company_name_check = 1
            else:
                company_name_check = 0
            sentence = sentence_list[si]
            number_of_sentence = len(sentence_list)
            sentence_length = len(sentence_list[si].split())
            normalized_location = si/len(sentence_list)
            
            result = self.single_sentence_predicter(sentence, sentence_length, company_name_check,
                                                    normalized_location, number_of_sentence, predict_fn)
            result_list.append(result)
        return(result_list)

