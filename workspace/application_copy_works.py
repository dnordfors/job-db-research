
# #%% 


# OS
import os
import sys
import urllib
import requests

# LOGGING / MONITORING
import inspect
import tqdm
from tqdm._tqdm_notebook import tqdm_notebook
import time

# redis - dictionary
import redis
regis = redis.Redis(host='localhost', port=6379, db=0)
regis.set('progress',0)
regis.set('progress_name','idle')
regis.set('current_params','none')
regis.set('current_process',' ')

# PROCESSING
import numpy as np
import pandas as pd
import scipy
import collections
from collections import Counter


# MODELS / FITTING
from scipy.cluster import hierarchy

# GEO / TIME
import addfips

# NETWORK GRAPHS
import networkx as nx
import pydot
from networkx.drawing.nx_pydot import write_dot

# DATABASE + I/O
import pickle
## mongo
import pymongo
from pymongo import MongoClient
import bson.json_util as bj
## json
import json
from json import dumps
from bson import ObjectId


# NLP
from bs4 import BeautifulSoup

## NLP synonyms: GloVe    https://nlp.stanford.edu/projects/glove/
# glove_vectors_file = "./glove_vectors_wikip_200.pkl"
# ## read glove-vectors from disk if und./glove_vectors_wikip_200.pklefined, otherwise not
# try:
#     if glove_dic:
#         pass
# except:
#     if glove_vectors_file[-3:] is 'pkl' or 'pickle':
#         glove_dic = pd.read_pickle(glove_vectors_file)
#         glove_df = pd.DataFrame(glove_dic)

from nltk.corpus import stopwords
stopw = set(stopwords.words("english"))

# function for cleaning each sentence
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')


# ## BOLD Graph
# ### read dictionary with clean & noisy titles
# try:
#     if BOLD_noisy_titles:
#         pass
# except:
#     with open('jobtitle_cleantitles_reference.cache.json','r') as file:
#          BOLD_noisy_titles = json.load(file)

# GRAPHICS
import seaborn as sns
from matplotlib import pyplot as plt

## plotly dash

import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import dash_bio as dashbio
import dash_table
import dash_table.FormatTemplate as FormatTemplate
from dash_table.Format import Sign
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import plotly.graph_objs as go
import plotly.graph_objects as go
import plotly.figure_factory as ff
from plotly.subplots import make_subplots

# LocalModules (sub-directory)
from LocalModules.GeoMap import GeoID
from LocalModules.MatrixFactorization import *
 

class JobDescriptions:
    '''
    JobDescriptions(db,client,collection) creates an instance of a MongoDB Collection
    containing job ads/descriptions.
    
    ## Connecting the database
    
    input    : client        = name of Mongo Client   : 'mongodb://localhost:27017/' 
    attribute: jd.client     = MongoClient(client)  
    
    input    : db            = name of Mongo database : 'job_db'
    attribute: jd.db         = jd.client[db]
    
    input    : collection    = name of db-collection  : 'ServicebusCreateMessage'
    attribute: jd.collection = jd.db[collection]
    
    ## Key attributes and functions:    
    
    jd.get()                    : get 'max_items' job descriptions with 'string' in 'field'
                                  places the dataset at the top of the stack. 
    jd.mark_duplicates()        : adds column to jd.df marking duplicate job descriptions
    jd.drop_duplicates()        : removes duplicate job descriptions from jd.df 
    jd.common_titles_count()    : counts occurences of job-title n-grams in jd.get()
    
      jd.params                 : parameters for current jd.get()
      jd.df                     : current jd.get(jd.params) job descriptions as dataframe 
      jd.common_titles          : result from latest common_titles_count()
      
  
    
    
    
    
    '''
    def __init__(self,                                         \
                 db              = 'job_db',                        \
                 client          = 'mongodb://localhost:27017/',    \
                 collection      = 'ServicebusCreateMessage',       \
                 data_path       = './data',                        \
                 geoid_entities  = ['counties']):
        
        self.client     = MongoClient(client)
        self.db         = self.client[db]
        self.collection = self.db[collection]      
        self.geo        = GeoID(path = data_path, entities = geoid_entities)
        # self.geoid      = self.GeoID(path = data_path, entities = geoid_entities)
        
        # Dictionaries: Function Caches
        # key named 'params' in functions
        self.get_dic                    = {}
        self.duplicates_dic             = {}
        self.common_titles_dic          = {}
        self.word_set_matrix_dic        = {}
        self.cluster_dic                = {}
        self.classify_job_description_dic = {}
        self.ngram_analysis_dic         = {}
        
        self.current_parameters         = {}

        self.title_dic                  = {}
        self.common_dic                 = {}




    def get(self, string = 'manager', field = 'Title', max_items = 10000):
        '''
        1. Gathers job descriptions in MongoDB "collection" for jobs with the sub-string 'string' in the job title. 
                Speeds up by indexing 'collection' on job titles if index does not already exist 
                - THIS TAKES TIME, but does only need to be done once for a collection.
        2. Parses gathered job descriptions into dictionaries

        Returns Pandas Dataframe with columns = [ObjectId, 
                                                time stamp, 
                                                title, 
                                                company,
                                                description,
                                                length, (#characters in clean description )
                                                parsed description,
                                                word_set]
        '''
        # Check if result already in dictionary
        par = {'string':string,'field':field,'max_items':max_items}
        self.current_parameters.update(par)
        self.params = str(par)
        params = self.params
        # If not in dictionary, connect to database and proceed...
        if params not in self.get_dic:
            collection = self.collection
            
            # Gather records with "string" in job title
            regis.set('current_process','Indexing '+field+' - This takes time, but it should be a one-time operation') 
            if field is 'Description':
                collection.create_index([('Message.'+field,pymongo.TEXT)]) 
            else:
                collection.create_index('Message.'+field)
            cursor = collection.find({'Message.'+field:{'$regex':"(?i).*"+string+".*"}})
            cursor.limit(max_items)
            progress_bar = tqdm_notebook(total=max_items,desc="get("+string+')')
            regis.set('current_process','Fetching job descriptions') 
            regis.set('progress_name','get '+self.params[1:-1])
            cnt = 0
            gather_list = []
            while cursor.alive:
                progress_bar.update(1)
                cnt +=1 
                regis.set('progress',100* cnt / max_items)
                db_item = cursor.next()
                titl    = db_item.get('Message',{'Title':'N/A'}).get('Title').lower()
                comp    = db_item.get('Message',{'Company':'N/A'}).get('Company')
                state   = db_item.get('Message',{'State':'N/A'}).get('State')
                city    = db_item.get('Message',{'City':'N/A'}).get('City')
                country = db_item.get('Message',{'Country':'N/A'}).get('Country')
                # location= ', '.join(map(str,[city,state,country]))
                # location= location.replace('None','')
                coords  = db_item.get('Message',{'Coordinates':'N/A'}).get('Coordinates')                                 
                id_nr   = db_item.get('_id')        # ObjectId().generation_time contains time stamp
                time    = id_nr.generation_time
                msg     = db_item.get('Message',{'Description':'N/A'}).get('Description')
                msg2    = db_item.get('Model',{'Description':'N/A'}).get('Description')
                length  = len(msg2)
                gather_list.append([id_nr,time,length,titl,comp,city,state,country,coords,msg,msg2])

            # Convert 'gather_list' (above) to DataFrame     
            #   Column labels matching variables in gather_list 
            regis.set('current_process','Building dataframe')
            df_columns  = ['ObjectId','time stamp','length', 'title',\
                           'company','city','state','country','coordinates','description','clean description']            
            df          = pd.DataFrame(gather_list, columns = df_columns )
            # Convert coordinates from string to numbers
            df['coordinates'] = df['coordinates'].apply(eval).apply(lambda x: (x[1],x[0]))
            # Parse job descriptions
            regis.set('current_process','Parse job descriptions')
            df['parsed description']    = df['description'].apply(self.parse_job_description)
            df['word_set']              = df['clean description'].apply(lambda x: set(x.split()))
            df['title set']             = df['title'].apply(lambda x: set(x.split()))            
            # Save result in dictionary
            regis.set('current_process','Saving to dictionary')
            self.get_dic[params] = df  
        self.df = self.get_dic[params]
        self.current_parameters.update({'fetched items':len(self.df)})
        regis.set('current_params',str(self.current_parameters))
        regis.set('progress_name','idle')
        regis.set('progress',0)
        regis.set('current_process','')
        return self

    def add_geoid(self):
        df = self.df
        # Add Geo_ID (fips) codes. This is done with a lookup-table for coordinates. 
        # If no lookup-value, calculate Geo_ID and add to lookup. When ready, 
        # save updated lookup table to file. 
        ## this can take time - add **progress bar**
        max_items = len(df)
        ### progress bar for notebook
        progress_bar = tqdm_notebook(total=max_items,desc="adding Geo_id")
        ### progress bar for plotly/dash app
        regis.set('current_process','Building dataframe: Adding Geo_IDs')
        counter = Counter('t')
        ### add progress bar to function
        def geoid_progress(coords):
            progress_bar.update(1)
            counter.update('t')
            regis.set('progress',100* (counter['t']-1) / max_items)
            return self.geo.coords_to_geoid(coords)   
        ### apply function
        df['geo_id']      = df['coordinates'].apply(geoid_progress)
        regis.set('progress_name','idle')
        regis.set('progress',0)
        regis.set('current_process','Saving Geo_ID lookup table')
        self.geo.save_lookup()
        return self

    def plot_map(self,labels='title',counts=1, mode = 'markers', title = 'default' ):
        df = pd.DataFrame(self.df['coordinates'].to_dict()).T
        df.columns = ['long','lat']
        df['text'] = self.df[labels]
        if type(counts) is str:
            df['cnt'] = self.df['counts']
        else:
            df['cnt'] = counts
        fig = go.Figure(data=go.Scattergeo(
                lon = df['long'],
                lat = df['lat'],
                text = df['text'],
                mode = mode,
                marker = dict(
                            size = 2,
                            opacity = 0.8,
                            reversescale = True,
                            autocolorscale = False,
                            symbol = 'circle',
                            line = dict(
                                width=1,
                                color='rgba(102, 102, 102)'
                            ),
                            colorscale = 'Blues',
                            cmin = 0,
                            color = df['cnt'],
                            cmax = df['cnt'].max(),
                            # colorbar_title="Incoming flights<br>February 2011"
                        )
                ))
        if title is 'default':
            titl = 'job openings for '+str(jd.current_parameters['field'])+ \
                    ' including \'' +  str(jd.current_parameters['string'])+'\' - ' + \
                    str(jd.current_parameters['fetched items'])+' samples'
        fig.update_layout(
                title = titl,
                geo_scope='usa'
            )
        return fig



    def remove_html(self,column=False, separator = ' /n '):
        if not column:
            column = self.current_parameters['field']
        bbb = self.df[self.df[column].dropna().apply(lambda x: '<p>' in x)][column]
        aaa = bbb.apply(lambda x: BeautifulSoup(x).get_text(separator = separator))
        result = aaa.combine_first(self.df[column])
        self.df[column] = result
        return self
 
    # def classify_sentences(self,round_values = True, n_samples=False, random_state = 42):      
    #     if n_samples:
    #         df = self.df.sample(n_samples,random_state = random_state)
    #     else:
    #         df = self.df
    #     df_array = df.apply(lambda x: classify_one_job_description(x,round_values = round_values),axis = 1)
    #     df_classified = pd.concat(list(df_array),sort=False).reset_index(drop=True)
    #     self.classified_sentences = df_classified
    #     return self

     
    # def classify_job_descriptions(self,round_values = True, n_samples=False, random_state = 42):   
    #     if n_samples:
    #         df = self.df.sample(n_samples,random_state = random_state)
    #     else:
    #         df = self.df
    #     tqdm.notebook.tqdm.pandas(desc="classify_job_descriptions()")
    #     df_array = df.progress_apply(lambda x: classify_one_job_description(x,round_values = round_values),axis = 1)
    #     df_classified = pd.concat(list(df_array),sort=False).reset_index(drop=True)
    #     self.classified_sentences = df_classified
    #     return self
    
    def build_classified_word_set(self, category = 'QualificationList'):
        df = self.classified_sentences[
            self.classified_sentences[category] >0.5
        ][['ObjectId','SentenceList']].groupby('ObjectId').aggregate(list)
        df['cleaned'] = df['SentenceList'].apply(sentence_cleaner,tokenized = True)
        df[category+' word_set'] = df['cleaned'].apply(lambda x: set(sum(x,[])))
        self.df = self.df.merge(df,left_on = 'ObjectId',right_index = True)
        return self

    def parse_job_description(self,string):
        '''
        Converts description to dictionary
        1. Splits the job description into a list:  split at linebreak and html tags for bullet/paragraph
        2. A line ending with ":" is assigned as a key
        3. Immediately following lines beginning with "*" are assigned as values for the key   
        4. All other lines are assigned as values to key "000"

        (Developer's comment: Quick & dirty version. Can be done more elegantly with RexEx)
        '''
        # Split job description into items
        split_string = string.replace('<li>','\n *'). \
                                replace('</li>','\n').\
                                replace('<p>','\n').  \
                                replace('</p>','\n'). \
                                split('\n')
        # Initiate dictionary
        key = '000'
        dic = {}
        dic['000']= []

        # Sort items into dictionary
        for item in split_string:
            item  = item.lower()
            item0 = item.replace(' ','')
            if item0 == '':
                pass
            elif item0[0] is "*":
                dic[key].append(item[1:])
            elif item0[-1] == ':':
                key = item.replace(':','') 
                dic[key] =[]
            else:
                dic['000'].append(item)
        return dic
    
    # DUPLICATES 
    def mark_duplicates(self,inplace = True,thresh=0.8):
        '''
        Scalable procedure for identifying duplicates 
        1. Select companies with at least two job descriptions
        2. For each company, sort job descriptions by length (from longer to shorter)
        3. Calculate word_set-similarity between neighboring job descriptions
        4. If the word_set (jaccard) similarity with previous neighbor is larger than 0.8, mark as duplicate
        '''
        #regis
        self.current_parameters['duplicates']={}
        self.current_parameters['duplicates']['threshold']=thresh
        regis.set('current_process','Mark duplicates')
        
        params = self.params
        df = self.df
        if 'duplicate' not in df.columns:
            grp_co = df[['length','ObjectId','company','word_set']].groupby('company').agg(lambda x: list(x))
            grp_co['nr of posts'] = grp_co['length'].apply(len) 

            # Select companies with THREE or more job postings
            # ********* TODO ******** : REMOVE DUPLICATES FOR COMPANIES sith TWO POSTINGS.
            companies = grp_co[grp_co['nr of posts'] > 2]
            # Identify duplicate job descriptions, selected by word_set-similarity between descriptions of similar lengths 
            # Save their ObjectId's in a dict
            dupes_dic = {}
            for company in companies.index:
                company_df         = pd.DataFrame(companies.loc[company].to_dict()).sort_values(by='length',ascending = False)
                dupes_dic[company] = self.dupes(company_df,thresh = thresh)['ObjectId']

            # Save the dict with company names and ObjectIds for dupes 
            self.duplicates_dic[params] = dupes_dic
            dupes_df = pd.DataFrame(1,index = np.concatenate(list(self.duplicates_dic[self.params].values())),columns = ['duplicate'])
            self.df = pd.merge(self.df,dupes_df,left_on='ObjectId',right_index = True, how='outer')
        if inplace is True:
            self.get_dic[params] = self.df
         
        #regis
        self.current_parameters['duplicates']['count']=int(self.df['duplicate'].count())
        self.current_parameters['duplicates']['status'] = 'marked'
        regis.set('current_params',str(self.current_parameters))
        
        return self

    def drop_duplicates(self,inplace = False):    
        '''
        Duplicate job descriptions are dropped from the dataframe self.df
        If inplace = True they are permanently erased from the dataframe 
        They will remain available in 
         - self.duplicates_dic[self.params] and in 
         - self.duplicates_dic["{'string': <value>, 'max_items': <value>}"]
        '''
        #regis
        regis.set('current_process','Dropping duplicates')
        regis.set('progress_name','Active')
        
        if 'duplicate' not in self.df.columns:
            self.mark_duplicates()
        self.df = self.df[self.df['duplicate']!=1].drop(columns = ['duplicate'])
        if inplace is True:
            self.get_dic[self.params] = self.df
        
        #regis
        regis.set('current_process',' ')
        regis.set('progress_name','Idle')
        self.current_parameters['duplicates']['status'] = 'dropped'
        regis.set('current_params',str(self.current_parameters))
        
        return self  
 
    
    def dupes(self,df,thresh = 0.8):
        '''
        Measures word set similarity 
        '''
        crps = pd.DataFrame(self.crpsim_previous(df['word_set']),index = df.index)
        return df[crps[0] > thresh]
    
    def crpsim_previous(self,corpora):
        return [0] +[self.word_set_similarity(pair) for pair in self.consecutive_pairs(corpora)] 
    
    def word_set_similarity(self,corps):
        '''
        Measures similarity between two word_sets (type:set)
        '''
        corp1,corp2 = corps
        diff = len(corp1.symmetric_difference(corp2))
        total= len(corp1)+len(corp2)
        return 1- diff/total 
    
    def consecutive_pairs(self,array):
        return np.array([array[1:],array[:-1]]).transpose()


    def bullets_column(self,label,keystrings,bullet_formatted=True, *args,**kwargs):
        regis.set('current_process','Extracting bullets for: '+str(keystrings))
        self.df[label] = self.df["parsed description"].apply(lambda dic: self.identify_bullets(dic,keystrings))
        if bullet_formatted:
            self.df[label] = self.df[label].apply(lambda lst: str(lst).replace(
                                    '\"','\''       ).replace(
                                    '[[\'','*'      ).replace(
                                    '\', \'','\n*'  ).replace(
                                    '\'], [\'','\n*').replace(
                                    '\']]',''       ).replace(
                                    '[]',''         )
                                    )
        self.current_parameters['bullets for'] = keystrings
        regis.set('current_params',str(self.current_parameters)) 
        regis.set('current_process','Idle')
        return self
    
    def identify_bullets(self,dic,keystrings):
        '''
        return values for keys containing any of the listed strings
        '''
        def return_true(item,truth = False): ## TODO: Replace with filter()
            if truth:
                return item

        return list(filter(None,[return_true(item[1],any(key in item[0] for key in keystrings)) for item in dic.items()]))
        
    def nn_classify(self,*args,**kwargs):
        '''
        '''
        self.get(self.title,**self.kwargs)['nn classified']=self.get(self.title,**self.kwargs)[['description','company']].T.apply(lambda x: SentenceClassifier.predict_Sentence(**x))


    def ngrams(self,array, N=2, string_form = False):
        '''
        Returns all N-element long sequences of neighboring elements. Works both for strings and lists.
        string_form = False       : operates on lists, no splits
                     'sentences'  : splits text into list of sentences, then splits sentences into words
                     'words'      : splits string into words
                     'characters' : operates on strings, no splits

        Examples:
        [in ]  ngrams([1,2,3,4])
        [out]  array([[1, 2],[2, 3],[3, 4]])

        [in ]  ngrams('one two three four', string_form = 'words')
        [out]  array(['one two', 'two three', 'three four'], dtype='<U10')

        [in ]  ngrams('one two three four', string_form = 'characters')
        [out]  array(['on', 'ne', 'e ', ' t', 'tw', 'wo', 'o ', ' t', 'th', 'hr', 're',
                      'ee', 'e ', ' f', 'fo', 'ou', 'ur'], dtype='<U2')
        '''
        # Split string
        if string_form:
            if type(array) is str:
                if string_form   is 'words':
                    array = array.split()
                else:
                    string_form = 'characters'
                    array = [i for i in array]

        # Build N-grams
        if type(array[0]) in (list, np.ndarray):
            result = [self.ngrams(item,N=N) for item in array]
        else:
            array0 = np.append(np.array(array),0)
            result = np.transpose([array0[i:i-N] for i in range(N)])

        # Return result
        if string_form:
            spacer = {'words':' ', 'characters':''}
            result = np.array([spacer[string_form].join(item) for item in result])
        return result

    def ngrms(self,column, N_max = 8):
        '''
        '''
        regis.set('current_process','Creating n-gram tables') 
        regis.set('progress_name','ngrms('+column+')')
        regis.set('progress',0)
        regis.set('cnt',0)
        regis.set('max', len(self.df[column]))


        sntc = lambda x: sentence_cleaner(x,tokenized = True)
        n_grms = lambda x: [
            [' '.join(ngr) for ngr in self.ngrams(sntc(x), N=nlen)] 
            for nlen in range(min((len(sntc(x))+1),N_max))
        ]
#        cnt = lambda x: Counter(np.concatenate(n_grms(x)))
        def cnt(x):
            regis_max = eval(regis.get('max'))
            regis_cnt = eval(regis.get('cnt'))+1
            regis.set('cnt',regis_cnt)
            regis.set('progress',100* regis_cnt / regis_max)
            return Counter(np.concatenate(n_grms(x)))

        tqdm_notebook.pandas(desc='ngrms('+column+')')
        self.df[column + ' ngrams'] = self.df[column].progress_apply(cnt) 

        regis.set('current_process','Idle') 
        regis.set('progress_name',' ')
        regis.set('progress',0)
        return self

    def common_words_count(self,column):
        cnt = Counter()
        self.ngrms(column).df[column+' ngrams'].apply(lambda x: cnt.update(x))
        self.common_words = cnt
        return self

#### map_common / ngrams_count / n_grammer can be made to replace map_common_titles 

    def map_common(self,column,threshold = 5):
        '''
        Counts n-grams for 'column' and maps the n-grams with more than 'threshold' counts on the column 
        '''
        self.ngrams_count(column)
        ngr = pd.Series(self.ngr)
        
        map_from = set(ngr[ngr >= threshold].keys())
        map_on   = self.df[column+' ngrams'] 
        map_func = lambda x: set(x).intersection(set(map_from))
        self.df[column+' common'] = map_on.apply(map_func)
        return self


    def ngrams_count(self,column):
        cnt = Counter()
        self.ngrammer(column).df[column+' ngrams'].apply(lambda x: cnt.update(x))
        self.ngr = cnt
        return self

    def ngrammer(self,column):
        if not (column + ' ngrams') in self.df:
            sntc = lambda x: sentence_cleaner(x,tokenized = True)
            ngrms = lambda x: [
                [' '.join(ngr) for ngr in self.ngrams(sntc(x), N=nlen)] 
                for nlen in range(len(sntc(x))+1)
            ]
            cnt = lambda x: Counter(np.concatenate(ngrms(x)))
            tqdm_notebook.pandas(desc="ngrammer("+column+')')
            self.df[column + ' ngrams'] = self.df[column].progress_apply(cnt) 
        return self


## N-GRAM ANALYSIS

    def ngram_analysis(self, field = 'title', ngram_min_count=10, *args, **kwargs):
        '''
        Perform n-gram analysis of 'field' in self.df

        1) 
        '''
        regis.set('current_process','ngram_analysis('+field+')')
        # set parameters        
        self.current_parameters['field']           = field
        self.current_parameters['ngram_min_count'] = ngram_min_count
        
        if self.params not in self.ngram_analysis_dic:
            self.ngram_analysis_dic[self.params] = {}
        if field not in self.ngram_analysis_dic[self.params]:
            self.ngram_analysis_dic[self.params][field] = {}

        # Build n_grams and count them
        self.common_words_count(field)
        self.map_common(field)
        #convert jd.common_words to pandas series / include only ngrams with at least n_min counts  
        cw = pd.Series(self.common_words) 
        cw = cw[cw >= ngram_min_count]
        cw = cw.sort_values(ascending = False)
        # use remaining common_words as vocabulary
        # add column 'title vocab' to jd.df = 'title common' with ngrams outside of the vocabulary removed
        vocab = set(cw.index)
        tqdm_notebook.pandas(desc="reduced n-gram vocabulary")
        self.df[field+' vocab'] = self.df[field+' common'].progress_apply(lambda st: vocab.intersection(st))
        self.build_tree_graph()
        regis.set('current_params',str(self.current_parameters))
        return self
    
    
    def build_tree_graph(self):
        '''
        BUILDING A TREE-DIAGRAM OF COMMON WORDS (NGRAMS)
        '''
        field           = self.current_parameters['field'] 
        root            = eval(self.params)['string'] 
        ngram_min_count = self.current_parameters['ngram_min_count']
        # Create a dataframe from the pandas series of common words (raw count / not normalized)
        cw = pd.Series(self.common_words) 
        cw = cw[cw >= ngram_min_count] 
        cw = cw.sort_values(ascending = False)
        c1 = cw.copy().reset_index()
        c1['list']=c1['index'].apply(lambda x: x.split())
        c1['len'] = c1['list'].apply(len)
        c1['set'] = c1['list'].apply(set)
        c1['set_len'] = c1['set'].apply(len)
        c1['set_str'] = c1['set'].apply(str)
        c1.set_index('index',inplace = True, drop=True)
        
        # Keep only words including the root-word
        c1 = c1[c1['list'].apply(lambda x: root in x)]
        

        # Build the tree-structure as a dictionary, the levels are the number of words in the n-grams

        tree_dic = {}
        n_levels = c1['len'].max()
        leaf_sum_dic = {}
        for n in range(1,n_levels-1):
            branches = c1[c1['len']==n]
            leaves = c1[c1['len']==n+1]
            tree_dic[n]={}
            for branch in branches.index:
                branch_leaves = leaves[[(branch in leaf) for leaf in leaves.index]].index
                #Remove the branch if it has only one leaf, the leaf takes over the role of the branch
                if len(branch_leaves) == 1:
                    old_branch = branch
                    new_branch = branch_leaves[0]
                    for item in tree_dic[n-1].items():
                        key  = item[0]
                        vals = item[1]
                        for m in range(2,n-1):
                            tree_dic[m][key] = [new_branch if x==old_branch else x for x in vals]
                if len(branch_leaves) >1 :
                    tree_dic[n][branch] = list(branch_leaves)

        # convert dictionary to graph
        ldata = list(tree_dic.values())
        data = {}
        [data.update(dic) for dic in ldata]
        self.tree_dic = tree_dic
        # convert the dictionary into a graph with networkx
        self.tree_graph = nx.Graph(data) 
        # using a vocabulary consisting of only the nodes, construct 'field node vocab' in self.df
        self.node_vocab = set(self.tree_graph.nodes)
        self.df[field+' node vocab']= self.df[field+' common'].apply(lambda x: self.node_vocab.intersection(x))
        return self

    def plot_tree(self, prog = 'twopi'):
        G = self.tree_graph
        # plot the tree
        pos = nx.nx_pydot.pydot_layout(G, prog=prog)
        plt.figure(3,figsize=(14,14)) 
        nx.draw_networkx(G,pos,node_size=20,width = 0.4,font_size=10,alpha = 1)
        # nx.draw_networkx(G,pos,node_size=0,font_color=(1,0,0),font_size=10,alpha = 1)
        plt.show()
        return self
    
    def build_archetypes(self, n_archetypes = 10, node_vocab = False):
        self.current_parameters['n_archetypes'] = n_archetypes
        self.current_parameters['node_vocab']   = node_vocab
        field     = self.current_parameters['field'] 
        
        if node_vocab:
            vocab = field+' node vocab'
        else:
            vocab = field+' vocab'
        # *** ARCHETYPING
        if 'archetypes' not in self.ngram_analysis_dic[self.params][field]:
            self.ngram_analysis_dic[self.params][field]['archetypes'] = {}
        if vocab not in self.ngram_analysis_dic[self.params][field]['archetypes']:
            self.ngram_analysis_dic[self.params][field]['archetypes'][vocab]={}
        if n_archetypes not in self.ngram_analysis_dic[self.params][field]['archetypes'][vocab]:
            # Construct the word-matrix, each job-title is a word-vector
            # Normalize for each word, so that the matrix shows the distribution of a word across the job-titles
            word_mat = pd.DataFrame(self.df[vocab].apply(Counter).to_dict()).fillna(0)
            word_matn = word_mat.T.apply(norm_dot).T
            # Archetype the vocabulary, using the matrix of normalized word-vectors
            arc    = Archetypes(word_matn.T,n_archetypes)
            self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes] = arc
        self.archetypes = self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes]
        return self

    # Heatmap plot of Archetypes, choose an archetype (col-number), sort in descending order, cut at threshold & plot
    # - they seem quite well separated

    def plot_archetypes(self,col=4,thresh=0.05, node_vocab_filter = True, root_vocab_filter= False, figsize=(16, 10)):
        n_archetypes=self.current_parameters['n_archetypes']
        field       =self.current_parameters['field']
        node_vocab  =self.current_parameters['node_vocab']
        root = eval(self.params)['string']
        if node_vocab:
            vocab = field+' node vocab'
        else:
            vocab = field+' vocab'
       
        arc = self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes]
        arc_f = arc.f.T.copy()
        if node_vocab_filter:
            arc_f = arc_f.loc[self.node_vocab.intersection(arc_f.index)]
        if root_vocab_filter:
            arc_f = arc_f[arc_f.index.map(lambda x: root in x)]
        arc_fn = arc_f.apply(norm_dot) 
   
        plt.figure(figsize=figsize)
        arc_plot = arc_fn[arc_fn[col]>thresh].sort_values(by=col,ascending = False)
        sns.heatmap(arc_plot)
        self.current_arc_plot = arc_plot
        return
    
    def correlated_ngrams(self,keyword,node_vocab_filter = True, root_vocab_filter= False):
        
        n_archetypes=self.current_parameters['n_archetypes']
        field       =self.current_parameters['field']
        node_vocab  =self.current_parameters['node_vocab']
        root = eval(self.params)['string']
        if node_vocab:
            vocab = field+' node vocab'
        else:
            vocab = field+' vocab'
       
        arc = self.ngram_analysis_dic[self.params][field]['archetypes'][vocab][n_archetypes]
        arc_f = arc.f.T.copy()
        if node_vocab_filter:
            arc_f = arc_f.loc[self.node_vocab.intersection(arc_f.index)]
        if root_vocab_filter:
            arc_f = arc_f[arc_f.index.map(lambda x: root in x)]
        arc_fn = arc_f.apply(norm_dot) 

        # Calculate word-embedding: shows which words are similar / co-occur
        word_corr = arc_fn@arc_fn.T

        # Example: 
        # Here only correlated words including 'nurse' - i.e. potential job titles - are included
        # root       = keyword in the latest get(keyword)
        print('keyword: '+ keyword +'\nCorrelated words:')
        correlated = word_corr[keyword].sort_values(ascending = False)
        return correlated
    

    


# ## CLUSTERING 

#     def word_vectors(self,column='QualificationList word_set',word2vec=glove_dic):
#         '''
#         ** What: Converts word sets (in 'column') to word-vectors, using GloVe, returning a DataFrame
#         ** How: The word vector for a set is the  sum of the word-vectors for the words in the set. 
#         todo: this is very course - should be improved 
#         ** Output: DataFrame - each row is the word-vec for a job description word-set  
#         '''
#         cluster_dic = self.cluster_dic
#         if self.params not in cluster_dic:
#             cluster_dic[self.params] = {}
#         df = self.df.copy()
#         glove_set = set.union(*df[column]).intersection(glove_dic)
#         cluster_dic[self.params]['glove_set'] = glove_set
#         df['glove_set'] = df[column].apply(lambda word_set: word_set.intersection(glove_set)-stopw)
#         coords = df['glove_set'].apply(lambda x: glove_df[x].T.sum()).T
#         coords.columns = df['ObjectId']
#         return coords
    
#     def cluster(self,column='QualificationList word_set',n_clusters=10,n_samples=500,random_state=42):
#         '''
#         Cluster job descriptions by description content.
#         Steps:
#         1. transform to word_vectors  
#         2. Create clusters with K-Means (todo: test other cluster methods)
#         3. Tag each job description with the cluster number
#         '''
#         params = self.params
#         X             = self.word_vectors(column).T
#         X_train       = X.sample(n_samples,random_state = random_state)
#         print('*** cluster: fitting model....')
#         model         = KMeans(n_clusters=n_clusters, random_state=random_state).fit(X_train)
#         self.model    = model
#         self.cluster_dic[params]['model'] = model
#         print('*** cluster: predicting....')
#         self.cluster_predict = model.predict
#         col_title     = str(n_clusters)+' clusters'
#         print('*** cluster: writing self.clusters ....')
#         X[col_title]  = model.predict(X)
#         self.cluster_dic[params]['X'] = X
#         self.clusters = X
#         print('*** cluster: merging cluster assignments into self.df ....')
#         self.df       = self.df.merge(X[col_title],left_on="ObjectId",right_index = True)
#         print('*** cluster: complete')
#         return self


#     def get_word_set_matrix(self):
#         if self.params not in self.word_set_matrix_dic:
#             tcrp = Counter()
#             self.df['word_set'].apply(lambda x: tcrp.update(x))
#             df = pd.DataFrame.from_dict(tcrp, orient = 'index', columns = ['counts']).sort_values(by='counts',ascending = False)
#             for i in jd.df['word_set'].index:
#                 df[i] = pd.Series(1,index = jd.df['word_set'].loc[i])
#             self.word_set_matrix_dic[self.params] = df.T
#         self.word_set_matrix = self.word_set_matrix_dic[self.params]
#         return self.word_set_matrix
    
    

    
    
    def subtract_fundamental(self,normalize = True):
        '''
        *** Experimental / under development *** 
        
        operates on the corpus-matrix
        
        Returns the corpus_matrix, subtracting the 0th eigenvector component from each corpus.
        The intention is to reduce features that don't differentiate the corpora, such as stopwords. 
        
        The 0th eigenvector is approximated by the normalized vector of word occurences across all corpora. 
        
        Challenge: returned vectors can have negative coefficients, getting in the way of NMF
        
        Suggested continuation - split the positive & negative parts of the returned vectors, i.e. the 'domi-matrix'
        
        '''
        def sub_ev0(vec,normalize = normalize):
            v0   = norm_dot(df0.T['counts'])
            v    = norm_dot(vec)
            frac = np.sqrt(v@v0)
            if normalize:
                vec1 = (v - frac * v0)
                return norm_dot(vec1)
            else:
                nrm  = np.linalg.norm(vec)
                vec1 = (v - frac * v0)*nrm
                return vec1
        df0 = self.get_corpus_matrix().fillna(0)
        df  = pd.DataFrame([sub_ev0(df0.loc[idx]) for idx in df0.index], index = df0.index)
        return df
'''
# JOB DESCRIPTION DATABASE

* 1.5M active job ads
* 3-month time window
* Untapped source of info - good for knowledge graph

## DB ORIGINAL CONTENT

* Timestamp & Status
* Original ad text (formatted)
* Pre-processed text:
    - unformatted ad text
    - extracted keywords / entities / skills 

The preprocessed keywords / entities / skills are compact and general and may have 
involved more data in the processing, such as Onet data for occupations.  
'''

## MATH
# # %%
# # Test: Get the first job description in the collection
# jd.collection.find()[0]
classify_job_description_dic = {}
# def classify_one_job_description(pd_series,round_values=True):
#     objectid = pd_series['ObjectId']
#     if objectid not in classify_job_description_dic:
#         Description= pd_series['description']
#         Company=pd_series['company']
#         result = SentenceClassifier.predict_Sentence(Description, Company)
#         df_classification = pd.DataFrame([dict((subitem[1],subitem[0]) for subitem in item) for item in result['SentenceTag']])
#         if round_values:
#             df_classification = df_classification.fillna(0).applymap(round)
#         df_sentence = pd.DataFrame(pd.Series(objectid, index = range(len(result['SentenceList']))),columns = ['ObjectId'])
#         df_sentence['SentenceList'] = result['SentenceList']
#         df = df_sentence.merge(df_classification, left_index=True, right_index = True, how='left')
#         classify_job_description_dic[objectid] = df
#     return classify_job_description_dic[objectid]


jd = JobDescriptions()




#%%
#####################################
#####################################   WEB APP  ######################################
#####################################


## DASH/PLOTLY  WEB APP


class DashTable:
    def __init__(self,df,page_size = 10):
        self.df        = df
        self.PAGE_SIZE = page_size
        self.operators =[['ge ', '>='],
                        ['le ', '<='],
                        ['lt ', '<'],
                        ['gt ', '>'],
                        ['ne ', '!='],
                        ['eq ', '='],
                        ['contains '],
                        ['datestartswith ']]
        self.layout = self.make_layout() 
    def make_layout(self):    
        layout =html.Div([
                dash_table.DataTable(
                    id='table-sorting-filtering',
                    style_data={'whiteSpace': 'pre-line'},
                    columns=[
                        {'name': i, 'id': i, 'deletable': True} for i in self.df.columns
                    ],
                    fixed_rows={ 'headers': True, 'data': 0},
                    style_table={
                         'maxWidth': '1100px',                       
                         'overflowX': 'scroll',
                         'maxHeight': '1000px',
                         'overflowY': 'scroll'},
                    style_cell={
                        #'height': 'auto',
                        'textAlign': ['left'],
                        # all three widths are needed
                        'minWidth': '0px', 'width': '80px', 'maxWidth': '300px',
                        'whiteSpace': 'normal'
                    },
                    style_cell_conditional=[
                        {'if': {'column_id': 'title'},
                        'width': '20%'},
                        {'if': {'column_id': 'company'},
                        'width': '7%'},
                        {'if': {'column_id': 'city'},
                        'width': '4%'},
                        {'if': {'column_id': 'state'},
                        'width': '2%'},
                        {'if': {'column_id': 'country'},
                        'width': '2%'},
                        {'if': {'column_id': 'bullets for'},
                        'width': '64%'},
                    ],
                    page_current= 0,
                    page_size= self.PAGE_SIZE,
                    page_action='custom',

                    filter_action='custom',
                    filter_query='',

                    sort_action='custom',
                    sort_mode='multi',
                    sort_by=[]
                )
            ])
        return layout

    def split_filter_part(self,filter_part):
        for operator_type in self.operators:
            for operator in operator_type:
                if operator in filter_part:
                    name_part, value_part = filter_part.split(operator, 1)
                    name = name_part[name_part.find('{') + 1: name_part.rfind('}')]

                    value_part = value_part.strip()
                    v0 = value_part[0]
                    if (v0 == value_part[-1] and v0 in ("'", '"', '`')):
                        value = value_part[1: -1].replace('\\' + v0, v0)
                    else:
                        try:
                            value = float(value_part)
                        except ValueError:
                            value = value_part

                    # word operators need spaces after them in the filter string,
                    # but we don't want these later
                    return name, operator_type[0].strip(), value

        return [None] * 3
    
    def update_table(self,page_current, page_size, sort_by, filter):
        filtering_expressions = filter.split(' && ')
        dff = self.df
        for filter_part in filtering_expressions:
            col_name, operator, filter_value = self.split_filter_part(filter_part)

            if operator in ('eq', 'ne', 'lt', 'le', 'gt', 'ge'):
                # these operators match pandas series operator method names
                dff = dff.loc[getattr(dff[col_name], operator)(filter_value)]
            elif operator == 'contains':
                dff = dff.loc[dff[col_name].str.contains(filter_value)]
            elif operator == 'datestartswith':
                # this is a simplification of the front-end filtering logic,
                # only works with complete fields in standard format
                dff = dff.loc[dff[col_name].str.startswith(filter_value)]

        if len(sort_by):
            dff = dff.sort_values(
                [col['column_id'] for col in sort_by],
                ascending=[
                    col['direction'] == 'asc'
                    for col in sort_by
                ],
                inplace=False
            )

        page = page_current
        size = page_size
        return dff.iloc[page * size: (page + 1) * size].to_dict('records')
dashtable = DashTable(pd.DataFrame())



external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css',dbc.themes.BOOTSTRAP]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
def load_jd_df():
    df = jd.df[['title','company','city','state','country','bullets for']]
    df_bulletsonly = df[df['bullets for'].apply(lambda x: len(x)>0)]
    return df_bulletsonly

dashtable = DashTable(pd.DataFrame())

###############
# APP LAYOUT
##############

def div_bar(lst = [dbc.Row(dcc.Markdown('#### sample text'))], position = 'fixed'):
    bar =     html.Div(children =[ html.Ul(children= lst)],style={'background-color':'honeydew','height':'10%','position':position,
    'top':'0','width':'100%'})
    return bar

# TOP NAV/INFO BAR
info_bar = html.Div([
        dbc.Row([
            dbc.Col([
                dcc.Markdown(children='# **JobDX**',
                        className = "nine columns offset-by-one",
                        style={
                        'margin-top': 20,
                        'margin-right': 20
                        },
                )
            ],width = {'size': 2}
            ),
            dbc.Col([
                # Current process
                dbc.Row(dcc.Markdown('#### current process')),
                dbc.Row(html.Div(id='current_process',children='')),
            ],width = {'size':3}
            ),
            dbc.Col([
                # Progress
                dbc.Row(dcc.Markdown('#### progress bar')),
                dbc.Row(html.Div(id='progress_name',children='')),
                html.Div(
                    [
                        dcc.Interval(id="interval", n_intervals=0, interval=250),
                        dbc.Progress(id="progress"),
                    ]
                ),
            ],width = {'size':4}
            ),       
        ])
    ],  className = "nine columns offset-by-one",
    )

app.layout = html.Div(html.Div([
    # Get job descriptions 
    html.Div(
        [
        # these rows are covered by the top bar
        dbc.Row(dcc.Markdown('#### space')),
        dbc.Row(dcc.Markdown('#### space')),
        dbc.Row(dcc.Markdown('#### space')),
        # Current set in use
        dbc.Row([
            dbc.Col([
                
                dbc.Row(dcc.Markdown('------')),
                dbc.Row(dcc.Markdown('#### current parameters')),
                dbc.Row(html.Div(id='current_get',children='')),
                dbc.Row(html.Div(id='current_params',children='')),
                dbc.Row(html.Div(id='current_archetypes',children='')),
            ],width = {}),
        ]),

        dbc.Row(html.Div('__________________________________')),
        dbc.Row(dcc.Markdown('### get job descriptions')),
        dbc.Row([   
                
                # button: submit
                dbc.Col(html.Div([
                    html.Div('Submit'),
                html.Button(id='submit', type='submit', children='GET')
                    ]),width = 2
                ),
                # input field: search string 
                dbc.Col(html.Div([
                    html.Div('Search string'),
                    dcc.Input(id='search_string', value='', type='text', size='50')
                        ]),width = 5
                ),
                # dropdown: field to search
                dbc.Col(html.Div([
                    html.Div('Search Field'),
                    dcc.Dropdown(
                            id='field',
                            options=[
                                {'label': 'Title', 'value': 'Title'},
                                {'label': 'Company', 'value': 'Company'},
                                {'label': 'Description', 'value': 'Description'}
                            ],
                            value='Title'
                        )
                    ]),width = 3         
                ),
                # # dropdown: field to do n-gram analysis on
                # dbc.Col(html.Div([
                #     html.Div('Word Analysis on'),
                #     dcc.Dropdown(
                #             id='ngrams',
                #             options=[
                #                 {'label': 'title', 'value': 'title'},
                #                 {'label': 'description', 'value': 'description'}
                #             ],
                #             value='title'
                #         )
                #     ])         
                # ),
                # input field: max_items
                dbc.Col(html.Div([
                    html.Div('Max Items Fetched'),
                    dcc.Input(id='max_items', value=10000, type='number')
                        ]),width = 2
                ),
                # dbc.Col(html.Div([
                #         html.Div('Drop Duplicates'),
                #         dcc.Dropdown(
                #             id='drop_duplicates',
                #             options=[
                #                 {'label': 'Yes', 'value': 'Yes'},
                #                 {'label': 'No', 'value': 'No'},
                #             ],
                #             value='Yes'
                #         ),
                #     ])
                # ),
            ]),
        # dbc.Row(html.Div(id='dashtable',children = dashtable.layout )
        # ),
        ], className = "nine columns offset-by-one" 
    ),
    html.Div([
        dbc.Row([
            dbc.Col( 
                html.Div([
                    dcc.Graph(
                        id='job_map'
                    )
                ])
            )
        ], className = "nine columns offset-by-one"),  
    ]),
    html.Div([
        dbc.Row(
                html.Div(style={'fontSize':14, 'font-family':'sans-serif'},children=[
                    html.Button(id='dropper', type='submit', children='UPDATE TABLE'),
                    html.Div(id= 'dashtable', children = dashtable.layout),
                    ]) 
            )
        ], className = "nine columns offset-by-one"
    ),
    # Archetypes / Topics / Personas
    html.Div(
        [
            dbc.Row(html.Div('__________________________________')),
            dbc.Row(dcc.Markdown('### personas')),

            # Build Archetypes / Topics / Personas
            dbc.Row(
                [  
                    # button: submit
                    # dbc.Col(html.Div([
                    #     html.Div('Submit'),
                    # html.Button(id='arcs_submit', type='submit', children='BUILD')
                    #     ]),width = 2
                    # ),
                    # dropdown: field to do n-gram analysis on
                    dbc.Col(html.Div([
                        html.Div('Based on'),
                        dcc.Dropdown(
                                id='ngrams',
                                options=[
                                    {'label': 'titles', 'value': 'title'},
                                    {'label': 'descriptions', 'value': 'description'}
                                ],
                                #value='select',
                               
                            )
                        ]), width = 2        
                    ),
                    # dropdown: number of archetypes / topics
                    dbc.Col(html.Div([
                        html.Div(' #Personas'),
                        dcc.Dropdown(
                            id='n_arcs',
                            options=[{'label': k, 'value': k}
                                     for k in range(1, 16)],
                            value=4,
                            multi=False,
                            style={'width':'100px'}
                            ),
                        ]), width = 2 
                    ),
                    # input field: display/threshold
                    dbc.Col(html.Div([
                        html.Div('Cut at %'),
                        dcc.Dropdown(
                            id='arcs_thresh',
                            options=[{'label': str(k) , 'value': k/100}
                                     for k in range(1,100)],
                            value=30/100,
                            multi=False
                            ),
                        # html.Div('threshold'),
                        # dcc.Input(id='arcs_thresh', value=0.05, type='number')
                        ]), width = 2 
                    ),
                    # input field: display/normalize
                    dbc.Col(html.Div([
                        html.Div('Normalize:'),
                        dcc.Dropdown(
                                id='norm_arcs',
                                options=[
                                    {'label': 'None', 'value': 'none'},
                                    {'label': 'Sum of Keyword values in Persona =1', 'value': 'arcs_sum'},
                                    {'label': 'Sum of Persona values in Keyword =1 ', 'value': 'ngrams_sum'},
                                    {'label': 'Max Keyword value in Persona =1', 'value': 'arcs_scale'},
                                    {'label': 'Max Persona value in Keyword =1 ', 'value': 'ngrams_scale'},
                                    {'label': 'Persona scalar product w self =1', 'value': 'arcs_dot'},
                                    {'label': 'Keyword scalar product w selt =1 ', 'value': 'ngrams_dot'},
                                ],
                                value='ngrams_sum'
                            ),
                        ])
                    ),
                ]
            ),

            # PERSONA FIGURES
            dbc.Row([
                html.Div([
                    dcc.Graph(
                        id='arcs_clustermap'
                    )
                ])
            ]),

            dbc.Row([
                html.Div([
                    dcc.Graph(
                        id='arcs_sorted'
                    )
                ])
            ]),


        ], className = "nine columns offset-by-one" 
    ),

    # Top Bar
    div_bar(info_bar),
    
]),style={'fontSize':14, 'font-family':'sans-serif'})

# BUILD ARCHETYPES / TOPICS


@app.callback([Output('arcs_sorted','figure'),
            Output('arcs_clustermap','figure'),], 
            [Input('n_arcs','value'),
            Input('arcs_thresh','value'),
            Input('norm_arcs','value'),
            Input('ngrams','value')
            ])

def update_output_get(n_arcs,thresh,norm_type,ngrams):
    if type(ngrams) is not str:
        raise PreventUpdate
    else:
        if jd.current_parameters['field'] is not ngrams:
            jd.ngram_analysis(field = ngrams)
        regis.set('current_process','building '+str(n_arcs)+' topics')
        df = jd.build_archetypes(n_arcs).archetypes.f
        vocab = jd.node_vocab.intersection(df.columns)
        df = df[vocab]
        norm_fun = {
            'none'   : (lambda x: x.T),
            'ngrams_sum' : (lambda x: x.apply(norm_sum).T),
            'arcs_sum'   : (lambda x: x.T.apply(norm_sum)),
            'ngrams_scale' : (lambda x: x.apply(scale).T),
            'arcs_scale'   : (lambda x: x.T.apply(scale)),
            'ngrams_dot' : (lambda x: x.apply(norm_dot).T),
            'arcs_dot'   : (lambda x: x.T.apply(norm_dot)),
        }
        dfn = norm_fun[norm_type](df)

        #dfn = df.T.apply(norm_dot)

        def f(i):
            sorted = dfn.sort_values(by=i,ascending=False) #Sort by archetype/topic i
            cut = sorted[sorted[i]>=thresh]
            result = cut.sort_values(by=i)
            return result

        maxrows = int(1+ n_arcs//3)
        cols = 3
        fig = make_subplots(rows=maxrows, cols=cols, horizontal_spacing=0.2)  
        for i in range(n_arcs):
            fig.add_trace( go.Heatmap(  z = f(i),
                                        y = f(i).index,
                                        x = f(i).columns,
                                        xgap = 1,
                                        ygap = 1,
                            ), col = i%cols +1,row = int(i//cols)+1
                )
        fig.update_layout(height=400*maxrows, width=1200, title_text="TOPICS")
        
        dfc =dfn.copy().T
        dfc['str_index'] = 'persona '
        dfc['str_index'] = dfc['str_index'] + dfc.index.map(str)
        dfc = dfc.set_index('str_index',drop=True)
        dfc = clustermap(dfc)


        figc = go.Figure(
                    data=go.Heatmap(
                        z=dfc.values,
                        x=dfc.columns,
                        y=dfc.index,
                        xgap = 0.3,
                        ygap = 0.3
                    ),
                    layout= go.Layout(
                                        width=(n_arcs+1)*60, height=dfc.shape[0]*24, 
                                        yaxis={"title": "WORD COMBINATIONS","tickfont": {"size": 8}, "tickangle": -20},
                                        xaxis={"title": "PERSONAS",
                                            "tickmode": "array","tickvals":dfc.columns,
                                            "tickfont": {"size": 8}, "tickangle": -20}
                    )
        ) 
        


        
        
        regis.set('current_process','')
        return fig, figc



# GET JOB DESCRIPTIONS 
@app.callback(Output('job_map', 'figure'), 
            [Input('submit','n_clicks')], 
            [State('search_string','value'),
             State('field', 'value'),
             State('max_items','value')])

def update_output_get(n_clicks, search_string, field, max_items):
    global dashtable
    if n_clicks is None:
        raise PreventUpdate
    else:
        regis.set('current_process','Collecting Job Descriptions')
        jd.get(string=search_string,field=field,max_items=max_items)
        #Plot map
        regis.set('current_process','Plotting geomap')
        job_map = jd.plot_map()  
        LAYOUT = {'height':500,
          'plot_bgcolor': 'white',
          'paper_bgcolor': 'white'}
        job_map['layout'].update(LAYOUT) 
        #Remove duplicates
        regis.set('current_process','Removing Duplicates')
        jd.drop_duplicates()
        # regis.set('current_process','Analyzing word combinations')
        # jd.ngram_analysis(field=ngrams)
        regis.set('current_process','Building dashtable')
        dashtable = DashTable(jd.df)
        #Bullets for
        string = 'require'
        regis.set('current_process','Extracting bullets for ['+string+']')
        jd.bullets_column('bullets for',[string])
        regis.set('current_process','Idle')
        return job_map

@app.callback(Output('dashtable','children'), 
            [Input('dropper','n_clicks')])
def update_table(n_clicks):
    if n_clicks is None:
        raise PreventUpdate
    else:
        global dashtable
        dashtable = DashTable(load_jd_df())
        return dashtable.layout

@app.callback(
    Output('table-sorting-filtering', 'data'),
    [Input('table-sorting-filtering', "page_current"),
     Input('table-sorting-filtering', "page_size"),
     Input('table-sorting-filtering', 'sort_by'),
     Input('table-sorting-filtering', 'filter_query')])
def update_table(page_current, page_size, sort_by, filter):
    return dashtable.update_table(page_current, page_size, sort_by, filter)


# @app.callback(
#     Output('table-sorting-filtering', 'data'),
#     [Input('dashtable','children'),
#      Input('table-sorting-filtering', "page_current"),
#      Input('table-sorting-filtering', "page_size"),
#      Input('table-sorting-filtering', 'sort_by'),
#      Input('table-sorting-filtering', 'filter_query')])
# def update_table(children,page_current, page_size, sort_by, filter):
#     global dashtable
#     return dashtable.update_table(page_current, page_size, sort_by, filter)


# CURRENT PROCESS
@app.callback(
    Output(component_id='current_process', component_property='children'),
    [Input("interval", "n_intervals")]
)
def update_output_div(input_value):
    return regis.get('current_process').decode()

# CURRENT PARAMETERS
@app.callback(
    Output(component_id='current_params', component_property='children'),
    [Input("progress_name", "children")]
)
def update_output_div(input_value):
    return regis.get('current_params').decode()

# PROGRESS BAR
@app.callback(
    Output(component_id='progress_name', component_property='children'),
    [Input("interval", "n_intervals")]
)
def update_output_div(input_value):
    return regis.get('progress_name').decode()

@app.callback(  [Output("progress", "value"),
                 Output("progress", "children")], 
                [Input("interval", "n_intervals")])
def advance_progress(n):
    progress = int(eval(regis.get('progress')))
    return progress, f"{progress} %" if progress >= 5 else ""


#%%


if __name__ == '__main__':
    app.run_server(port=8080, debug=True)


# %%
