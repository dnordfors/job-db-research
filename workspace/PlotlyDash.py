#%%
import dash
from dash.dependencies import Input, Output
import dash_table
import pandas as pd


class DashTable:
    def __init__(self,df, id = 'table-sorting-filtering', page_size = 5):
        self.df = df
        self.id = id
        self.PAGE_SIZE = page_size

    def layout(self): 
        table = dash_table.DataTable(
            id=self.id,
            columns=[
                {'name': i, 'id': i, 'deletable': True} for i in sorted(self.df.columns)
            ],
            page_current= 0,
            page_size= self.PAGE_SIZE,
            page_action='custom',

            filter_action='custom',
            filter_query='',

            sort_action='custom',
            sort_mode='multi',
            sort_by=[]
        )
        return table

    def split_filter_part(self,filter_part):
        operators = [['ge ', '>='],
                ['le ', '<='],
                ['lt ', '<'],
                ['gt ', '>'],
                ['ne ', '!='],
                ['eq ', '='],
                ['contains '],
                ['datestartswith ']]
        for operator_type in operators:
            for operator in operator_type:
                if operator in filter_part:
                    name_part, value_part = filter_part.split(operator, 1)
                    name = name_part[name_part.find('{') + 1: name_part.rfind('}')]

                    value_part = value_part.strip()
                    v0 = value_part[0]
                    if (v0 == value_part[-1] and v0 in ("'", '"', '`')):
                        value = value_part[1: -1].replace('\\' + v0, v0)
                    else:
                        try:
                            value = float(value_part)
                        except ValueError:
                            value = value_part

                    # word operators need spaces after them in the filter string,
                    # but we don't want these later
                    return name, operator_type[0].strip(), value

        return [None] * 3

    def callback(self):
        @app.callback(
            Output(self.id, 'data'),
            [Input(self.id, "page_current"),
            Input(self.id, "page_size"),
            Input(self.id, 'sort_by'),
            Input(self.id, 'filter_query')])
        
        def update_table(page_current, page_size, sort_by, filter):
            filtering_expressions = filter.split(' && ')
            dff = df
            for filter_part in filtering_expressions:
                col_name, operator, filter_value = self.split_filter_part(filter_part)

                if operator in ('eq', 'ne', 'lt', 'le', 'gt', 'ge'):
                    # these operators match pandas series operator method names
                    dff = dff.loc[getattr(dff[col_name], operator)(filter_value)]
                elif operator == 'contains':
                    dff = dff.loc[dff[col_name].str.contains(filter_value)]
                elif operator == 'datestartswith':
                    # this is a simplification of the front-end filtering logic,
                    # only works with complete fields in standard format
                    dff = dff.loc[dff[col_name].str.startswith(filter_value)]

            if len(sort_by):
                dff = dff.sort_values(
                    [col['column_id'] for col in sort_by],
                    ascending=[
                        col['direction'] == 'asc'
                        for col in sort_by
                    ],
                    inplace=False
                )

            page = page_current
            size = page_size
            return dff.iloc[page * size: (page + 1) * size].to_dict('records')


df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder2007.csv')
dashtable = DashTable(df,page_size = 10,id = 'putte')

#%%

app = dash.Dash(__name__)
app.layout = dashtable.layout
dashtable.callback()

if __name__ == '__main__':
    app.run_server(debug=True)