package com.bold.datascience.restful;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bold.datascience.beans.KeywordBean;
import com.bold.datascience.listener.WebappContext;
import com.bold.datascience.rabbitMqClient.Manager;
import com.whalin.MemCached.MemCachedClient;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.container.TimeoutHandler;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Path("v1")
@Api(value="/KeyWords", description="Rest API for getting KeyWords from job description")
public class JobKeyWordService {
	
	Logger logger = LogManager.getLogger(JobKeyWordService.class.getName());
	
	@GET
	@Path("/job-keywords/{jobId}")
	@SuppressWarnings("serial")
	@ApiOperation(value="Get keywords for an input job-id")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),@ApiResponse(code = 500, message = "Server exception")})
	public void getJobKeywords(@Suspended final AsyncResponse asyncResponse, 
			@ApiParam(value="jobId",required=true) @PathParam("jobId") String jobId){
		logger.info("job id = " + jobId);
		
		asyncResponse.setTimeoutHandler(new TimeoutHandler() {

			@Override
			public void handleTimeout(AsyncResponse asyncResponse) {
				JsonObjectBuilder hob = Json.createObjectBuilder().
						add("TIMEOUT_ERROR", "Operation time out in job-keywords GET request.");
				asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(hob.build().toString()).build());
			}
		});

		asyncResponse.setTimeout(10, TimeUnit.SECONDS);
		
		if(jobId.equals("") || jobId == null){
			JsonObjectBuilder retVal = Json.createObjectBuilder();
	    	retVal.add("ERROR", "JobId not provided");
           	asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(retVal.build().toString()).build());	
		}
		
		MemCachedClient mcc = WebappContext.mcc;
		Object data = null;
		
		String combined_text = jobId + "-keyword" ;
		String cache_key = DigestUtils.md5Hex(combined_text);
		logger.info("Cache key is " + cache_key);
		
		data = mcc.get(cache_key);
		if (data != null) {
			logger.info("Results found in cache");
			asyncResponse.resume(Response.status(Response.Status.OK).entity(data).build());
		}else{
			HashMap<String, Object> requestParams = new HashMap<String, Object>() {{
	        	put("TYPE", "JobKeyWords");
				put("jobId", jobId);
			}};
			
			new Thread(new Runnable() {
				@Override
		        public void run() {
					Thread currentThread = Thread.currentThread();
					String thread_name = currentThread.getName();
					Manager managerObj = WebappContext.myManager;
					
					String requestId = managerObj.registerRequest(requestParams, currentThread);
					
					try {
						managerObj.publishMessage(requestId);
					} catch (IOException e1) {
						//e1.printStackTrace();
						logger.info(thread_name + " " + " error publishing message to rabbit mq");
						JsonObjectBuilder errorVal = Json.createObjectBuilder();
				    	errorVal.add("ERROR", "TIMEOUT FROM SERVER, Rabbit mq error");
				    	managerObj.unregisterRequest(requestId);
			           	asyncResponse.resume(Response.status(Response.Status.REQUEST_TIMEOUT).entity(errorVal.build().toString()).build());
					    return;
					}
					
					String response = managerObj.getRequestResponse(requestId);
					JsonReader jsonReader = Json.createReader(new StringReader(response));
					JsonObject object = jsonReader.readObject();
					jsonReader.close();
					Integer errorCode = object.getInt("ErrorCode", 200);
					
					logger.info(object.isEmpty());
					if (object.isEmpty()) {
                       logger.info(thread_name + " " + " got error in getting job keywords");
    				    
                        JsonObjectBuilder errorVal = Json.createObjectBuilder();
				    	    errorVal.add("ERROR", "Error in getting job keywords!");
    					managerObj.unregisterRequest(requestId);
    					asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorVal.build().toString()).build());
    				    return;	
					}
					else if(errorCode == 400){
						logger.info(thread_name + " " + "No job found for jobId -" + jobId.toString());
						JsonObjectBuilder errorVal = Json.createObjectBuilder();
			    	    errorVal.add("ERROR", "No job found for jobId -" + jobId.toString());
			    	    managerObj.unregisterRequest(requestId);
			    	    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(errorVal.build().toString()).build());
			    	    return;	
					}
					else if(errorCode == 500){
						logger.info(thread_name + " " + "Error occurred for jobId -" + jobId.toString());
						JsonObjectBuilder errorVal = Json.createObjectBuilder();
			    	    errorVal.add("ERROR", "Unknown Error Occurred for jobId -" + jobId.toString());
			    	    managerObj.unregisterRequest(requestId);
			    	    asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorVal.build().toString()).build());
			    	    return;	
					}
					else {
						logger.info(thread_name + " " + " returning result returned from rabbit mq");
    				    
    					Date d = new Date( System.currentTimeMillis() + 43200000L);
    					mcc.set(cache_key, object.toString(), d);
    					managerObj.unregisterRequest(requestId);
    					asyncResponse.resume(Response.status(Response.Status.OK).entity(object.toString()).build());
    				    return;	
					}
				}
			}).start();
		}
		
	}
	
	
	@POST
	@Path("/job-keywords")
	@SuppressWarnings("serial")
	@ApiOperation(value="Get keywords for an input job title & job description")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),@ApiResponse(code = 500, message = "Server exception")})
	public void getJobKeywordsPost(@Context HttpServletRequest request,@Suspended final AsyncResponse asyncResponse,
			@ApiParam(value = "Get keywords for input job", required = true) KeywordBean inputBean){
		
		asyncResponse.setTimeoutHandler(new TimeoutHandler() {

			@Override
			public void handleTimeout(AsyncResponse asyncResponse) {
				JsonObjectBuilder hob = Json.createObjectBuilder().
						add("TIMEOUT_ERROR", "Operation time out in job-keywords POST request.");
				asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
						.entity(hob.build().toString()).build());
			}
		});
		
		// Setting timeout for 10 seconds
        asyncResponse.setTimeout(10, TimeUnit.SECONDS);
        
        String jobTitle = inputBean.getTitle();
        String jobDescription = inputBean.getJobDescription();
        String companyName = inputBean.getCompanyName();
        
        if(jobTitle.equals("") || jobDescription.equals("")){
			JsonObjectBuilder retVal = Json.createObjectBuilder();
	    	retVal.add("ERROR", "JobTitle & JobDescription cannot be left blank");
           	asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(retVal.build().toString()).build());	
		}
        
        HashMap<String, Object> requestParams = new HashMap<String, Object>() {{
        	put("TYPE", "JobKeyWordsPost");
			put("jobTitle", jobTitle);
			put("jobDescription", jobDescription);
			put("companyName", companyName);
		}};
		
		new Thread(new Runnable() {
			@Override
	        public void run() {
				Thread currentThread = Thread.currentThread();
				String thread_name = currentThread.getName();
				Manager managerObj = WebappContext.myManager;
				
				String requestId = managerObj.registerRequest(requestParams, currentThread);
				
				try {
					managerObj.publishMessage(requestId);
				} catch (IOException e1) {
					//e1.printStackTrace();
					logger.info(thread_name + " " + " error publishing message to rabbit mq");
					JsonObjectBuilder errorVal = Json.createObjectBuilder();
			    	errorVal.add("ERROR", "TIMEOUT FROM SERVER, Rabbit mq error");
			    	managerObj.unregisterRequest(requestId);
		           	asyncResponse.resume(Response.status(Response.Status.REQUEST_TIMEOUT).entity(errorVal.build().toString()).build());
				    return;
				}
				
				String response = managerObj.getRequestResponse(requestId);
				JsonReader jsonReader = Json.createReader(new StringReader(response));
				JsonObject object = jsonReader.readObject();
				jsonReader.close();
				Integer errorCode = object.getInt("ErrorCode", 200);
				
				logger.info(object.isEmpty());
				if (object.isEmpty()) {
                   logger.info(thread_name + " " + " got error in eri request");
				    
                    JsonObjectBuilder errorVal = Json.createObjectBuilder();
			    	    errorVal.add("ERROR", "Error in getting job keywords!");
					managerObj.unregisterRequest(requestId);
					asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorVal.build().toString()).build());
				    return;	
				}
				else if(errorCode >= 400){
					logger.info(thread_name + " " + "Error Occurred");
					JsonObjectBuilder errorVal = Json.createObjectBuilder();
		    	    errorVal.add("ERROR", "Unknown Error Occured");
		    	    managerObj.unregisterRequest(requestId);
		    	    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(errorVal.build().toString()).build());
		    	    return;	
				}
				else {
					logger.info(thread_name + " " + " returning result returned from rabbit mq");
					managerObj.unregisterRequest(requestId);
					asyncResponse.resume(Response.status(Response.Status.OK).entity(object.toString()).build());
				    return;	
				}
			}
		}).start();
        
	}

}
