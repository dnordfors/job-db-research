package com.bold.datascience.restful;

import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bold.datascience.beans.SalaryInputBean;
import com.bold.datascience.job.models.JobTitleSalaryERI;
import com.bold.datascience.listener.WebappContext;
import com.bold.datascience.rabbitMqClient.Manager;
import com.whalin.MemCached.MemCachedClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Path("salary/v1")
@Api(value="/Salary", description="Rest APIS for Salary Data")
public class JobERISalaryService {

	Logger logger = LogManager.getLogger(JobERISalaryService.class.getName());
	
	@POST
	@Path("/{source}/distribution")
	@ApiOperation(value="Get Salary data for job title and location, sources supported are ERI(salary expert)")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),@ApiResponse(code = 500, message = "Server exception")})
	public void getERISalaryList(@Context HttpServletRequest request,@Suspended final AsyncResponse asyncResponse,
			@ApiParam(value = "Salary data for job title and/or city and state combinations", required = true) SalaryInputBean input,
			@ApiParam(value="source",required=true) @PathParam("source") String source){
		
		String title = input.getTitle();
		String city = input.getCity();
		String state = input.getState();
		Integer titleId = input.getTitleId();
		
		asyncResponse.setTimeoutHandler(new TimeoutHandler() {
	    	 
		    @Override
		    public void handleTimeout(AsyncResponse asyncResponse) {
		    	logger.error("Operation timeout in getResumeScore");
		    	JsonObjectBuilder retVal = Json.createObjectBuilder();
		    	retVal.add("ERROR", "TIMEOUT FROM SERVER");
	           	asyncResponse.resume(Response.status(Response.Status.REQUEST_TIMEOUT).entity(retVal.build().toString()).build());
		        }
		});
		
		// Setting timeout for 10 seconds
        asyncResponse.setTimeout(10, TimeUnit.SECONDS);
		
        new Thread(new Runnable() {
        	
	        @Override
	        public void run() {
	        	Thread currentThread = Thread.currentThread();
				String thread_name = currentThread.getName();
				
				
				if (source.trim().equalsIgnoreCase("ERI")) {
					logger.info(thread_name + " source is equal to eri");
				}else {
					logger.info(thread_name + " source is NOT equal to eri");
					JsonObjectBuilder retVal = Json.createObjectBuilder();
			    	retVal.add("ERROR", "Source invalid (currently only ERI source supported)");
		           	asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(retVal.build().toString()).build());
				    return;
				    
				}
				
				if( title.equals("") ^ titleId != 0){
					JsonObjectBuilder retVal = Json.createObjectBuilder();
			    	retVal.add("ERROR", "Either provide Title or TitleId as an input, not both.");
		           	asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(retVal.build().toString()).build());	
				    return;
				}
				
				
				MemCachedClient mcc = WebappContext.mcc;
				Object data = null;
				
				String combined_text = title + " " + city + " " + state;
    			String cache_key = DigestUtils.md5Hex(combined_text);
    			logger.info(thread_name + " cache key is " + cache_key);
    			
    			data = mcc.get(cache_key);
    			if (data != null) {
    				logger.info(thread_name + " results found in cache");
    				asyncResponse.resume(Response.status(Response.Status.OK).entity(data).build());
    			    return;
    			}else{
    				JsonObjectBuilder retVal = Json.createObjectBuilder();
    				
    				HashMap<String, Object> val = new HashMap<String, Object>();
    				if(!title.equals("")) {
    					val.put("title_query", title);
    				}else {
    					val.put("title_id", titleId);
    				}
    				val.put("city", city);
    				val.put("state", state);
    				
    				try {
    					logger.info(thread_name + " " + val.toString());
    					JobTitleSalaryERI obj = WebappContext.jobTitleSalaryERIDAO.find(val);
    					logger.info(thread_name + " " + obj.getPercentile25());	
    					retVal.add("percentile25",obj.getPercentile25());
    					retVal.add("percentile50", obj.getPercentile50());
    					retVal.add("percentile75", obj.getPercentile75());
    					retVal.add("percentile25National", obj.getPercentile25National());
    					retVal.add("percentile50National", obj.getPercentile50National());
    					retVal.add("percentile75National", obj.getPercentile75National());
    					retVal.add("localData", obj.getLocalData());
    					retVal.add("stateData", obj.getStateData());
    					retVal.add("nationalData", obj.getNationalData());
    					logger.info(thread_name + " " + "Found in mongo || Data for title -> "+ title + " || state -> " + state + 
    							" || city -> "+ city + " || title-id -> " + titleId);
    					Date d = new Date( System.currentTimeMillis() + 43200000L);
    					String response_eri = retVal.build().toString();
    					mcc.set(cache_key, response_eri, d);
    					
    					asyncResponse.resume(Response.status(Response.Status.OK).entity(response_eri).build());
    					return;
    					
    				} catch (Exception e) {
    					String response = null;
    					logger.error(thread_name + " " + "NOT Found in mongo || Data for title -> "+ title + " || state -> " + state + 
    							" || city -> "+ city + " || title-id -> " + titleId + " hence sending to rabbit mq");
    					Manager managerObj = WebappContext.myManager;
    					HashMap<String, Object> rabbitPayload = new HashMap<String, Object>();
    					
    					
    					rabbitPayload.put("TYPE", "ERI_SALARY");
    					if(!title.equals("")) {
    						rabbitPayload.put("title", title);
    					} else {
    						rabbitPayload.put("id", titleId);
    					}
    					rabbitPayload.put("city", city);
    					rabbitPayload.put("state", state);
    					
    					String requestId = managerObj.registerRequest(rabbitPayload, currentThread);
    					try {
    						managerObj.publishMessage(requestId);
    					} catch (IOException e1) {
    						//e1.printStackTrace();
    						logger.info(thread_name + " " + " error publishing message to rabbit mq");
    						JsonObjectBuilder errorVal = Json.createObjectBuilder();
    				    	errorVal.add("ERROR", "TIMEOUT FROM SERVER, Rabbit mq error");
    				    	managerObj.unregisterRequest(requestId);
    			           	asyncResponse.resume(Response.status(Response.Status.REQUEST_TIMEOUT).entity(errorVal.build().toString()).build());
    					    return;
    					}
    					response = managerObj.getRequestResponse(requestId);
    					JsonReader jsonReader = Json.createReader(new StringReader(response));
    					JsonObject object = jsonReader.readObject();
    					jsonReader.close();
    					Integer errorCode = object.getInt("ErrorCode", 200);
    					
    					logger.info(object.isEmpty());
    					if (object.isEmpty()) {
	                       logger.info(thread_name + " " + " got error in eri request");
        				    
	                        JsonObjectBuilder errorVal = Json.createObjectBuilder();
   				    	    errorVal.add("ERROR", "Error in eri request!");
        					managerObj.unregisterRequest(requestId);
        					asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorVal.build().toString()).build());
        				    return;	
    					}
    					else if(errorCode >= 400){
    						logger.info(thread_name + " " + " got error message from eri request");
    						JsonObjectBuilder errorVal = Json.createObjectBuilder();
    			    	    errorVal.add("ERROR", object.getString("Error"));
    			    	    managerObj.unregisterRequest(requestId);
    			    	    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(errorVal.build().toString()).build());
    			    	    return;	
    					}
    					else {
    						logger.info(thread_name + " " + " returning result returned from rabbit mq");
        				    
        					Date d = new Date( System.currentTimeMillis() + 43200000L);
        					mcc.set(cache_key, object.toString(), d);
        					managerObj.unregisterRequest(requestId);
        					asyncResponse.resume(Response.status(Response.Status.OK).entity(object.toString()).build());
        				    return;	
    					}
    					
    				}	
    			}
            
					
	        }
    }).start();   
	}
}
