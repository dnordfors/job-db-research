package com.bold.datascience.beans;

import javax.xml.bind.annotation.XmlElement;

public class SalaryInputBean {
	@XmlElement private String title;
	@XmlElement private String city;
	@XmlElement private String state;
	@XmlElement private Integer titleId;
	
	public SalaryInputBean(){}
		
	public String getTitle(){
		return (this.title == null) ? "": this.title.toLowerCase().trim();
	}
	
	public String getCity(){
		return (this.city == null) ? "": this.city.toLowerCase().trim();
	}
	
	public String getState(){
		return (this.state == null) ? "": this.state.toLowerCase().trim();
	}
	
	public Integer getTitleId() {
		return (this.titleId == null) ? 0: this.titleId;
	}
}
