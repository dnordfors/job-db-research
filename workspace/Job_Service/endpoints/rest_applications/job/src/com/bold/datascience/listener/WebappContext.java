package com.bold.datascience.listener;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mongodb.morphia.Datastore;

import com.bold.datascience.job.models.JobTitleSalaryERIDAO;
import com.bold.datascience.job.models.JobTitleSalaryERI;
import com.bold.datascience.job.models.MorphiaService;
import com.bold.datascience.rabbitMqClient.Manager;
import com.whalin.MemCached.MemCachedClient;
import com.whalin.MemCached.SockIOPool;

public class WebappContext implements ServletContextListener{
	
	Logger logger = LogManager.getLogger(WebappContext.class.getName());
	public static String jobDBName = "job_services_db";
	public MorphiaService jobMorphiaServie;
	public static JobTitleSalaryERIDAO jobTitleSalaryERIDAO;
	public static Manager myManager;
	public static MemCachedClient mcc;
	
	public void contextInitialized(ServletContextEvent evt) {
		jobMorphiaServie = new MorphiaService(jobDBName);
		logger.info("morphiaServie Webapp context initialized");
		Datastore jobDs = jobMorphiaServie.getDatastore();
		logger.info("Webapp context initialized");
		jobTitleSalaryERIDAO = new JobTitleSalaryERIDAO(JobTitleSalaryERI.class, jobDs);
		SockIOPool pool = null;
		try{
			//String[] servers = {"10.0.0.10:11211"};
			String[] servers = {"127.0.0.1:11211"};
			pool = SockIOPool.getInstance("ds_job_services");
			pool.setServers( servers );
			pool.setFailover( true );
			pool.setInitConn( 50 );
			pool.setMinConn( 5 );
			pool.setMaxConn( 500 );
			pool.setMaintSleep( 30 );
			pool.setNagle( false );
			pool.setSocketTO( 3000 );
			pool.setAliveCheck( true );
			pool.setBufferSize(2*1024*1025);
			pool.initialize();
			
			mcc = new MemCachedClient("ds_job_services");
			
			String filePath = "/opt/tomcat/webapps/job/WEB-INF/rabbitMQConfig.properties";
			myManager = new Manager(filePath);
			myManager.consumeMessages();
			logger.info("Manager is now ready to publish messages to RabbitMq");
		} catch(IOException | TimeoutException | KeyManagementException | NoSuchAlgorithmException | URISyntaxException e) {
			logger.error("Error occurred while connecting to RabbitMQ", e);
		}
	}
	
	public void contextDestroyed(ServletContextEvent evt) {
		jobMorphiaServie.CloseMongoConnection();
		if (myManager!= null) {
			try {
				myManager.closeConnection();
				logger.info("Closed Connection with the Manager!!!");
			}
			catch (IOException | TimeoutException e) {
				logger.error("Error occurred while closing connection to Manager", e);
			}
		}
		logger.info("Webapp context destroyed");
	}
}
