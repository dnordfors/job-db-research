package com.bold.datascience.health;

import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bold.datascience.rabbitMqClient.Manager;
import com.bold.datascience.listener.WebappContext;
import com.whalin.MemCached.MemCachedClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@Path("health/v1")
@Api(value="/Health", description="Rest API for Health Monitoring")
public class HeathMonitor {
	Logger logger = LogManager.getLogger(HeathMonitor.class.getName());
	
	private JsonObjectBuilder getErrorMLJson(){
		JsonObjectBuilder serviceResponse = Json.createObjectBuilder()
		        .add("Name", "ML Python consumer")
		        .add("ServiceCD", "")
		        .add("AppCD", "")
		        .add("Status", "DOWN")
		        .add("Type", "A");
		
		return serviceResponse;
	}
	
	private JsonObjectBuilder getErrorRabbitJson(){
		JsonObjectBuilder serviceResponse = Json.createObjectBuilder()
		        .add("Name", "Rabbit MQ")
		        .add("ServiceCD", "")
		        .add("AppCD", "")
		        .add("Status", "DOWN")
		        .add("Type", "QS");
		
		return serviceResponse;
	}
	
	private JsonObjectBuilder getServiceJson(boolean error){
		
		JsonObjectBuilder serviceResponse = Json.createObjectBuilder()
		        .add("Name", "DataScienceJobService")
		        .add("ServiceCD", "DSJS_PRD_A_COR")
		        .add("AppCD", "DSJS")
		        .add("Type", "A");
		if(error){
			serviceResponse.add("Status", "DOWN");
		}else{
			serviceResponse.add("Status", "UP");
		}
		return serviceResponse;
	}
	
	private JsonObjectBuilder getJavaBackendJson(){
		JsonObjectBuilder javaBackendResponse = Json.createObjectBuilder().
				add("Name", "JavaRestBackend").
		        add("ServiceCD", "").
		        add("AppCD", "").
		        add("Status", "UP").
		        add("Type", "A");
		
		return javaBackendResponse;
	}
	
	private JsonObjectBuilder getMemcacheJson(boolean error){
		JsonObjectBuilder memcacheResponse = Json.createObjectBuilder().
				add("Name", "Memcache").
		        add("ServiceCD", "").
		        add("AppCD", "").
		        add("Type", "MCH");
		
		if(error){
			memcacheResponse.add("Status", "DOWN");
		}else{
			memcacheResponse.add("Status", "UP");
		}
		
		return memcacheResponse;
	}
	
	private boolean checkMemcache(){
		try {
			MemCachedClient mcc = WebappContext.mcc;
			Date d = new Date( System.currentTimeMillis() + 10000L);
			String cache_key = "test";
			mcc.set(cache_key, "test", d);
			Object value = mcc.get(cache_key);
			if(value.toString().equals("test")){
				return true;
			}else{
				return false;
			}
				
		} catch (Exception e) {
			return false;
		}
	}
	
	private JsonObjectBuilder getErrorResponse(){
		JsonArrayBuilder components = Json.createArrayBuilder();
     	JsonObjectBuilder serviceJson = getServiceJson(true);
		JsonObjectBuilder javaJson = getJavaBackendJson();
		components.add(javaJson);
		
		if (checkMemcache()){
			JsonObjectBuilder memcacheJson = getMemcacheJson(false);
			components.add(memcacheJson);
		}else{
			JsonObjectBuilder memcacheJson = getMemcacheJson(true);
			components.add(memcacheJson);
		}
		JsonObjectBuilder errorML = getErrorMLJson();
		components.add(errorML);
		JsonObjectBuilder errorRabbit = getErrorRabbitJson();
		components.add(errorRabbit);
		
		serviceJson.add("Components", components);
		return serviceJson;
	}
	
	@GET
	@Path("/pingdom")
	@Produces(MediaType.TEXT_HTML)
	@ApiOperation(value="Returns html for pingdom")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),@ApiResponse(code = 500, message = "Server exception")})
	public void getPindomHtml(@Context HttpServletRequest  request, @Suspended final AsyncResponse asyncResponse) {
		
		String html = "<html><head>Welcome pingdom</head></html>";
		asyncResponse.resume(html);
		
	}
	
	@GET
	@Path("/monitor")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="Get list of dependency status")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),@ApiResponse(code = 500, message = "Server exception")})
	public void getServiceHealth(@Context HttpServletRequest  request, @Suspended final AsyncResponse asyncResponse){
		
		
		
		
		asyncResponse.setTimeoutHandler(new TimeoutHandler() {
	    	 
		    @Override
		    public void handleTimeout(AsyncResponse asyncResponse) {
		    	logger.error("Operation timeout in getServiceHealth");
		     	JsonObjectBuilder hob = getErrorResponse();
    			
		        asyncResponse.resume(Response.status(Response.Status.OK).entity(hob.build().toString()).build());
		        }
		    });
			
			// Setting timeout for 30 seconds
			asyncResponse.setTimeout(3, TimeUnit.SECONDS);
		
			new Thread(new Runnable() {
	        	
	            @Override
	            public void run() {    	
	            	
	        		String response = null;
	        		
	        		try {
	        			//int x = 1/0;
	        			Manager managerObj = WebappContext.myManager;
	        			boolean error = false;
	        			HashMap<String, Object> serviceResponse = new HashMap<String, Object>();
	        			JsonArrayBuilder components = Json.createArrayBuilder();
	        			
	        			JsonObjectBuilder javaJson = getJavaBackendJson();
	        			components.add(javaJson);
	        			
	        			if (checkMemcache()){
	        				JsonObjectBuilder memcacheJson = getMemcacheJson(false);
	        				components.add(memcacheJson);
	        			}else{
	        				JsonObjectBuilder memcacheJson = getMemcacheJson(true);
	        				components.add(memcacheJson);
	        				error = true;
	        			}
	        			JsonObjectBuilder serviceJson = getServiceJson(error);
	        			serviceJson.add("Components", components);
	        			serviceResponse.put("response", serviceJson.build().toString());
	        			serviceResponse.put("TYPE", "healthMonitor");
	        			
	        			Thread currentThread = Thread.currentThread();
    					String requestId = managerObj.registerRequest(serviceResponse, currentThread);
    					managerObj.publishMessage(requestId);
    					response = managerObj.getRequestResponse(requestId);
    					logger.info("Health api: Got response from rabbit mq server");
    					JsonReader jsonReader = Json.createReader(new StringReader(response));
    					JsonObject object = jsonReader.readObject();
    					jsonReader.close();
    					
    					logger.info("Health api: Returning results");
    					managerObj.unregisterRequest(requestId);
    					asyncResponse.resume(Response.status(Response.Status.OK).entity(object.toString()).build());
	        			
	        		}catch  (Exception e) {
	       			    logger.error("Exception occurred in Health api");
	        			e.printStackTrace();
	     		    }
	        	
	        	JsonObjectBuilder serviceJson = getErrorResponse();
	        	
	           	asyncResponse.resume(Response.status(Response.Status.OK).entity(serviceJson.build().toString()).build());
	        }
	      }).start();
	        		
		
		
		
	}
}
