package com.bold.datascience.restful;

import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.bold.datascience.beans.TitleBean;
import com.bold.datascience.listener.WebappContext;
import com.bold.datascience.rabbitMqClient.Manager;
import com.whalin.MemCached.MemCachedClient;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("title/v1")
@Api(value="/Title", description="Rest APIS for Title Data")
public class JobERITitleService {
	
Logger logger = LogManager.getLogger(JobERITitleService.class.getName());
	
	@POST
	@Path("/{source}/details")
	@ApiOperation(value="Get related titles for an input job title, sources supported are ERI(salary expert)")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Success"),@ApiResponse(code = 500, message = "Server exception")})
	public void getRelatedJobTitles(@Context HttpServletRequest request,@Suspended final AsyncResponse asyncResponse,
			@ApiParam(value = "Related JobTitles for an input job title", required = true) TitleBean input,
			@ApiParam(value="source",required=true) @PathParam("source") String source){
		
		asyncResponse.setTimeoutHandler(new TimeoutHandler() {
	    	 
		    @Override
		    public void handleTimeout(AsyncResponse asyncResponse) {
		    	logger.error("Operation timeout in getResumeScore");
		    	JsonObjectBuilder retVal = Json.createObjectBuilder();
		    	retVal.add("ERROR", "TIMEOUT FROM SERVER");
	           	asyncResponse.resume(Response.status(Response.Status.REQUEST_TIMEOUT).entity(retVal.build().toString()).build());
		        }
		});
		
		// Setting timeout for 10 seconds
        asyncResponse.setTimeout(10, TimeUnit.SECONDS);
        
        if (source.trim().equalsIgnoreCase("ERI")) {
			logger.info("Source is equal to eri");
		}else {
			logger.info("Source is NOT equal to eri");
			JsonObjectBuilder retVal = Json.createObjectBuilder();
	    	retVal.add("ERROR", "Source invalid (currently only ERI source supported)");
           	asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(retVal.build().toString()).build());
		    return;
		    
		}
        
        String jobTitle = input.getTitle();
        Boolean showDesc = input.getShowDescription();
        Integer count = input.getCount();
        
        if(jobTitle.equals("")){
			JsonObjectBuilder retVal = Json.createObjectBuilder();
	    	retVal.add("ERROR", "Title not provided");
           	asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(retVal.build().toString()).build());	
		    return;
		}
        
        if(count <= 0) {
        	JsonObjectBuilder retVal = Json.createObjectBuilder();
	    	retVal.add("ERROR", "Count parameter cannot be less than 0");
           	asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(retVal.build().toString()).build());	
		    return;
        }
        
        MemCachedClient mcc = WebappContext.mcc;
		Object data = null;
		
		String combined_text = jobTitle + " " + showDesc.toString() + " " + count.toString();
		String cache_key = DigestUtils.md5Hex(combined_text);
		logger.info("Cache key is " + cache_key);
		
		data = mcc.get(cache_key);
		if (data != null) {
			logger.info("Results found in cache");
			asyncResponse.resume(Response.status(Response.Status.OK).entity(data).build());
		    return;
		}else{
	        @SuppressWarnings("serial")
			HashMap<String, Object> requestParams = new HashMap<String, Object>() {{
	        	put("TYPE", "ERI_TITLES");
				put("title", jobTitle);
				put("showDescription", showDesc);
				put("count", count);
			}};
			
			new Thread(new Runnable() {
				@Override
		        public void run() {
					Thread currentThread = Thread.currentThread();
					String thread_name = currentThread.getName();
					Manager managerObj = WebappContext.myManager;
					
					String requestId = managerObj.registerRequest(requestParams, currentThread);
					
					try {
						managerObj.publishMessage(requestId);
					} catch (IOException e1) {
						//e1.printStackTrace();
						logger.info(thread_name + " " + " error publishing message to rabbit mq");
						JsonObjectBuilder errorVal = Json.createObjectBuilder();
				    	errorVal.add("ERROR", "TIMEOUT FROM SERVER, Rabbit mq error");
				    	managerObj.unregisterRequest(requestId);
			           	asyncResponse.resume(Response.status(Response.Status.REQUEST_TIMEOUT).entity(errorVal.build().toString()).build());
					    return;
					}
					
					String response = managerObj.getRequestResponse(requestId);
					JsonReader jsonReader = Json.createReader(new StringReader(response));
					JsonObject object = jsonReader.readObject();
					jsonReader.close();
					Integer errorCode = object.getInt("ErrorCode", 200);
					
					logger.info(object.isEmpty());
					if (object.isEmpty()) {
                       logger.info(thread_name + " " + " got error in eri request");
    				    
                        JsonObjectBuilder errorVal = Json.createObjectBuilder();
				    	    errorVal.add("ERROR", "Error in eri request!");
    					managerObj.unregisterRequest(requestId);
    					asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorVal.build().toString()).build());
    				    return;	
					}
					else if(errorCode >= 400){
						logger.info(thread_name + " " + " got empty response from eri request");
						JsonObjectBuilder errorVal = Json.createObjectBuilder();
			    	    errorVal.add("ERROR", "No related titles found!");
			    	    managerObj.unregisterRequest(requestId);
			    	    asyncResponse.resume(Response.status(Response.Status.BAD_REQUEST).entity(errorVal.build().toString()).build());
			    	    return;	
					}
					else {
						logger.info(thread_name + " " + " returning result returned from rabbit mq");
    				    
    					Date d = new Date( System.currentTimeMillis() + 43200000L);
    					mcc.set(cache_key, object.toString(), d);
    					managerObj.unregisterRequest(requestId);
    					asyncResponse.resume(Response.status(Response.Status.OK).entity(object.toString()).build());
    				    return;	
					}
				}
			}).start();
		}
	}
}
