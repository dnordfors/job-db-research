package com.bold.datascience.beans;

import javax.xml.bind.annotation.XmlElement;

public class KeywordBean {
	
	@XmlElement private String title;
	@XmlElement private String jobDescription;
	@XmlElement private String companyName;
	
	public KeywordBean() {}
		
	public String getTitle(){
		return (this.title == null) ? "": this.title.trim();
	}
	
	public String getJobDescription(){
		return (this.jobDescription == null) ? "": this.jobDescription.trim();
	}
	
	public String getCompanyName(){
		return (this.companyName == null) ? "": this.companyName.trim();
	}

}
