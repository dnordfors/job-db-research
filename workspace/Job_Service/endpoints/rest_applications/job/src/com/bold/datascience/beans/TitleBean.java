package com.bold.datascience.beans;

import javax.xml.bind.annotation.XmlElement;

public class TitleBean {
	
	@XmlElement private String title;
	@XmlElement private Boolean showDescription;
	@XmlElement private Integer count;
	
	public TitleBean() {}
		
	public String getTitle(){
		return (this.title == null) ? "": this.title.toLowerCase().trim();
	}
	
	public Boolean getShowDescription(){
		return (this.showDescription == null) ? false: this.showDescription;
	}
	
	public Integer getCount(){
		return (this.count == null) ? 0: this.count;
	}

}
