package com.bold.datascience.job.models;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

public class MorphiaService {

	private Morphia morphia;
	private Datastore datastore;
	private MongoClient client;
	private String databaseName;
	
	@SuppressWarnings("deprecation")
	public MorphiaService(String databaseName) {
		try{
			this.databaseName = databaseName;
			
			this.morphia = new Morphia();
			
			ObjectMapper mapper = new ObjectMapper();
			String file_path = "config.properties";
			//ConfigParser config = mapper.readValue(new File(file_path), ConfigParser.class);
			InputStream is = getClass().getResourceAsStream(file_path);
			ConfigParser config = mapper.readValue(is, ConfigParser.class);
			Mongo mongoObj = config.Mongo;
			
			List<ServerAddress> addrs = new ArrayList<ServerAddress>();
			List<Node> nodes = mongoObj.getNodes();
			String username = mongoObj.getUserName();
			String password = mongoObj.getPassword();
			String database = mongoObj.getDBName();
			for(Node item: nodes){
				addrs.add( new ServerAddress(item.getHost(), item.getPort()));
			}
	
			List<MongoCredential> credentialsList = new ArrayList<MongoCredential>();
			
			MongoCredential credentia = MongoCredential.createCredential(
				    username,database, password.toCharArray());
			
			credentialsList.add(credentia);
			
			client = new MongoClient(addrs, credentialsList);
			this.datastore = this.morphia.createDatastore(client, databaseName);
			
			client.setWriteConcern(WriteConcern.REPLICAS_SAFE);
			client.setReadPreference(ReadPreference.secondaryPreferred());
			System.out.println("Read Prefrence: " + client.getReadPreference());
			this.datastore.ensureIndexes();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void CloseMongoConnection(){
		client.close();
	}
	
	public Morphia getMorphia() {
		return morphia;
	}
 
	public void setMorphia(Morphia morphia) {
		this.morphia = morphia;
	}
 
	public Datastore getDatastore() {
		return datastore;
	}
 
	public void setDatastore(Datastore datastore) {
		this.datastore = datastore;
	}

	public String getDatabaseName(){
		return this.databaseName;
	}

}
