package com.bold.datascience.job.models;

import java.util.List;

public class ConfigParser {
	
	public Mongo Mongo;

}

class Mongo {
	public String username;
	public String database;
	public String password;
	public String replicaSetName;
	public List<Node> nodes;
	
	public List<Node> getNodes(){
		return this.nodes;
	}
	
	public String getUserName(){
		return this.username;
	}
	
	public String getPassword(){
		return this.password;
	}
	
	public String getReplicaSetName(){
		return this.replicaSetName;
	}
	
	public String getDBName(){
		return this.database;
	}
}

class Node{
	public String nodeType;
	public String host;
	public Integer port;
	
	public String getHost(){
		return this.host;
	}
	
	public Integer getPort(){
		return this.port;
	}
}