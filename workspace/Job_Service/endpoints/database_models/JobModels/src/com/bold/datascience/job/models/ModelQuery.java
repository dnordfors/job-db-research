package com.bold.datascience.job.models;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;

public class ModelQuery<T> extends BasicDAO<T, ObjectId> {


	public ModelQuery(Class<T> entityClass, Datastore ds) {
		super(entityClass, ds);
	}
	
	public Query<T> getQuery(HashMap<String, Object> hash) {
		Query<T> query = createQuery();
		Iterator<Entry<String, Object>> iter = hash.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<String, Object> entry = (Entry<String, Object>)iter.next();
			query = query.field(entry.getKey()).equal(entry.getValue());
		}
		return query;
	}
 
	public T find(HashMap<String, Object> hash) {
		return getQuery(hash).get();
	}
	
	public List<T> findMany(HashMap<String, Object> hash) {
		return getQuery(hash).asList();
	}

}
