package com.bold.datascience.job.models;

import java.util.HashMap;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;

import com.bold.datascience.job.models.ModelQuery;

public class JobTitleSalaryERIDAO extends BasicDAO<JobTitleSalaryERI, ObjectId>{

	private ModelQuery<JobTitleSalaryERI> modelQuery;
	private Datastore datastore;
	@SuppressWarnings({ "unchecked", "rawtypes" })
	
	public JobTitleSalaryERIDAO(Class<JobTitleSalaryERI> entityClass, Datastore ds) {
		super(entityClass, ds);
		this.datastore = ds;
		modelQuery = new ModelQuery(entityClass, ds);
		// TODO Auto-generated constructor stub
	}

	public JobTitleSalaryERI find(HashMap<String, Object> hash) {
		return modelQuery.find(hash);
	}
	
	public long findCount(){
		return datastore.createQuery(JobTitleSalaryERI.class).count();
	}
	
	public long titleSalaryCount(String search_title,String city, String state){
		
		Query<JobTitleSalaryERI> titleQuery = datastore.createQuery(JobTitleSalaryERI.class);
		
		long count = titleQuery.filter("title_query", search_title).filter("city", city).filter("state", state).count();
		
		return count;
	}
	
}
