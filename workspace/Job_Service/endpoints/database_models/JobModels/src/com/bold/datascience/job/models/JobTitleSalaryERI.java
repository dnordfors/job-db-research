package com.bold.datascience.job.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Id;

@org.mongodb.morphia.annotations.Entity("JobTitleSalaryERI")
public class JobTitleSalaryERI {

	@Id
	private ObjectId objectId;
	
	private Integer percentile25;
	private Integer percentile50;
	private Integer percentile75;
	
	private Integer percentile25National;
	private Integer percentile50National;
	private Integer percentile75National;
	
	private String title;
	private String city;
	private String state;
	private String title_query;
	private Integer title_id;
	
	private boolean localData;
	private boolean stateData;
	private boolean nationalData;
	
	public boolean getLocalData(){
		return this.localData;
	}
	
	public boolean getStateData(){
		return this.stateData;
	}
	
	public boolean getNationalData(){
		return this.nationalData;
	}
	public Integer getPercentile25() {
		return this.percentile25;
	}
	
	public Integer getPercentile50() {
		return this.percentile50;
	}
	
	public Integer getPercentile75() {
		return this.percentile75;
	}
	
	public Integer getPercentile25National() {
		return this.percentile25National;
	}
	public Integer getPercentile50National() {
		return this.percentile50National;
	}
	
	public Integer getPercentile75National() {
		return this.percentile75National;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public Integer getTitleId() {
		return this.title_id;
	}
	
	public String getCity() {
		return this.city;
	}
	public String getState() {
		return this.state;
	}
	public String getTitleQueryString() {
		return this.title_query;
	}
	
}
