import re
import time
import nltk
import operator
import requests
from pprint import pprint
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from collections import OrderedDict, Counter
from Levenshtein import ratio, distance
from configuration.config import ConfigParser
from job_keywords_handler.service import Service


config_obj = ConfigParser()
env = config_obj.environment


def sanitize_text(document):
    # Function to convert a document to a sequence of words,

    # 1. Remove HTML
    review_text = BeautifulSoup(document, 'html5lib').get_text()

    # 2. Remove URLs
    review_text = re.sub(r'https?:\/\/.*[\r\n]*', '', review_text)

    # 3. Remove non-letters
    review_text = re.sub("[^a-zA-Z]", " ", review_text)
    review_text = re.sub(r'([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))', r'\1 ', review_text)

    # 4. Convert words to lower case and split them
    review_text = review_text.lower()

    # 5. Replace multiple spaces with single space
    document = re.sub(' +', ' ', review_text).strip()
    return document


def get_word_frequencies(doc):
    stop = set(stopwords.words('english'))
    doc = [word for word in doc.split() if word not in stop]
    return Counter(doc)


def single_word_filter(dictionary, classified_text, min_occ = 3):
    result_keywords = {}
    tags = nltk.tag.pos_tag(classified_text.split(' '))
    for word, tag in tags:
        frequency = dictionary.get(word, 0)
        if tag in {"NN", "NNS", "JJ", "JJR","JJS"} and frequency >= min_occ and len(word) >= 4 and word.isalpha():
            result_keywords[word] = frequency

    return result_keywords


def get_single_word_keywords(job_description, classified_text, max_count=10):
    job_description = sanitize_text(job_description)
    classified_text = sanitize_text(classified_text)

    word_frequencies = get_word_frequencies(job_description)
    keywords = single_word_filter(word_frequencies, classified_text)
    keywords = OrderedDict(sorted(keywords.items(), key=lambda x: (-x[1], x[0]))[:max_count])

    return keywords


def get_job_details(job_id, collection, logger):

    if not hasattr(get_job_details, 'service_obj'):
        logger.info("Setting Services object for the first time !!!")
        obj = Service(logger)
        setattr(get_job_details, 'service_obj', obj)

    logger.info("Fetching job details for job-id - {}".format(job_id))
    projection = {'_id': 0, 'JobId': 1, 'Message': 1}
    document = collection.find_one({'JobId': job_id}, projection)
    if document and isinstance(document.get('Message'), dict):
        logger.info("Job found in mongo!!!")
        job_details = document.get('Message')
    else:
        logger.info("Job not found in mongo. Getting job from job-search API")
        job_details = get_job_details.service_obj.get_job_details(job_id)
    return job_details


def clean_sentence(data):
    data = re.sub(r"[\"#@<>{}`+=~|*]", "", data)
    #print("after re >>> ", data)

    data = data.split(" ")
    data = [x.strip() for x in data if bool(x.strip())]
    if len(data) < 5:
        return(None)

    data = ' '.join(data)
    data = data.strip()

    if data[-1].isdigit() or data[-1].isalpha():
        data = data+". "
    else:
        pass
    data = data.strip()
    return(data)


def get_classified_job_text(job_description, dompany, classifier):
    Result = classifier.predict_Sentence(job_description, dompany)
    # It is a dictionary contrain two keys: SentenceList and SentenceTag
    classified_job_text = ""

    for sent in range(len(Result["SentenceList"])):
        data = Result["SentenceList"][sent]
        distribution = Result["SentenceTag"][sent]

        if distribution[0][1] != "PositionList":
            continue
        else:
            if (distribution[0][0] - distribution[1][0]) <= 0.25:
                continue

        data = clean_sentence(data)

        if data == None:
            continue
        classified_job_text = classified_job_text + " " + data
        classified_job_text = classified_job_text.strip()

        # if the documents have less than 50 words:
    if len(classified_job_text.split()) >= 50:
        return (classified_job_text)
    else:
        classified_job_text = ""
        for sent in range(len(Result["SentenceList"])):
            data = Result["SentenceList"][sent]
            distribution = Result["SentenceTag"][sent]

            if distribution[0][1] not in ["PositionList", "QualificationList"]:
                continue

            data = clean_sentence(data)

            if data == None:
                continue
            classified_job_text = classified_job_text + " " + data
            classified_job_text = classified_job_text.strip()

    return classified_job_text


def levenshtein(s1, s2):
    if len(s1) < len(s2):
        s1, s2 = s2, s1

    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]/float(len(s1))


def get_keywords_from_job_text(div_rank_keyword, sg_rank_keyword, logger):
    super_set_dict = {}

    # Merge the keywords extracted from div-rank & sg-rank algorithms and
    # store the keywords in a separate dictionary. For the keywords that
    # are common to both the algorithms take the average of their weights

    if div_rank_keyword:
        for data in div_rank_keyword:
            try:
                super_set_dict.setdefault(data[0], []).append(data[1])
            except:
                logger.error("error in keyword - {}".format(data))

    if sg_rank_keyword:
        for data in sg_rank_keyword:
            try:
                super_set_dict.setdefault(data[0], []).append(data[1])
            except:
                logger.error("error in keyword - {}".format(data))

    for k, v in super_set_dict.items():
        avg = round(sum(v) / 2, 2)
        super_set_dict[k] = avg

    # Create an ordered dictionary of the above super set dictionary
    # with keywords ordered by decreasing order of the weights

    super_set_dict = OrderedDict(sorted(super_set_dict.items(), key=lambda x: (-x[1], x[0])))
    super_set = [x.lower() for x, y in super_set_dict.items()]

    final_keywords = set()

    # Run a loop on all the keywords in super set dictionary
    for x in range(0, len(super_set)):
        data_x = super_set[x]
        data_x_tokens = data_x.split()
        discard = False
        data_x_score = super_set_dict.get(data_x, 0)
        # For every keyword in super set dictionary, check against every other keyword
        for y in range(0, len(super_set)):
            # ignore while checking for same item
            if x == y:
                continue
            data_y = super_set[y]
            data_y_score = super_set_dict.get(data_y, 0)

            # Check if a keyword-x is present in some form in keyword-y. If yes, then
            # check if the keyword-y weight is less than keyword-x weight. If yes,
            # then remove keyword-y from the final list of keywords to be returned
            if data_x in data_y.split(" ") or data_x == data_y or data_x in data_y:
                if data_y in final_keywords and data_y_score < data_x_score:
                    final_keywords.remove(data_y)
                else:
                    discard = True
                break

            data_y_tokens = data_y.split()

            # Below check to handle 2 keywords which are basically the same
            # but having different ordering of words. If 2 such keywords are
            # found then pick the one with the highest weight.
            identical = True
            for x_token in data_x_tokens:
                found = False
                for y_token in data_y_tokens:
                    if x_token == y_token or x_token in y_token:
                        found = True
                        break

                if found is False:
                    identical = False
                    break

            if identical is True:
                if data_y in final_keywords and data_y_score < data_x_score:
                    final_keywords.remove(data_y)
                else:
                    discard = True
                break

            # Below check to handle keywords which same set of words like
            # customer service representative & customer care representative
            # The idea will be to choose the keyword with highest weight.
            if len(data_x_tokens) > 1 and len(data_y_tokens) > 1:
                token_intersection = set(data_y_tokens) & set(data_x_tokens)
                if len(token_intersection) >= 2 or (
                        len(token_intersection) >= 1 and levenshtein(data_x.lower(), data_y.lower()) > 0.7):
                    if data_x in final_keywords or data_y in final_keywords:
                        if data_y in final_keywords and data_y_score < data_x_score:
                            final_keywords.remove(data_y)
                            # break
                        else:
                            discard = True
                            # break
                        break

        if discard is False:
            final_keywords.add(data_x)

    # discarded = []
    # for x in super_set_dict.keys():
    #     if x not in final_keywords:
    #         discarded.append(x)
    # print("discarded keywords -> ",discarded)
    # print("final keywords -> ", list(final_keywods))

    final_output = {}
    for value in final_keywords:
        # if super_set_dict.get(value, 0) > 0.01 and \
        #         kw_filter_object.phrase_filter(value, job_title, job_description, print_reason=True):
        if super_set_dict.get(value, 0) > 0.01:
            final_output[value] = super_set_dict.get(value, 0)

    sorted_keywords = OrderedDict(sorted(final_output.items(), key=lambda x: (-x[1], x[0])))
    return sorted_keywords


def convert_func(mystr):
    return mystr.replace(".", "--")


def transform_dict_keys(keywords_dict):
    """
        Convert a nested dictionary from one convention to another.
        Args:
            keywords_dict (dict): dictionary (nested or not) to be converted.
        Returns:
            Dictionary with the new keys.
        """
    new = {}
    for k, v in keywords_dict.items():
        new_v = v
        if isinstance(v, dict):
            new_v = transform_dict_keys(v)
        elif isinstance(v, list):
            new_v = list()
            for x in v:
                new_v.append(transform_dict_keys(x))
        new[convert_func(k)] = new_v
    return new


if __name__ == '__main__':
    for s1, s2 in [('Hello world!', 'Holly grail!'), ('Brian', 'Jesus'), ('Thorkel', 'Thorgier'), ('Dinsdale', 'D'),
                   ('park', 'spam'), ('Levenshtein', 'Lenvinsten'), ('Levenshtein', 'Levenshtein')]:
        print("\n", s1, s2)
        max_len = max(len(s1), len(s2))
        t0 = time.time()
        print(levenshtein(s1, s2))
        print("time elapsed - ", round(time.time() - t0, 7))
        t0 = time.time()
        print(ratio(s1, s2))
        print("time elapsed - ", round(time.time() - t0, 7))
        t0 = time.time()
        print(distance(s1, s2)/ max_len)
        print("time elapsed - ", round(time.time() - t0, 7))
