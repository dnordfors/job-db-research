#!/bin/bash -e

BASEDIR=/data/1/eri_salary_server/
VENVPATH=/data/1/eri_salary_server/eriEnv/bin/python3

export PYTHONPATH=${PYTHONPATH}:$BASEDIR/job_services/Job_Service/queue_consumers/job_server/

nohup $VENVPATH $BASEDIR/job_services/Job_Service/queue_consumers/job_server/job_keywords_handler/populate_job_keywords.py &
