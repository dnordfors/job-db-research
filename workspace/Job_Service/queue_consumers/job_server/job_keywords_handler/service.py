import time
import json
import requests
from datetime import datetime
from configuration.config import ConfigParser

config_obj = ConfigParser()


def timeit(method):
    def inner(*args, **kwargs):
        logger = getattr(args[0], "logger")
        t0 = time.time()
        result = method(*args, **kwargs)
        t1 = time.time()
        if logger:
            logger.info("Time taken to call %s for params- %s is - %s seconds" % (method.__name__, args[1:], round(t1 - t0, 2)))

        return result

    return inner


class Service:

    def __init__(self, logger):
        self.env = config_obj.environment
        self.logger = logger
        self.auth_token = None
        self.auth_token_gen_time = None
        self.client_cd = 'JPRS_SND_J_COR' if self.env == "TEST" else "JPRS_PRD_J_COR"
        self.client_secret = 'VGFuYWppIE1hbHVzdXJl' if self.env == "TEST" else "UmFpZ2FyaCBmb3J0"
        self.generate_auth_token()

    @timeit
    def generate_auth_token(self):
        if self.env == "TEST":
            url = "http://sandbox-auth.livecareer.com/oauth/token"
        else:
            url = "http://auth.livecareer.com/oauth/token"

        payload = {
            "grant_type": "client_credentials",
            "client_id": self.client_cd,
            "client_secret": self.client_secret,
            "response_type": "token"
        }
        response = requests.request("POST", url, data=payload)
        if response.status_code < 400:
            json_resp = json.loads(response.text)
            self.auth_token = json_resp.get("access_token")
            self.auth_token_gen_time = datetime.utcnow()

    def refresh_auth_token(self):
        current_time = datetime.utcnow()
        diff = current_time - self.auth_token_gen_time
        days, seconds = diff.days, diff.seconds
        hours = days * 24 + seconds // 3600
        if hours >= 20:
            self.generate_auth_token()

    def get_auth_token(self):
        return self.auth_token

    @timeit
    def get_job_details(self, job_id):
        self.refresh_auth_token()
        if self.env == "TEST":
            api_url = "http://api-sandbox-jobsearch.livecareer.com/v2/jobsearch/{}".format(job_id)
        else:
            api_url = "http://api-jobsearch.livecareer.com/v2/jobsearch/{}".format(job_id)

        headers = {
            "authorization": "Bearer %s" % self.auth_token,
            "content-type": "application/json"
        }
        response = requests.request("GET", api_url, headers=headers)
        if response.status_code < 400:
            job_details = response.json()
            if 'JobTitle' in job_details:
                job_details['Title'] = job_details['JobTitle']
        else:
            err_msg = "Job details could not be fetched for job-id: {}".format(job_id)
            job_details = {"ErrorCode": 404, "Error": err_msg}
            self.logger.info("Error fetching job detail for job-id: {}. Error Code: {}".format(job_id, response.status_code))
        return job_details
