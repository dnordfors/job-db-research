import re
import time
import spacy
import textacy.keyterms
from pprint import pprint
from helpers import custom_logger
from configuration.config import ConfigParser
from phrase_filter.phrase_filter import PhraseFilter
from job_keywords_handler.utils import get_job_details, get_classified_job_text, \
    get_keywords_from_job_text, get_single_word_keywords

debug = False
config_obj = ConfigParser()
data_dir = config_obj.symspell_dictionary
kw_object = PhraseFilter(data_dir, percentile_max = 80)
en = textacy.load_spacy('en_core_web_lg', disable=('parser', 'ner'))


def extract_div_rank_keywords(document, logger):
    try:
        nlpDoc = en(document)
        ky = textacy.keyterms.key_terms_from_semantic_network(
            nlpDoc, normalize='lower', window_width=500, edge_weighting='cooc_freq',
            ranking_algo='divrank', join_key_words=True, n_keyterms=300)
        kwords = [item for item in ky if item[1] > 0.01]
        return kwords
    except Exception as e:
        logger.exception("Exception Occurred!!!")


def extract_sg_rank_keywords(document, logger):
    try:
        if document == 'NA' or len(document) < 3:
             return "NA"
        doc_clean = textacy.preprocess_text(document, lowercase=True, no_punct=False, no_emails=True, no_numbers=False, no_phone_numbers=False, )
        doc_text = textacy.Doc(doc_clean, lang=en)

        kw = textacy.keyterms.sgrank(doc_text, normalize='lower', ngrams=(1, 2, 3), window_width=100, n_keyterms=300)
        sgRankKw = [k for k in kw if k[1] > 0.01]
        return sgRankKw
    except Exception as e:
        logger.exception("Exception Occurred!!!")


def get_job_keywords(msg_payload, mongo_conn, classifier, logger):
    job_id = None
    collection = mongo_conn.job_db.ServicebusCreateMessage
    request_type = msg_payload.get("TYPE", "JobKeyWords")
    if request_type == "JobKeyWords":
        job_id = msg_payload.get("jobId", "")
        t0 = time.time()
        job_details = get_job_details(job_id, collection, logger)
        if 'ErrorCode' in job_details:
            return job_details
        logger.info("Time taken to fetch job details - {}".format(round(time.time() - t0, 4)))
        job_title, job_description, job_company = job_details.get("Title", ""), job_details.get('Description', ""), \
                                                  job_details.get('Company', "")
    else:
        job_title, job_company, job_description = msg_payload.get("jobTitle", ""), msg_payload.get('companyName', ""), \
                                                  msg_payload.get('jobDescription', "")

    # remove HTML tags & Replace multiple spaces with single space
    regex = re.compile(r'<.*?>')
    job_description = regex.sub(' ', job_description)
    job_description = re.sub(' +', ' ', job_description).strip()

    classified_text = get_classified_job_text(job_description, job_company, classifier)
    if debug:
        print("=" * 50)
        print(classified_text)

    if bool(classified_text) is False:
        logger.info("Classified Job Text is None. Using original job description!!!")
        classified_text = job_description

    t0 = time.time()
    div_rank_keywords = extract_div_rank_keywords(classified_text, logger)
    div_rank_keywords = [x for x in div_rank_keywords if kw_object.phrase_filter(x[0], job_title, job_description, print_reason=debug)]
    if debug:
        print("=" * 50)
        pprint(div_rank_keywords)
    logger.info("Time for keyword extraction from div-rank algo - {}".format(round(time.time() - t0, 4)))
    t1 = time.time()
    sg_rank_keywords = extract_sg_rank_keywords(classified_text, logger)
    sg_rank_keywords = [x for x in sg_rank_keywords if kw_object.phrase_filter(x[0], job_title, job_description, print_reason=debug)]
    if debug:
        print("=" * 50)
        pprint(sg_rank_keywords)
        print("=" * 50)
    logger.info("Time for keyword extraction from sg-rank algo - {}".format(round(time.time() - t1, 4)))
    keywords = get_keywords_from_job_text(div_rank_keywords, sg_rank_keywords, logger)

    single_word_keywords = get_single_word_keywords(job_description, classified_text)
    # single_word_keywords = [(x,y) for x, y in single_word_keywords.items() if
    #                     kw_object.phrase_filter(x, job_title, job_description, print_reason=debug)]

    if request_type == "JobKeyWords":
        logger.info("Total time in extracting keywords for job-id - {} is {}". format(job_id, round(time.time() - t0, 4)))
    else:
        logger.info(
            "Total time in extracting keywords is {}".format(round(time.time() - t0, 4)))

    return {"Keywords": {"ScoreBased": keywords, "FrequencyBased": single_word_keywords}}


if __name__ == '__main__':
    import sentence_classifier
    from pprint import pprint
    logger = custom_logger.get_basic_logger()
    # tf_model_dir = "/Users/anuragsharma/Work/premium_match/sentence_classifier/"
    # json_model_dir = "/Users/anuragsharma/Work/premium_match/sentence_classifier/"
    tf_model_dir = config_obj.tf_model_dir
    json_model_dir = config_obj.json_model_dir
    msg_payload = {"jobId": "4c00b3a62c270fcb4860e8c0a89ebcce"}
    mongo_conn = config_obj.get_mongo_connection()
    classifier = sentence_classifier.main_classifer.SentenceClassifier()
    classifier.load_model(tf_model_dir, json_model_dir)
    keywords = get_job_keywords(msg_payload, mongo_conn, classifier, logger)
    pprint(keywords)
