import sys
import time
import queue
import multiprocessing
import sentence_classifier
from pymongo import UpdateOne
from helpers import custom_logger
from datetime import datetime, timedelta
from pymongo.errors import BulkWriteError
from configuration.config import ConfigParser
from job_keywords_handler.utils import transform_dict_keys
from job_keywords_handler.keywords import get_job_keywords


config_obj = ConfigParser()
time_out = config_obj.queue_timeout
bulk_insert_limit = config_obj.bulk_insert_limit
jobs_window_in_hour = config_obj.time_window_in_hours
logger = custom_logger.get_logger(config_obj.job_keywords_log_file_name)


def write_to_mongo(output_queue):
    bulk_records = []
    logger.info("Inside write to mongo function")
    mongo_conn = config_obj.get_mongo_connection()
    collection = mongo_conn.job_db.ServicebusCreateMessage
    while 1:
        try:
            output = output_queue.get(True, time_out)
            job_id, job_keywords = output
            bulk_records.append(UpdateOne({'JobId': job_id}, {'$set': job_keywords}, upsert=False))
            if len(bulk_records) >= bulk_insert_limit:
                result = collection.bulk_write(bulk_records, ordered=False)
                logger.info("Matched Count: {}, Modified Count: {}, Inserted Count: {}, Upserted Count: {}".format(
                    result.matched_count, result.modified_count, result.inserted_count, result.upserted_count))
                bulk_records[:] = []

        except queue.Empty as e:
            logger.warning("Queue is empty. Quitting!!!")
            break
        except BulkWriteError as bwe:
            logger.error(bwe.details)
            bulk_records[:] = []
            raise
        except Exception as e:
            logger.exception("Exception Occurred!!!")

    if len(bulk_records) > 0:
        result = collection.bulk_write(bulk_records, ordered=False)
        logger.info("Matched Count: {}, Modified Count: {}, Inserted Count: {}, Upserted Count: {}".
                    format(result.matched_count, result.modified_count, result.inserted_count, result.upserted_count))
    logger.info("Work done Quitting!!!")


def process_record(input_queue, output_queue):
    job_id = None
    logger.info("Inside process record function")
    mongo_conn = config_obj.get_mongo_connection()
    classifier = sentence_classifier.main_classifer.SentenceClassifier()
    classifier.load_model(config_obj.tf_model_dir, config_obj.json_model_dir)
    while 1:
        try:
            msg_payload = input_queue.get(True, time_out)
            job_id = msg_payload.get('jobId')
            logger.info("Processing JobId: {}".format(job_id))
            job_keywords = get_job_keywords(msg_payload, mongo_conn, classifier, logger)

            if 'ErrorCode' in job_keywords:
                logger.info("Job removed from system. Skipping")
                continue

            transformed_keywords = transform_dict_keys(job_keywords)

            output_queue.put((job_id, transformed_keywords), block=True)

        except queue.Empty as e:
            logger.warning("Queue is empty. Quitting!!!")
            break
        except Exception as e:
            logger.exception("Exception Occurred in processing JobId: {}!!!".format(job_id))

    logger.info("Work done Quitting!!!")


def get_records_from_mongo(input_queue, fetch_records="default"):
    num_hours = jobs_window_in_hour
    time_window = get_last_n_hour_time(num_hours=num_hours)
    db_client = config_obj.get_mongo_connection()
    create_message_coll = db_client.job_db.ServicebusCreateMessage
    if fetch_records == "all":
        logger.info("Fetching all Active jobs form ServicebusCreateMessage collection")
        filter_condition = {'IsActive': True}
    elif fetch_records == "missing":
        logger.info("Fetching all Active jobs form ServicebusCreateMessage collection with keywords field not populated")
        filter_condition = {'IsActive': True, 'Keywords': {'$eq': None}}
    else:
        logger.info("Fetching Active jobs form ServicebusCreateMessage collection form time: %s" % time_window)
        filter_condition = {'$and': [{'IsActive': True}, {'ModifiedDate': {'$gte': time_window}}]}

    projection = {'_id': 0, 'JobId': 1}
    num_docs = create_message_coll.count(filter_condition)

    logger.info("Number of documents to process: %s" % num_docs)

    cursor = create_message_coll.find(filter_condition, projection)
    for document in cursor:
        input_dict = {"TYPE": "JobKeyWords", "jobId": document.get("JobId")}
        input_queue.put(input_dict)

    logger.info("Done Fetching records from mongo")


def handle_error(e):
    logger.exception(e)


def get_last_n_hour_time(num_hours):
    current_date = datetime.utcnow()
    delta = timedelta(hours=num_hours)
    previous_day_date = (current_date - delta).replace(minute=0, second=0, microsecond=0)
    return previous_day_date


def populate_job_keywords_data():
    logger.info("'fetch_records' parameter is : %s" % fetch_records)

    input_queue = multiprocessing.Manager().Queue()
    output_queue = multiprocessing.Manager().Queue()
    num_processes = config_obj.num_workers

    pool = multiprocessing.Pool(num_processes)
    pool.apply_async(func=get_records_from_mongo, args=(input_queue, fetch_records), error_callback=handle_error)
    time.sleep(5)
    jobs = [pool.apply_async(process_record, (input_queue, output_queue), error_callback=handle_error)
            for i in range(num_processes - 2)]

    pool.apply_async(write_to_mongo, (output_queue,), error_callback=handle_error)

    pool.close()
    pool.join()


if __name__ == '__main__':
    t0 = time.time()
    if len(sys.argv) >= 1:
        logger.info("Execution Started!!!")
        fetch_records = "default"

        if len(sys.argv) > 1:
            if sys.argv[1] in {"all", "missing"}:
                fetch_records = sys.argv[1]

        populate_job_keywords_data()

    else:
        logger.info("Not all arguments are supplied")

    logger.info("Time elapsed - {}".format(time.time() - t0))
    logger.info("Finished execution")
