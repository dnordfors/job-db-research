import re
from pymongo import UpdateOne
from datetime import datetime
from helpers.custom_logger import get_basic_logger
from configuration.config import ConfigParser

config_obj = ConfigParser()

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

def handle_error(e):
    print(e)


def save_response(mongo_conn, job_title, response_list, logger):
    cleaned_title = re.sub(r'[^a-z]', '', job_title.lower())
    if len(response_list) > 0 and response_list[0].get('title'):
        eri_title_collection = mongo_conn.job_services_db.ERITitle
        lookup_collection = mongo_conn.job_services_db.ERITitleLookup

        mapped_job_title = response_list[0].get('title')

        bulk_records, job_title_list = [], []
        for record in response_list:
            title = record.get('title')
            if title:
                job_title_list.append(title)
                record.update({"modifiedOn": datetime.utcnow()})
                bulk_records.append(UpdateOne({"title": title}, {'$set': record}, upsert=True))

        result = eri_title_collection.bulk_write(bulk_records, ordered=False)
        logger.info("Matched Count: %s, Modified Count: %s, Inserted Count: %s, Upserted Count: %s" %
                         (result.matched_count, result.modified_count, result.inserted_count, result.upserted_count))
        mongo_ids = [x.get('_id') for x in eri_title_collection.find({'title': {'$in': job_title_list}}, {'_id':1})]

        data_dict = {"cleanedTitle": cleaned_title, "MappedTitle": mapped_job_title, "relatedTitles": mongo_ids,
                     "modifiedOn": datetime.utcnow(), "jobTitle": job_title}

        lookup_collection.update_one({"cleanedTitle": cleaned_title}, {"$set": data_dict}, upsert=True)



if __name__ == '__main__':
    mongo_conn = config_obj.db_conn
    logger = get_basic_logger()
    response_list = [{
      "title": "Cashier",
      "titleId": "4014",
      "socCode": "412011"
    },
    {
      "title": "Cashier & Salesperson",
      "titleId": "75873",
      "socCode": "412011",
    },
    {
      "title": "Cashier & Waiter Waitress",
      "titleId": "111355",
      "socCode": "353031"
    },
    {
      "title": "Cashier and Salesperson",
      "titleId": "245616",
      "socCode": "412031",
    }]
    save_response(mongo_conn, "cashier 1", response_list, logger)
