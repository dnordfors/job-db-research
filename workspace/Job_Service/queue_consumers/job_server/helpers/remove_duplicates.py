from pymongo import MongoClient


connection_string = "mongodb://admin:ds-admin@10.0.0.8:27017/admin"
client = MongoClient(connection_string)
collection = client.job_services_db.JobTitleSalaryERI

pipeline = [
 {
    '$group': { '_id': {"title_query": "$title_query", "city": "$city", "state": "$state"}, 'count': {'$sum':1}}
 },
 {
  '$sort':{"count":-1}
}]

def remove_duplicates():
    duplicates = list(collection.aggregate(pipeline, allowDiskUse=True))

    for document in duplicates:
        count = document.get('count')
        filter_dict = document.get('_id')
        if count >1:
            cursor = collection.find(filter_dict, {'_id':1})
            object_ids = [x.get('_id') for x in cursor]
            result = collection.delete_many({'_id': {'$in': object_ids[1:]}})
            print("{}, Total documents - {}, removed - {}".format(filter_dict, count, result.deleted_count))
        break


if __name__ == '__main__':
    remove_duplicates()