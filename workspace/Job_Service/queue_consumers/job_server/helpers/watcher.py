import time
import signal

def timeout(func, *args, timeout_duration=5, default=None, logger=None, **kwargs):

    class TimeoutError(Exception):
        pass

    def handler(signum, frame):
        raise TimeoutError()

    # set the timeout handler
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout_duration)

    #from common.logger.custom_logger import get_basic_logger
    #if not logger: logger = get_basic_logger()

    try:
        result = func(*args, **kwargs)
    except TimeoutError as exc:
        #logger.error("Timeout of {}secs reached while executing function - {}".format(timeout_duration, func.__name__))
        
        result = default
    except Exception:
        #logger.exception("Generic exception reached while saving screenshot")
        result = default
    finally:
        signal.alarm(0)

    return result

