import re
import copy
from datetime import datetime
import requests
import urllib.parse
from requests.auth import HTTPDigestAuth
import xml.etree.cElementTree as ET

from helpers import custom_logger
from helpers.utils import save_response
from configuration.config import ConfigParser

config_obj = ConfigParser()
logger = custom_logger.get_logger(config_obj.get_log_file_name())
# logger = custom_logger.get_basic_logger()


class EriRequest:

    def __init__(self):
        self.URL = "http://ws.salaryexpert.com/se-xml/default.asmx"
        self.USER_NAME = "aff18"
        self.PASSWORD = "l!v3c@r33r09"
        self.headers = {"content-type": "text/xml; charset=utf-8"}
        self.TitleLocationSalaryDataSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                <soap:Body>
                                    <GetSalaryData xmlns="https://ws.salaryexpert.com">
                                        <id>%s</id>
                                        <city>%s</city>
                                        <stateAbv>%s</stateAbv>
                                    </GetSalaryData>
                                </soap:Body>
                            </soap:Envelope>"""

        self.TitleSalaryDataSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                <soap:Body>
                                    <GetUSSalaryData xmlns="https://ws.salaryexpert.com">
                                        <id>%s</id>
                                    </GetUSSalaryData>
                                </soap:Body>
                            </soap:Envelope>"""        

        self.TitleSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                   <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                   <soap:Body>
                   <GetTitles xmlns="https://ws.salaryexpert.com">
                       <searchStr>%s</searchStr>
                       <showDescription>%s</showDescription>
                       <maxResults>%s</maxResults>
                   </GetTitles>
                   </soap:Body>
                   </soap:Envelope>"""

    def get_title_soap_xml(self, title, show_description="true", count=1):
        return self.TitleSoapXML % (urllib.parse.quote(title, safe=" "), show_description, count)

    def get_salary_data_soap_xml(self, record_id, xml_type="us_data", city="", state_abv=""):
        if xml_type == "us_data":
            return self.TitleSalaryDataSoapXML % (record_id)
        else:
            return self.TitleLocationSalaryDataSoapXML % (record_id, city, state_abv)

    def eri_request_title_salary_data(self, record_id, city, state):

        if city or state:
            data = self.get_salary_data_soap_xml(record_id, "location_specific_data", city, state)
        else:
            data = self.get_salary_data_soap_xml(record_id, "us_data")

        response = requests.request("POST", self.URL, data=data, headers=self.headers, auth=HTTPDigestAuth(self.USER_NAME, self.PASSWORD))
        # print(response.text)
        if response.status_code >= 400:
            logger.info(response.content)
            return {}, response.status_code, None

        xmlstring = re.sub(r' xmlns="[^"]+"', '', response.text, count=1)
        root = ET.fromstring(xmlstring)

        errorMsg, result_dict = "", {}

        for child in root:
            for val in child:
                for x in val:
                    for data in x:
                        # print(data.tag)
                        if "percentile25National" == data.tag:
                            result_dict["percentile25National"] = data.text
                        elif "percentile50National" == data.tag:
                            result_dict["percentile50National"] = data.text
                        elif "percentile75National" == data.tag:
                            result_dict["percentile75National"] = data.text
                        elif "percentile25" == data.tag:
                            result_dict["percentile25"] = data.text
                        elif "percentile50" == data.tag:
                            result_dict["percentile50"] = data.text
                        elif "percentile75" == data.tag:
                            result_dict["percentile75"] = data.text
                        elif "localData" == data.tag:
                            result_dict["localData"] = data.text
                        elif "stateData" == data.tag:
                            result_dict["stateData"] = data.text
                        elif "nationalData" == data.tag:
                            result_dict["nationalData"] = data.text
                        elif "titleDescription" == data.tag:
                            result_dict["titleDescription"] = data.text
                        elif "title" == data.tag:
                            result_dict["title"] = data.text
                        elif "errorMsg" == data.tag:
                            errorMsg = data.text

        return result_dict, response.status_code, errorMsg

    def eri_title_request(self, job_title, show_desc="true", count=1, return_as_list=False):
        num_records = 0
        record_id = 0
        title = ""
        titleDescription = ""
        socCode = 0
        max_limit = 20
        count = min(count, max_limit)
        id_list, title_list, description_list, soc_code_list = [], [], [], []

        data = self.get_title_soap_xml(job_title, show_desc, count)
        response = requests.request("POST", self.URL, data=data, headers=self.headers, auth=HTTPDigestAuth(self.USER_NAME, self.PASSWORD))
        if response.status_code >=400:
            logger.info(response.content)
            return num_records, record_id, title, titleDescription, socCode, response.status_code

        root = ET.fromstring(response.content)
        # print(response.text)
        for child in root:
            for val in child:
                for x in val:
                    for data in x:
                        if 'totalRecords' in data.tag:
                           num_records = data.text
                        else:
                            if "id" in data.tag:
                                id_list = [value.text for value in data]
                                record_id = id_list[0] if len(id_list) > 0 else 0
                            elif "titleDescription" in data.tag:
                                description_list = [value.text for value in data]
                                titleDescription = description_list[0] if len(description_list) > 0 else ""
                            elif "title" in data.tag:
                                title_list = [value.text for value in data]
                                title = title_list[0] if len(title_list) > 0 else ""
                            elif  "socCode" in data.tag:
                                soc_code_list = [value.text for value in data]
                                socCode = soc_code_list[0] if len(soc_code_list) > 0 else 0

        if count == 1 and not return_as_list:
            return num_records, record_id, title, titleDescription, socCode, response.status_code
        else:
            # some times even after passing count as 10 more than 10 records are returned
            return num_records, id_list, title_list, description_list, soc_code_list, response.status_code

    def get_eri_title_salary_data(self, title_query, record_id="", city="", state=""):
        socCode = None
        if not record_id:
            num_records, record_id, title, titleDescription, socCode, title_status_code  = self.eri_title_request(title_query)
            if title_status_code >= 400:
                logger.error("in get_eri_title_salary_data got status code "+str(title_status_code)+ " for title -> " +
                             title_query + " from eri_title_request")
                return {}, {}

        result_dict, salary_status_code, errorMsg  = self.eri_request_title_salary_data(record_id, city, state)
        if salary_status_code >= 400:
            logger.error("in get_eri_title_salary_data got status code "+str(salary_status_code)+ " for title -> " +
                         title_query + " from eri_request_title_salary_data")
            return {}, {}

        if "APPLICATION ERROR" in errorMsg:
            logger.error("getting error message from eri || eri message "+ errorMsg)
            return {}, {"Error": "Invalid job-title or title-id provided", "ErrorCode": 400}
        elif "DATA VALIDATION ERROR" in errorMsg:
            logger.error("getting error message from eri || eri message " + errorMsg)
            return {}, {"Error": "Invalid job-title or title-id provided", "ErrorCode": 400}

        data_to_insert = {}
        data_to_insert["title_id"] = int(record_id)
        data_to_insert["title"] = result_dict.get("title")
        data_to_insert["title_query"] = title_query
        data_to_insert["titleDescription"] = result_dict.get("titleDescription")
        data_to_insert["socCode"] = socCode
        data_to_insert["percentile25"] = int(result_dict.get("percentile25"))
        data_to_insert["percentile50"] = int(result_dict.get("percentile50"))
        data_to_insert["percentile75"] = int(result_dict.get("percentile75"))
        data_to_insert["percentile25National"] = int(result_dict.get("percentile25National"))
        data_to_insert["percentile50National"] = int(result_dict.get("percentile50National"))
        data_to_insert["percentile75National"] = int(result_dict.get("percentile75National"))
        data_to_insert["localData"] = True if result_dict.get("localData") == "true" else False
        data_to_insert["stateData"] = True if result_dict.get("stateData") == "true" else False
        data_to_insert["nationalData"] = True if result_dict.get("nationalData") == "true" else False
        data_to_insert["city"] = city
        data_to_insert["state"] = state
        data_to_insert["modified"] = datetime.utcnow()

        data_to_return = {}
        data_to_return["percentile25"] = data_to_insert["percentile25"]
        data_to_return["percentile50"] = data_to_insert["percentile50"]
        data_to_return["percentile75"] = data_to_insert["percentile75"]
        data_to_return["percentile25National"] = data_to_insert["percentile25National"]
        data_to_return["percentile50National"] = data_to_insert["percentile50National"]
        data_to_return["percentile75National"] = data_to_insert["percentile75National"]
        data_to_return["localData"] = data_to_insert["localData"]
        data_to_return["stateData"] = data_to_insert["stateData"]
        data_to_return["nationalData"] = data_to_insert["nationalData"]

        return data_to_insert, data_to_return

    def get_eri_data(self, message, mongo_conn):
        collection = mongo_conn.job_services_db.JobTitleSalaryERI
        title = message.get("title", "")
        city = message.get("city", "")
        state = message.get("state", "")
        record_id = message.get("id", "")
        logger.info("received request for title -> {} city -> {}, state -> {} and title-id -- {}".
                                format(title, city, state, record_id))
       
        if not bool(title) and not bool(record_id):
            return {}
        else:
            logger.info("invoking get_eri_title_salary_data logic")
            title_salary_data, ret_val = self.get_eri_title_salary_data(title, record_id, city, state)
            logger.info("got results from get_eri_title_salary_data")
            try:
                if bool(title_salary_data) and bool(ret_val) and title:
                    logger.info("trying to insert in mongo for title -> {} city -> {}, state -> {} and title-id -- {}".
                                format(title, city, state, record_id))
                    result = collection.update_one({'title_query': title, 'city': city, 'state': state},
                                            {'$set': title_salary_data}, upsert=True)
                    logger.info("Matched Count: %s, Modified Count: %s, UpsertedId: %s" %
                                (result.matched_count, result.modified_count, result.upserted_id))
            except Exception as e:
                logger.exception("error inserting in mongo for title -> {} city -> {}, state -> {} and title-id -- {}".
                                format(title, city, state, record_id))
            logger.info("returning results of get_eri_data")
            return ret_val

    def get_eri_title_data(self, payload, mongo_conn):
        # print(payload)
        title = payload.get("title", "")
        show_description= payload.get("showDescription", False)
        count = payload.get("count", 1)
        max_limit = 20
        count = min(count, max_limit)
        logger.info("received request for title -> " + title + " || showDescription -> " + str(show_description)
                    + " || count -> " + str(count))
        if not bool(title):
            return {"Error": "Title Cannot be left blank", "ErrorCode": 400}

        lookup_collection = mongo_conn.job_services_db.ERITitleLookup
        cleaned_title = re.sub(r'[^a-z]', '', title.lower())
        document = lookup_collection.find_one({"cleanedTitle": cleaned_title})
        if document:
            logger.info("Fetching results from mongo!!!")
            mongo_ids = document.get('relatedTitles')
            eri_title_collection = mongo_conn.job_services_db.ERITitle
            result = [x for x in eri_title_collection.find({'_id': {'$in': mongo_ids[:count]}}, {'_id': 0, 'modifiedOn': 0})]

            if show_description == False:
                _ = [x.pop("titleDescription") for x in result]

            return {"Records": result[:count], "Count": len(result[:count])}
        else:
            logger.info("Invoking eri_title_request logic!!!")
            num_records, id_list, title_list, description_list, soc_code_list, status_code = \
                self.eri_title_request(title, show_desc="true", count=20, return_as_list=True)
            logger.info("got results from eri_title_request")
            if status_code >= 400:
                logger.error("in get_eri_title_data got status code " + str(status_code) + " for title -> "
                             + title + " from eri_title_request")
                return {"Error": "Got - {} status code from ERI endpoint".format(status_code), "ErrorCode": status_code}
            elif len(id_list) == len(title_list) == len(description_list) == len(soc_code_list):
                output, unique_title_ids = [], set()
                for item in zip(id_list, title_list, description_list, soc_code_list):
                    title_id, job_title, title_desc, soc_code = item
                    if title_id in unique_title_ids:
                        continue
                    else:
                        unique_title_ids.add(title_id)
                        output.append({"titleId": title_id, "title": job_title, "titleDescription": title_desc, "socCode": soc_code})

                save_response(mongo_conn, title, copy.deepcopy(output), logger)
                if show_description == False:
                    _ = [x.pop("titleDescription") for x in output]

                return {"Records": output[:count], "Count": len(output[:count])} if output else {"ErrorCode": 400}
            else:
                logger.error("Record mismatch in get_eri_title_data for title -> " + title + " from eri_title_request")
                return {"Error": "Record mismatch for title - {}".format(title)}


if __name__ == '__main__':
    payload = {
        "title": "software developer",
        "showDescription": False,
        "count": 50
    }
    mongo_conn = config_obj.get_mongo_connection()
    eri_handler_obj = EriRequest()
    # resp = eri_handler_obj.get_eri_title_data(payload, mongo_conn)
    resp = eri_handler_obj.get_eri_title_salary_data("", 111111111)
    from pprint import pprint
    pprint(resp)
