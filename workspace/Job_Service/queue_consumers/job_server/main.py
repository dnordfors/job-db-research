import time
import pika
import json
import copy
import datetime
import multiprocessing
import sentence_classifier
from configuration.config import ConfigParser
from helpers.watcher import timeout
from helpers.utils import static_vars, handle_error
from helpers import custom_logger
from eri_salary_handler.eri_request import EriRequest
from job_keywords_handler.keywords import get_job_keywords

config_obj = ConfigParser()
logger = custom_logger.get_logger(config_obj.get_log_file_name())

NUM_UN_ACK_MESSAGES = 1
resp_queue_name = config_obj.get_rabbit_response_queue_name()
request_queue_name = config_obj.get_rabbit_request_queue_name()

def get_request_response(msg_payload, mongo_conn, classifier):
    try:
        request_type = msg_payload.get('TYPE')
        if request_type == "healthMonitor":
            response = msg_payload.get("response", {})
            response = eval(response)
            rabbit = {"Type":"QS", "Name":"Rabbit MQ", "ServiceCD":"", "AppCD":"", "Status":"UP"}
            ml_consumer = {"Type":"A", "Name":"ML Python consumer", "ServiceCD":"", "AppCD":"", "Status":"UP"}
                 
            component = response.get("Components", [])
            component.append(rabbit)
            component.append(ml_consumer)

            for val in component:
                if val["Status"] != "UP":
                    response["Status"] = "DOWN"
 
            response["Components"] = component
            return response
            
            #return json.dumps({"response": json.dumps(response), "payloadIdentifier":payloadIdentifier})

        elif request_type == "ERI_SALARY":
            start = time.time()
            logger.info("handling eri salary request")
            eri_handler_obj = EriRequest() 
            resp = eri_handler_obj.get_eri_data(msg_payload, mongo_conn)
            logger.info("returning eri request in time > " + str(time.time() - start))
            return resp
        elif request_type == "ERI_TITLES":
            start = time.time()
            logger.info("handling eri title request")
            eri_handler_obj = EriRequest()
            resp = eri_handler_obj.get_eri_title_data(msg_payload, mongo_conn)
            logger.info("returning eri request in time > " + str(time.time() - start))
            return resp
        elif request_type in {"JobKeyWords", "JobKeyWordsPost"}:
            start = time.time()
            logger.info("handling job keywords request")
            resp = get_job_keywords(msg_payload, mongo_conn, classifier, logger)
            logger.info("returning job keywords request in time > " + str(time.time() - start))
            return resp
        else:
            error_message = "Unknown request-type - {}".format(request_type)
            resp = {"Error": error_message}
        return resp
    except Exception as e:
        logger.exception("Exception Occurred!!!")
        return {"Error": "Unknown Error Occurred", "ErrorCode": 500}

@static_vars(counter=0)
def process_request(ch, method, props, body, mongo_conn, classifier):
    process_request.counter += 1
    message = json.loads(body.decode('utf-8'))
    # Remove spaces around values in the payload dict
    try:
        msg_payload = message.get('payload', {})
        msg_payload = {key: value.strip() if isinstance(value, str) else value for key, value in msg_payload.items()}
    except Exception as e:
        logger.exception("Error extracting payload from - {}". format(body))
    else:
        t0 = time.time()
        response = timeout(get_request_response, msg_payload, mongo_conn, classifier, timeout_duration=10, default=-1)
        t1 = time.time()
        message['response'] = json.dumps(response)
        ch.basic_publish(exchange='',
                         routing_key=resp_queue_name,
                         body=json.dumps(message))

    if process_request.counter % NUM_UN_ACK_MESSAGES == 0:
        logger.info("Sending ack for outstanding messages")
        ch.basic_ack(delivery_tag=0, multiple=True)


def consume():
    rabbit_mq_hostname = config_obj.get_rabbit_hostname()
    mongo_conn = config_obj.get_mongo_connection()
    classifier = sentence_classifier.main_classifer.SentenceClassifier()
    classifier.load_model(config_obj.tf_model_dir, config_obj.json_model_dir)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbit_mq_hostname))
    channel = connection.channel()
    channel.queue_declare(queue=request_queue_name, arguments={'x-message-ttl': 30000})
    channel.queue_declare(queue=resp_queue_name, arguments={'x-message-ttl': 30000})
    channel.basic_qos(prefetch_count=100)
    channel.basic_consume(lambda ch, method, properties, body: process_request(
                ch, method, properties, body, mongo_conn=mongo_conn, classifier=classifier), queue=request_queue_name)

    logger.info(" [x] Waiting for messages. To exit press CTRL+C")
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        pass


def main():
    workers = config_obj.num_workers

    pool = multiprocessing.Pool(processes=workers)
    for i in range(0, workers):
        pool.apply_async(consume, error_callback=handle_error)

    # Stay alive
    try:
        while True:
            continue
    except KeyboardInterrupt:
        logger.info(' [*] Exiting...')
        pool.terminate()
        pool.join()


if __name__ == '__main__':
    main()

