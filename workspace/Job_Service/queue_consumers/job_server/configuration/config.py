import os
import configparser
from pymongo import MongoClient

class Singleton(type):
    '''Singleton Meta class'''
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ConfigParser(metaclass=Singleton):
    '''Configuration class for serving data'''

    def __init__(self):
        config = configparser.RawConfigParser()
        current_path = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(current_path, 'config.ini')
        config.readfp(open(file_path))
        
        self.version = config.get('Version', 'VERSION')
        self.production = config.get('Version', 'PRODUCTION')
        
        self.server_log_file_name = config.get('Log Path', 'SERVER_LOG_FILE_NAME')
        self.job_keywords_log_file_name = config.get('Log Path', 'JOB_KEYWORDS_LOG_FILE_NAME')

        self.num_workers = int(config.get('RabbitMQ Config', 'NUM_WORKERS'))
        self.rabbit_mq_hostname = config.get('RabbitMQ Config', 'HOSTNAME')
        self.rabbit_mq_port = config.get('RabbitMQ Config', 'PORT')
        self.rabbit_mq_request_queue_name = config.get('RabbitMQ Config', 'REQUEST_QUEUE_NAME')
        self.rabbit_mq_response_queue_name = config.get('RabbitMQ Config', 'RESPONSE_QUEUE_NAME')

        self.job_db_name = config.get('MongoDb', 'MONGO_JOB_DB_NAME')
        self.mongo_hostname = config.get('MongoDb', 'MONGO_DB_HOSTNAME')
        self.mongo_port = config.get('MongoDb', 'MONGO_DB_PORT')
        self.mongo_replica_set_name = config.get('MongoDb', 'MONGO_REPLICA_SET_NAME')
        self.mongo_user_name = config.get('MongoDb', 'USERNAME')
        self.mongo_password = config.get('MongoDb', 'PASSWORD').encode()
        self.mongo_auth_source = config.get('MongoDb', 'AUTH_SOURCE').encode()
        self.mongo_key = config.get('MongoDb', 'KEY').encode()
        self.mongo_connection_uri = config.get('MongoDb', 'CONNECTION_URI')

        self.environment = config.get("ENVIRONMENT", "ENV")

        self.tf_model_dir = config.get('TF Model', 'MODEL_PATH')
        self.json_model_dir = config.get('TF Model', 'JSON_PATH')

        self.symspell_dictionary = config.get('Keyword Filter', 'SYM_SPELL_DICTIONARY')

        self.bulk_insert_limit = int(config.get('Cron Job Params', 'BULK_INSERT_LIMIT'))
        self.queue_timeout = int(config.get('Cron Job Params', 'QUEUE_TIMEOUT_SEC'))
        self.time_window_in_hours = int(config.get('Cron Job Params', 'TIME_WINDOW_IN_HOURS'))


        self.connect_string = self.get_mongo_connection_uri()


    def get_log_file_name(self):
        return self.server_log_file_name

    def get_rabbit_hostname(self):
        return self.rabbit_mq_hostname

    def get_rabbit_port(self):
        return self.rabbit_mq_port

    def get_rabbit_request_queue_name(self):
        return self.rabbit_mq_request_queue_name

    def get_rabbit_response_queue_name(self):
        return self.rabbit_mq_response_queue_name

    def get_mongo_connection_uri(self):
        from cryptography.fernet import Fernet
        cipher_suite = Fernet(self.mongo_key)
        password = cipher_suite.decrypt(self.mongo_password).decode()
        auth_source = cipher_suite.decrypt(self.mongo_auth_source).decode()
        connection_string = self.mongo_connection_uri % (self.mongo_user_name, password, self.mongo_hostname, self.mongo_port, self.job_db_name, auth_source)
        return connection_string

    def get_mongo_connection(self):
        return MongoClient(self.connect_string)
