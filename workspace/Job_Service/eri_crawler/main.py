import sys
import xlrd 
import datetime  
import requests
from requests.auth import HTTPDigestAuth
import xml.etree.cElementTree as ET
from pymongo import MongoClient

URL = "http://ws.salaryexpert.com/se-xml/default.asmx"
USER_NAME = "aff18"
PASSWORD = "l!v3c@r33r09"


client = MongoClient("mongodb://admin:ds-admin@10.0.0.8:27017/admin")
db = client.job_db
collection = db.JobTitleSalaryERI

def eri_salary_data(record_id):
    headers = {"content-type": "text/xml; charset=utf-8"}

    GetUSSalaryDataSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                <soap:Body>
                                    <GetUSSalaryData xmlns="http://ws.salaryexpert.com">
                                        <id>%s</id>
                                    </GetUSSalaryData>
                                </soap:Body>
                            </soap:Envelope>"""

    response = requests.request("POST", URL, data=GetUSSalaryDataSoapXML % (record_id), headers=headers, auth=HTTPDigestAuth(USER_NAME, PASSWORD))

    root = ET.fromstring(response.content)
    percentile25 = ""
    percentile50 = ""
    percentile75 = ""
    percentile25National = ""
    percentile50National = ""
    percentile75National = ""
    localData = ""
    stateData = ""
    nationalData = ""

    for child in root:
        for val in child:
            for x in val:
                for data in x:
                    if "percentile25National" in data.tag:
                        percentile25National = data.text
                    elif "percentile50National" in data.tag:
                        percentile50National = data.text
                    elif "percentile75National" in data.tag:
                        percentile75National = data.text
                    elif "percentile25" in data.tag:
                        percentile25 = data.text
                    elif "percentile50" in data.tag:
                        percentile50 = data.text
                    elif "percentile75" in data.tag:
                        percentile75 = data.text
                    elif "localData" in data.tag:
                        localData = data.text
                    elif "stateData" in data.tag:
                        stateData = data.text
                    elif "nationalData" in data.tag:
                        nationalData = data.text

    return percentile25, percentile50, percentile75, percentile25National, percentile50National, percentile75National, localData, stateData, nationalData

def eri_title_request(job_title):
    num_records = 0
    record_id = 0
    title = ""
    titleDescription = ""
    socCode = 0

    headers = {"content-type": "text/xml; charset=utf-8"}

    GetTitlesSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                   <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                   <soap:Body>
                   <GetTitles xmlns="http://ws.salaryexpert.com">
                       <searchStr>%s</searchStr>
                       <showDescription>true</showDescription>
                       <maxResults>1</maxResults>
                   </GetTitles>
                   </soap:Body>
                   </soap:Envelope>"""


    response = requests.request("POST", URL, data=GetTitlesSoapXML % (job_title), headers=headers, auth=HTTPDigestAuth(USER_NAME, PASSWORD))

    root = ET.fromstring(response.content)
    for child in root:
        for val in child:
            for x in val:
                for data in x:
                    if 'totalRecords' in data.tag:
                        num_records = data.text
                    else:
                        if "id" in data.tag:
                            for value in data:
                                record_id = value.text
                        elif "titleDescription" in data.tag:
                            for value in data:
                                titleDescription = value.text
                        elif "title" in data.tag:
                            for value in data:
                                title = value.text
                        elif  "socCode" in data.tag:
                            for value in data:
                                socCode = value.text
    return num_records, record_id, title, titleDescription, socCode

file_loc = sys.argv[1]
loc = (file_loc) 
  
# To open Workbook 
wb = xlrd.open_workbook(loc) 
sheet = wb.sheet_by_index(0) 


for i in range(6199):
    job_title = sheet.cell_value(i, 0)
    num_records, record_id, title, titleDescription, socCode = eri_title_request(job_title.lower())
    percentile25, percentile50, percentile75, percentile25National, percentile50National, percentile75National, localData, stateData, nationalData = eri_salary_data(record_id)
    print(job_title.lower(), "  >>>>   ", title)
    #print("num_records " , num_records, "  ", type(num_records))
    #print("record_id " , record_id, "  ", type(record_id))
    #print("title " , title, "  ", type(title))
    #print("titleDescription " , titleDescription, "  ", type(titleDescription))
    #print("socCode " , socCode, "  ", type(socCode))
    #print("percentile25 " , percentile25, "  ", type(percentile25))
    #print("percentile50 " , percentile50, "  ", type(percentile50))
    #print("percentile75 " , percentile75, "  ", type(percentile75))
    #print("percentile25National " , percentile25National, "  ", type(percentile25National))
    #print("percentile50National " , percentile50National, "  ", type(percentile50National))
    #print("percentile75National " , percentile75National, "  ", type(percentile75National))
    #print("localData " , localData, "  ", type(localData))
    #print("stateData " , stateData, "  ", type(stateData))
    #print("nationalData " , nationalData, "  ", type(nationalData))

    data_to_insert = {}
    data_to_insert["title_id"] = int(record_id)
    data_to_insert["title"] = title
    data_to_insert["title_query"] = job_title.lower()
    data_to_insert["titleDescription"] = titleDescription
    data_to_insert["socCode"] = socCode
    data_to_insert["percentile25"] = int(percentile25)
    data_to_insert["percentile50"] = int(percentile50)
    data_to_insert["percentile75"] = int(percentile75)
    data_to_insert["percentile25National"] = int(percentile25National)
    data_to_insert["percentile50National"] = int(percentile50National)
    data_to_insert["percentile75National"] = int(percentile75National)
    data_to_insert["localData"] = True if localData == "true" else False
    data_to_insert["stateData"] = True if stateData == "true" else False
    data_to_insert["nationalData"] = True if nationalData == "true" else False
    data_to_insert["city"] = ""
    data_to_insert["state"] = ""
    data_to_insert["modified"] = datetime.datetime.now()
    #print(data_to_insert)
    if i == 0:
        continue
    try:
        collection.insert(data_to_insert)
    except:
        print("EXCEPTION")
        print(data_to_insert)
    #break

