import os
import time
import math
import datetime
import custom_logger
from pymongo import MongoClient
from eri_request import EriRequest

curr_directory = os.getcwd()
logger = custom_logger.get_logger(curr_directory+"/logs/eri_salary_update_utility.log")

connection = MongoClient("mongodb://admin:ds-admin@10.0.0.8:27017/admin")

job_services_db = connection.job_services_db
mongo_conn = job_services_db.JobTitleSalaryERI


class DistinctCombinationOperations:

    def __init__(self):
        self.out_collection_count = 0

    def populateOutCombinations(self):
        #export distinct combinations to out collection
        #set count of out collection


        logger.info("deleting previously populated ERITitleLocationDistinctCombinations")
        job_services_db.ERITitleLocationDistinctCombinations.drop()

        #time.sleep(30)

        pipeline = [{
                      "$group": {
                           "_id": {
                                     "title": "$title", 
                                     "city": "$city", 
                                     "state": "$state"
                                  }
                               }
                        },
                        { "$out" : "ERITitleLocationDistinctCombinations" }
                   ]

        logger.info("generating new ERITitleLocationDistinctCombinations")
        cursor = mongo_conn.aggregate(pipeline)

        logger.info("setting the size of ERITitleLocationDistinctCombinations")
        self.out_collection_count = job_services_db.ERITitleLocationDistinctCombinations.count()
        logger.info("size of ERITitleLocationDistinctCombinations found is -> "+ str(self.out_collection_count))

    def getOutCollectionData(self, skip, limit):
        ret_val = job_services_db.ERITitleLocationDistinctCombinations.find({}).sort([("_id",1)]).skip(skip).limit(limit);
        return list(ret_val)



class RefreshERISalaryData:

    def pipeline(self):

        obj = DistinctCombinationOperations()
        obj.populateOutCombinations()

        limit_count = 100
        total_count = obj.out_collection_count

        max_chunks = math.ceil(total_count/limit_count)
        logger.info("going to update all datasets " + str(datetime.datetime.now()))
        logger.info("max chunks calculated -> "+ str(max_chunks))
        start = time.time()

        for i in range(max_chunks):
            skip_count = i * limit_count
            data = obj.getOutCollectionData(skip_count, limit_count)

            for x in data:
                title = x.get("_id",{}).get("title","").strip()
                state = x.get("_id",{}).get("state","").strip()
                city = x.get("_id",{}).get("city","").strip()

                if not bool(title):
                	continue


                payload = {"title":title, "state":state, "city":city}
                eriRequestObj = EriRequest()
                eriRequestObj.get_eri_data(payload, mongo_conn)
                
            
        logger.info("done updating all datasets " + str(datetime.datetime.now()))
        end = time.time()
        diff = end - start
        logger.info("total time spent in updation is -> "+ str(diff))
            

obj = RefreshERISalaryData()
obj.pipeline()
