import os
import datetime
import requests
from requests.auth import HTTPDigestAuth
import xml.etree.cElementTree as ET

import custom_logger

curr_directory = os.getcwd()
logger = custom_logger.get_logger(curr_directory+"/logs/eri_salary_update_utility.log")

class EriRequest:

    def __init__(self):
        self.URL = "http://ws.salaryexpert.com/se-xml/default.asmx"
        self.USER_NAME = "aff18"
        self.PASSWORD = "l!v3c@r33r09"
        self.headers = {"content-type": "text/xml; charset=utf-8"}
        self.TitleLocationSalaryDataSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                <soap:Body>
                                    <GetSalaryData xmlns="https://ws.salaryexpert.com">
                                        <id>%s</id>
                                        <city>%s</city>
                                        <stateAbv>%s</stateAbv>
                                    </GetSalaryData>
                                </soap:Body>
                            </soap:Envelope>"""

        self.TitleSalaryDataSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                                <soap:Body>
                                    <GetUSSalaryData xmlns="https://ws.salaryexpert.com">
                                        <id>%s</id>
                                    </GetUSSalaryData>
                                </soap:Body>
                            </soap:Envelope>"""        

        self.TitleSoapXML = """<?xml version="1.0" encoding="utf-8"?>
                   <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                   <soap:Body>
                   <GetTitles xmlns="https://ws.salaryexpert.com">
                       <searchStr>%s</searchStr>
                       <showDescription>true</showDescription>
                       <maxResults>1</maxResults>
                   </GetTitles>
                   </soap:Body>
                   </soap:Envelope>"""

    def eri_request_title_location_salary_data(self, record_id, city, state):

        response = requests.request("POST", self.URL, data=self.TitleLocationSalaryDataSoapXML % (record_id, city, state), headers=self.headers, auth=HTTPDigestAuth(self.USER_NAME, self.PASSWORD))

        try:

            root = ET.fromstring(response.content)
            percentile25 = ""
            percentile50 = ""
            percentile75 = ""
            percentile25National = ""
            percentile50National = ""
            percentile75National = ""
            localData = ""
            stateData = ""
            nationalData = ""
            errorMsg = ""

            for child in root:
                for val in child:
                    for x in val:
                        for data in x:
                            if "percentile25National" in data.tag:
                                percentile25National = data.text
                            elif "percentile50National" in data.tag:
                                percentile50National = data.text
                            elif "percentile75National" in data.tag:
                                percentile75National = data.text
                            elif "percentile25" in data.tag:
                                percentile25 = data.text
                            elif "percentile50" in data.tag:
                                percentile50 = data.text
                            elif "percentile75" in data.tag:
                                percentile75 = data.text
                            elif "localData" in data.tag:
                                localData = data.text
                            elif "stateData" in data.tag:
                                stateData = data.text
                            elif "nationalData" in data.tag:
                                nationalData = data.text
                            elif "errorMsg" in data.tag:
                                errorMsg =  data.text

            return percentile25, percentile50, percentile75, percentile25National, percentile50National, percentile75National, localData, stateData, nationalData, response.status_code, errorMsg
        except:
            logger.error("got Exception in eri_request_title_location_salary_data for record_id - " + str(record_id) + " city - "+ city + " state - " + state)
            return None, None, None, None, None, None, None, None, None, None, None

    def eri_request_title_salary_data(self, record_id):

        response = requests.request("POST", self.URL, data=self.TitleSalaryDataSoapXML % (record_id), headers=self.headers, auth=HTTPDigestAuth(self.USER_NAME, self.PASSWORD))

        try:
            root = ET.fromstring(response.content)
            percentile25 = ""
            percentile50 = ""
            percentile75 = ""
            percentile25National = ""
            percentile50National = ""
            percentile75National = ""
            localData = ""
            stateData = ""
            nationalData = ""
            errorMsg = ""

            for child in root:
                for val in child:
                    for x in val:
                        for data in x:
                            if "percentile25National" in data.tag:
                                percentile25National = data.text
                            elif "percentile50National" in data.tag:
                                percentile50National = data.text
                            elif "percentile75National" in data.tag:
                                percentile75National = data.text
                            elif "percentile25" in data.tag:
                                percentile25 = data.text
                            elif "percentile50" in data.tag:
                                percentile50 = data.text
                            elif "percentile75" in data.tag:
                                percentile75 = data.text
                            elif "localData" in data.tag:
                                localData = data.text
                            elif "stateData" in data.tag:
                                stateData = data.text
                            elif "nationalData" in data.tag:
                                nationalData = data.text
                            elif "errorMsg" in data.tag:
                                errorMsg = data.text

            return percentile25, percentile50, percentile75, percentile25National, percentile50National, percentile75National, localData, stateData, nationalData, response.status_code, errorMsg

        except:
            logger.error("got Exception in eri_request_title_salary_data for record_id - " + str(record_id))
            return None, None, None, None, None, None, None, None, None, None, None

    def eri_title_request(self, job_title):
        num_records = 0
        record_id = 0
        title = ""
        titleDescription = ""
        socCode = 0

        response = requests.request("POST", self.URL, data=self.TitleSoapXML % (job_title), headers=self.headers, auth=HTTPDigestAuth(self.USER_NAME, self.PASSWORD))
        
        if response.status_code != 200:
            return None, None, None, None, None, response.status_code

        try:
            root = ET.fromstring(response.content)
            for child in root:
                for val in child:
                    for x in val:
                        for data in x:
                            if 'totalRecords' in data.tag:
                                num_records = data.text
                            else:
                                if "id" in data.tag:
                                    for value in data:
                                        record_id = value.text
                                elif "titleDescription" in data.tag:
                                    for value in data:
                                        titleDescription = value.text
                                elif "title" in data.tag:
                                    for value in data:
                                        title = value.text
                                elif  "socCode" in data.tag:
                                    for value in data:
                                        socCode = value.text
            return num_records, record_id, title, titleDescription, socCode, response.status_code
        except:
            logger.error("Exception occured in eri_title_request for job title "+ job_title)
            return None, None, None, None, None, response.status_code


    def get_eri_title_salary_data(self, title_query):
        num_records, record_id, title, titleDescription, socCode, title_status_code  = self.eri_title_request(title_query)
        if title_status_code != 200:
            logger.error("in get_eri_title_salary_data got status code "+str(title_status_code)+ " for title -> " + title_query + " from eri_title_request")
            return {}
        percentile25, percentile50, percentile75, percentile25National, percentile50National, percentile75National, localData, stateData, nationalData, salary_status_code, errorMsg  = self.eri_request_title_salary_data(record_id)
        if salary_status_code >= 400:
            logger.error("in get_eri_title_salary_data got status code "+str(salary_status_code)+ " for title -> " + title_query + " from eri_request_title_salary_data")
            return {}
        elif percentile25 == None and percentile50 == None and percentile75 == None:
            logger.error("in get_eri_title_salary_data got Exception from eri_request_title_salary_data "+ " for title -> " + title_query + " from eri_request_title_salary_data")
            return {}

        if "APPLICATION ERROR" in errorMsg:
            logger.error("getting error message from eri || eri message "+ errorMsg) 
            return {}
 
        data_to_insert = {}
        data_to_insert["title_id"] = int(record_id)
        data_to_insert["title"] = title
        data_to_insert["title_query"] = title_query
        data_to_insert["titleDescription"] = titleDescription
        data_to_insert["socCode"] = socCode
        data_to_insert["percentile25"] = int(percentile25)
        data_to_insert["percentile50"] = int(percentile50)
        data_to_insert["percentile75"] = int(percentile75)
        data_to_insert["percentile25National"] = int(percentile25National)
        data_to_insert["percentile50National"] = int(percentile50National)
        data_to_insert["percentile75National"] = int(percentile75National)
        data_to_insert["localData"] = True if localData == "true" else False
        data_to_insert["stateData"] = True if stateData == "true" else False
        data_to_insert["nationalData"] = True if nationalData == "true" else False
        data_to_insert["city"] = ""
        data_to_insert["state"] = ""
        data_to_insert["modified"] = datetime.datetime.now()

        return data_to_insert

    def get_eri_title_location_salary_data(self, title_query, city, state):
        num_records, record_id, title, titleDescription, socCode, title_status_code  = self.eri_title_request(title_query)
        if title_status_code != 200:
            logger.error("in get_eri_title_location_salary_data got status code "+str(title_status_code)+ " for title -> " + title_query + " || city -> "+ city + " state -> "+ state + " from eri_title_request")
            return {}

        percentile25, percentile50, percentile75, percentile25National, percentile50National, percentile75National, localData, stateData, nationalData, salary_status_code, errorMsg  = self.eri_request_title_location_salary_data(record_id, city, state)
        if salary_status_code >= 400:
            logger.error("in get_eri_title_location_salary_data got status code "+str(salary_status_code)+ " for title -> " + title_query + " || city -> "+ city + " state -> "+ state + " from eri_request_title_location_salary_data")
            return {}
        elif percentile25 == None and percentile50 == None and percentile75 == None:
            logger.error("in get_eri_title_location_salary_data got Exception from eri_request_title_location_salary_data "+ " for title -> " + title_query + " from eri_request_title_location_salary_data")
            return {}

        if "APPLICATION ERROR" in errorMsg:
            logger.error("getting error message from eri || eri message "+ errorMsg) 
            return {}

        data_to_insert = {}
        data_to_insert["title_id"] = int(record_id)
        data_to_insert["title"] = title
        data_to_insert["title_query"] = title_query
        data_to_insert["titleDescription"] = titleDescription
        data_to_insert["socCode"] = socCode
        data_to_insert["percentile25"] = int(percentile25)
        data_to_insert["percentile50"] = int(percentile50)
        data_to_insert["percentile75"] = int(percentile75)
        data_to_insert["percentile25National"] = int(percentile25National)
        data_to_insert["percentile50National"] = int(percentile50National)
        data_to_insert["percentile75National"] = int(percentile75National)
        data_to_insert["localData"] = True if localData == "true" else False
        data_to_insert["stateData"] = True if stateData == "true" else False
        data_to_insert["nationalData"] = True if nationalData == "true" else False
        data_to_insert["city"] = city
        data_to_insert["state"] = state
        data_to_insert["modified"] = datetime.datetime.now()

        return data_to_insert


    def get_eri_data(self, message, mongo_conn):
        title = message.get("title", "")
        city = message.get("city", "")
        state = message.get("state", "")
        logger.info("received request for title -> "+ title + " || city -> "+ city + " || state -> "+ state)
       
        if not bool(title) and not bool(city) and not bool(state):
            return {}
        elif not bool(state) and not bool(city):
            logger.info("invoking get_eri_title_salary_data logic")
            title_salary_data = self.get_eri_title_salary_data(title)
            
            try:
                if bool(title_salary_data):
                    logger.info("trying to update in mongo for title -> "+ title + " || city -> "+ city + " || state -> "+ state)
                    #mongo_conn.insert(title_salary_data)
                    mongo_conn.update({"title":title, "city":city, "state":state},{"$set":{"titleDescription": title_salary_data.get("titleDescription", ""),\
                     "socCode": title_salary_data.get("socCode", ""), "percentile25":title_salary_data.get("percentile25", ""), "percentile50":title_salary_data.get("percentile50", ""), \
                        "percentile75": title_salary_data.get("percentile75", ""), "percentile25National": title_salary_data.get("percentile25National", ""), "percentile50National": title_salary_data.get("percentile50National", ""),\
                        "percentile75National": title_salary_data.get("percentile75National", ""), "localData": title_salary_data.get("localData", ""),\
                         "stateData": title_salary_data.get("stateData", ""), "nationalData":title_salary_data.get("nationalData", ""), "modified":title_salary_data.get("modified", "")}},upsert=False, multi=True)
                else:
                    logger.info("Encountered problems in getting data from ERI for -> "+ title + " || city -> "+ city + " || state -> "+ state)
            except:
                logger.error("error inserting in mongo for title -> "+ title + " || city -> "+ city + " || state -> "+ state)
            logger.info("returning from get_eri_data")
            
        else:
            logger.info("invoking get_eri_title_location_salary_data logic")
            title_location_salary_data  = self.get_eri_title_location_salary_data(title, city, state)
            
            try:
                if bool(title_location_salary_data):
                    logger.info("trying to update in mongo for title -> "+ title + " || city -> "+ city + " || state -> "+ state)
                    mongo_conn.update({"title":title, "city":city, "state":state},{"$set":{"titleDescription": title_location_salary_data.get("titleDescription", ""),\
                     "socCode": title_location_salary_data.get("socCode", ""), "percentile25":title_location_salary_data.get("percentile25", ""), "percentile50":title_location_salary_data.get("percentile50", ""), \
                        "percentile75": title_location_salary_data.get("percentile75", ""), "percentile25National": title_location_salary_data.get("percentile25National", ""), "percentile50National": title_location_salary_data.get("percentile50National", ""),\
                        "percentile75National": title_location_salary_data.get("percentile75National", ""), "localData": title_location_salary_data.get("localData", ""),\
                         "stateData": title_location_salary_data.get("stateData", ""), "nationalData":title_location_salary_data.get("nationalData", ""), "modified":title_location_salary_data.get("modified", "")}},upsert=False, multi=True)
                    
            except:
                logger.error("error inserting in mongo for title -> "+ title + " || city -> "+ city + " || state -> "+ state)
            logger.info("returning from get_eri_data")
            

