from LocalModules.DashParts import DashTable
import pandas as pd
import dash
import dash_html_components as html


app = dash.Dash(__name__)

df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder2007.csv')
dashtable1 = DashTable(df,page_size = 5,id = 'putte', selectable = False, deletable = False, app = app, \
                        action = 'custom', \
                        style_cell_conditional=[
                                {'if': {'column_id': 'country'},
                                'textAlign': 'left'}])

dashtable2 = DashTable(df,page_size = 15,id = 'svejs', app = app, \
                        style_cell_conditional=[
                                {'if': {'column_id': 'country'},
                                'textAlign': 'right'}])




#%%
app.layout = html.Div([dashtable1.layout(),dashtable2.layout()])

for tab in [dashtable1,dashtable2]:
    tab.filter_callback()

if __name__ == '__main__':
    app.run_server(port=8052,debug=True)