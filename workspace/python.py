#%%
'''
Ho to Start local Mongo instance 

* Download mongo database dump: https://docs.google.com/document/d/1NtpGGV8vr52iIrDuogOShAAUUcyrI452s7oX4SPpYCo/edit?usp=sharing

* Check if Mongo is running: 
$ ps -ef | grep mongod

* Start MongoD on local machine: 
$sudo brew services start mongodb-community@3.4

* Load the dump
mongorestore --host localhost:27017 --authenticationDatabase admin --db job_db --collection ServicebusCreateMessage [full path here]/ServicebusCreateMessage.bson
'''

import numpy as np
import pandas as pd
import collections
from collections import Counter
from collections import defaultdict

import networkx as nx

from sklearn.decomposition import NMF

### NLP
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize 
from nltk.corpus import stopwords
# nltk.download("stopwords")
import warnings 

import gensim
from gensim.models import Word2Vec 

import ngram

## GRAPHICS & PRESENTATION
import seaborn as sns
from matplotlib import pyplot as plt

import pprint

## DATA MANAGEMENT + I/O
import json
from json import dumps
from bson import ObjectId
from bs4 import BeautifulSoup

import neo4j
from neo4j import GraphDatabase

from pymongo import MongoClient
import bson.json_util as bj

## OS
import os
import sys
import urllib
import requests





class JobDescriptions:
    '''
    JobDescriptions(db,client,collection) creates an instance of a MongoDB Collection
    containing job ads/descriptions.
    '''
    def __init__(self,                                         \
                 title      = 'manager',                        \
                 db         = 'job_db',                        \
                 client     = 'mongodb://localhost:27017/',    \
                 collection = 'ServicebusCreateMessage',
                  **kwargs):
        
        self.client     = MongoClient(client)
        self.db         = self.client[db]
        self.collection = self.db[collection]
        self.title      = title
        self.kwargs     = kwargs


        
        # Function Caches
        self.get_dic                    = {}
        self.duplicates_dic             = {}
        self.get_without_duplicates_dic = {}

        self.title_dic = {}
        self.df         = self.get(title,**kwargs)

    def get(self,string, max_items = 50000 , display_result = True, remove_duplicates = True):
        '''
        1. Gathers job descriptions in MongoDB "collection" for jobs with the sub-string 'string' in the job title. 
                Speeds up by indexing 'collection' on job titles if index does not already exist 
                - THIS TAKES TIME, but does only need to be done once for a collection.
        2. Parses gathered job descriptions into dictionaries

        Returns Pandas Dataframe with columns = [ObjectId, 
                                                time stamp, 
                                                title, 
                                                company,
                                                description,
                                                clean description, (no formatting)
                                                length, (#characters in clean description )
                                                parsed description,
                                                corpus]
        '''
        # Check if result already in dictionary
        params = str({'string':string,'max_items':max_items})
        if params not in self.get_dic:
            collection = self.collection
            
            # Gather records with "string" in job title
            collection.create_index('Message.Title') 
            cursor = collection.find({'Message.Title':{'$regex':"(?i).*"+string+".*"}})
            cursor.limit(max_items)
            gather_list = []
            while cursor.alive:
                db_item = cursor.next()
                titl    = db_item.get('Message',{'Title':'N/A'}).get('Title').lower()
                comp    = db_item.get('Message',{'Company':'N/A'}).get('Company')
                id_nr   = db_item.get('_id')        # ObjectId().generation_time contains time stamp
                time    = id_nr.generation_time
                msg     = db_item.get('Message',{'Description':'N/A'}).get('Description')
                msg2    = db_item.get('Model',{'Description':'N/A'}).get('Description')
                length  = len(msg2)
                gather_list.append([id_nr,time,length,titl,comp,msg,msg2])

            # Convert 'gather_list' (above) to DataFrame     
            #   Column labels matching variables in gather_list 
            df_columns  = ['ObjectId','time stamp','length', 'title',\
                           'company','description','clean description']            
            df          = pd.DataFrame(gather_list, columns = df_columns )
            # Parse job descriptions
            df['parsed description']    = df['description'].apply(parse_job_description)
            df['corpus']                = df['clean description'].apply(lambda x: x.split()).apply(set)
            df['title set']             = df['title'].apply(lambda x: x.replace(',',' ').split()).apply(set)

            
            # Save result in dictionary
            self.get_dic[params] = df
        
        # Identify Duplicates
        if params not in self.duplicates_dic:
            # Group by company 
            grp_co = df[['length','ObjectId','company','corpus']].groupby('company').agg(lambda x: list(x))
            grp_co['nr of posts'] = grp_co['length'].apply(len) 

            # Select companies with THREE or more job postings
            # ********* TODO ******** : REMOVE DUPLICATES FOR COMPANIES sith TWO POSTINGS.
            companies = grp_co[grp_co['nr of posts'] > 2]
            # Identify duplicate job descriptions, selected by corpus-similarity between descriptions of similar lengths 
            # Save their ObjectId's in a dict
            dupes_dic = {}
            for company in companies.index:
                company_df         = pd.DataFrame(companies.loc[company].to_dict()).sort_values(by='length',ascending = False)
                dupes_dic[company] = dupes(company_df)['ObjectId'].values

            # Save the dict with company names and ObjectIds for dupes 
            self.duplicates_dic[params] = dupes_dic

        # RETURN:  
        # a. Return without displaying result
        if not display_result:    
            return

        # b. Return without duplicates
        if remove_duplicates:
            if params not in self.get_without_duplicates_dic:
                # Make a list of all duplicates for all companies
                all_dupes = np.concatenate(list(self.duplicates_dic[params].values()))
                # Remove duplicates
                self.get_without_duplicates_dic[params] = self.get_dic[params].set_index('ObjectId').drop(all_dupes).reset_index()
            return self.get_without_duplicates_dic[params]
        # c. Return All 
        return self.get_dic[params] 

    def bullets_column(self,label,keystrings,*args,**kwargs):
        self.get(self.title,**self.kwargs)[label] = self.get(self.title,**self.kwargs)["parsed description"].apply(lambda dic: identify_bullets(dic,keystrings))


    def titles(self,                              \
                title          ='program manager', \
                root           ='manager',         \
                direction      ='longer' ,         \
                **kwargs
                   ):
        df        = self.get(root,**kwargs)
        title_set = set((root+' '+title).split())
        if direction == 'longer':
            return df[(title_set - df['title set']).apply(len) == 0]
        if direction == 'shorter':
            return df[(df['title set'] - title_set).apply(len) == 0]

    def add_common_titles(self,*args,**kwargs):
        self.get(self.title,**self.kwargs)['common titles']=self.get(self.title,**self.kwargs)['title set'].apply(lambda titset : map_common_titles(test_title_corpus = titset,root_title = self.title) )

        
def titles_count(df,title_string='manager'):

    def listem(nn):
        return df['title'].apply(lambda x: ngrams(x,N = nn, string_form = 'words')).apply(lambda array: list(filter(lambda string: title_string in string, array)))

    def countem():
        cnt = Counter()
        [cnt.update(np.concatenate(listem(i).values)) for i in range(2,7)]
        return cnt.most_common()

    df_titles_all     = pd.DataFrame(countem(), columns = ['title','count'])
    df_titles_manager = df_titles_all[df_titles_all['title'].apply(lambda titles: titles[-len(title_string):] == title_string)]
    df_titles         = df_titles_manager.copy()[df_titles_manager['count']>1]
    df_titles['corpus'] = df_titles['title'].apply(lambda x: set(x.split()))
    df_titles['corpus length'] = df_titles['corpus'].apply(len)
    return df_titles
        

def parse_job_description(string):
    '''
    Converts description to dictionary
    1. Splits the job description into a list:  split at linebreak and html tags for bullet/paragraph
    2. Lines beginning with "*" are assigned as bullets
    3. Lines ending with ":" are assigned as keys 
    4. All other lines are added to key "000"

    (Developer's comment: Preliminary version. To be done more elegantly with RexEx)
    '''
    # Split job description into items
    split_string = string.replace('<li>','\n *'). \
                            replace('</li>','\n').\
                            replace('<p>','\n').  \
                            replace('</p>','\n'). \
                            split('\n')
    # Initiate dictionary
    key = '000'
    dic = {}
    dic['000']= []

    # Sort items into dictionary
    for item in split_string:
        item  = item.lower()
        item0 = item.replace(' ','')
        if item0 == '':
            pass
        elif item0[0] is "*":
            dic[key].append(item[1:])
        elif item0[-1] == ':':
            key = item.replace(':','') 
            dic[key] =[]
        else:
            dic['000'].append(item)
    return dic

def dupes(df, thresh = 0.8):
    crps = pd.DataFrame(crpsim_previous(df['corpus']),index = df.index)
    return df[crps[0] > thresh]

def crpsim_previous(corpora):
    return [0] +[corpus_similarity(pair) for pair in consecutive_pairs(corpora)] 

def corpus_similarity(corps):
    '''
    Measures similarity between two corpuses (type:set)
    '''
    corp1,corp2 = corps
    diff = len(corp1.symmetric_difference(corp2))
    total= len(corp1)+len(corp2)
    return 1- diff/total
crpsim = corpus_similarity

def consecutive_pairs(array):
    return np.array([array[1:],array[:-1]]).transpose()


def ngrams(array, N=2, string_form = False):
    '''
    Returns all N-element long sequences of neighboring elements. Works both for strings and lists.
    
    Examples:
    [in ]  ngrams([1,2,3,4])
    [out]  array([[1, 2],[2, 3],[3, 4]])
    
    [in ]  ngrams('one two three four', string_form = 'words')
    [out]  array(['one two', 'two three', 'three four'], dtype='<U10')
    
    [in ]  ngrams('one two three four', string_form = 'characters')
    [out]  array(['on', 'ne', 'e ', ' t', 'tw', 'wo', 'o ', ' t', 'th', 'hr', 're',
                  'ee', 'e ', ' f', 'fo', 'ou', 'ur'], dtype='<U2')
    '''
    # Split string
    if string_form:
        if type(array) is str:
            if string_form   is 'words':
                array = array.split()
            else:
                string_form = 'characters'
                array = [i for i in array]
    
    # Build N-grams
    if type(array[0]) in (list, np.ndarray):
        result = [ngrams(item,N=N) for item in array]
    else:
        array0 = np.append(np.array(array),0)
        result = np.transpose([array0[i:i-N] for i in range(N)])
    
    # Return result
    if string_form:
        spacer = {'words':' ', 'characters':''}
        result = np.array([spacer[string_form].join(item) for item in result])
    return result


'''
# JOB DESCRIPTION DATABASE

* 1.5M active job ads
* 3-month time window
* Untapped source of info - good for knowledge graph

## DB ORIGINAL CONTENT

* Timestamp & Status
* Original ad text (formatted)
* Pre-processed text:
    - unformatted ad text
    - extracted keywords / entities / skills 

The preprocessed keywords / entities / skills are compact and general and may have 
involved more data in the processing, such as Onet data for occupations.  
'''

## MATH
# # %%
# # Test: Get the first job description in the collection
# jd.collection.find()[0]


## IDENTIFY COMMON TITLES, MAP THEM ONTO THE TITLES OF THE JOB DESCRIPTIONS
        
common_titles_dic = {}

def common_titles(string = 'manager', **kwargs):
    threshold = 1/5000
    param = str({'string': string,'kwargs' : str(kwargs)})
    if param not in common_titles_dic:
        titcnt = titles_count(jd.get(string, **kwargs),string)
        common_titles_dic[param] = titcnt[titcnt['count'] > titcnt['count'].sum() * threshold]
    return common_titles_dic[param]

def map_common_titles(test_title_corpus = {'assistant','project','manager'},root_title = 'manager', **kwargs):
    '''
    checks which corpora of common titles are subsets of the corpus of a test titles.
    '''
    comtit= common_titles(root_title, **kwargs)
    return comtit[comtit['corpus'].apply(lambda x: x.issubset(test_title_corpus))]['title'].values

def df_add_common_titles(test_title,**kwargs):
    jd.get(test_title,**kwargs)['common titles']=jd.get(test_title)['title set'].apply(lambda titset : map_common_titles(test_title_corpus = titset,root_title = test_title) )

def return_true(item,truth = False): ## TODO: Replace with filter()
    if truth:
        return item

def identify_bullets(dic,keystrings):
    '''
    return values for keys containing any of the listed strings
    '''
    return list(filter(None,[return_true(item[1],any(key in item[0] for key in keystrings)) for item in dic.items()]))


#%%

# RUN THE SCRIPT:
# Extract job descriptions containing the string 'test_title', remove duplicates, parse the description for keys 
# and bullets, compute common titles and map on the titles of the descriptions 

#jd.get(test_title)['common titles']=jd.get(test_title)['title set'].apply(lambda titset : map_common_titles(test_title_corpus = titset,root_title = test_title) )

# Add columns with responsibilities- and requirements- keys and bullets, extracted from the parsed description

# requirement_keys = ['skill','experi','require',' educ','qualif','abiliti','diploma ',' certifi']
# responsibil_keys = ['duti','responsib','role','in charge','deliver']


# def return_true(item,truth = False): ## TODO: Replace with filter()
#     if truth:
#         return item

# def identify_bullets(dic,keystrings):
#     '''
#     return values for keys containing any of the listed strings
#     '''
#     return list(filter(None,[return_true(item[1],any(key in item[0] for key in keystrings)) for item in dic.items()]))



# def key_strings(dic,keystrings):
#     '''
#     return values for keys containing any of the listed strings
#     '''
#     return list(filter(None,[return_true(item[1],any(key in item[0] for key in keystrings)) for item in dic.items()]))


# def requirements_items(dic):
#     '''
#     Retrieve bullets for requirement-keys from a parsed description 
#     '''
#     return key_strings(dic,requirement_keys)

# def responsibilities_items(dic):
#     '''
#     Retrieve bullets for responsibilities-keys from a parsed description 
#     '''
#     return key_strings(dic,responsibil_keys)

# jd.get(test_title)["requirements_items"]     = jd.get(test_title)["parsed description"].apply(requirements_items)
# jd.get(test_title)["responsibilities_items"] = jd.get(test_title)["parsed description"].apply(responsibilities_items)

if __name__ == "__main__":
    # RUN FROM COMMANDLINE
    test_title = sys.argv[1]
    jd = JobDescriptions(test_title)
    jd.add_common_titles()
    jd.df['keys in text']= jd.df['parsed description'].apply(lambda x: x.keys())
    jd.bullets_column("requirements",['skill','experi','require',' educ','qualif','abiliti','diploma ',' certifi'])
    jd.bullets_column("responsibilities",['duti','responsib','role','in charge','deliver'])

    ## SAVE RESULTS TO EXCEL FILE
    out_xls = jd.df.copy()[['time stamp','length', 'title','common titles','company', 'clean description','parsed description','keys in text','requirements','responsibilities']]
    out_xls['time stamp'] = out_xls['time stamp'].apply(str)
    out_xls = out_xls.applymap(str)
    out_xls.to_excel(str(test_title)+".xls")

