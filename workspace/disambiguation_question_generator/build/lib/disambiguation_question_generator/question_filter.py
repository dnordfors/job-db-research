import nltk
from disambiguation_question_generator.common.reader import Reader



class Question_Filter:

    def __init__(self, filtered_data_path):
        self.kws_outliers = Reader().read_json(filtered_data_path)['kws_outliers']
        # ["new", "old", "day", 'sites', 'job', 'members', 'level', 'daily']
        self.type_list = ['VB','VBN','VBD','VBG','VBP','VBZ', 'NN', 'NNS']

    def filter_industry(self, title_matches, Include_ind):
        result_list = []
        for k,v in title_matches.items():
            if title_industry_dict.get(k)!=None:
                ind_list = set([ind for ind,val in title_industry_dict[k].items() if float(val)>0.1])
                if len(ind_list & set(Include_ind)) == len(set(Include_ind)):
                    result_list.append((k,v))
        return(result_list)

    def filter_title(self, title_matches, title, min_weight = 0):
        title = title.lower()
        result_list = []
        list2 = []
        for k,v in title_matches.items():
            if v >= min_weight:
                if title in k.lower():
                    result_list.append((k,v))
                else:
                    list2.append((k,v))
        return(result_list, list2)


    def ttc_filter_title(self, ttc_matches, title_list, ttc_title_dict, min_weight = 0):
        result_list = []
        list2 = []
        title_list = set(title_list)

        for k,v in ttc_matches.items():
            if v >= min_weight:
                title_sets = set(ttc_title_dict[k]['title']) | set(ttc_title_dict[k]['title_frequent'])\
                | set(ttc_title_dict[k]['title_less_frequent']) | set(ttc_title_dict[k]['title_industry_not_match'])
                
                if len(title_list & title_sets)>0:
                    result_list.append((k,v))
                else:
                    list2.append((k,v))
        return(result_list, list2)

    def kw_filter(self, kw, other_kws):
        
        token = nltk.word_tokenize(kw)
        check = nltk.pos_tag(token)
        if check[0][1]=="JJ":
            if len(check)==3:
                if " ".join([check[1][0], check[2][0]]) in other_kws:
                    return(False)
        
        if len(token) == 2:
            if check[0][1] not in self.type_list:
                return(False)
        for ti in token:
            if ti in self.kws_outliers:
                return(False)
        return(True)







