import json 
import pickle
import pandas as pd

class Reader:

    def __init__(self):
        pass
    
    def read_json(self, filepath:str):
        with open(filepath, 'r') as f:
            file = json.load(f)
        return(file)

    def read_pickle(self, filepath:str):
        with open(filepath, 'rb') as fp:
            file = pickle.load(fp)
        return(file)

    def read_csv(self, filepath:str):
        df = pd.read_csv(filepath)
        return(df)