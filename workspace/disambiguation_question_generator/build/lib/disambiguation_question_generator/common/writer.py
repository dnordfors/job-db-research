import json 
import pickle
import pandas as pd

class Writer:

    def __init__(self):
        pass
    
    def write_json(self, data, filepath:str):
        with open(filepath, 'w') as f:
            json.dump(data, f)

    def write_pickle(self, data, filepath:str):
        with open(filepath, 'wb') as f:
            pickle.dump(data, f)

    def write_csv(self, data, filepath:str):
        data.to_csv(filepath)