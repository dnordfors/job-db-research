#%%
# TO DO: Check that dependencies are OK, add test-example at the end


# OS
import os
import sys
import urllib
import requests

# LOGGING / MONITORING
import inspect
import tqdm
from tqdm._tqdm_notebook import tqdm_notebook
import time

# redis - dictionary
import redis
regis = redis.Redis(host='localhost', port=6379, db=0)
regis.set('progress',0)
regis.set('progress_name','idle')
regis.set('current_params','none')
regis.set('current_process',' ')

# PROCESSING
import numpy as np
import pandas as pd
import scipy
import collections
from collections import Counter

# MODELS / FITTING
import sklearn
from sklearn.cluster import KMeans
from sklearn.decomposition import NMF
from scipy.linalg import svd
from scipy.sparse.linalg import svds
from scipy.cluster import hierarchy

# GEO / TIME
import addfips

# NETWORK GRAPHS
import networkx as nx
import pydot
from networkx.drawing.nx_pydot import write_dot


# NLP
from bs4 import BeautifulSoup

## NLP synonyms: GloVe    https://nlp.stanford.edu/projects/glove/
# glove_vectors_file = "./glove_vectors_wikip_200.pkl"
# ## read glove-vectors from disk if und./glove_vectors_wikip_200.pklefined, otherwise not
# try:
#     if glove_dic:
#         pass
# except:
#     if glove_vectors_file[-3:] is 'pkl' or 'pickle':
#         glove_dic = pd.read_pickle(glove_vectors_file)
#         glove_df = pd.DataFrame(glove_dic)

from nltk.corpus import stopwords
stopw = set(stopwords.words("english"))

# function for cleaning each sentence
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')


# ## BOLD Graph
# ### read dictionary with clean & noisy titles
# try:
#     if BOLD_noisy_titles:
#         pass
# except:
#     with open('jobtitle_cleantitles_reference.cache.json','r') as file:
#          BOLD_noisy_titles = json.load(file)

# GRAPHICS
import seaborn as sns
from matplotlib import pyplot as plt


# # MATRIX-FACTORIZATION: DIMENSIONALITY REDUCTION & ARCHETYPING

# ## CLUSTER FEATURES INTO OCCUPATION CATEGORIES
# ## Use non-zero matrix factorization for clustering
# ## Use singular value decomposition first state for determining overall similarity


## GENERAL FUNCTIONS 

### LOGGING
def retrieve_name(var):
        """
        Gets the name of var. Does it from the out most frame inner-wards.
        :param var: variable to get name from.
        :return: string
        """
        for fi in reversed(inspect.stack()):
            names = [var_name for var_name, var_val in fi.frame.f_locals.items() if var_val is var]
            if len(names) > 0:
                return names[0]


### LOGIC

def identity(x):
    return x

### CLUSTERING
#### Dendrogram

def dendro_order(df, method='average', metric='correlation',**kwargs):
    '''
    Reorders a dataframe, clustering the columns as a dendrogram. 
    '''
    z = hierarchy.linkage(df.values.T, method, metric, **kwargs)
    order = hierarchy.leaves_list(z)
    df_reordered = df.iloc[:,order]
    return df_reordered.copy()

def clustermap(df, method='average', metric='correlation',**kwargs):
    clm = dendro_order(
            dendro_order(df,method, metric, **kwargs).T,
        method, metric, **kwargs 
    )
    return clm

### GEO / TIME

## SLOW!
# def fips(coords_dic,showall='true',form='json'): 
#     lat,long = coords_dic.values()
#     resp = requests.get(        \
#         'https://geo.fcc.gov/api/census/block/find?latitude='+str(lat)+'&longitude='+str(long)+'&showall='+showall+'&format='+form
#     )
#     return eval(resp.content.decode())

### NORMALIZATION
#### Statistic normalization - subtract mean, scale by standard deviation
def norm_stat(vec, weights = False):
    '''
    Normalizes a vector v-v.mean())/v.std() 
    '''
    if weights:
        return  np.mean(abs(vec - vec.mean()))  
    return (vec-vec.mean())/vec.std()

#### Algebraic normalization - dot product
def norm_dot(vec, weights = False):
    '''
    Normalizes a vector - dot product: v @ v = 1
    '''
    if weights:
        return  np.sqrt(vec @ vec)
    
    return vec / np.sqrt(vec @ vec)

#### Algebraic normalization - dot product
def norm_sum(vec, weights = False):
    '''
    Normalizes a vector - sum: v.sum = 1
    '''
    if weights:
        return  vec.sum()
    
    return vec / vec.sum()

#### Scaled Normalization -
def scale(vec, weights = False):
    '''
    Normalizes a vector: v.min = 0, v.max = 1
    '''
    stop_divide_by_zero = 0.00000001
    if weights:
        return (vec.max()-vec.min() + stop_divide_by_zero)
    return (vec-vec.min())/(vec.max()-vec.min() + stop_divide_by_zero)

### NLP

def sentence_cleaner(sentence_array,tokenized=False):
    tknz = {
        True : (lambda x:x),
        False: (lambda x:' '.join(x))
    }[tokenized]
    
    func = {
        str : (lambda x:  tknz(tokenizer.tokenize(x.lower()))),
        list: (lambda x: [tknz(tokenizer.tokenize(sentnc.lower())) for sentnc in x])
    }.get(type(sentence_array),(lambda x:x))
    
    return func(sentence_array)

def ngrams(array, N=2, string_form = False):
    '''
    Returns all N-element long sequences of neighboring elements. Works both for strings and lists.
    string_form = False       : operates on lists, no splits
                 'sentences'  : splits text into list of sentences, then splits sentences into words
                 'words'      : splits string into words
                 'characters' : operates on strings, no splits

    Examples:
    [in ]  ngrams([1,2,3,4])
    [out]  array([[1, 2],[2, 3],[3, 4]])

    [in ]  ngrams('one two three four', string_form = 'words')
    [out]  array(['one two', 'two three', 'three four'], dtype='<U10')

    [in ]  ngrams('one two three four', string_form = 'characters')
    [out]  array(['on', 'ne', 'e ', ' t', 'tw', 'wo', 'o ', ' t', 'th', 'hr', 're',
                  'ee', 'e ', ' f', 'fo', 'ou', 'ur'], dtype='<U2')
    '''
    # Split string
    if string_form:
        if type(array) is str:
            if string_form   is 'words':
                array = array.split()
            else:
                string_form = 'characters'
                array = [i for i in array]

    # Build N-grams
    if type(array[0]) in (list, np.ndarray):
        result = [self.ngrams(item,N=N) for item in array]
    else:
        array0 = np.append(np.array(array),0)
        result = np.transpose([array0[i:i-N] for i in range(N)])

    # Return result
    if string_form:
        spacer = {'words':' ', 'characters':''}
        result = np.array([spacer[string_form].join(item) for item in result])
    return result


# def sentence_cleaner(sentence_array):
#     if type(sentence_array) is str:
#         return tokenizer.tokenize(sentence_array.lower())
#     else:
#         cleaned_array = [tokenizer.tokenize(sentnc.lower()) for sentnc in sentence_array]
#         return cleaned_array

# ######### ARCHETYPES / SVD ETC ############################

class Archetypes:
    '''
    Archetypes: Performs NMF of order n on X and stores the result as attributes. 
    The X-matrix has the shape (k,l) with k objects (rows) that have l features (columns) and is
    decomposed  
                  X(k,l) = w(k,n) @ h(n,l) 

    w contains the k objects mapped onto n archetypes (archetype object matrix, as DataFrame self.o )
    h contains n archetypes mapped onto l features (archetype feature matrix, as DataFrame self.f )  

        my_archetypes.n         - order / number of archetypes
        my_archetypes.X         - input matrix
        
        my_archetypes.model     - NMF model 
        my_archetypes.w         - NMF w-matrix 
        my_archetypes.h         - NMF h-matrix 
        
        my_archetypes.o         - objects x archetypes matrix (from w-matrix)
        my_archetypes.on        - occupations x normalized archetypes matrix (from w-matrix) - SOCP number as index. 
        
        my_archetypes.f         - features x archetypes matrix (from h-matrix)
        my_archetypes.fn        - features x normalized archetypes matrix
    
    More about the NMF model here: https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.NMF.html
       
        
    '''
    def __init__(self,X,n_components,norm_o = norm_sum, norm_f = norm_dot, *args, **kwargs):
        self.n = n_components
        self.params = {'n_components':n_components,'norm_o': norm_o,'norm_f': norm_f}.update(kwargs)
        self.X = X
        self.model = NMF(n_components=self.n, **kwargs)
        self.w = self.model.fit_transform(self.X)
        self.o = pd.DataFrame(self.w,index=self.X.index)
        self.on = self.o.T.apply(norm_o).T
#        self.occ = self.occ.set_index('Occupations')
        self.h = self.model.components_
        self.f = pd.DataFrame(self.h,columns=X.columns)
        self.fn =self.f.T.apply(norm_f).T
        self.plot_occupations_dic ={}
        self.plot_features_dic ={}

        
    def plot_features(self,fig_scale = (1,3.5),metric='cosine', method = 'single',vertical = False): 
        '''
        Plot Archetypes as x and features as y. 
        Utilizes Seaborn Clustermap, with hierarchical clustering along both axes. 
        This clusters features and archetypes in a way that visualizes similarities and diffferences
        between the archetypes. 
        
        Archetypes are normalized (cosine-similarity): dot product archetype[i] @ archetype[i] = 1.
        The plot shows intensities (= squared feature coefficients) so that the sum of intensities = 1.  
        fig_scale: default values (x/1, y/3.5) scales the axes so that all feature labels are included in the plot.
        
        For other hyperparameters, see seaborn.clustermap
     
        '''
        param = (fig_scale,metric,method,vertical)
        if param in self.plot_features_dic.keys():
            fig = self.plot_features_dic[param]
            return fig.fig

        df = np.square(self.fn)

        if vertical:
            fig = sns.clustermap(df.T,robust = True, z_score=1,figsize=(
                self.n/fig_scale[0],self.X.shape[1]/fig_scale[1]),method = method,metric = metric)        
        else: # horizontal
            fig = sns.clustermap(df,robust = True, z_score=0,figsize=(
                self.X.shape[1]/fig_scale[1],self.n/fig_scale[0]),method = method,metric = metric)        
        self.features_plot = fig
        return fig


    def plot_occupations(self,fig_scale = (1,3.5),metric='cosine', method = 'single',vertical = False):
        '''
        Plot Archetypes as x and occupations as y. 
        Utilizes Seaborn Clustermap, with hierarchical clustering along both axes. 
        This clusters occupations and archetypes in a way that visualizes similarities and diffferences
        between the archetypes. 
        
        Occupations are normalized (cosine-similarity): dot product occupation[i] @ occupation[i] = 1.
        The plot shows intensities (= squared feature coefficients) so that the sum of intensities = 1.  
        fig_scale: default values (x/1, y/3.5) scales the axes so that all feature labels are included in the plot.
        
        For other hyperparameters, see seaborn.clustermap
     
        '''
        param = (fig_scale,metric,method,vertical)
        if param in self.plot_occupations_dic.keys():
            fig = self.plot_occupations_dic[param]
            #return
            return fig.fig

        df = np.square(self.occ)
        if vertical:
            fig = sns.clustermap(df, figsize=(
                self.n/fig_scale[0],self.X.shape[0]/fig_scale[1]),method = method,metric = metric)
        else: # horizontal
            fig = sns.clustermap(df.T, figsize=(
                self.X.shape[0]/fig_scale[1],self.n/fig_scale[0]),method = method,metric = metric)
        self.plot_occupations_dic[param] = fig
        #return
        return fig.fig
    

class Svd:
    ''''
    Singular value decomposition-as-an-object
        my_svd = Svd(X) returns
        my_svd.u/.s/.vt – U S and VT from the Singular Value Decomposition (see manual)
        my_svd.f        – Pandas.DataFrame: f=original features x svd_features
        my_svd.o        - Pandas.DataFrame: o=occupations x svd_features
        my_svd.volume(keep_volume) 
                        - collections.namedtuple ('dotted dicionary'): 
                          Dimensionality reduction. keeps 'keep_volume' of total variance
                          
                          
    '''
    def __init__(self,X, sparse = False):
        if sparse:
            sv = svds
        else: 
            sv = svd
            
        self.u,self.s,self.vt = svd(np.array(X))
        self.f = pd.DataFrame(self.vt,columns=X.columns)
        self.o = pd.DataFrame(self.u,columns=X.index)
        
    def volume(self,keep_volume):
        ''' 
        Dimensionality reduction, keeps 'keep_volume' proportion of original variance
        Type: collections.namedtuple ('dotted dictionary')
        Examples of usage:
        my_svd.volume(0.9).s - np.array: eigenvalues for 90% variance 
        my_svd.volume(0.8).f - dataframe: features for 80% variance
        my_svd.volume(0.5).o - dataframe: occupations for 50% variance      
        '''
        dotted_dic = collections.namedtuple('dotted_dic', 's f o')
        a1 = self.s.cumsum()
        a2 = a1/a1[-1]
        n_max = np.argmin(np.square(a2 - keep_volume))
        cut_dic = dotted_dic(s= self.s[:n_max],f= self.f.iloc[:n_max], o= self.o.iloc[:n_max])
        return cut_dic